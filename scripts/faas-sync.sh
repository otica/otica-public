#!/usr/bin/env bash
set -e

# copy all the faas files to FAAS_BUILD_DIR
# render all the templates
# used to prepare the faas build dir

THIS_DIR=$(dirname "$0")

# defaults
FAAS_CODE_DIR=${FAAS_CODE_DIR:-functions}
FAAS_BUILD_DIR=${FAAS_BUILD_DIR:-build}

# create FAAS_BUILD_DIR if it doesn't exist
if [[ ! -d ${FAAS_BUILD_DIR} ]] ; then
	mkdir -p ${FAAS_BUILD_DIR}
fi

# sync FAAS_CODE_DIR to FAAS_BUILD_DIR
echo
echo "Copy ${FAAS_CODE_DIR} to ${FAAS_BUILD_DIR} ..."
rsync -dr ${FAAS_CODE_DIR}/ ${FAAS_BUILD_DIR}

# generate files from templates in build dir
for file in $(ls ${FAAS_BUILD_DIR}/*.tmpl* 2> /dev/null) ; do
	base="${file%%.*}"
	extension="${file##*.}"
	cat $file | render.sh > ${base}.${extension}
	rm -f $file
done
