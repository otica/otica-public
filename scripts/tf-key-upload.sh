#!/usr/bin/env bash
set -e

# Used by makefile_parts/terraform.mk
# to generating a customer supplied encryption key 
# encrypt the key by KMS key and upload to 
# gs://${GCP_INFRASTRUCTURE_BUCKET}/${TF_BACKEND_PREFIX}terraform.key.enc

if [ -z ${GCP_INFRASTRUCTURE_KEY_ID} ]; then
	echo KMS GCP_INFRASTRUCTURE_KEY_ID is not defined!
	exit 0
fi
if [ -z ${GCP_INFRASTRUCTURE_BUCKET} ]; then
	echo ERROR: GCP_INFRASTRUCTURE_BUCKET is not defined!
	exit 1
fi

TF_BACKEND_PREFIX=${TF_BACKEND_PREFIX:-terraform/${GCP_PROJECT_ID}/${GCP_ENVIRONMENT}/state}
TF_KEY_URI=gs://${GCP_INFRASTRUCTURE_BUCKET}/${TF_BACKEND_PREFIX}/terraform.key.enc

if gsutil ls -p ${GCP_PROJECT_ID} ${TF_KEY_URI} &> /dev/null; then
	echo gs://${GCP_INFRASTRUCTURE_BUCKET}/terraform.key.enc exists
else
	echo Creating ${TF_KEY_URI} ... ; 
	head -c 32 /dev/urandom | base64 | tr -d '\n\r' | gcloud kms encrypt \
			--key ${GCP_INFRASTRUCTURE_KEY_ID} \
			--ciphertext-file - \
			--plaintext-file - \
	| base64 \
	| tr -d "\n\r" \
	| gsutil cp -  ${TF_KEY_URI}
fi
