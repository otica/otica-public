#!/usr/bin/env bash
STATUS=${STATUS:-'Unknown'}
TITLE=${TITLE:-'Unknown'}
MESSAGE=${MESSAGE:-'Empty'}
cat <<EOF
{
    "blocks": [
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": "${STATUS} *${TITLE}*"
            }
        },
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": "*Message*: \n${MESSAGE}"
            }
        },
        {
            "type": "divider"
        }
    ]
}
EOF
