#!/bin/sh

# Generate the diff between manifests of a running instance and the
# chart that _would_ be running next time the helm upgrade is run.

. functions.sh

DEBUG=

running_manifest=$(mktemp)
potential_manifest=$(mktemp)

if [[ -z "$HELM_RELEASE" ]]; then
    exit_with_error "required variable HELM_RELEASE not defined"
fi

progress "HELM_RELEASE is '$HELM_RELEASE'"

###############################################################
gen_running_template() {
    helm get manifest "${HELM_RELEASE}" > $1
}

gen_potential_template() {
    helm template \
         -f "../common/data/common/values-local.yaml" \
	     -f "../common/data/${APP_ENV}/values-local.yaml" \
         $VERSION_OPTION \
         "$HELM_RELEASE" "$HELM_REPO_NAME"/"$HELM_CHART_NAME" > $1
}
###############################################################

## If HELM_CHART_VERSION is not empty and not equal to "latest", use
## the "--version" option to use a specific Helm chart version.
if [[ ! -z "$HELM_CHART_VERSION" ]] && [[ "$HELM_CHART_VERSION" != "latest" ]]; then
    VERSION_OPTION=" --version $HELM_CHART_VERSION "
    progress "using chart version '$HELM_CHART_VERSION'"
else
    VERSION_OPTION=""
    progress "not using the --version option"
fi

progress "VERSION_OPTION is '$VERSION_OPTION'"

progress "generate the running chart's template"
gen_running_template   "$running_manifest"

progress "generate the potential chart's template"
gen_potential_template "$potential_manifest"

progress "generate the diff"
diff "$running_manifest" "$potential_manifest"

#echo "$running_manifest"
#echo  "$potential_manifest"
unlink "$running_manifest"
unlink "$potential_manifest"

