#!/usr/bin/env bash

THIS_DIR=$(dirname "$0")
source $THIS_DIR/functions.sh

# Push the chart to a GCS bucket.

if [[ -z "$HELM_CHART_NAME" ]]; then
    exit_with_error "HELM_CHART_NAME is not defined"
fi

if [[ -z "$HELM_REPO_NAME" ]]; then
    exit_with_error "HELM_REPO_NAME is not defined"
fi

# 1. Make sure there is EXACTLY one chart
NUM_FILES=$(ls "${HELM_CHART_NAME}"*.tgz | wc -l)

if (( NUM_FILES == 0 )); then
    exit_with_error "no packaged HELM charts found"
elif (( NUM_FILES > 1 )); then
    exit_with_error "more than one packaged HELM chart found"
fi

# 2. We have a single Helm package, so we can now upload it.
if [[ -n "$HELM_PUSH_FORCE" ]]; then
    echo "forcing helm push..."
    helm gcs push *.tgz "$HELM_REPO_NAME" --force
else
    helm gcs push *.tgz "$HELM_REPO_NAME"
fi
