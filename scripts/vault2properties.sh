#!/usr/bin/env bash
set -e

# Usage:
#   vault2properties <secret.yml>

# Replace %%vault-secret-path%% in a kube secret template file with secret
# value stored at vault-secret-path in orginal secret value format (i.g.
# if it's base64 encrypted, unencrypt it). Used in helm chart or elsewhere
# that the plain secret format is needed.

OLDIFS=$IFS
IFS=''
while read line
do
    path=$(echo $line | grep -v '^#' | grep -o -e '%%.*%%' | tr -d '%')
    if [ -z $path ]
    then
        if [[ $line =~ ^\# ]] || [[ -z $line ]]; then
            continue
        fi

        echo $line
    else
        # Assemble secret from the value retrieved from Vault.
        pre="${line%%%%${path}%%*}"
        post="${line##*%%${path}%%}"
        echo "${pre}$(vault-read.sh $path)${post}"
    fi
done < "${1:-/dev/stdin}"
IFS=$OLDIFS
