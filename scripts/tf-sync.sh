#!/usr/bin/env bash
set -e

# copy all the terraform files to TF_BUILD_DIR
# render all the templates
# used to prepare the terraform build dir

THIS_DIR=$(dirname "$0")

# defaults
NO_VAULT=${NO_VAULT:-false}
GOOGLE_ENCRYPTION_KEY=${GOOGLE_ENCRYPTION_KEY:-""}
TF_DIR=${TF_DIR:-terraform}
TF_MODULES_DIR=${TF_MODULES_DIR:-terraform-modules}
TF_BUILD_DIR=${TF_BUILD_DIR:-build}

# sync or cp TF_DIR to TF_BUILD_DIR
if [ -d ${TF_DIR} ] ; then
	echo
	if [ -d ${TF_BUILD_DIR} ] ; then
		echo "Update ${TF_BUILD_DIR} ..."
		rsync \
			--exclude=.terraform* \
			--exclude-from=${TF_SYNC_EXCLUDE_FROM} \
			-dr \
			--delete ${TF_DIR}/ ${TF_BUILD_DIR}
	else
		echo "Copy ${TF_DIR} to ${TF_BUILD_DIR} ..."
		cp -r ${TF_DIR} ${TF_BUILD_DIR}
	fi
fi

# copy local tf files to ${TF_BUILD_DIR} for local customizations.
if ls *.tf &> /dev/null ; then
	cp -f *.tf ${TF_BUILD_DIR}/
fi
if ls *.tfvars &> /dev/null ; then
	cp -f *.tfvars ${TF_BUILD_DIR}/
fi

# link terraform modules to build, i.e. module source is relative to ./terraform-modules
if [ -d ${TF_MODULES_DIR} ] ; then
	ln -sf ${TF_MODULES_DIR} ${TF_BUILD_DIR}/
fi

# if GOOGLE_ENCRYPTION_KEY not defined
if [ -z "${GOOGLE_ENCRYPTION_KEY}" ] ; then
	if [ -f ${TF_BUILD_DIR}/.terraform.key ] ; then
		# get customer supplied encryption key for GCR backend encryption.
		export GOOGLE_ENCRYPTION_KEY=$(cat ${TF_BUILD_DIR}/.terraform.key)
	elif ( ! ${NO_VAULT} ) && [ ! -z "${TF_KEY_VAULT_PATH}" ] ; then
		# get customer supplied encryption key for vault
		vault-login.sh
		export GOOGLE_ENCRYPTION_KEY=$(vault-read.sh ${TF_KEY_VAULT_PATH})
	fi
fi

if [ ! -z "${GOOGLE_ENCRYPTION_KEY}" ] ; then
	echo "Terraform state is encrypted with the customer supplied GOOGLE_ENCRYPTION_KEY."
	# some templates may still using TERRAFORM_KEY
	export TERRAFORM_KEY=${GOOGLE_ENCRYPTION_KEY}
fi

# generate terraform files from templates in build dir
for file in $(ls ${TF_BUILD_DIR}/*.tmpl* 2> /dev/null) ; do
	base="${file%%.*}"
	extension="${file##*.}"
	cat $file | render2.sh > ${base}.${extension}
	rm -f $file
done
