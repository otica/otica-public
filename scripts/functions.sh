#!/usr/bin/env bash

###############################################################################
# Helper functions
###############################################################################

# Include this file in other script files using "source". For example:
#
# #!/usr/bin/env bash
# #
# # source functions.sh

###############################################################################

progress() {
    local msg
    local prefix
    msg=$1
    prefix=${2:-progress: }
    if [[ -n "${DEBUG:-}" ]]; then
        echo "$prefix$msg"
    fi
}

# Timestamped progress (no prefix)
progress_ts() {
    local msg
    msg=$1
    timestamp=$(date +"%R:%S.%N")
    if [[ -n "${DEBUG:-}" ]]; then
        echo "progress: [$timestamp] $msg"
    fi
}

# colors
yellow='\[\033[0;33m\]'
red='\[\033[0;31m\]'
reset='\[\033[0m\]'

# http://stackoverflow.com/a/25515370
yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 111; }
try() { "$@" || die "cannot $*"; }

err() {
  echo
  echo ${red}ERROR: ${@}$reset
  echo
  exit 1
}

exit_with_error () {
    echo "error: $1"
    exit 1
}

err_report() {
  echo "$1: error on line $2"
}

trap_errors() {
  if [ "$debug_scripts" = "true" ]; then
    set -x
  fi

  set -eeuo pipefail
  trap 'err_report $BASH_SOURCE $LINENO' err
  export shellopts
}

# check if a variable is set. useful for set -u
is_set() {
  declare -p $1 &> /dev/null
}

# is the variable set and have length?
not_empty_var() {
  is_set $1 && eval val=\$$1 && [[ "$val" ]]
}

# is the variable unset or zero length? useful for set -u
empty_var() {
   ! not_empty_var $1
}

is_null() {
  [[ $1 == "null" ]]
}

is_null_or_empty() {
  [[ $1 == "null" ]] || [[ -z "${1// }" ]]
}

confirm() {
  ## Ask to confirm an action
  echo "$*"
  echo "CONTINUE? [Y/N]: "; read ANSWER
  [[ $ANSWER == "Y" ]]
}

get_root_dir() {
  local f
  if [[ $1 == /* ]]; then f=2; else f=1; fi
  echo "$1" | cut -d "/" -f$f
}

# test if a path is a KV v2 path
is_kv() {
  local kv_mount=$(get_root_dir $1)
  vault secrets list -format=json \
    | jq -re --arg v "$kv_mount/" '.[$v] | select(((.type=="kv") or (.type=="generic")) and (.options.version=="2"))' \
    | grep version &>/dev/null
}

vault_read_cmd() {
    echo "vault kv get"
}

vault_write_cmd() {
    echo "vault kv put"
}

vault_list_cmd() {
    echo "vault kv list"
}

urlencode() {
    # urlencode <string>
    old_lc_collate=$LC_COLLATE
    LC_COLLATE=C

    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%%%02X' "'$c" ;;
        esac
    done

    LC_COLLATE=$old_lc_collate
}

urldecode() {
    # urldecode <string>

    local url_encoded="${1//+/ }"
    printf '%b' "${url_encoded//%/\\x}"
}

# Find the parent directory containing the file given by parameter $1.
# For example "find_parent .otica-top-level"
find_up () {
    path=$(pwd)
    while [[ "$path" != "" && ! -e "$path/$1" ]]; do
        path=${path%/*}
    done
    echo "$path"
}

number_files_in_dir() {
    local directory
    directory=$1
    find "$directory" -type f | wc -l
}

# Use:
#   render-file <filename>
#
# Runs the file <filename> through render2.sh.
#
# Value of environment variable RENDER_MODE:
#
# * "NONE": does not run <filename> through render2.sh, i.e.,
#   <filename> remains unchanged.
#
# * "ALL": runs <filename> through render2.sh
#
# * "TMPL": runs <filename> through render2.sh but ONLY if <filename> has
#   the form "SOMETHING.tmpl.EXT"; the resulting file name will be
#   "SOMETHING.EXT". NOTE!! In this case the original ".tmpl" file is
#   is DELETED!
#
# * unset: if RENDER_MODE is unset then behave as if it were set to "ALL".
#
# The resulting file will be in the same directory as <filename>.
render_file() {
    local file
    local tmpfile
    local msg
    local progress_pfx
    local regex1
    local regex2
    local dest_name

    progress_pfx="progress (render-file): "

    file="$1"

    if [[ -z "$file" ]]; then
        echo "render-file <filepath>"
        exit 0
    fi

    if [[ ! -f "$file" ]]; then
        msg="cannot find file '$file'"
        exit_with_error "$msg"
    fi

    local file_name
    local file_dir
    file_name="$(basename "${file}")"
    file_dir="$(dirname "${file}")"
    progress "file_name is '$file_name'" "$progress_pfx"
    progress "file_dir  is '$file_dir'" "$progress_pfx"

    # Get RENDER_MODE environment variable.
    if [[ -z "$RENDER_MODE" ]]; then
        msg="RENDER_MODE variable not set, so defaulting to 'ALL'"
        progress "$msg" "$progress_pfx"
        RENDER_MODE="ALL"
    fi

    progress "RENDER_MODE set to '$RENDER_MODE'" "$progress_pfx"

    if [[ "$RENDER_MODE" == "NONE" ]]; then
        progress "processing file in no-op render mode so nothing to do" "$progress_pfx"
    elif [[ "$RENDER_MODE" == "ALL" ]]; then
        progress "in ALL render mode so sending file through render script" "$progress_pfx"
        tmpfile=$(mktemp /tmp/render-file.XXXXXX)
        render2.sh < "$file" > "$tmpfile"
        cp "$tmpfile" "$file"
        unlink "$tmpfile"
    elif [[ "$RENDER_MODE" == "TMPL" ]]; then
        progress "in TMPL render mode" "$progress_pfx"

        regex1="^.+[.]tmpl$"
        regex2="^.+[.]tmpl[.].+$"
        if [[ "$file_name" =~ $regex1 ]]; then
            dest_name=${file_name%%.tmpl}
        elif [[ "$file_name" =~ $regex2 ]]; then
            dest_name=${file_name//.tmpl/}
        else
            dest_name=""
        fi
        progress "dest_name is '$dest_name'" "$progress_pfx"

        if [[ -z "$dest_name" ]]; then
            progress "'$file' is not a .tmpl file so will not render" "$progress_pfx"
        else
            progress "'$file' is a .tmpl file so will render" "$progress_pfx"

            destination="$file_dir/$dest_name"

            render2.sh < "$file" > "$destination"
            progress "rendered $file into $destination" "$progress_pfx"

            rm "$file"
            progress "deleted template file $file" "$progress_pfx"
        fi
    fi

    progress "render-file.sh finished processing '$file' ($elaped_time seconds)" "$progress_pfx"
}


export -f yell die try err err_report trap_errors is_set not_empty_var empty_var confirm is_null is_null_or_empty get_root_dir
export -f is_kv vault_read_cmd vault_write_cmd vault_list_cmd
export -f urlencode urldecode find_up
