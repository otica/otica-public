#!/usr/bin/env bash

##########################################################
# Delete the NAT server and routing rules
# See create-nat-gateway.sh
##########################################################

source ../functions.sh

if [[ empty_var GCP_NAT_PREFIX ]]; then
    exit_with_error "missing required variable GCP_NAT_PREFIX"
fi

NETWORK=${NETWORK:-default}

GCP_ZONE=${GCP_ZONE:-us-west1-a}
GCP_REGION=$(echo $GCP_ZONE | cut -d'-' -f 1-2)


# delete the nat-gateway if exists
if gcloud compute instances describe ${GCP_NAT_PREFIX}-nat-gateway &> /dev/null
then
  echo deleting ${GCP_NAT_PREFIX}-nat-gateway ...
  gcloud compute instances delete -q ${GCP_NAT_PREFIX}-nat-gateway
else
  echo ${GCP_NAT_PREFIX}-nat-gateway not found.
fi

# delete the net-gateway static ip
# if gcloud compute addresses describe ${GCP_NAT_PREFIX}-nat-gateway --region ${GCP_REGION} &> /dev/null
# then
#     echo deleting static ${GCP_NAT_PREFIX}-nat-gateway ...
#     gcloud compute addresses delete -q ${GCP_NAT_PREFIX}-nat-gateway --region ${GCP_REGION}

# else
#     echo static ip ${GCP_NAT_PREFIX}-nat-gateway not found.
# fi
