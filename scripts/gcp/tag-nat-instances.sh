#!/usr/bin/env bash

##########################################################
# Tag all instances with a tag.
# Used to tag instances for nat routing
##########################################################

TAG=$1

# Tag instances for NAT routing (excluding the nat server itself)
gcloud compute instances list \
  --zones ${GCP_ZONE} \
  --filter='NOT tags.items:nat-server AND status:RUNNING' \
  --format json \
   | jq -r '.[].name' \
   | xargs -I{} gcloud compute instances add-tags {} --tags ${TAG}
