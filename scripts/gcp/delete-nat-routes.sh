#!/usr/bin/env bash

# NAT routes are defined in GCP_NAT_DEST_FILE
# must in format:
# <route name><space-or-tab><destination CIDR>

GCP_NAT_DEST_FILE=${1:-nat-destinations.conf}

if [[ ! -f ${GCP_NAT_DEST_FILE} ]]
then
  echo "File ${GCP_NAT_DEST_FILE}: not found"
  exit
fi

while read line; do
  if ! [[ (-z "$line" )  || ( $line == \#* ) ]]
  then
    route_name=`echo $line | awk '{print $1}'`; \
    if gcloud compute routes describe ${route_name} &> /dev/null
    then
    	echo "Deleting route ${route_name} ..."
    	gcloud compute routes delete -q ${route_name}
  	else
			echo "Route ${route_name} not found."
    fi
  fi
done < ${GCP_NAT_DEST_FILE}
