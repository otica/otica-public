#!/usr/bin/env bash

###############################################################################
# used in
# - irt-gst/gke-cluster
# - irt-lane/my-app-dev
# - makefile_parts/nat.mk

# Setup a NAT server and routing all traffic to DEST_RANGES
# through the nat-server
# Test with
#    $ traceroute server.example.com
#
#    ____________________________
#   |          GCP               |
#   |      [ CLUSTER ]           |
#   |           |                |
#   |   [ GCP Route Rules (3) ]  | \
#   |           |                | |
#   |   [ NAT VM Instance (2) ]  | |-- this script
#   |         (link)             | |
#   |    [ GCP STATIC IP (1) ]   | /
#   ------------------------------
#               |
#    ____________________________
#   |        Company            |
#   |           |               |
#   | [   Compant Firewall    ] |
#   | [  allows GCP STATIC IP ] |
#   |           |               |
#   |   [ Company Services ]    |
#   -----------------------------
#
###############################################################################

source ../functions.sh

if [[ empty_var GCP_NAT_PREFIX ]]; then
    exit_with_error "missing required variable GCP_NAT_PREFIX"
fi

NAT_NETWORK=${NAT_NETWORK:-default}
NAT_MACHINE_TYPE=${NAT_MACHINE_TYPE:-g1-small}
NAT_IMAGE_FAMILY=${NAT_IMAGE_FAMILY:-debian-9}
NAT_IMAGE_PROJECT=${NAT_IMAGE_PROJECT:-debian-cloud}
GCP_ZONE=${GCP_ZONE:-us-west1-a}
GCP_REGION=$(echo $GCP_ZONE | cut -d'-' -f 1-2)

# allow for usage of an old IP static address
# (specifically for AS project, which has an old static IP w/ different naming convention)
if [ -z ${NAT_LEGACY_IP_ADDR_NAME} ]; then
  ip_addr_name=${GCP_NAT_PREFIX}-nat-gateway
else
  ip_addr_name=${NAT_LEGACY_IP_ADDR_NAME}
fi

# (1) CREATE A STATIC IP FOR NET-GATEWAY IF DOESN'T EXIST
if gcloud compute addresses describe ${ip_addr_name} --region ${GCP_REGION} &> /dev/null
then
  echo static ip ${ip_addr_name} already exsited.
else
  echo creating static ${ip_addr_name} ...
  gcloud compute addresses create ${ip_addr_name} --region ${GCP_REGION}
fi

# (2) CREATE THE NAT-GATEWAY INSTANCE IF DOESN'T EXIST
# create nat-gateway startup script
read -r -d '' STARTUP_SCRIPT <<'EOF'
#!/usr/bin/env bash
sudo sysctl -w net.ipv4.ip_forward=1
# make sysctl setting persistent
sed -i= 's/^[# ]*net.ipv4.ip_forward=[[:digit:]]/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
# install stackdriver agent see https://cloud.google.com/logging/docs/agent/installation#joint-install
curl -sSO "https://dl.google.com/cloudagents/install-logging-agent.sh"
sudo bash install-logging-agent.sh
rm -f install-logging-agent.sh
EOF

if gcloud compute instances describe ${GCP_NAT_PREFIX}-nat-gateway &> /dev/null
then
  echo ${GCP_NAT_PREFIX}-nat-gateway already exsited.
else
  echo creating ${GCP_NAT_PREFIX}-nat-gateway ...
  gcloud compute instances create ${GCP_NAT_PREFIX}-nat-gateway \
    --network ${NAT_NETWORK} \
    --address ${ip_addr_name} \
    --can-ip-forward \
    --zone ${GCP_ZONE} \
    --image-family ${NAT_IMAGE_FAMILY} \
    --image-project ${NAT_IMAGE_PROJECT} \
    --machine-type ${NAT_MACHINE_TYPE}\
    --metadata "startup-script=${STARTUP_SCRIPT}" \
    --tags nat-server
fi

# (3) CREATE ROUTES FOR NAT, IF DON'T EXIST
# See create-nat-routes.sh

# OUT PUT nat-gateway's IP
addr=$(gcloud compute addresses describe ${ip_addr_name} --region ${GCP_REGION} | grep 'address:')

echo The nat-gateway static ip: $addr
