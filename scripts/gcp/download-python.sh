#!/usr/bin/env bash

# Download Python packages from Google's Artifact Registry using Google
# authentication credentials.
#
# DOCKER_ARTIFACT_REGISTRY must be specified as the full path to the
# Artifact Registry Python.
# Example: DOCKER_ARTIFACT_REGISTRY="https://us-west1-docker.pkg.dev/uit-et-iedo-services/python1"
#
# Puts the downloaded packages in BUILD_DIR/download

# ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## #
usage() {
    script_name=$(basename "$0")
    echo "Usage: $script_name \"pkg1 pkg2 pkg3\""
    echo "-----"
    echo "To indicate versions append version number to the package name,"
    echo "for example: $script_name \"pkg1 pkg2:3.2.1 pkg3\""
    echo "-----"
    echo "Files downloaded to ${BUILD_DIR}/download"
}

progress_me() {
    local msg
    msg=$1
    pfx="[download-python.sh] "
    progress "$msg" "$pfx"
}

validate() {
    # 1. Validate required environment variables.
    if [[ -z ${DOCKER_ARTIFACT_REGISTRY} ]]; then
        echo "missing DOCKER_ARTIFACT_REGISTRY; aborting"
        exit 1
    fi

    if [[ -z ${BUILD_DIR} ]]; then
        echo "missing BUILD_DIR; aborting"
        exit 1
    fi
}

download_whl() {
    # Download Python package file $1. If $2 is supplied use that as the
    # version to download.
    local python_pkg_name="$1"
    local python_pkg_version="$2"

    DOWNLOAD_URL="${DOCKER_ARTIFACT_REGISTRY}/simple/"
    progress_me "about to download python package '${python_pkg_name}' from $DOWNLOAD_URL"
    if [[ -z "$python_pkg_version" ]]; then
        # python_pkg_version is empty, so download the latest version
        progress_me "downloading latest version"
        "$PIP" download --no-deps --dest="$TEMPFILE" --index-url "$DOWNLOAD_URL" "${python_pkg_name}"
    else
        # python_pkg_version is NOT empty, so download the specified version
        progress_me "downloading version '$python_pkg_version'"
        "$PIP" download --no-deps --dest="$TEMPFILE" --index-url "$DOWNLOAD_URL" "${python_pkg_name}"=="$python_pkg_version"
    fi
    progress_me "finished downloading package ${python_pkg_name}"
}

create_venv() {
    python3 -m venv "$TEMPFILE"

    PIP="$TEMPFILE/bin/pip"

    # Install the necessary Python modules in the virtual environment.
    $PIP install keyrings.google-artifactregistry-auth
}

# ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## #

set -e

THIS_DIR=$(dirname "$0")
source "$THIS_DIR"/../functions.sh

## 1. Validate required parameters/environment variables.
validate
progress_me "validated required environment variables"

## 2. Convert command line into separate pieces.
# That is, parse "pkg1:ver1 pkg2:ver2 ..." into the array
# [pkg1:ver1, pkg2:ver2, etc.]
IFS=' ' read -r -a package_pairs <<< "$1"
number_package_pairs=${#package_pairs[@]}
progress_me "parsed command line argument into $number_package_pairs package pairs"
if (( number_package_pairs == 0 )); then
    usage
    exit 0
fi

## 3. Create some directories in BUILD_DIR.
mkdir -p "${BUILD_DIR}"

DESTDIR="${BUILD_DIR}/download"
mkdir -p "${DESTDIR}"

# Make a temporary directory in BUILD_DIR.
TEMPFILE=$(mktemp -d -p "${BUILD_DIR}")
progress_me "created some directories in BUILD_DIR ($BUILD_DIR)"

## 4. Make a Python virtual environment in TEMPFILE and install the
##    necessary modules.
create_venv
progress_me "created Python virtual environment"

# 5. Download the files.
for package_pair in "${package_pairs[@]}"; do
    IFS=':' read -r -a parts <<< "$package_pair"
    package_name="${parts[0]}"
    package_version="${parts[1]}"
    progress_me "about to download package '$package_name'"
    download_whl "$package_name" "$package_version"
done

# 5. Remove any existing whl files.
rm -f "$DESTDIR"/*
progress_me "removing any left-over .whl files"

# 6. Copy .whl files into destination directory.
cp -p "$TEMPFILE"/*.whl "$DESTDIR"
progress_me "copied .whl file(s) into $DESTDIR"

# 7. Cleanup.
rm -rf "$TEMPFILE"
progress_me "removed temporary directory $TEMPFILE"

echo "your files are in $DESTDIR"
