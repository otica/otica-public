#!/usr/bin/env bash

# A wrapper around the helm executable used by the helm.mk makefile module
# to get around some of the unpleasantness of formatting complex commands
# in a Gnu Make file.
#
# Use: helm-wrapper.sh ACTION YAML-FILES
#
# ACTION must be one of the following:
#
#   template
#   upgrade
#   install
#   install-dryrun
#
# HELM_YAML_FILES is an environment variable and should contain a
# white-space delimited string of paths to files that will be fed to helm
# as values file.
#
# In HELM_YAML_FILES order MATTERS! The YAML files will be loaded
# in the order specified with _later_ values overriding _earlier_ ones.
#
# Example:
#
# $ export HELM_YAML_FILES="/tmp/values1.yaml ../common/data/common/values-local.yaml /tmp/values2.yaml"
# $ helm-wrapper install
# will result in this helm command being run:
#
# helm $1 --namespace ${APP_NAMESPACE} \
#         -f "/tmp/value1.yaml" \
#         -f "../common/data/common/values-local.yaml" \
#         -f "/tmp/value2.yaml" \
#         "$HELM_RELEASE" "$HELM_REPO_NAME"/"$HELM_CHART_NAME"
#
####################################################################################

THIS_DIR=$(dirname "$0")
source "$THIS_DIR/functions.sh"

export DEBUG=XXX

VERSION_OPTION=""
DRYRUN_OPTION=""

####################################################################################
check_var () {
    var="$1"
    value="${!var}"
    if empty_var "$var"; then
        exit_with_error "$var is not set"
    else
        part1="pass: $var"
        msg=$(printf '%-24s is set ("%s")' "$part1" "${!var}")
        progress "$msg"
    fi
}

validate () {
    check_var "APP_NAMESPACE"
    check_var "HELM_REPO_NAME"
    check_var "HELM_CHART_NAME"
    check_var "HELM_RELEASE"
}

construct_yaml_include () {
    y_files=($HELM_YAML_FILES)

    YAML_INCLUDE_STRING=""
    for y_file in "${y_files[@]}"
    do
        YAML_INCLUDE_STRING="$YAML_INCLUDE_STRING -f ${y_file}"
        # do whatever on $i
    done
}

do_helm_with_namespace_command () {
    # $1 is the action (e.g., 'install')
    cmd=""
    cmd="${cmd} helm $1 --namespace ${APP_NAMESPACE}"
    cmd="${cmd} ${YAML_INCLUDE_STRING}"
    cmd="${cmd} --debug"
    cmd="${cmd} ${VERSION_OPTION} ${DRYRUN_OPTION}"
    cmd="${cmd} ${HELM_RELEASE} ${HELM_REPO_NAME}/${HELM_CHART_NAME}"

    progress "cmd is '$cmd'"
    eval "$cmd"
}

do_helm_command () {
    # $1 is the action (e.g., 'install')
    cmd=""
    cmd="${cmd} helm $1"
    cmd="${cmd} ${YAML_INCLUDE_STRING}"
    cmd="${cmd} --debug"
    cmd="${cmd} ${VERSION_OPTION}"
    cmd="${cmd} ${HELM_RELEASE} ${HELM_REPO_NAME}/${HELM_CHART_NAME}"

    progress "cmd is '$cmd'"
    eval "$cmd"
}

####################################################################################


ACTION=$1
progress "ACTION is '$ACTION'"

construct_yaml_include
progress "YAML_INCLUDE_STRING is '$YAML_INCLUDE_STRING'"
echo "YAML_INCLUDE_STRING is '$YAML_INCLUDE_STRING'"

validate

## If HELM_CHART_VERSION is not empty and not equal to "latest", use
## the "--version" option to use a specific Helm chart version.
if [[ ! -z "$HELM_CHART_VERSION" ]] && [[ "$HELM_CHART_VERSION" != "latest" ]]; then
    VERSION_OPTION=" --version $HELM_CHART_VERSION "
    progress "using chart version '$HELM_CHART_VERSION'"
else
    VERSION_OPTION=""
    progress "not using the --version option"
fi

## Carry out the requested action.

if [[ "$ACTION" == "template" ]]; then
    do_helm_command "template"
fi

if [[ "$ACTION" == "upgrade" ]]; then
    do_helm_command "upgrade"
fi

if [[ "$ACTION" == "install" ]]; then
    do_helm_with_namespace_command "install"
fi

if [[ "$ACTION" == "install-dryrun" ]]; then
    DRYRUN_OPTION=" --dry-run "
    do_helm_with_namespace_command "install"
fi

exit 0
