#!/usr/bin/env bash
set -e

# Delete GCP default network firewall rules and the default network
# All deletion command will ask for confirmation.
#

project=$1
if [[ -z "$project" ]] || ! gcloud projects describe $project > /dev/null 2>&1; then
  echo "Valid GCP roject id is required."
  exit 1
fi

# Firewall rules in default network to be deleted
default_rules="default-allow-ssh default-allow-rdp default-allow-internal default-allow-icmp"

echo Will delelet the following default firewall rules in $project:
echo
for i in $default_rules
do
  echo $i
done
confirm.sh

for i in $default_rules
do
  gcloud compute firewall-rules delete $i --project=$project --quiet
done

gcloud compute networks delete default --project=$project
