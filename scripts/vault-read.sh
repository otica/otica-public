#!/usr/bin/env bash

# Usage:
#   vault-read <path>

# Prints to standard out the vaule of the Vault secret at <path>. If the
# value was stored in Vault as Base64-encoded decode before printing.

THIS_DIR=$(dirname "$0")

source $THIS_DIR/functions.sh

if [[ -z "$VAULT_ADDR" ]]; then
   exit_with_error "missing required environment variable VAULT_ADDR"
fi

export GCP_ENVIRONMENT=${GCP_ENVIRONMENT:-dev}
export SEC_ENV=${SEC_ENV:-$GCP_ENVIRONMENT}

if ! vault --version | grep 'v1.' &> /dev/null
then
    (>&2 echo "The vault version is too old, please upgrade the vault cmd.")
    exit 1
fi

path=$(echo $1 | ${THIS_DIR}/render.sh)

# Get the kv sec from path in json
j=$(vault kv get -format=json $path)

return_code=$?
if [ "$return_code" -ne "0" ]; then
    exit $return_code
fi

f=$(echo "$j" | jq -r '.data.format//.data.data.format' 2> /dev/null)
v=$(echo "$j" | jq -r '.data.value//.data.data.value')

# if value format is base64, decode it
if [ "base64" == "$f" ]
then
    echo -n "$v" | base64 --decode
else
    echo -n "$v"
fi

exit 0

DOCS=<<__END_OF_DOCS__

=head1 NAME

vault-read.sh -- Read secrets from Hashicorp Vault

=head1 USAGE

B<vault-read.sh> I<vault-path>

=head1 DESCRIPTION

Given a I<vault-path> B<vault-read.sh> reads the Vault secret at that path
and writes the results to standard out. The script gets the Vault address
from the environment variable C<VAULT_ADDR>; if C<VAULT_ADDR> is not
defined B<vault-read.sh> will exit with an error.

=head1 BASE64 ENCODING

The C<vault-read.sh> script uses the "format" key-value pair written by
C<vault-write.sh> and associated with the Vault secret to determine if the
value should be Base64-decoded or not. As such you should always use
C<vault-write.sh> and C<vault-read.sh> together: if a secret is written by
C<vault-write.sh> use C<vault-read.sh> to read it.

You I<can> use C<vault-read.sh> to read a secret that was not written by
C<vault-write.sh> but C<vault-read.sh> will not attempt to Base64-decode the
result.

=head1 OPTIONS

There are no options.

=head1 EXIT VALUE

Returns C<0> on success, C<1> on any failure.
