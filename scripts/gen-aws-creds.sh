#!/usr/bin/env bash
set -e

##
## Generates time-bound AWS credentials with vault secrets engine
## caches credentials in the vault token cubbyhole to avoid generating
## new credentials when previously generated credentials have not expired
##

#################
##  FUNCTIONS  ##
#################

usage() {
    echo
    echo "Generate time-bound AWS credentials"
    echo
    echo "usage:"
    echo "${0} <aws_mount> <iam_role>"
    echo
    echo "required parameters:"
    echo "<aws_mount> the aws secret engine path in Vault"
    echo "<iam_role>  the name of the IAM role to be attached to the generated credentials"
    echo
}

error() {
    message=$1
    [[ -n ${message} ]] && echo "ERROR: ${message}"
    exit 1
}

validate_cached_creds() {
    request_time=$1
    cached_creds=$2
    aws_mount=$3

    [[ -z ${cached_creds} ]] && return 1
    [[ ${cached_creds} =~ ^(Error|No value) ]] && return 1

    # Set vars
    cached_lease_id="$(echo ${cached_creds} | jq -r '.data.lease_id')"
    cached_mount_name=$(echo ${cached_lease_id} | awk -F "/" '{print $1}')
    cached_access_key_id="$(echo ${cached_creds} | jq -r '.data.access_key_id')"
    cached_secret_access_key="$(echo ${cached_creds} | jq -r '.data.secret_access_key')"
    cached_expiration="$(echo ${cached_creds} | jq -r '.data.expiration')"
    cached_expiration_epoch=$(date -d "${cached_expiration}" "+%s" 2>/dev/null) || {
        cached_expiration_epoch=$(date -j -f "%m/%d/%Y %T" "${cached_expiration}" "+%s")
    }

    # Import existing AWS credentials (if any)
    aws_access_key_id=$(aws configure get default.aws_access_key_id)
    aws_secret_access_key=$(aws configure get default.aws_secret_access_key)

    # Check if cached credentials match the environment and are not expired
    if
        [[ ${cached_mount_name} == ${aws_mount} ]] &&
        [[ ${cached_access_key_id} == ${aws_access_key_id} ]] &&
        [[ ${cached_secret_access_key} == ${aws_secret_access_key} ]] &&
        [[ ${request_time} -lt $(expr ${cached_expiration_epoch} - 60) ]]
    then
        echo "found cached credentials:"
        echo "lease_id = ${cached_lease_id}"
        echo "using cached credentials expiring on ${cached_expiration}"
        return 0
    fi
    return 1
}

generate_new_creds() {
    request_time=$1
    aws_creds=$2
    full_mount_path=$3

    [[ -z ${aws_creds} ]] && return 1
    [[ ${aws_creds} =~ ^(No value) ]] && return 1

    # Set vars
    lease_id="$(echo ${aws_creds} | jq -r '.lease_id')"
    access_key="$(echo ${aws_creds} | jq -r '.data.access_key')"
    secret_key="$(echo ${aws_creds} | jq -r '.data.secret_key')"
    lease_duration="$(echo ${aws_creds} | jq -r '.lease_duration')"
    expiration_epoch=$(expr ${request_time} + ${lease_duration} 2>/dev/null)
    expiration=$(date -d "@${expiration_epoch}" "+%m/%d/%Y %T" 2>/dev/null) || {
        expiration=$(date -r "${expiration_epoch}" "+%m/%d/%Y %T")
    }

    # Generate new credentials & add credentials to Vault
    if [[ ${access_key} ]] && [[ ${secret_key} ]] ; then
        aws configure set default.aws_access_key_id ${access_key}
        aws configure set default.aws_secret_access_key ${secret_key}
        echo "generating credentials with ${full_mount_path} ..."
        # Wait for 15 seconds for credentials to activate
        sleep 15
        echo "generated AWS credentials:"
        echo "lease_id: ${lease_id}"
        echo "lease_expiration: ${expiration}"
        cache_response=$(vault write cubbyhole/aws_creds access_key_id="${access_key}" \
                secret_access_key="${secret_key}" expiration="${expiration}" lease_id="${lease_id}" 2>&1)
        [[ ! ${cache_response} =~ ^(Success) ]] && echo "WARNING: Unable to cache generated AWS credentials."
        return 0
    fi
    return 1
}

#################
##    MAIN     ##
#################

[[ ! $(which vault) ]] && usage && error "vault not installed. Exiting."
[[ ! $(which jq) ]] && usage && error "jq not installed. Exiting."
[[ $# -lt 2 ]] && usage && error

# SET VARS
AWS_MOUNT=$1
IAM_ROLE=$2

if [[ -z "$VAULT_ADDR" ]]; then
   error "missing required environment variable VAULT_ADDR"
fi

# ENSURE THE VAULT TOKEN IS VALID
VAULT_RESPONSE=$(vault token lookup -format=json 2>&1 || true)
if [[ -z ${VAULT_RESPONSE} ]] || [[ ${VAULT_RESPONSE} =~ "Error looking up token" ]] ; then
    error "Missing client token. Login to Vault."
fi

# VALIDATE CACHED CREDENTIALS (if available)
CACHED_CREDS=$(vault read -format=json cubbyhole/aws_creds 2>&1 || true)
validate_cached_creds "$(date +%s)" "${CACHED_CREDS}" "${AWS_MOUNT}" && exit 0

# GENERATE NEW CREDENTIALS (if required)
AWS_CREDS=$(vault read -format=json ${AWS_MOUNT}/creds/${IAM_ROLE} 2>/dev/null || true)
generate_new_creds "$(date +%s)" "${AWS_CREDS}" "${AWS_MOUNT}/creds/${IAM_ROLE}" || {
    error "Unable to generate credentials with ${AWS_MOUNT}/creds/${IAM_ROLE}"
}

exit 0
