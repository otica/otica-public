#!/usr/bin/env python3

import os
import sys
import jinja2
import yaml

config_file="otica.yaml"

### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##
def exit_with_error(msg):
    sys.stderr.write(f"error: {msg}")
    sys.stderr.write("\n")
    sys.exit(1)

### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##

cur_dir = os.path.dirname(os.path.abspath(__file__))
makefile_parts_dir = os.path.join(os.path.dirname(cur_dir), 'makefile_parts')

templateLoader = jinja2.FileSystemLoader(searchpath=makefile_parts_dir)
templateEnv = jinja2.Environment(loader=templateLoader)

template = templateEnv.get_template('Makefile.tmpl')


# Read in YAML file
with open(config_file) as file:
    make_config = yaml.load(file, Loader=yaml.FullLoader)

# Check that there is a "platform" YAML entry and that it is one of the
# allowed types.
allowed_platforms = ['gcp', 'aws', 'generic']
if ('platform' not in make_config):
    msg = "missing platform definition"
    exit_with_error(msg)
elif (make_config['platform'] not in allowed_platforms):
    msg = f"platform \'{make_config['platform']}\' not recognized"
    exit_with_error(msg)

print(template.render(make_config=make_config))
