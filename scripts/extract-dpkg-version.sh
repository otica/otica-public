#!/usr/bin/env bash

# Given a Git URL pointed to by the environment variable
# DEBIAN_PACKAGE_GIT_URL, clone that Git repository and extract the Debian
# package version number from debian/changelog.
#
# The assumption is that the layout in the cloned repository is in the
# standard layout where debian/changelog is in the root of the repository.
#
# The LAST line of this script is the Debian package version number. So, to get
# the version number do this:
#
#  extract-dpkg-version.sh | tail -n1

set -e

####
progress() {
    if [[ -n "$DEBUG" ]]; then
        echo "progress: $1"
    fi
}
####

if [[ -z "$DEBIAN_PACKAGE_GIT_URL" ]]; then
    echo "missing required DEBIAN_PACKAGE_GIT_URL"
    exit 1
fi
progress "DEBIAN_PACKAGE_GIT_URL is $DEBIAN_PACKAGE_GIT_URL"

if [[ -z "$DEBIAN_PACKAGE_DOCKER_IMAGE" ]]; then
    DEBIAN_PACKAGE_DOCKER_IMAGE="macrotex/debian-package:sid"
fi
progress "DEBIAN_PACKAGE_DOCKER_IMAGE is $DEBIAN_PACKAGE_DOCKER_IMAGE"

# 1. Create a temporary directory to clone repository into.
TEMPDIR=$(mktemp -d)
progress "temporary directory is '$TEMPDIR'"

# 2. Clone repo.
git clone --quiet "$DEBIAN_PACKAGE_GIT_URL" "$TEMPDIR"

# 3. Pull Docker image used to extract Debian package version:
docker pull --quiet "$DEBIAN_PACKAGE_DOCKER_IMAGE"

# 4. Run image to extract version.
VERSION=$(docker run -v "${TEMPDIR}:/tmp/repo" "$DEBIAN_PACKAGE_DOCKER_IMAGE" dpkg-parsechangelog \
                 --file /tmp/repo/debian/changelog -S version)
progress "Debian package version is '$VERSION'"

# 5. Cleanup
rm -rf "$TEMPDIR"
progress "deleted temporary directory '${TEMPDIR}'"

# 6. The very last thing we do is echo the version
echo "$VERSION"

exit 0
