#!/usr/bin/env bash

PLUGIN=$1
URL=$2

if [[ -z "$PLUGIN" ]]; then
    echo "missing plugin name"
    exit 1
fi

if [[ -z "$URL" ]]; then
    echo "missing plugin URL"
    exit 1
fi

# Is the plugin already installed?
REGEX="^$PLUGIN "
RESULT=$(helm plugin list | grep -E -e $REGEX)

if [[ -z "$RESULT" ]]; then
    INSTALLED=0
else
    INSTALLED=1
fi

if [[ "$INSTALLED" == "0" ]]; then
    helm plugin install $URL
else
    echo "skipping (plugin already installed)"
fi

exit 0


