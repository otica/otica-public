#!/usr/bin/env bash
# Usage:
#   renew-kube-cert.sh cert-name [namespace]
# Force renew Let's Encrypt cert
# See  https://github.com/jetstack/cert-manager/issues/854

cert_name=$1
if [ -z "$cert_name" ]; then
    echo "Usage: renew-kube-cert.sh cert-name [namespace]"
    exit 1
fi

if [ -z "$2" ]; then
    sec_ns=default
else
    sec_ns=$2
    shift
fi

crt=$(kubectl get cert  $cert_name -n ${sec_ns} ${sec} -o json)
if [ -z "$crt" ]; then
    #echo "Error: $crt is not found in namespace ${sec_ns}."
    exit 1;
fi

if echo $crt | jq -r '.spec.issuerRef.name' | grep "letsencrypt" &> /dev/null; then
    sec_name=$(echo $crt | jq -r ".spec.secretName")
    echo "Force renew Let's Encrypt cert $crt_name, by deleting kube secret $sec_name"
    kubectl delete -n ${sec_ns} secret $sec_name
    echo "NOTE: You may also need to restart the apps to pickup the renewed cert."
fi
