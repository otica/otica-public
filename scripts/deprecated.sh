#!/usr/bin/env bash

# Print out a deprecation warning and sleep for one second so that
# the user sees the warning.

echo "--------------------"
printf '\e[1;31m%-6s\e[m\n' "OTICA FRAMEWORK DEPRECATION WARNING"
echo "$1"
echo "--------------------"
sleep 1

