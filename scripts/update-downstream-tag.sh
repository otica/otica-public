#!/usr/bin/env bash
set -e
set -u

# Update <downstream_repo>/${TAG_FILE} with TAG=<tag>
# Usage $0 <downstream_repo> <tags> [<commit-msg>]

# Git server format must be in one of
#     git@<host>:
# or  https://<host>

# include functions
source $THIS_DIR/functions.sh

if [[ -z "$GIT_SERVER" ]]; then
    exit_with_error "missing required environment variable GIT_SERVER"
fi

TAG_FILE=${TAG_FILE:-common/tag.sh}

function usage() {
    echo "Update ${GIT_SERVER}/<downstream_repo>/${TAG_FILE} with <tag> and optional <commit-message>"
    echo "Usage: $(basename $0) <downstream_repo> <tag> [<commit-msg>]"
}

if [ "$#" -lt 2 ]; then
    usage
    exit 1
fi

DEFAULT_TAG=$(git rev-parse HEAD)
TEMP_DIR=${TEMP_DIR:-/tmp}

DOWNSTREAM_REPO=$1
shift
TAG=${1:-$DEFAULT_TAG}
shift

GIT_REPO=$(git remote -v | grep push | tr "\t| " "\n" | grep '.git' | xargs basename -s ".git")
GIT_USER=$(git log -1 --pretty=format:'%an')
GIT_USER_EMAIL=$(git log -1 --pretty=format:'%ae')
DEFAULT_COMMIT_MSG="Update ${TAG_FILE} by upstream ${GIT_REPO} - ${GIT_USER}"
COMMIT_MSG=${1:-$DEFAULT_COMMIT_MSG}

mkdir -p ${TEMP_DIR}

if [ -d "${TEMP_DIR}/${DOWNSTREAM_REPO}" ]; then
    rm -rf ${TEMP_DIR}/${DOWNSTREAM_REPO}
fi

# Clone the downstream repos
git clone -q ${GIT_SERVER}/${DOWNSTREAM_REPO} ${TEMP_DIR}/${DOWNSTREAM_REPO}
pushd ${TEMP_DIR}/${DOWNSTREAM_REPO}
git pull

# Set git user.name and user.email if missing
if [ -z "$(git config user.name)" ]; then
    # Use uppersteam commit user.name and user.email as the commit user
    # because who made the real change, in most of cases.
    [ ! -z "${GIT_USER}" ] && git config --local user.name "${GIT_USER}"
    [ ! -z "${GIT_USER_EMAIL}" ] && git config --local user.email "${GIT_USER_EMAIL}"
fi

# Update the TAG_FILE of downstream repos
echo "# ${COMMIT_MSG}" > ${TAG_FILE}
echo "export TAG=${TAG}" >> ${TAG_FILE}
if git status -s | grep ${TAG_FILE} &> /dev/null; then
    git add ${TAG_FILE}
    git commit -q -m "${COMMIT_MSG}"
    git push
    echo "${DOWNSTREAM_REPO}: ${COMMIT_MSG}; Set TAG=${TAG}"
else
    echo "Nothing changed."
fi

popd
rm -rf ${TEMP_DIR}/${DOWNSTREAM_REPO}
