#!/usr/bin/env bash

# Get the absoute path to the top-level project directory.
#
# The directory tree from the current directory on up will be traversed
# until a directory containing the file ".otica-top-level" is found.
#
# However, the above process is skipped if the environment variable
# TL_BASEDIR_OVERRIDE is set; in this case the value of
# TL_BASEDIR_OVERRIDE will be used.
#
# CAUTION! The use of TL_BASEDIR_OVERRIDE to get around the usual
# directory structure of an Otica project is NOT recommended as that can
# lead to confusing and unexpected behavior.

if [[ -n "$TL_BASEDIR_OVERRIDE" ]]; then
    echo "$TL_BASEDIR_OVERRIDE"
else
    THIS_DIR=$(dirname "$0")

    # include functions
    source "$THIS_DIR"/functions.sh

    find_up ".otica-top-level"
fi
