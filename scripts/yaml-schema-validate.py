#!/usr/bin/env python3

import argparse
import os
import sys

import yamale

class YamlSchemaValidate():

    def __init__(self,
                 verbose=False,
                 schema_file=None,
                 yaml_file_to_validate=None,):

        self.schema_file           = schema_file
        self.yaml_file_to_validate = yaml_file_to_validate

        if (not os.path.exists(self.schema_file)):
            msg = f"schema file '{self.schema_file}' does not exist"
            self.exit_with_error(msg)

        if (not os.path.exists(self.yaml_file_to_validate)):
            msg = f"YAML file '{self.yaml_file_to_validate}' to validate does not exist"
            self.exit_with_error(msg)


    def validate(self):
        schema = yamale.make_schema(self.schema_file)

        data = yamale.make_data(self.yaml_file_to_validate)

        # Validate data against the schema. Throws a ValueError if data is invalid.
        yamale.validate(schema, data)

    def exit_with_error(self, msg):
        print(f"error: {msg}")
        sys.exit(1)

#######

def main():
    description_str = 'Validate YAML file against Yamale schema.'
    parser = argparse.ArgumentParser(description=description_str)

    parser.add_argument('schema', nargs=1,
                        help='path to Yamale schema file')
    parser.add_argument('yaml_file', metavar='yaml-file', nargs=1,
                        help='YAML file to validate')
    # Options
    parser.add_argument('--verbose', '-v', action='store_true',
                        help='show more detail when running this script')
    args = parser.parse_args()

    ysv = YamlSchemaValidate(verbose=False,
                             schema_file=args.schema[0],
                             yaml_file_to_validate=args.yaml_file[0])


    ysv.validate()

    # If we get here then there were no errors
    print("looks good: no YAML schema errors found.")
    sys.exit(0)


if __name__ == '__main__':
    print("checking YAML against Yamale schema...", end="")
    main()
