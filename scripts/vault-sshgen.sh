#!/usr/bin/env bash
# generate ssh rsa keys and save to vault path

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <vault-secret-path> <key-id>"
    echo "You must have the write privilege to <vault-secret-path>"
    exit 1
fi

if vault-read.sh ${1}/ssh_id_rsa &> /dev/null
then 
    echo "ERROR: ${1}/ssh_id_rsa exists."
else 
    echo Generate a new ssh keys and save to the ${1}/ssh_id_rsa and ${1}/ssh_id_rsa.pub
	ssh-keygen -b 4096 -t rsa -f /tmp/ssh_id_rsa -q -N "" -C $2 -m PEM
	vault-write.sh ${1}/ssh_id_rsa @/tmp/ssh_id_rsa
	vault-write.sh ${1}/ssh_id_rsa.pub @/tmp/ssh_id_rsa.pub
    rm -rf /tmp/id_rsa /tmp/id_rsa.pub
fi
