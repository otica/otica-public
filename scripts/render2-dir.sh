#!/usr/bin/env bash

# Use:
#   render2-dir.sh <source-directory> <target-directory>

# Copy every file in <source-directory> (recursively) to
# <target-directory> and then run every text file in <target-directory>
# (recursively) through render2.sh. This is a safe operation as the only
# files that render2.sh will change are those with a recognized "#!"
# string on their first line.

# WARNING: this script does NOT remove any files already in
# <target-directory> before running, so you may want to do a "rm -rf
# <target-directory>/*" first.

set -e

THIS_DIR=$(dirname "$0")
source $THIS_DIR/functions.sh


# Step 0: Initialization
usage() {
    echo "Usage: render2-dir.sh <source-directory> <target-directory>"
}

SOURCE_DIR=$1
TARGET_DIR=$2

if [[ -z "$SOURCE_DIR" ]]; then
    echo "error: missing SOURCE_DIR argument"
    usage
    exit 1
fi

if [[ -z "$TARGET_DIR" ]]; then
    echo "error: missing TARGET_DIR argument"
    usage
    exit 1
fi

# Step 1. Copy everything from SOURCE_DIR to TARGET_DIR:
cp -pR "${SOURCE_DIR}/." "${TARGET_DIR}"

# Step 2: Convert everything in TARGET_DIR with render2.sh.
cd "${TARGET_DIR}"

shopt -s globstar
for file in ./**/*
do
    if [[ -f "$file" ]]; then
        echo -n "processing $file"

        # Is this a binary file? If so, skip.
        binary_mime_type=$(file --mime $file)

        regex='^.*charset=binary.*$'
        if [[ "$binary_mime_type" =~ $regex ]]; then
            echo "...binary file skipped"
        else
            progress "file '$file' is a text file so we can render it"

            tmpfile=$(mktemp /tmp/render2-dir.XXXXXX)
            render2.sh < "$file" > "$tmpfile"

            # Did the file change? If the 'diff' utility is installed
            # we can check.
            if command -v diff > /dev/null; then
                if diff --brief "$tmpfile" "$file" > /dev/null; then
                    echo "...no change"
                else
                    echo "...file changed"
                fi
            else
                echo ""
            fi

            # Copy the file.
            cp "$tmpfile" "$file"

            rm -f "$tmpfile"
        fi
    fi
done
