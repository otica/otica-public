#!/usr/bin/env bash
set -e

# Merge $KUBECONFIG and <a kubeconfig file>
# Usage:
#     $0 <a_kubeconfig_file>
#

KUBECONFIG=${KUBECONFIG:-${HOME}/.kube/config}
KUBECONFIG_TMP=/tmp/kubeconfig

if [ -z "$1" ]; then
    echo "Merge <a_kubeconfig_file> into \$KUBECONFIG ($KUBECONFIG)"
    echo "Usage: $0 <a_kubeconfig_file>"
    exit 1;
fi

if [ -f "${KUBECONFIG}" ]; then
    cp ${KUBECONFIG} ${KUBECONFIG}.bak
    KUBECONFIG=${1}:${KUBECONFIG} kubectl config view --flatten > ${KUBECONFIG_TMP} && mv ${KUBECONFIG_TMP} ${KUBECONFIG}
else
    cp ${1} ${KUBECONFIG}
fi
