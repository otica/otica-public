#!/usr/bin/env bash

#################
##  FUNCTIONS  ##
#################

usage() {
    app=$(basename ${0})
    echo "Usage:"
    echo "    ${app} -l <NAMESPACE>        List failed pods in specified namespace"
    echo "    ${app} -L                    List failed pods in all namespaces"
    echo "    ${app} -d <NAMESPACE>        Delete failed pods in specified namespace"
    echo "    ${app} -D                    Delete failed pods in all namespaces"
    echo "    ${app} -h                    Display this help message"
    exit 1
}

longest_value() {
    length=-1
    for i in $@ ; do
        if [ ${#i} -gt ${length} ] ; then
            length=${#i}
        fi
    done
}

list_failed_pods() {
    declare -A "PODS=( $(kubectl get pods ${NS} --output=json | jq -rc  '.items[] | select(.status.phase == "Failed") | .metadata | "[\(.name)]=\(.namespace)"' | xargs) )"

    longest_value ${!PODS[@]}
    rows="\e[1;33m%s\e[m %-${length}s \e[1;33m%s\e[m %s\n"
    COUNT=0
    for POD in "${!PODS[@]}" ; do
        NAMESPACE=${PODS[${POD}]}
        printf "${rows}" "POD:" "${POD}" "NAMESPACE:" "${NAMESPACE}"
        COUNT=$((COUNT+1))
    done
    if [[ ${COUNT} -le 0 ]] ; then
        printf "\e[1;33m%s\e[m\n" "No Failed Pods found."
	return
    fi
    printf "\e[1;33m%s\e[m%s\n" "Number of Failed Pods: " "${COUNT}"
}

delete_failed_pods() {
    declare -A "PODS=( $(kubectl get pods ${NS} --output=json | jq -rc  '.items[] | select(.status.phase == "Failed") | .metadata | "[\(.name)]=\(.namespace)"' | xargs) )"

    COUNT=0
    for POD in "${!PODS[@]}" ; do
        NAMESPACE=${PODS[${POD}]}
        kubectl delete "pod/${POD}" -n ${NAMESPACE}
        COUNT=$((COUNT+1))
    done
    if [[ ${COUNT} -le 0 ]] ; then
        printf "\e[1;33m%s\e[m\n" "No Failed Pods found."
	return
    fi
    printf "\e[1;33m%s\e[m%s\n" "Number of Failed Pods deleted: " "${COUNT}"
}

#################
##    MAIN     ##
#################

if [[ ${#} -eq 0 ]]; then
    usage
fi

while getopts ":l:Ld:Dh" opt ; do
    case ${opt} in
        l)
            NS="-n ${2}"
            list_failed_pods
            exit 0
            ;;
        L)
            NS="--all-namespaces"
            list_failed_pods
            exit 0
            ;;
        d)
            NS="-n ${2}"
            delete_failed_pods
            exit 0
            ;;
        D)
            NS="--all-namespaces"
            delete_failed_pods
            exit 0
            ;;
        h)
            usage
            ;;
        *)
            echo "Invalid Option: -${OPTARG}" 1>&2
            echo
            usage
            ;;
    esac
done
