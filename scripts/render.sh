#!/usr/bin/env bash

###################################################################################
# a filter that renders a template, usage: cat <file> | render.sh
# Note: if gomlate ds vault is used, VAULT_ADDR must be defined and auth done.
####################################################################################

THIS_DIR=$(dirname "$0")
FRAMEWORK_DIR=${FRAMEWORK_DIR:-..}
ENVVARS=${ENVVARS:-.envvars}

# include functions
#source $THIS_DIR/functions.sh

# if envvars are defined, source all envvars
# it is very handly for import all sec envvars
if [ -f ENVVARS ]; then
    source ${ENVVARS}
fi

# gomplpate tmpt for get vault kv value from SEC_PATH
# usage example in $FRAMEWORK_DIR/gomplate-templates/vault-kv.tmpl
read -r -d '' gomplate_func <<'EOF'
{{- define "vault-kv" }}
{{- $response := (datasource "vault" (strings.ReplaceAll "secret/" "secret/data/" . )).data }}
{{- if eq $response.format "base64" }}{{ base64.Decode $response.value  }}
{{- else if eq $response.format "text" }}
{{- $response.value }}{{ end }}{{ end -}}
EOF

IFS= read -r first_line

case "$first_line" in
        *\#!gomplate*   )
            # render gomplate tmpt
            ( echo $gomplate_func; cat - ) | gomplate -d vault="vault://" "$@"
            ;;
        *\#!vault2kube*  )
            # render vault2kube tmpt
            cat - | envsubst | vault2kube.sh
            ;;
        *\#!vault2text*  | *\#!vault2chart* )
            # render vault2text tmpt
            cat - | envsubst | vault2text.sh
            ;;
        *\#!vault2properties*  )
            # render vault2properties tmpt
            cat - | envsubst | vault2properties.sh
            ;;
        *\#!envsubst*   )
            # render envsubst tmpt
            cat - | envsubst
            ;;
        *               )
            # do nothing
            # echo $first_line
            # cat -

            # default render envsubst tmpt, don't strip the first_line
            (echo $first_line; cat - ) | envsubst
            ;;
esac
