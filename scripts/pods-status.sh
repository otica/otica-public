#!/usr/bin/env bash

kubectl get po --all-namespaces | grep -vE '1/1|2/2|3/3|4/4|5/5|Completed|Terminating'
