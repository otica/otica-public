#!/usr/bin/env bash

source functions.sh

########################################################
check_var () {
    var="$1"
    value="${!var}"
    if empty_var "$var"; then
        exit_with_error "$var is not set"
    else
        part1="pass: $var"
        msg=$(printf '%-24s is set ("%s")' "$part1" "${!var}")
        progress "$msg"
    fi
}

progress () {
    if [[ "$VERBOSE" == "1" ]]; then
        echo "$1"
    fi
}

exit_with_error () {
    echo "error: $1"
    exit 1
}
########################################################

check_var "APP_NAMESPACE"
check_var "APP_ENV"
check_var "HELM_REPO_NAME"
check_var "HELM_REPO_URL"
check_var "HELM_CHART_NAME"
check_var "HELM_CHART_VERSION"
check_var "HELM_RELEASE"

printf 'optional: %s is ("%s")' "HELM_YAML_FILES" "$HELM_YAML_FILES"
echo ""


exit 0
