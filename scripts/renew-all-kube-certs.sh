#!/usr/bin/env bash
# Usage:
#   renew-all-kube-cert.sh 
# Force renew all Let's Encrypt certs in current cluster
# See  https://github.com/jetstack/cert-manager/issues/854


THIS_DIR=$(dirname "$0")

function get_ns() {
    echo $1
}

function get_cert() {
    echo $2
}

crts=$(kubectl get certs --all-namespaces -o json)
if [ -z "$crts" ]; then
    echo "Error: no certs found."
    exit 1;
fi

echo $crts | jq -r '.items[] | .metadata.namespace + " " + .metadata.name' |
while read -r line; do
    ns=$(get_ns $line)
    cert=$(get_cert $line)
    [[ -z "$ns" ]] || [[ -z "$cert" ]] && continue
    ${THIS_DIR}/renew-kube-cert.sh $cert $ns
done



