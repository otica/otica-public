#!/usr/bin/env python3

# pylint: disable=superfluous-parens
# pylint: disable=bad-whitespace
# pylint: disable=attribute-defined-outside-init

import argparse
import logging
import os
import pathlib
import re
import sys
import subprocess
import yaml

# Add './lib' to search path. We need to do this
# so that we can import otica_abstract
MY_DIRECTORY = pathlib.Path(__file__).parent.resolve()
sys.path.append(os.path.join(MY_DIRECTORY, 'lib'))

from otica_abstract import OticaAbstract

class GitRepo(OticaAbstract):
    """A simple git repository model. Allows for cloning, fetching, and pulling.
    """
    def __init__(self, url, working_dir):
        super().__init__()
        self.url         = url
        self.working_dir = working_dir

    ## START OF PROPERTIES
    @property
    def url(self):
        """ Getter method for the "url" property.
        """
        return self._url

    @url.setter
    def url(self, value):
        """ Setter method for the "url" property.
        """
        self._url = value

    @property
    def domain(self):
        """ Getter method for the "domain" property.
        """
        return self._domain

    @domain.setter
    def domain(self, value):
        """ Setter method for the "domain" property.
        """
        self._domain = value

    @property
    def working_dir(self):
        """ Getter method for the "working_dir" property.
        """
        return self._working_dir

    @working_dir.setter
    def working_dir(self, value):
        """ Setter method for the "working_dir" property.
        """
        self._working_dir = value
    ## END OF PROPERTIES

    def run_cmd(self, command):
        """Run a command and return stdout, stderr, and the return code.
        """
        self.progress(f"command is '{' '.join(command)}'")

        # If we are in a version of Python prior to 3.7 we call
        # the subprocess.run a bit differently.
        version_major = sys.version_info.major
        version_minor = sys.version_info.minor
        if (version_major < 3):
            raise Exception('this script does not work with Python 2.')

        if ((version_major == 3) and (version_minor < 7)):
            # NOTICE: Please remove this section of the if-else once
            # everyone has stopped using versions of Python prior to
            # Python 3.7.
            #
            # For Python 3.6 and earlier, the subprocess.run function does not
            # recognize the parameters "capture_output" and "text". So, we use a different
            # version of subprocess.run for these older Pythons.
            # See also https://stackoverflow.com/a/53209196/268847
            from subprocess import PIPE
            completed_proc = subprocess.run(
                command,
                shell=False,
                check=False,
                stdout=PIPE, stderr=PIPE, # instead of capture_output=True,
                universal_newlines=True,  # instead of text=True
            )
        else:
            completed_proc = subprocess.run(
                command,
                shell=False,
                check=False,
                capture_output=True,
                text=True
            )

        stdout     = completed_proc.stdout
        stderr     = completed_proc.stderr
        returncode = completed_proc.returncode

        if (self.verbose):
            print(f"stdout:     {stdout}")
            print(f"stderr:     {stderr}")
            print(f"returncode: {returncode}")

        return [stdout, stderr, returncode]

    def run_git_cmd(self, command):
        """Run a git command
        """
        git_cmd = ['git'] + command
        return self.run_cmd(git_cmd)

    def run_git_wd(self, command):
        """Run a git command relative to a git working copy directory.
        """
        return self.run_git_cmd(['-C', self.working_dir] + command)

    def clone(self):
        """Clone a git repository.
        """
        return self.run_git_cmd(['clone', self.url, self.working_dir])

    def fetch(self):
        """Fetch a git repository.
        """
        return self.run_git_wd(['fetch'])

    def pull(self):
        """Pull a git repository.
        """
        return self.run_git_wd(['pull'])

    def working_dir_fullpath(self):
        """ Return the full path of the full path
        """
        return os.path.join(os.getcwd(), self._working_dir)

    def is_git_repo(self):
        """Returns true if a git repo, false otherwise.
        """
        if (os.path.isdir(os.path.join(self.working_dir, '.git'))):
            tldg = self.top_level_git_dir()
            self.progress(f"top-level git directory is '{tldg}'")
            self.progress(f"working_dir is          is '{self.working_dir_fullpath()}'")
            if (tldg is None):
                return False
            else:
                return (tldg == self.working_dir_fullpath())
        else:
            return False

    def top_level_git_dir(self):
        """Get the top-level git directory relative to the workign directory. If
        the working directory is a git repository then working directory
        is returned, othwewise git will go up the directory chain until it
        finds the containing git repository.

        """
        command = ['git',
                   'rev-parse',
                   '--show-toplevel',
                   ]

        completed_proc = subprocess.run(command,
                                        shell=False,
                                        check=False,
                                        capture_output=True,
                                        text=True,
                                        cwd=self.working_dir)

        stdout     = completed_proc.stdout
        stderr     = completed_proc.stderr
        returncode = completed_proc.returncode

        if (stderr):
            return None
        else:
            return stdout.strip()


class SubProject(OticaAbstract):
    """
    sub-projects:
      # Example of full-syntax
      - name:     myproject-apache
        url:  git@gitlab.example.com:devops-team/web-apps/myproject-apache.git
        category: aws
        two-step: true
        description: |-
          GKE cluster creation
    """

    def __init__(self, subp, subprojects_dir):
        super().__init__()
        self.config_file = 'repos.yaml'

        self.subprojects_dir = subprojects_dir

        if ('url' not in subp):
            msg = "missing required propery 'url'"
            self.exit_with_error(msg)
        else:
            # Get the domain
            url = subp['url']
            match_obj = re.match(r'^([^\/]+):(.+)$', url)
            if (match_obj is None):
                self.exit_with_error(f"could not find domain portion of '{url}'")
            else:
                domain   = match_obj.group(1)
                url_path = match_obj.group(2)
                domain = re.sub('^git@', '', domain)

            # Check that domain is valid. It should contain only letters,
            # numbers, dashes, and dots.
            match_obj = re.match(r'^[\w\-.]+$', domain)
            if (match_obj is None):
                self.exit_with_error(f"domain '{domain}' is not a valid domain name")
            else:
                self.domain = domain

            self.url_path = url_path

            self.url = subp['url']

        if ('name' not in subp):
            # Extract name from "url". Get the basename and remove any
            # ".git" extension.
            self.name = os.path.basename(self.url)
            self.name = re.sub('[.]git$', '', self.name)
        else:
            self.name = subp['name']

        if ('description' in subp):
            self.description = subp['description']
        else:
            self.description = None

        if ('category' in subp):
            self.category = subp['category']
        else:
            self.category = None

        if ('two-step' in subp):
            self.two_step = subp['two-step']
        else:
            self.two_step = False

    ## START OF PROPERTIES
    @property
    def name(self):
        """ Getter method for the "name" property.
        """
        return self._name

    @name.setter
    def name(self, value):
        """ Setter method for the "name" property.
        """
        self._name = value

    @property
    def description(self):
        """ Getter method for the "description" property.
        """
        return self._description

    @description.setter
    def description(self, value):
        """ Setter method for the "description" property.
        """
        self._description = value

    @property
    def url(self):
        """ Getter method for the "url" property.
        """
        return self._url

    @url.setter
    def url(self, value):
        """ Setter method for the "url" property.
        """
        # Strip of any leading "git@"
        self._url = value

    @property
    def url_path(self):
        """ Getter method for the "url_path" property.
        """
        return self._url_path

    @url_path.setter
    def url_path(self, value):
        """ Setter method for the "url_path" property.
        """
        self._url_path = value

    @property
    def category(self):
        """ Getter method for the "cateogory" property.
        """
        return self._category

    @category.setter
    def category(self, value):
        """ Setter method for the "category" property.
        """
        self._category = value

    @property
    def two_step(self):
        """ Getter method for the "two_step" property.
        """
        return self._two_step

    @two_step.setter
    def two_step(self, value):
        """ Setter method for the "two_step" property.
        """
        self._two_step = value

    @property
    def subprojects_dir(self):
        """ Getter method for the "subprojects_dir" property.
        """
        return self._subprojects_dir

    @subprojects_dir.setter
    def subprojects_dir(self, value):
        """ Setter method for the "subprojects_dir" property.
        """
        self._subprojects_dir = value
    ## END OF PROPERTIES

    def full_url(self):
        """Return the full git URL for this sub-project.
        """
        return 'git@' + self.domain + ':' + self.url_path

    def progress(self, msg):
        """Override the progress method to give a bit of indent.
        """
        msg = f"  {msg}"
        super().progress(msg)

    def to_string(self):
        """Return a string version of a SubProject object.
        """
        return f"""
name:        {self.name}
description: {self.description}
url-domain:  {self.domain}
url-path:    {self.url_path}
category:    {self.category}
two-step:    {self.two_step}
path:        {self.get_path()}
""".strip()

    def get_path(self):
        """Return the path the working copy directory.
        """
        if (self.category is None):
            subp_path = os.path.join(self.subprojects_dir, self.name)
        else:
            subp_path = os.path.join(self.subprojects_dir, self.category, self.name)

        return subp_path


    def repo(self):
        """Create a GitRepo object based on self's working directory and
        full_url."""
        working_dir  = self.get_path()
        full_url     = self.full_url()
        return GitRepo(url = full_url, working_dir = working_dir)


    def create(self):
        """Create the working copy directory for this sub-project.
        """
        self.progress(f"creating directory for {self.name}")

        working_dir = self.get_path()
        full_url = self.full_url()

        msg = f"about to clone {full_url} into {working_dir}"
        print(f"creating {working_dir}...")
        if (self.dry_run):
            self.dry_run_msg(msg)
        else:
            self.progress(msg)
            repo = self.repo()

            [stdout, stderr, returncode] = repo.clone()
            if (returncode != 0):
                print(f"error cloning {full_url}")
                print(stderr.strip())
            elif (stdout):
                print(stdout)

        print("")

    def update(self):
        """Update the working copy directory for this sub-project.
        """
        self.progress(f"updating directory for {self.name}")

        working_dir  = self.get_path()
        full_url     = self.full_url()

        msg = f"about to update '{full_url}' in '{working_dir}'"
        if (self.dry_run):
            self.dry_run_msg(msg)
        else:
            print(f"updating {working_dir}...")
            self.progress(msg)

            repo = GitRepo(url = full_url, working_dir = working_dir)
            repo.verbose = self.verbose

            if (repo.is_git_repo()):
                msg = f"'{working_dir}' _is_ a git repository; proceeding with update"
                self.progress(msg)
            else:
                msg = f"'{working_dir}' is not a git repository"
                self.exit_with_error(msg)

            self.progress("about to fetch")
            [stdout, stderr, returncode] = repo.fetch()
            if (stdout):
                print(stdout.strip())
            if (stderr):
                print(f"error fetching {working_dir} (rc: {returncode})")
                print(stderr)

            self.progress("about to pull")
            [stdout, stderr, returncode] = repo.pull()
            if (stdout):
                print(stdout.strip())
            if (stderr):
                print(f"Error pulling {working_dir} (rc: {returncode})")
                print(stderr)

            print("")

    def upsert(self):
        """Create or update the working copy directory for this sub-project.
        """
        name = self.name

        self.progress(f"updating sub-project '{name}'")

        # If the directory already exists then this is an update,
        # otherwise it is a create.
        if (os.path.isdir(self.get_path())):
            self.progress(f"directory for {name} already exists; will update")
            self.update()
        else:
            self.progress(f"directory for {name} does not exist; will create")
            self.create()

    def unpushed_commits(self):
        """Returns a list of all unpushed commits. Returns None if no
        unpushed commits. We ignore errors.
        """
        repo = self.repo()
        [stdout, stderr, rc] = repo.run_git_wd(['log',
                                                '--branches',
                                                '--not',
                                                '--remotes',
                                                '--oneline',
                                                ])
        if (stdout.strip() == ""):
            return None
        else:
            return stdout.strip()

class ReposYaml(OticaAbstract):
    """A class represnting the repos.yaml file"""
    def __init__(self):
        super().__init__()
        self.config_file = 'repos.yaml'
        self.subprojects_dir = 'sub-projects'
        self.subprojects = []

    ## START OF PROPERTIES
    @property
    def subprojects_dir(self):
        """ Getter method for the "subprojects_dir" property.
        """
        return self._subprojects_dir

    @subprojects_dir.setter
    def subprojects_dir(self, value):
        """ Setter method for the "subprojects_dir" property.
        """
        self._subprojects_dir = value
    ## END OF PROPERTIES

    def read_config(self):
        """Read and parse the repos.yaml file"""
        self.progress("starting config file parsing")

        # Read yaml file.
        with open(self.config_file) as file:
            self.progress(f"parsing repos yaml file '{self.config_file}'")
            repos_yaml = yaml.load(file, Loader=yaml.FullLoader)

            # The top-level object containing all the subprojects is
            # "sub-projects".
            if ('sub-projects' not in repos_yaml):
                msg = "missing required top-level 'sub-projects' element"
                self.exit_with_error(msg)
            else:
                if ('sub-projects' in repos_yaml):
                    sub_projects = repos_yaml['sub-projects']

                if (sub_projects is None):
                    sub_projects = []

                for subp in sub_projects:
                    subproject = SubProject(subp, self.subprojects_dir)
                    subproject.progress = self.progress
                    subproject.dry_run  = self.dry_run
                    subproject.verbose  = self.verbose

                    if (self.verbose):
                        print(subproject.to_string())
                        print("")

                    self.subprojects.append(subproject)

        self.progress(f"read in {len(self.subprojects)} sub-projects")
        return True

    def pre_update_check(self):
        """Check for problems before running update."""
        if (not os.path.isdir(self.subprojects_dir)):
            msg = f"required directory '{self.subprojects_dir}' does not exist"
            self.exit_with_error(msg)
        else:
            msg = f"required directory '{self.subprojects_dir}' exists"
            self.progress(msg)

    def update(self):
        """Update all the repos. Returns the number of repos examined (not necessarily updated).
        """
        self.progress("starting repos update")

        # See if "repos" environment variable was passed in.
        # We use that as a filter.
        if ('filter' in os.environ):
            repos_filter = os.environ['filter']
        else:
            repos_filter = '.*'

        self.progress(f"filtering sub-projects using pattern '{repos_filter}'")
        pattern = re.compile(repos_filter)

        # Make sure everything is OK to do an update. (Will
        # exit if any issues.)
        self.pre_update_check()

        # Loop over the subprojects and update or create.
        projects_examined = 0
        for subp in self.subprojects:
            match = pattern.search(subp.name)
            if (match is not None):
                self.progress(f"processing sub-project {subp.name}")
                subp.upsert()
                projects_examined = projects_examined + 1
            else:
                self.progress(f"skipping sub-project {subp.name} (does not match '{repos_filter}')")

        return projects_examined

    def show_uncommitted(self):
        """Print out status vis-a-vis uncommited changes for all repositories.
        This will also indicate if there are any commits that have not
        been pushed.
        """
        self.progress("starting uncommitted")

        # Loop over the subprojects and see if there are any uncommited changes.
        projects_examined = 0
        for subp in self.subprojects:
            self.progress(f"examining sub-project {subp.name}")
            repo = subp.repo()
            [stdout, stderr, rc] = repo.run_git_wd(['status', '--porcelain=v1'])
            if (stderr):
                print(f"{subp.name}: ERROR ({stderr.strip()})")
            elif (stdout):
                print(f"{subp.name}: has uncommited changes")
            else:
                # See if there are any unpushed commits
                if (subp.unpushed_commits() is not None):
                    print(f"{subp.name}: has unpushed commits")
                else:
                    print(f"{subp.name}: clean")


    def show_orphaned(self):
        """ Display a message about any directories in sub-projects that are _not_
        in repos.yaml."""
        missing_dirs = self.directories_not_in_repos_yaml()
        if (missing_dirs):
            print("")
            print("The following directories are in sub-projects/ but do not appear in repos.yaml:")
            for dir1 in missing_dirs:
                print(f"  - {dir1}")

    def directories_not_in_repos_yaml(self):
        """Get a list of all directories in sub-projects (or any category directory) that
        are not represented in repos.yaml.

        This is slightly complicated since we not only have the directory
        sub-folders/ itself but also any "category" sub-directories that
        contain subprojects.
        """
        categories = {}
        projects   = {}
        # First, get all categories in repos.yaml
        for subp in self.subprojects:
            category = subp.category
            categories[category] = True
            if (category not in projects):
                projects[category] = []

            projects[category].append(subp.name)

        all_categories = categories.keys()
        self.progress(f"categories are {all_categories}")

        orphaned_dirs = []
        for category in all_categories:
            self.progress(f"looking for any orphaned subprojects in category {category}")
            self.progress(f"projects for this category {projects[category]}")
            if (category is None):
                category_dir = os.path.join(self.subprojects_dir)
            else:
                category_dir = os.path.join(self.subprojects_dir, category)

            dirs = [f.path for f in os.scandir(category_dir) if f.is_dir()]
            for dir1 in dirs:
                name = os.path.basename(os.path.normpath(dir1))

                # We don't care about names matching a category.
                if (name not in all_categories):
                    self.progress(f"name is {name}")
                    if (name not in projects[category]):
                        orphaned_dirs.append(dir1)

        self.progress(f"orphaned directories: {orphaned_dirs}")
        return orphaned_dirs

    def to_string(self):
        """Return a string version this objecs."""
        return f"""
config_file:  {self.config_file}
dry-run:      {self.dry_run}
""".strip()

    @staticmethod
    def normalize_boolean(value):
        """Given a string value convert to True or False.
        """
        # pylint: disable=no-else-return
        if (not value):
            return False
        elif (re.match(r'^(yes|true)$', value, re.IGNORECASE)):
            return True
        elif (re.match(r'^(no|false)$', value, re.IGNORECASE)):
            return False
        else:
            return False


############################################################################

def main():
    """Parse the arguments and run the desired action"""
    # Set up argparse
    parser = argparse.ArgumentParser(description='Manage repos via YAML configuration.')

    # Actions
    parser.add_argument('action', choices=['check', 'list', 'details', 'update', 'orphaned', 'uncommitted'])

    # Options
    parser.add_argument("--verbose",
                        help="verbose output",
                        action="store_true")
    parser.add_argument("--dry-run",
                        help="dry-run mode (update action only)",
                        action="store_true")

    # Parse opions
    args = parser.parse_args()

    repos_yaml = ReposYaml()

    if args.verbose:
        repos_yaml.verbose = True
    else:
        repos_yaml.verbose = False

    if (('VERBOSE' in os.environ) and (os.environ['VERBOSE'])):
        verbose_env_value = os.environ['VERBOSE']
        verbose_env_value = ReposYaml.normalize_boolean(verbose_env_value)
        repos_yaml.verbose = verbose_env_value

    if (args.verbose):
        logging.basicConfig(level=logging.DEBUG, format='%(message)s',)
    else:
        logging.basicConfig(level=logging.INFO, format='%(message)s',)

    if args.dry_run:
        repos_yaml.dry_run = True
    else:
        repos_yaml.dry_run = False

    if (args.action != 'check'):
        repos_yaml.read_config()

    if (args.action == 'check'):
        if (repos_yaml.read_config()):
            msg = f"configuration file '{repos_yaml.config_file}' is valid"
            print(msg)
            exit(0)
        else:
            msg = f"error reading configuration {repos_yaml.config_file}"
            repos_yaml.exit_with_error(msg)
    elif (args.action == 'list'):
        for subp in repos_yaml.subprojects:
            print(subp.name)
    elif (args.action == 'details'):
        for subp in repos_yaml.subprojects:
            print(subp.to_string())
            print("")
    elif (args.action == 'update'):
        projects_examined = repos_yaml.update()
        if (projects_examined == 0):
            logging.info("no repos examined")
    elif (args.action == 'orphaned'):
        repos_yaml.show_orphaned()
    elif (args.action == 'uncommitted'):
        uncommitted_repos = repos_yaml.show_uncommitted()


if __name__ == "__main__":
    # execute only if run as a script
    main()
    sys.exit(0)
