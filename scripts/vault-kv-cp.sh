#!/usr/bin/env bash
set -e

# List all kv path recursively from a given path
# Usage:
#   $0 <path>

export VAULT_ADDR=${VAULT_ADDR:-http://127.0.0.1:8200}

######
# Main
######

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <kv-src-path> <kv-dst-path>"
    echo "You must have the read privilege to <kv-src-path> and write privilege to <kv-dst-path>"
    exit 1
fi

vault kv get -format=json $1  | jq '.data.data' | vault kv put $2 -
