#!/usr/bin/env bash

########################################################
progress () {
    if [ "$VERBOSE" == "1" ]; then
        echo "progress: $1"
    fi
}

exit_with_error () {
    echo "error: $1"
    exit 1
}
########################################################

FILE=$1

# Only process files that have at least one line of the form
#
#    something:  ## explanation blah blah blah ...
#
# "something" must follow variable-naming conventions: start with a letter
# and then be followed by zero more more letters, numbers, hyphens, or underscores.
if [[ -f $FILE ]] && grep -qE '^[a-zA-Z]+[a-zA-Z0-9_-]*:.*?## .*$$' $FILE; then
    BASENAME=$(basename $FILE)

    # If HOME appears in FILE replace with "~".
    simple_path="${FILE/$HOME/\~}"
    echo
    echo '-------------------------------------------------------------------------------'
    printf '\e[1;32m%-6s\e[m' "$BASENAME"
    echo " ($simple_path)"
    echo '-------------------------------------------------------------------------------'
    cat $FILE | grep -E '^[0-9a-zA-Z_-]+:.*?## .*$' \
        | sort \
        | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $1, $2}' \
        || true
fi


exit 0
