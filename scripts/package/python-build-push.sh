#!/usr/bin/env bash

set -e

# This script does both Python package builds and Python package uploads.
# The environment variable PKG_ACTION determines which (build or push). It
# should be run in an environment with Python 3 installed (e.g., a Docker
# container).
#
# Run this script with DEBUG set to any non-empty string to see extra
# diagnostic messages.

# BUILD
#
# For building, this script expects to find the source package files in
# the directory pointed to by SOURCE_DIR. If there are any extra Python
# packages needed for the package build they should be included in the
# file ${SOURCE_DIR}/requirements.txt
#
# Build output is put in the directory pointed to by OUTPUT_DIR.

# PUSH
# For pushes, this script will use the file /root/pkg/credentials/.pypirc
# as the configuration file for uploading.
#
# If /root/pkg/credentials/.pypirc does not exist this script will upload
# using "implicit" credentials to the URL pointed to by PKG_REPO_URL.


# ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ##
progress () {
    if [[ -n "$DEBUG" ]]; then
        msg="$1"
        echo "progress: $msg"
    fi
}
progress_nocr () {
    if [[ -n "$DEBUG" ]]; then
        msg="$1"
        echo -n "progress: $msg"
    fi
}
progress_done () {
    if [[ -n "$DEBUG" ]]; then
        echo "done"
    fi
}

create_python_venv() {
    progress_nocr "creating a Python virtual environment in $PYTHON_VENV_DIR..."
    python3 -m venv --system-site-packages ${PYTHON_VENV_DIR}
    progress_done
}

activate_python_venv() {
    progress_nocr "activating the Python virtual environment..."
    source ${PYTHON_VENV_DIR}/bin/activate
    progress_done
}

add_needed_python_packages() {
    # Install any necessary Python package-building and pushing packages.
    progress_nocr "installing some Python build packages..."
    pip install build twine keyrings.google-artifactregistry-auth
    progress_done

#    ------------------------------------------------------------
#    Why do you need to install the dependecies when BUILDING a package?
#    There is no need! So, don't do this following part anymore.
#
#    # Install the package dependency files requirements.txt (if included)
#    requirements_file="${SOURCE_DIR}/requirements.txt"
#    if [[ -f "${requirements_file}" ]]; then
#        progress_nocr "installing Python packages from ${requirements_file}..."
#        pip install -r "${requirements_file}"
#        progress_done
#    else
#        progress "no Python packages requirements.txt found"
#    fi
#    ------------------------------------------------------------
}

build_python_package() {
    # Build the Python package
    progress "building the Python package (this might take a while)..."
    cd "$SOURCE_DIR"
    python -m build --outdir "$OUTPUT_DIR"
    progress_done

    # Because the Docker container run may be run as root some of the
    # generated files may be created in the local environment with
    # owner "root" making clean-up difficult. So, we change the
    # permissions on some of the files.
    progress_nocr "fixing file permissions..."
    chmod -R a+w "$SOURCE_DIR"/*.egg-info*
    progress_done
}

set_google_application_credentials() {
    # Set the GOOGLE_APPLICATION_CREDENTIALS environment variable from
    # google-creds.json
    google_creds_path="/root/pkg/credentials/google-creds.json"
    if [[ -f "$google_creds_path" ]]; then
        GOOGLE_APPLICATION_CREDENTIALS="$google_creds_path"
        export GOOGLE_APPLICATION_CREDENTIALS
        msg="setting GOOGLE_APPLICATION_CREDENTIALS to $google_creds_path"
        progress "$msg"

        file_info=$(ls -l $google_creds_path)
        msg="GOOGLE_APPLICATION_CREDENTIALS file $google_creds_path information:"
        progress "$msg"
        msg="   $file_info"
        progress "$msg"
    else
        msg="skipping setting GOOGLE_APPLICATION_CREDENTIALS as $google_creds_path does not exist"
        progress "$msg"
    fi
}

push_python_package() {
    if [[ -n "$PKG_REPO_URL" ]]; then
        msg="uploading Python package to $PKG_REPO_URL..."
        progress "$msg"

        # We use .pypirc, if it exists, otherwise use PKG_REPO_URL
        if [[ -f /root/pkg/credentials/.pypirc ]]; then
            msg="using .pypirc"
            progress "$msg"
            python -m twine upload --config-file /root/pkg/credentials/.pypirc \
               --skip-existing --verbose --repository python-repo "$OUTPUT_DIR"/*
        else
            msg="using implicit credentials (probably GOOGLE_APPLICATION_CREDENTIALS)"
            progress "$msg"
            progress "GOOGLE_APPLICATION_CREDENTIALS is $GOOGLE_APPLICATION_CREDENTIALS"
            python -m twine upload \
               --skip-existing --verbose --repository-url "$PKG_REPO_URL" "$OUTPUT_DIR"/*
        fi

        progress_done
    else
        progress "skipping Python package upload (PKG_REPO_URL not defined)"
    fi
}

# ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ##

PYTHON_VENV_DIR=/root/pkg/venv

progress "SOURCE_DIR   is '$SOURCE_DIR'"
progress "OUTPUT_DIR   is '$OUTPUT_DIR'"
progress "PKG_REPO_URL is '$PKG_REPO_URL'"
progress "PKG_ACTION   is '$PKG_ACTION'"

# Step 1. Create a Python virtual environment
create_python_venv

# Step 2. "Activate" the Python virtual environment
activate_python_venv

# Step 3. Install some necessary Python package-building and pusing packages.
add_needed_python_packages

if [[ "$PKG_ACTION" == "build" ]]; then
    build_python_package
elif [[ "$PKG_ACTION" == "push" ]]; then
    set_google_application_credentials
    push_python_package
else
    msg="unknown PKG_ACTION '$PKG_ACTION"
    echo "$msg"
    exit 1
fi

progress "Python package action script finished"
exit 0
