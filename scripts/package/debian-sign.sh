#!/usr/bin/env bash

set -e

DEBUG=XXXX

# ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ##
progress () {
    if [[ -n "$DEBUG" ]]; then
        msg="$1"
        echo "progress: $msg"
    fi
}
# ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ##

progress "entered sign-debian.sh"

# Validation checks.

# Check 1. PGP_SIGNATURE environment variable.
if [[ -z "$PGP_SIGNATURE" ]]; then
    echo "missing required environment variable PGP_SIGNATURE"
    exit 1
fi

# Check 2. GPG passphrase file.
if [[ ! -f "/root/pkg/credentials/passphrase" ]]; then
    echo "missing gpg passphrase file '/root/pkg/credentials/passphrase'"
    exit 1
fi

progress "passed validation checks"

# Step 1. Move the .gnupg into /root.
mv /root/pkg/credentials/.gnupg /root
chown root:root /root/.gnupg
progress "moved .gnupg into /root and set permissions"


# Step 2. Cache gnupg database credentials.
gpg --homedir=/root/.gnupg --pinentry-mode loopback --passphrase-file=/root/pkg/credentials/passphrase \
    --output=/dev/null --clearsign /etc/services
progress "successfully cached gpg credentials"

gpg --homedir=/root/.gnupg --list-secret-keys

# Step 3. Sign the Debian changes file.
cd /root/pkg/output

progress "signing Debian changes file..."
debsign --re-sign -m "$PGP_SIGNATURE" *.changes
progress "...finished signing Debian changes file"
