#!/usr/bin/env bash

set -e

DEBUG=XXXX

# ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ##
progress () {
    if [[ -n "$DEBUG" ]]; then
        msg="$1"
        echo "progress: $msg"
    fi
}
progress_nocr () {
    if [[ -n "$DEBUG" ]]; then
        msg="$1"
        echo -n "progress: $msg"
    fi
}
progress_done () {
    if [[ -n "$DEBUG" ]]; then
        echo "done"
    fi
}
# ### ### ### ### ### ### ### ### ### ### ### ### ### ### ### ##

PYTHON_VENV_DIR=/root/pkg/venv

progress "BUILD_DIRECTORY      is '$BUILD_DIRECTORY'"
progress "OUTPUT_DIRECTORY     is '$OUTPUT_DIRECTORY'"
progress "PYTHON_REGISTRY_PATH is '$PYTHON_REGISTRY_PATH'"

# Step 1. Create a Python virtual environment
progress_nocr "creating a Python virtual environment in $PYTHON_VENV_DIR..."
python3 -m venv --system-site-packages ${PYTHON_VENV_DIR}
progress_done

# Step 2. "Activate" the Python virtual environment
progress_nocr "activating the Python virtual environment..."
source ${PYTHON_VENV_DIR}/bin/activate
progress_done

# Step 3. Install some necessary Python package-building packages.
progress_nocr "installing some Python build packages..."
pip install build twine keyrings.google-artifactregistry-auth
progress_done

# Step 4. Install the package dependency files requirements.txt (if included)
requirements_file="${BUILD_DIRECTORY}/requirements.txt"
if [[ -f "${requirements_file}" ]]; then
    progress_nocr "installing Python packages from ${requirements_file}..."
    pip install -r ${requirements_file}
    progress_done
else
    progress "no Python packages requirements.txt found"
fi

# Step 5. Build the Python package
progress "building the Python package (this might take a while)..."
cd "$BUILD_DIRECTORY"
python -m build --outdir $OUTPUT_DIRECTORY
progress_done

# Step 6. Because the Docker container run may be run as root some of the
# generated files may we be created in the local environment with owner "root"
# making clean-up difficult. So, we change the permissions on some of the files.
progress_nocr "fixing file permissions..."
chmod -R a+w $BUILD_DIRECTORY/*.egg-info*
progress_done

# Step 7.
if [[ -n "$PYTHON_REGISTRY_PATH" ]]; then
    progress_nocr "uploading Python package to $PYTHON_REGISTRY_PATH..."
    python -m twine upload --skip-existing --verbose --repository-url "$PYTHON_REGISTRY_PATH" "$OUTPUT_DIRECTORY"/*
    progress_done
else
    progress "skipping Python package upload (PYTHON_REGISTRY_PATH not defined)"
fi
