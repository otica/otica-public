#!/usr/bin/env bash

# To be used to prepare credentials that will be mounted in a Docker container.

# Each "retrieve_" routine should be run in turn and should gracefully handle missing
# environment variables.
#
# All Vault paths should end in "_SEC_PATH".

set -e

# ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ##
progress () {
    if [[ -n "$DEBUG" ]]; then
        msg="[cred-helper.sh] $1"
        echo "[progress] $msg"
    fi
}

initialize() {
    msg="initializing"
    progress "$msg"

    # Make sure that the credentials directory exists:
    mkdir -p "${BUILD_DIR}/credentials/"
}

retrieve_gpg_keys() {
    msg="retrieving GPG keys"

    if [[ -n "$PKG_CREDS_VAULT_PATH" ]]; then
        progress "$msg"
        "${SCRIPTS_DIR}/vault-read.sh" "${PKG_CREDS_VAULT_PATH}/gnupg.tar" > "${BUILD_DIR}/credentials/gnupg.tar"
        echo 'Retrieving Passphrase'
        "${SCRIPTS_DIR}/vault-read.sh" "${PKG_CREDS_VAULT_PATH}/passphrase" > "${BUILD_DIR}/credentials/passphrase"

        # Untar gnupg.tar
        echo "Untar'ing GPG Keys into .gnupg"
        pushd "${BUILD_DIR}/credentials"
        tar -xf gnupg.tar

        # Set appropriate permissions on the .gnupg directory:
        chmod 700 .gnupg

        popd
    else
        msg="no GnuPG credentials to retrieve"
        progress "$msg"
    fi
}

retrieve_google_application_credentials() {
    msg="retrieving Google application credentials"
    progress "$msg"

    # Get GOOGLE_APPLICATION_CREDENTIALS file from Vault.
    if [[ -n "$GOOGLE_APPLICATION_CREDENTIALS_SEC_PATH" ]]; then
        # Read into a file.
        pushd "${BUILD_DIR}/credentials"
        echo "reading VAULT secret at '$GOOGLE_APPLICATION_CREDENTIALS_SEC_PATH'"
        vault-read.sh "$GOOGLE_APPLICATION_CREDENTIALS_SEC_PATH" > google-creds.json
        msg="Google application credentials retrieved"
        progress "$msg"

        popd
    else
        msg="no Google application credentials to retrieve"
        progress "$msg"
    fi
}

retrieve_pypi_api_token() {
    # This function credentials used to access a PyPi-type Python package repository.
    # The credentials are written to the file BUILD_DIR/credentials/.pypirc
    #
    # The format of the file .pypirc will look like:
    # [distutils]
    # index-servers =
    #     python-repo
    #
    # [python-repo]
    # repository = $PKG_REPO_URL
    # username = $PKG_REPO_USERNAME
    # password = $PKG_REPO_PASSWORD
    #
    # This function has been tested with:
    #  * PyPi (production PyPi at https://upload.pypi.org/legacy/
    #  * Test PyPi: https://test.pypi.org/legacy/
    #  * Gitlab package repository
    #
    msg="retrieving PyPi repository credentials"
    progress "$msg"

    # Get PKG_REPO_PASSWORD_SEC_PATH secret from Vault.
    if [[ -n "$PKG_REPO_PASSWORD_SEC_PATH" ]]; then
        pushd "${BUILD_DIR}/credentials"

        # Read into a file.
        password_file="pkg-repo-password"

        echo "reading VAULT secret at '$PKG_REPO_PASSWORD_SEC_PATH'"
        vault-read.sh "$PKG_REPO_PASSWORD_SEC_PATH" > "$password_file"
        msg="PyPi repository credentials retrieved (written to $password_file)"
        progress "$msg"

        # Read the first line of the file just created into the variable
        # PKG_REPO_PASSWORD.
        while read -r PKG_REPO_PASSWORD; do
            break
        done < "$password_file"
        msg="PyPi token stored in PKG_REPO_PASSWORD"
        progress "$msg"

        # Delete the password file (no longer needed).
        rm -f "$password_file"
        msg="removed file '$password_file'"
        progress "$msg"

        pypirc_string=$(cat <<EOF
[distutils]
index-servers =
    python-repo

[python-repo]
repository = $PKG_REPO_URL
username = $PKG_REPO_USERNAME
password = $PKG_REPO_PASSWORD
EOF
)
        echo "$pypirc_string" > .pypirc
        msg=".pypirc configuration file written"
        progress "$msg"

        popd
    else
        msg="no PyPi repository credentials to retrieve"
        progress "$msg"
    fi
}

# ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ## # ##

initialize

retrieve_gpg_keys
retrieve_google_application_credentials
retrieve_pypi_api_token
