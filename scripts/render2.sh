#!/usr/bin/env bash

###################################################################################
# A filter that renders a template, usage: cat <file> | render2.sh
#
# The following are the supported filters. Note that most of the filters
# do an envsubst BEFORE running through the other filter script.
#
# Be aware that the output (except in the "NOTHING" case) REMOVES the first
# line containing the '#!'.
#
# -----------
#
# #!gomplate
# Run through through the gomplate filter. Note that this filter does not run through
# envsubst.
#
# #!gomplate-vault
# Run through through the gomplate filter and insert
# vault secrets when using the "vault-kv" gomplate template.
#
# #!vault2kube
# Run through envsubst then through the vault2kube.sh filter. Used when inserting
# vault secrets in a kubernetes secrets template file.
#
# #!vault2text
# #!vault2chart
# Run through envsubst then through the vault2text.sh filter.
#
# #!vault2properties
# Run through envsubst then the vault2properties.sh filter.
#
# #!envsubst
# Run through the envsubst filter.
#
# NOTHING
# If there is NO first line with a '#!', or the '#!' is not one of the above,
# then the output from this script is the contents of the input unchanged. Note
# that this is a CHANGE from the original render.sh

# Based on the PS Cloud render.sh script.
####################################################################################

# Behavior settings:
NO_ENVSUBST=false

# We use our own progress to ensure that progress messages do not get
# written to standard output (remember that this script sends its results
# to standard output). Instead, we write them to standard error.
progress_stderr() {
    local msg
    local prefix
    msg=$1
    prefix=${2:-progress }
    if [[ -n "${DEBUG:-}" ]]; then
        echo "$prefix(render2.sh): $msg" 1>&2
    fi
}

progress_stderr "starting render2.sh"

####################################################################################
# This section stores in the variable gomplate_vault_kv_func the gomplate
# needed to replace Vault secrets in gomplate templates using the "vault-kv" template
# definition.
#
# Example.
#
# #!gomplate-vault
# my secret base64 encoded "{{ tmpl.Exec  "vault-kv"  "secret/projects/mygroup/app/secret.key"}}"
#
# Assuming that the Vault secret at "secret/projects/mygroup/app/secret.key" contains the string
# "very-secret", running the above file through this script will result in
#
# my secret base64 encoded "very-secret"

read -r -d '' gomplate_vault_kv_func <<'EOF'
{{- define "vault-kv" }}
{{- $response := (datasource "vault" (strings.ReplaceAll "secret/" "secret/data/" . )).data }}
{{- if eq $response.format "base64" }}{{ base64.Decode $response.value  }}
{{- else if eq $response.format "text" }}
{{- $response.value }}{{ end }}{{ end -}}
EOF
####################################################################################

THIS_DIR=$(dirname "$0")
source "$THIS_DIR"/functions.sh

for i in "$@"; do
  case $i in
    -v|--verbose)
      DEBUG=true
      shift # past argument with no value
      ;;
    --no-envsubst)
      NO_ENVSUBST=true
      progress_stderr "--no-envsubst detected; will suppress all environment variable substitution"
      shift # past argument with no value
      ;;
    --*|-*)
      echo "Unknown option $i"
      exit 1
      ;;
    *)
      ;;
  esac
done

### End of parse command line options

####################################################################################

# Are we running in "legacy mode"? Legacy mode means that if there is no
# recognized hash-bang first line we run the contents through envsubst just like the
# old render.sh.
if [[ -z "${RENDER_LEGACY_MODE+x}" ]]; then
    progress_stderr "not running in legacy mode"
    default_filter='cat'
elif [[ "${RENDER_LEGACY_MODE,,}" == "yes" ]]; then
    progress_stderr "running in legacy mode"
    if [[ "$NO_ENVSUBST" = true ]]; then
        exit_with_error "you cannot use the --no-envsubst option in legacy mode"
    else
        default_filter='envsubst'
    fi
else
    progress_stderr "not running legacy mode"
    default_filter='cat'
fi

####################################################################################

IFS= read -r first_line

envsubst='envsubst'

# If we have disabled envsubst set $envsubst to 'cat'.
if [[ "$NO_ENVSUBST" = true ]]; then
    envsubst='cat'
    progress_stderr "setting 'envsubst' equal to 'cat' to disable envsubst'ing"
fi

case "$first_line" in
        *\#!envsubst*   )
            cat - | $envsubst
            ;;
        *\#!vault2kube* )
            cat - | $envsubst | vault2kube.sh
            ;;
        *\#!vault2text*  | *\#!vault2chart* )
            cat - | $envsubst | vault2text.sh
            ;;
        *\#!vault2properties* )
            cat - | $envsubst | vault2properties.sh
            ;;
        *\#!gomplate-vault*   )
            ( echo "$gomplate_vault_kv_func"; cat - ) | gomplate -d vault="vault://" "$@"
            ;;
        *\#!gomplate*   )
            cat - | gomplate "$@"
            ;;
        *\#!render-jinja2*   )
            cat - | render-jinja2 "$@"
            ;;
        *               )
            # The original render.sh would handle this default case by
            # running the file through envsubst. This script is different
            # in that if there is no (or an unrecognized) '#!' on the
            # first line it outputs the file unchanged. The advantage of
            # this is it allows the running through this script of a large
            # set of files and _only_ affect those files with a recognized
            # '#!' header.
            progress_stderr "no recognized hash-bang as first line"
            progress_stderr "using default target with \$default_filter of '$default_filter'"
            (echo "$first_line"; cat - ) | $default_filter
            ;;
esac

progress_stderr "exiting render2.sh"

exit 0

