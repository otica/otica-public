#!/bin/sh
# Check whether the cert expires in the next days
#
function usage() {
    echo "Check whether the cert expires in the next days, exit 0 if so, 1 if not"
    echo "Usage: $0 <site addr>:<port> <days>"
}

function expire_in {
    declare -i seconds=60*60*24*$2
    ! echo \
    | openssl s_client -connect $1 2>/dev/null \
    | openssl x509 -noout -checkend $seconds
}

if [ $# -eq 2 ] ; then
    expire_in $1 $2
else
    usage
fi

