#!/usr/bin/env python

# Extract the version from a pyproject.toml file

import sys


def exit_with_error(msg: str) -> None:
    print(f"error: {msg}")
    sys.exit(1)

# This requires the toml module.
try:
    import toml
except Exception as excpt:
    exit_with_error("you must install the Python package 'toml' first")

# Get the .toml file path from ARG.
try:
    toml_file = sys.argv[1]
except Exception as excpt:
    exit_with_error("missing TOML file argument")

# Slurp the TOML file into toml_string.
with open(toml_file, 'r') as file1:
    toml_string = file1.read()

# Parse the TOML string.
parsed_toml = toml.loads(toml_string)

# Extract project|version.
if ('project' not in parsed_toml):
    msg = f"TOML file {toml_file} does not contain a 'project' section"
    exit_with_error(msg)

project = parsed_toml['project']

if ('version' not in project):
    msg = f"TOML file {toml_file} does not contain a project/version attribute"
    exit_with_error(msg)

version = project['version']

# Now that we have version simply print it and exit.
print(version)
sys.exit(0)
