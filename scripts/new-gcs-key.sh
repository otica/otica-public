#!/usr/bin/env bash
set -e

# Generate a customer supplied GCS encryption key and saved it to the gaven vault path

THIS_DIR=$(dirname "$0")

usage() {
	echo "Generate a customer supplied GCS encryption key and saved it to the gaven vault path"
	echo "Usage: $0 <vault_path_of_key>"
	echo "You must have the write permistion to <vault_path_of_key>"
	exit 1
}

vpath=$1
if [ -z "$vpath" ]; then
	usage
fi

${THIS_DIR}/vault-login.sh

if vault kv get "$vpath" &> /dev/null; then
	echo Existing key at $vpath.
	exit 0
fi

key=$(head -c 32 /dev/urandom | base64 | tr -d '\n\r')
${THIS_DIR}/vault-write.sh "$vpath" "$key"

echo "New key is saved to $vpath"
