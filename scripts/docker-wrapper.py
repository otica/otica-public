#!/usr/bin/env python3

# pylint: disable=invalid-name
# pylint: disable=superfluous-parens

"""
A Python3 script that wraps the "docker" executable. This wrapper script
takes into account the DOCKER_* environment variables used by the
docker-develop.mk makefile module.

The bulk of the code is in the docker_wrapper.py library in the lib/
directory.
"""

import argparse
import logging
import pathlib
import os
import sys

# Add './lib' to search path. We need to do this
# so that we can import helm_wrapper
MY_DIRECTORY = pathlib.Path(__file__).parent.resolve()
sys.path.append(os.path.join(MY_DIRECTORY, 'lib'))

from docker_wrapper import DockerWrapper
from otica_abstract import OticaAbstract

def main():
    """This function is called when this script is run on the command-line."""

    parser = argparse.ArgumentParser(description='Process otica.yaml file.')
    parser.add_argument('--verbose', '-v', action="store_true")
    parser.add_argument('--dryrun', action="store_true")
    parser.add_argument('--no-cache', action="store_true")
    parser.add_argument('action', nargs=1,
                        choices=['show-env', 'check-env', 'build',
                                 'tag-and-push', 'gen-build-script'],
                        help='create a new project/subproject or update an existing one')


    args = parser.parse_args()

    # Get the value of the VERBOSE environment variable (if defined).
    # If "truthy" set args.verbose to true.
    verbose_env_value = False
    if (('VERBOSE' in os.environ) and (os.environ['VERBOSE'])):
        verbose_env_value = os.environ['VERBOSE']
        verbose_env_value = OticaAbstract.normalize_boolean(verbose_env_value)

    if (verbose_env_value):
        args.verbose = True

    if (args.verbose):
        logging.basicConfig(level=logging.DEBUG, format='%(message)s',)
    else:
        logging.basicConfig(level=logging.INFO, format='%(message)s',)

    action = args.action[0]

    dwrap = DockerWrapper(
        verbose=args.verbose,
        no_cache=args.no_cache,
        dryrun=args.dryrun,
    )
    dwrap.progress(f"action is '{action}'")

    if (action == 'show-env'):
        dwrap.show_env()
    elif (action == 'check-env'):
        dwrap.check_env()
    elif (action == 'build'):
        dwrap.build()
    elif (action == 'gen-build-script'):
        # Write the Kaniko build command to the file "kaniko-build.cmd" in the
        # DOCKER_BUILD_DIR.
        dwrap.write_kaniko_build_script()
    elif (action == 'tag-and-push'):
        dwrap.tag_and_push()
    else:
        print(f"unknown action '{action}'")
        sys.exit(1)

    dwrap.progress('docker-wrap finished')

if (__name__ == "__main__"):
    main()
