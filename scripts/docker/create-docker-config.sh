#!/usr/bin/env bash
set -e

# Create docker config for docker registry auth
# Usage:
#     $0 registry_def_file [<output_file>]
#
# The format of registry_def_file is one line for each docker registry:
#   <registry_host> <registry_username> <regitry_passward>
#
#  The format of <regitry_passward>:
#           file://<path>                               get the value from the file
#           vault://<path>                              get the value from vault path
#           render://<template_to_be_rendered>          get the value by render the give template file   
#           string_value
#
# NOTE: vault must already logged in!

THIS_DIR=$(dirname "$0")

function usage() {
    echo "Usage $0 <registry_def_file>"
    echo "The format of registry_def_file is one line for each docker registry:"
    echo "  <registry_host> <registry_username> <regitry_passward>"
    echo "The format of <regitry_passward>:"
    echo "        file://<path>                         get the value from the file"
    echo "        vault://<path>                        get the value from vault path"
    echo "        render://<template_to_be_rendered>    get the value by render the give template file"  
    echo "        string_value"
} 

function get_value(){
    local value=$1
    if [[ $value == file://* ]]; then 
        # get the value from the file, 
        # NOTE make sure the file has no unwanted trailing '\n', e.g. added by vim
        echo -n "@${value#file://}"
    elif [[ $value == vault://* ]]; then
        # get the value from vault path
        vault-read.sh ${value#vault://}
    elif [[ $value == render://* ]]; then
        # render the give template file as the value
        cat ${value#render://} | render.sh
    else
        echo -n $value
    fi
}

function gen_registry() {
    reg=$1
    user=$2
    pass=$(get_value $3)
    auth=$(echo "${user}:${pass}" | base64 | tr -d '\n')

    cat <<EOF
        "$reg": {
            "auth": "${auth}"
        },
EOF
}

function add_all() {
    grep -v '^#' $1 | grep -v -e '^$' | 
    while read -r line; do
        gen_registry $line
    done | 
    # remove last ',' from last json array element
    # https://stackoverflow.com/questions/33680111/how-can-i-remove-the-last-character-of-the-last-line-of-a-file
    sed '$ s/.$//'
}


if [ ! -f "$1" ]; then
    usage;
    exit 1
fi

if [ -z "$2" ]; then
    output=/dev/stdout
else
    output=$2
fi

cat >$output <<EOF
{
    "auths": { 
        $(add_all $1) 
    }
}
EOF
