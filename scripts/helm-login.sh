#!/usr/bin/env bash

# Login to the Helm repository HELM_REPO_URL

set -e

THIS_DIR=$(dirname "$0")
source "$THIS_DIR"/functions.sh

############################################################
missing_required_variable () {
    exit_with_error "missing value for required variable $1"
}

login_via_gloud () {
    # Step 1. Extract the hostname from HELM_REPO_URL

    # If HELM_REPO_URL is an oci:// registry extract the hostname portion.
    # and set HELM_REPO_URL_HOSTNAME to that hostname
    if [[ $HELM_REPO_URL =~ ^oci:// ]]; then
        progress "HELM_REPO_URL is on OCI-based registry"
        HELM_REPO_URL_NO_SCHEME="${HELM_REPO_URL#*//}"
        progress "HELM_REPO_URL_NO_SCHEME is '$HELM_REPO_URL_NO_SCHEME'"
        HELM_REPO_HOST="${HELM_REPO_URL_NO_SCHEME%%/*}"
        progress "HELM_REPO_HOST is '$HELM_REPO_HOST'"

    else
        progress "$HELM_REPO_URL is not an OCI-based registry"
        HELM_REPO_HOST="${HELM_REPO_URL}"
    fi

    progress "logging into Helm registry using gcloud command-line"
    gcloud auth configure-docker "$HELM_REPO_HOST"
    progress "successfully logged into $HELM_REPO_HOST"
}
############################################################

progress "running 'auth configure-docker $HELM_REPO_URL'"

if [[ -z "$HELM_REPO_URL" ]]; then
    missing_required_variable "HELM_REPO_URL"
else
    progress "HELM_REPO_URL: $HELM_REPO_URL"
fi

if [[ -z "$HELM_REGISTRY_LOGIN_TYPE" ]]; then
    missing_required_variable "HELM_REGISTRY_LOGIN_TYPE"
else
    progress "HELM_REGISTRY_LOGIN_TYPE: $HELM_REGISTRY_LOGIN_TYPE"
fi

# All required variables presented and accounted for.

if [[ "$HELM_REGISTRY_LOGIN_TYPE" == "gcloud" ]]; then
    login_via_gloud
else
    msg="'$HELM_REGISTRY_LOGIN_TYPE' is not a recognized login type"
    exit_with_error "$msg"
fi

progress "exiting helm-login.sh script"

exit 0
