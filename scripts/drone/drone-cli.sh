#!/usr/bin/env bash
set -e

###############################################################################
# wrapper for drone CLI
###############################################################################

THIS_DIR=$(dirname "$0")

# include functions
source $THIS_DIR/functions.sh

# fail on error or undeclared vars
trap_errors

export DRONE_CLI_VERSION=${DRONE_CLI_VERSION:-1.2}
export DRONE_CLI=${DRONE_CLI:-drone}

if ! ${DRONE_CLI} --version | grep "${DRONE_CLI_VERSION}" > /dev/null ; then
    echo "ERROR: ${DRONE_CLI} ${DRONE_CLI_VERSION} is required!"
    exit 1
fi

if [ -z "${DRONE_TOKEN}" ]; then 
  if [ -f ${DRONE_TOKEN_FILE} ]; then
    export DRONE_TOKEN=$(cat ${DRONE_TOKEN_FILE});
  else
    >&2 echo "${DRONE_TOKEN_FILE} doesn't exist"
    exit 1
  fi
fi

if empty_var DRONE_REPO; then
    echo "ERROR: DRONE_REPO is not defined"
    exit 1
fi

