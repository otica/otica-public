#!/usr/bin/env bash
set -e

# remvoe a drone registry secret
# Usage:
#     $0
#
# Drone Ref.: https://docs.drone.io/pipeline/kubernetes/syntax/images/#pulling-private-images

THIS_DIR=$(dirname "$0")
DRONE_REG_SEC=dockerconfigjson

${THIS_DIR}/drone-rm-sec.sh ${DRONE_REG_SEC}
