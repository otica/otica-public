#!/usr/bin/env bash

###############################################################################
# remove a repo from drone
###############################################################################

THIS_DIR=$(dirname "$0")
# include functions
source ${THIS_DIR}/drone-cli.sh # export ${DRONE_CLI}


if ${DRONE_CLI} repo info ${DRONE_REPO} &> /dev/null; then
	read -p "Remove repo from drone will DESTROY BUILD HISTORY, are you sure? [Y/n] " ; \
  if [ "$REPLY" = 'Y' ]; then
		echo "${DRONE_CLI} repo rm ${DRONE_REPO}"
		${DRONE_CLI} repo rm ${DRONE_REPO}
	else
		echo "canceled remove repo action"
	fi
fi
