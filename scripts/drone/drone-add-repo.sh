#!/usr/bin/env bash

###############################################################################
# DEPRECATED: only used in drone.mk, which used by:
# - docker-vault-init
# - kube-vault-ui
# - provision-keycloak
# - provision-vault
# add a repo to drone
###############################################################################

THIS_DIR=$(dirname "$0")
# include functions
source ${THIS_DIR}/drone-cli.sh # export ${DRONE_CLI}

if ! ${DRONE_CLI} repo info ${DRONE_REPO} > /dev/null 2>&1; then
  ${DRONE_CLI} repo add ${DRONE_REPO}
fi
