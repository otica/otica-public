#!/usr/bin/env bash
set -e

# remvoe a drone sec
# Usage:
#     $0 <key>
#
# NOTE: vault must already logged in!

THIS_DIR=$(dirname "$0")
# include functions
source ${THIS_DIR}/drone-cli.sh # export ${DRONE_CLI}

key=$1

# remove the sec
if ${DRONE_CLI} secret ls --repository ${DRONE_REPO} | grep $key &> /dev/null ; then
    ${DRONE_CLI} secret rm  --repository ${DRONE_REPO} --name $key
    echo "Delete drone secret ${key}"
fi
