#!/usr/bin/env bash
set -e

# Getting sec value from vault and adding drone sec
# Usage:
#     $0 <key> <value> [command options]
#     value format
#           file://<path>                               get the value from the file
#           base64file://<path>                         get the base64 encoded value from the file
#           vault://<path>                              get the value from vault path
#           base64vault://<path>                        get the base64 encoded value value from vault path
#           render://<template_to_be_rendered>          get the value by render the give template file
#           base64render://<template_to_be_rendered>    get the base64 encoded value by render the give template file       
#           string_value
#
# NOTE: vault must already logged in!

THIS_DIR=$(dirname "$0")
# include functions
source ${THIS_DIR}/drone-cli.sh # export ${DRONE_CLI}

function is_cli_v1() {
    ${DRONE_CLI} --version | grep "1.2" &> /dev/null
    return $?
}

function add_sec() {
    key=$1
    value=$2
    shift
    shift
    options=$*
    # Hack to make it work for both drone v0.8 and v1.2
    if is_cli_v1; then
        ${DRONE_CLI} secret add --repository ${DRONE_REPO} --name "$key" --data "$value"  $options
    else
        ${DRONE_CLI} secret add --repository ${DRONE_REPO} --name "$key" --value "$value" $options
    fi
}

function update_sec() {
    key=$1
    value=$2
    shift
    shift
    options=$*
    # Hack to make it work for both drone v0.8 and v1.2
    if is_cli_v1; then
        ${DRONE_CLI} secret update --repository ${DRONE_REPO} --name "$key" --data "$value"  $options
    else
        ${DRONE_CLI} secret update --repository ${DRONE_REPO} --name "$key" --value "$value" $options
    fi
}

function get_value(){
    local value=$1
    if [[ $value == file://* ]]; then 
        # get the value from the file, 
        # NOTE make sure the file has no unwanted trailing '\n', e.g. added by vim
        echo -n "@${value#file://}"
    elif [[ $value == base64file://* ]]; then
        # get the value from the file and base64 encode it
        cat ${value#base64file://} | base64 | tr -d '\n'
    elif [[ $value == vault://* ]]; then
        # get the value from vault path
        vault-read.sh ${value#vault://}
    elif [[ $value == base64vault://* ]]; then
        # get the value from vault path and base64 encode it
        vault-read.sh ${value#base64vault://} | base64 | tr -d '\n'
    elif [[ $value == render://* ]]; then
        # render the give template file as the value
        cat ${value#render://} | render.sh
    elif [[ $value == base64render://* ]]; then
        # render the give template file as the value and base64 encoded
        cat "${value#base64render://}" | render.sh | base64 | tr -d '\n'
    else
        echo -n $value
    fi
}

key=$1
sec_path=$2
value=$(get_value $sec_path)
shift
shift
options=$*

if [[ -z "$value" ]]; then
    echo ERROR missing value for $key
    exit 1
fi

# update/add the sec before add
if ${DRONE_CLI} secret ls  --repository ${DRONE_REPO}  | grep "$key" &> /dev/null ; then
    update_sec "$key" "$value"  $options
    echo "Update drone secret $key with value from \"$sec_path\""
else
    add_sec "$key" "$value"  $options
    echo "Add drone secret $key with value from \"$sec_path\""
fi
