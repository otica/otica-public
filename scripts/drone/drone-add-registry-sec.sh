#!/usr/bin/env bash
set -e

# Getting registroy password value from vault and adding the docker registry to drone
# Usage:
#     $0 registry_def_file
# NOTE: vault must already logged in!
#
# The format of registry_def_file is one line for each docker registry:
#   <registry_host> <registry_username> <regitry_passward>
#
#  The format of <regitry_passward>:
#           file://<path>                               get the value from the file
#           vault://<path>                              get the value from vault path
#           render://<template_to_be_rendered>          get the value by render the give template file   
#           string_value
#
# See Also: ${THIS_DIR}/create-docker-config.sh
# Drone Ref.: https://docs.drone.io/pipeline/kubernetes/syntax/images/#pulling-private-images


THIS_DIR=$(dirname "$0")
DRONE_REG_SEC=dockerconfigjson

# include functions
source ${THIS_DIR}/drone-cli.sh # export ${DRONE_CLI}

if [ -f "$1" ]; then
    registry_def_file=$1
    dockerconfig=$(mktemp)
    ${THIS_DIR}/create-docker-config.sh ${registry_def_file} ${dockerconfig}

    # update or add the sec before add
    if ${DRONE_CLI} secret ls  --repository ${DRONE_REPO}  | grep ${DRONE_REG_SEC} &> /dev/null ; then
        ${DRONE_CLI} secret update --repository ${DRONE_REPO} --name ${DRONE_REG_SEC} --data @${dockerconfig}
        echo "Update drone secret ${DRONE_REG_SEC} with value from file $1"
    else
        ${DRONE_CLI} secret add --repository ${DRONE_REPO} --name ${DRONE_REG_SEC} --data @${dockerconfig}
        echo "Add drone secret ${DRONE_REG_SEC} with value from file $1"
    fi
    rm -f ${dockerconfig}
fi

