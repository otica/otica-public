# Drone Secrets Creation

## Examples of latest pattern
* `my-services/docker-apache` repo

## Changes w/ `drone08-gomplate.mk`

A number of changes have been introduced by `makefile_parts/drone08-gomplate.mk`. The following sections go over these changes:

### Templating Engine & File Format
* `envsubst` & custom vault retrieval code has been replaced with gomplate
* a gomplate-templated YAML file is used for secrets/registries instead of .def files

### File layout

The old layout was like this for each repo:
```
project-repo/
  .drone.sec (secrets)
  .drone.reg (registries)
```

The new layout is like this:
```
project-repo/
  .drone.sec.yml (secrets, optional)
  .drone.reg.yml (extra registries)
gcloud-scripts/
  gomplate-template/
    drone-registry-common.yaml (common registries)
    drone-sec-common.yaml (common secrets)
```

### `make drone-add-sec` calls `drone-add-all-sec-gomplate.sh`
`drone-add-all-sec-gomplate.sh` does these things:
- adds secrets to drone from `drone-sec-common.yaml`
    - both this file and `drone-sec-common.yaml` allow special syntax for fetching Approle role-id and generating secret-id that can be used to pull secrets from Vault in drone job:

    ```
    - name: VAULT_ROLE_ID
        value: {{ template "vault-drone-role-id" .Env.DRONE_APPROLE }}
    - name: VAULT_SECRET_ID
      secretIdValue:
        approle: {{ .Env.DRONE_APPROLE }}
        client: {{ .Env.DRONE_REPO }}
    ```

    - `secretIdValue` generates a new secret ID when `make drone-add-sec` is called, if there is not an existing secret ID for `approle` in vault with metadata indicating this is for `client`, or if this secret ID is not in drone
      - this is to ensure that the unique secret ID for this repo exists vault, and that Drone has it
- adds secrets to drone from `drone-sec-common.yaml`

**Example output:**
```
$ make drone-add-sec

Adding secrets from 'drone-sec-common.yaml' to drone:
- adding secret 'VAULT_APPROLE_LOGIN_TOKEN' to drone
- adding secret 'VAULT_ROLE_ID' to drone
- destroying past secret id for client 'irt-dcs/docker-apache'
- generating new secret id for approle 'my-services-viewer' and client 'irt-dcs/docker-apache'
- adding secret 'VAULT_SECRET_ID' drone
- adding secret 'slack_webhook' to drone

Adding secrets from '.drone.sec.yml' to drone:
- adding secret 'docker_username' to drone
- adding secret 'docker_password' to drone
```

### `make drone-rm-sec` calls `drone-rm-all-sec-gomplate.sh`
`drone-rm-all-sec-gomplate.sh` does these things:
- removes secrets defined in `drone-sec-common.yaml` from drone
- removes secrets defined in `.drone.sec.yml` from drone

```
$ make drone-rm-sec

You are logged in VAULT and have permission to read from secret/projects/my-services/*
Removing secrets from 'drone-sec-common.yaml' from drone
- removing secret 'VAULT_APPROLE_LOGIN_TOKEN' from drone
- removing secret 'VAULT_ROLE_ID' from drone
- removing secret 'VAULT_SECRET_ID' from drone
- removing secret 'slack_webhook' from drone
Removing secrets from '.drone.sec.yml' from drone
- removing secret 'docker_username' from drone
- removing secret 'docker_password' from drone
```

### `make drone-add-registry` calls `drone-add-registry-gomplate.sh`
`drone-add-registry-gomplate.sh` does these things:
- adds registries defined in `drone-registry-common.yaml` to drone
- adds registries defined in `.drone.reg.yml` to drone
### `make drone-rm-registry` calls `drone-rm-registry-gomplate.sh`
`drone-rm-registry-gomplate.sh` does these things:
- removes registries defined in `drone-registry-common.yaml` from drone
- removes registries defined in `.drone.reg.yml` from drone

