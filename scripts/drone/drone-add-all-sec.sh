#!/usr/bin/env bash
set -e

# Add all drone secrets defined in a file, $1
# Each line in the file defines one drone secret, the line format is:
#   <sec_name> <sec_value> <sec_options> ....
#
#   <sec_value> may be one of:
#           vault://<path>
#           file://<path>
#           @filename
#           string_value
#
#   <sec_option> may be one of:
#       --allow-pull-request    # permit read access to pull requests
#       --allow-push-on-pull-request  # permit write access to pull requests (e.g. allow docker push)
#

THIS_DIR=$(dirname "$0")

function get_key() {
    echo $1
}
function get_sec() {
    echo $2
}
function get_options() {
    shift
    shift
    echo $*
}

function add_all() {
    grep -v '^#' $1 | grep -v -e '^$' | render.sh |
    while read -r line; do
        key=$(get_key $line)
        sec=$(get_sec $line)
        [[ -z "$key" ]] || [[ -z "$sec" ]] && continue
        $THIS_DIR/drone-add-sec.sh $key $sec $(get_options $line)
    done
}

add_all $*
