#!/usr/bin/env bash
set -e

# remvoe a drone registry
# Usage:
#     $0 registry_def_file
#
# The format of registry_def_file is one line for each docker registry:
#   <registry_host> <registry_username> <regitry_passward>
#

THIS_DIR=$(dirname "$0")
# include functions
source ${THIS_DIR}/drone-cli.sh # export ${DRONE_CLI}

function rm_registry() {
    # remove the registry
    if ${DRONE_CLI} registry info  --repository ${DRONE_REPO} --name $1 > /dev/null ; then
        ${DRONE_CLI} registry rm  --repository ${DRONE_REPO} --name $1
    fi
}

grep -v '^#' $1 | grep -v -e '^$' |
while read -r line; do
    rm_registroy $line
done
