#!/usr/bin/env bash
set -e

# Remove all drone secrets defined in a file, $1
#

THIS_DIR=$(dirname "$0")

function key() {
    echo $1
}

grep -v '^#' $1 | grep -v -e '^$' |
while read -r line; do
    # get key
    key=$(key $line)
    [[ -z "$key" ]]  && continue
    ${THIS_DIR}/drone-rm-sec.sh $key
done
