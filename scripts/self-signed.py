#!/usr/bin/env python3

"""
Command-line interface to the self_signed Python 3 class.
"""

import argparse
import pathlib
import os
import sys

# Add './lib' to search path. We need to do this
# so that we can import self_signed.
MY_DIRECTORY = pathlib.Path(__file__).parent.resolve()
sys.path.append(os.path.join(MY_DIRECTORY, 'lib'))

from self_signed import SelfSignedWrapper


def main():
    """This function is called when this script is run on the command-line."""

    parser = argparse.ArgumentParser(description='Create a self-signed key-pair')
    parser.add_argument('--verbose', '-v', action="store_true")
    parser.add_argument('--dryrun', action="store_true")
    parser.add_argument('action', nargs=1,
                        choices=[
                            'show-env',
                            'gen-keypair',
                            'clean',
                            'write-vault',
                            'write-vault-force',
                                ],
                        help='action to take')

    args = parser.parse_args()

    self_signed = SelfSignedWrapper(
        verbose=args.verbose,
    )

    action = args.action[0]
    self_signed.progress(f"action is '{action}'")

    if (action == 'show-env'):
        self_signed.show_env()
    elif (action == 'gen-keypair'):
        self_signed.generate_keypair()
    elif (action == 'clean'):
        self_signed.cleanup_keypair()
    elif (action == 'write-vault'):
        self_signed.write_vault(force=False)
    elif (action == 'write-vault-force'):
        self_signed.write_vault(force=True)
    else:
        raise Exception(f"action is '{action}' (how did I get here?!?)")

if (__name__ == "__main__"):
    main()

