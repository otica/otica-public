#!/usr/bin/env bash

set -e

###############################################################################
# Show Kubernetes External DNS environment variables.
###############################################################################

THIS_DIR=$(dirname "$0")
source "$THIS_DIR/functions.sh"

progress "entering $(basename $0)"

###############################################################################
echo_result () {
    if empty_var "$1"; then
        msg="<EMPTY>"
    else
        msg="${!1}"
    fi

    printf '%-35s%s\n' "$1:" "$msg"
}

# Ensure that the values.yaml file in
# helm-charts/external-dns/EXTERNAL_DNS_VERSION exists.
check_for_yaml_file () {
    yaml_file="${FRAMEWORK_DIR}/helm-charts/external-dns/${EXTERNAL_DNS_VERSION}/values.tmpl"
    if [[ ! -f "$yaml_file" ]]; then
        error_msg="missing required values.tmpl file '$yaml_file';"
        error_msg="${error_msg} contact exdns maintainers to support this Helm release"
        exit_with_error "$error_msg"
    fi
}
###############################################################################

# See if KUBE_PLATFORM is supported.
regex='^gke$'
if [[ ! "$KUBE_PLATFORM" =~ $regex ]]; then
    echo "ERROR: KUBE_PLATFORM '$KUBE_PLATFORM' is not yet supported"
    exit 1
fi

check_for_yaml_file

exdns_variables=(CLOUD_PLATFORM KUBE_PLATFORM KUBE_CLUSTER_NAME
                 EXTERNAL_DNS_VERSION APP_NAMESPACE
                 EXTERNAL_DNS_KUBE_NAMESPACE \
                 EXTERNAL_DNS_GOOGLE_PROJECT EXTERNAL_DNS_GCP_CREDENTIALS_PATH \
	             EXTERNAL_DNS_EVENTS
                 EXTERNAL_DNS_INTERVAL
                 HELM_CHART_NAME
                 HELM_RELEASE
                 HELM_REPO_URL
                 HELM_REPO_NAME
                 HELM_YAML_FILES)

if [[ "$VERBOSE" == "1" ]]; then
    for variable in "${exdns_variables[@]}"
    do
        echo_result "$variable"
    done
fi

exit 0
