#!/usr/bin/env bash

set -e

CURRENT_CONTEXT=$(kubectl config -o=json view | jq '."current-context"')
kubectl config -o=json view | jq -r ".contexts[]|select(.name==$CURRENT_CONTEXT).context.namespace"

exit 0
