#!/usr/bin/env bash

# Run a cron job right now.

# Needed: An _existing_ CronJob name (if the CronJob does not exist
# the kubectl command at the bottom of this scritp will fail).

CRONJOB_NAME=$1

if [[ -z "$CRONJOB_NAME" ]]; then
    echo "missing CronJob name"
    exit 1
fi

MANUAL_JOB_NAME="${CRONJOB_NAME}-manual"

# Delete the old job if it exists
result=$(kubectl get --ignore-not-found "job/${MANUAL_JOB_NAME}")

if [[ ! -z $result ]]; then
    kubectl delete "job/${MANUAL_JOB_NAME}"
fi

# Deploy job
kubectl create job --from=cronjob/${CRONJOB_NAME} $MANUAL_JOB_NAME
