#!/usr/bin/env bash
set -e

###############################################################################
# get kubernetes cluster credentials from gcloud and verify
###############################################################################

THIS_DIR=$(dirname "$0")
source "$THIS_DIR/functions.sh"

#export DEBUG="YES"
progress "entering config-kube.sh"

THIS_DIR=$(dirname "$0")
GKE_CLUSTER_NAME=${GKE_CLUSTER_NAME:-$GCP_CLUSTER_NAME}
progress "cluster name: $GKE_CLUSTER_NAME"

# app namespace may not be same as the kubectl namespace (but all defaults to 'defualt'):
APP_NAMESPACE=${APP_NAMESPACE:-default}
KUBE_NAMESPACE=${KUBE_NAMESPACE:-$APP_NAMESPACE}

if [ ! -z "${GKE_CLUSTER_NAME}" ]
then
    progress "getting information for cluster $GKE_CLUSTER_NAME"
    if gcloud container clusters list --format=json | jq -r '.[].name' | grep ${GKE_CLUSTER_NAME} &> /dev/null
    then
        info=($(gcloud container clusters list --filter "name:${GKE_CLUSTER_NAME}" --format='value(zone,status)'))
        location=${info[0]}
        status=${info[1]}
        # region name has 1 '-', otherwise is a zone name
        if [ "$(echo $location | grep -o '-' | wc -l)" -eq "1" ]
        then
            opt="--region=$location"
        else
            opt="--zone=$location"
        fi
        gcloud container clusters get-credentials "${GKE_CLUSTER_NAME}" "$opt"
        echo "Current cluster status is: $status"

        if ! kubectl version &> /dev/null; then
            kubectl version || true
            echo
            echo ERROR: cannot connect to kubernetes cluster!
            echo
        fi
        ctx=$(kubectl config current-context)
        kubectl config set-context "$ctx" --namespace="$KUBE_NAMESPACE"
        echo Current Kubernetes Context: "$ctx"
        echo Current Namespace: "$KUBE_NAMESPACE"
    else
        echo "WARN: GKE cluster '${GKE_CLUSTER_NAME}' is not found or status is not RUNNING"
        kubectl config set current-context UNKNOWN &> /dev/null
    fi
fi
