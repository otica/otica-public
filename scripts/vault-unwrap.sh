#!/usr/bin/env bash
# Get the secret wrapped by a vault token
# Usage:
#   vault-unwrap.sh <wrap_token> <options>

THIS_DIR=$(dirname "$0")

# include functions
source $THIS_DIR/functions.sh

if [[ -z "$VAULT_ADDR" ]]; then
   exit_with_error "missing required environment variable VAULT_ADDR"
fi

token=$1

function hasCmd() {
    which $1 > /dev/null
}

if hasCmd vault; then
    vault unwrap -format=json -field=data $token
elif hasCmd curl; then
    if [ -z "$data" ]; then
        if hasCmd jq; then
            curl -sSL --header "X-Vault-Token: $token" --request POST $VAULT_ADDR/v1/sys/wrapping/unwrap \
            | jq -r '.data.data // .'
        else
            curl -sSL --header "X-Vault-Token: $token" --request POST $VAULT_ADDR/v1/sys/wrapping/unwrap
        fi
    else
        echo "Errors: * wrapping token is not valid or does not exist"
    fi
else
    echo 'Cannot find curl or vault cmd. Please intall curl or vault in $PATH'
    exit 1
fi


# cURL version
# curl -sSL --header "X-Vault-Token: $token" --request POST $VAULT_ADDR/v1/sys/wrapping/unwrap
