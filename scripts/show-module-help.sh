#!/usr/bin/env bash

help_file="$FRAMEWORK_DIR/docs/make-modules/$1/README.md"

if command -v pandoc &> /dev/null; then
    pandoc -f markdown-smart -t plain "$help_file" | grep -v '_TOC_'
else
    cat "$help_file"
fi
