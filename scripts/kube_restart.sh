#!/usr/bin/env bash

###############################################################################
# restart resource definitions from list of input templates
# using kubectl rollout restart, which is now available in v1.15 or newer. 
###############################################################################

THIS_DIR=$(dirname "$0")
PATH="${THIS_DIR}:${PATH}"

# include functions
source $THIS_DIR/functions.sh
if [ -f env.sh ]
then
  source env.sh
fi

# fail on error or undeclared vars
trap_errors

# optional vars
set +u
debug=$debug
set -u

template_files=$@

for template in $template_files; do
  if [ "$debug" = "true" ]; then
    cat $template | render.sh
  else
    echo Rollout restart kubernetes resources with $template
    cat $template | render.sh | kubectl rollout restart -f -
  fi
done
