
#!/usr/bin/env bash

# Copyright 2019 Xu Wang.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This script receives a new set of credentials for auth server
# that are based on TLS client certificates. This is better than using
# cloud provider specific auth "plugins" because it works on any cluster
# and does not require any extra binaries. It produces kubeconfig to build/kubeconfig

# This script can be used to retreive x509 certificate from a GKE cluster
# which can be used for accessing the CSR API with Teleport
#
# Create kubeconfig for a given ServiceAcccount
# Usage $0 <service_account>

function usage(){
  cmd=`basename "$0"`
  echo
  echo "Create kubeconfig for a service account and save to vault"
  echo "  $cmd <namespace> <service_account> [<kubeconfig_vault_path>]"
  echo "  The kubernetes <service_account> must exist and has a RBAC role binding"
  echo "  You must also already login to valut and have the write permition to <kubeconfig_vault_path>"
  exit 1;
}

function get_service_account_sec() {
  svc=$(kubectl get serviceaccount -n $1 $2 -o json)
  if [ ! -z "$svc" ] ; then
    sec_name=$(echo $svc | jq -r ".secrets[0].name")
    kubectl get secret $sec_name -n $1 -o json
  fi
}

function create_kubeconfig() {
  NS=$1
  SVC=$2

  # Extract CA and Auth token from service account's secret
  sec=$(get_service_account_sec $NS $SVC )  # get svc's secret
  if [ -z "$sec" ] ; then 
    exit 1
  fi

  CA_CRT=$(echo $sec | jq -r '.data."ca.crt"')
  SVC_TOKEN=$(echo $sec | jq -r '.data.token' | base64 --decode | tr -d '\n')
  USER="${SVC}-${NS}"

  # Extract cluster IP from the current context
  CURRENT_CONTEXT=$(kubectl config current-context)
  CURRENT_CLUSTER=$(kubectl config view -o jsonpath="{.contexts[?(@.name == \"${CURRENT_CONTEXT}\"})].context.cluster}")
  CURRENT_CLUSTER_ADDR=$(kubectl config view -o jsonpath="{.clusters[?(@.name == \"${CURRENT_CLUSTER}\"})].cluster.server}")

  cat <<EOF
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: ${CA_CRT}
    server: "${CURRENT_CLUSTER_ADDR}"
  name: ${CURRENT_CLUSTER}
contexts:
- context:
    cluster: ${CURRENT_CLUSTER}
    user: "${USER}"
  name: ${CURRENT_CONTEXT}
current-context: ${CURRENT_CONTEXT}
kind: Config
preferences: {}
users:
- name: "${USER}"
  user:
    token: ${SVC_TOKEN}
EOF
}

if ! [ $# -eq 2 ]; then
    usage
fi

kubeconfig=$(create_kubeconfig $1 $2)
if [ -z "$kubeconfig" ]; then
  usage
else
  if [ -z "$3" ]; then
    echo "$kubeconfig" 
  else
    echo "$kubeconfig" | vault kv put $3 format=text value=-
  fi
fi
