#!/usr/bin/env bash
set -e

###############################################################################
# Set up gcloud config for both local and CI/CD jobs
###############################################################################

THIS_DIR=$(dirname "$0")

# include functions
source $THIS_DIR/functions.sh

# fail on error or undeclared vars
trap_errors

# The domain name to append to the user when authenticating.
# If not defined abort.
if [[ -z "$GCP_USER_AUTH_DOMAIN" ]]; then
    exit_with_error "missing required variable GCP_USER_AUTH_DOMAIN"
fi

function gcloud_auth_user() {
  >&2 echo "Authenticating with personal credentials..."
  USER=${USER:-unknown}
  # for some, the local USER is not the same as GCP_USER, allow override with GCP_USER (i.e. SUNet ID)
  user=${GCP_USER:-${USER}}

  if ! gcloud auth list --format json | jq -er ".[] | select(.account == \"${user}@$GCP_USER_AUTH_DOMAIN\") | .status == \"ACTIVE\"" > /dev/null 2>&1
  then
    gcloud auth login --brief
  fi

  # If REQUIRE_APPLICATION_DEFAULT_CREDENTIALS AND
  # file application_default_credentials.json is missing,
  # set application-default-credentials
  if not_empty_var REQUIRE_APPLICATION_DEFAULT_CREDENTIALS \
    && [ ! -f ~/.config/gcloud/application_default_credentials.json ]
  then
      >&2 echo "Set application-default-credentials with personal credentials..."
      gcloud auth application-default login
  fi
}

function gcloud_auth_service_account() {
  key_file=$1
  >&2 echo "Authenticating with service account credentials..."
	gcloud auth activate-service-account --key-file "$key_file"
  # set application-default-credential
  mkdir -p ~/.config/gcloud
  cp -f "$key_file" ~/.config/gcloud/application_default_credentials.json
}

function gcloud_config() {
  >&2 echo "Set up gcloud configurations..."
  empty_var GCP_CONFIGURATION && GCP_CONFIGURATION=default

  if ! gcloud config configurations describe ${GCP_CONFIGURATION} &> /dev/null
  then
    # create a new gcloud conf if doesn't exist
    gcloud config configurations create --activate ${GCP_CONFIGURATION}
  else
    # activate config
    gcloud config configurations activate ${GCP_CONFIGURATION}
  fi

  DISABLE_UPGRADE_CHECK=${DISABLE_UPGRADE_CHECK:-true}

  # disable upgrade notification
  gcloud config set component_manager/disable_update_check "$DISABLE_UPGRADE_CHECK" > /dev/null 2>&1
  # try set config from environment variables (env.sh)
  # config set core/project could fail *after* setting if the cluster conceals metadata, with
  # ERROR: gcloud crashed (MetadataServerException): The request is rejected. Please check if the metadata server is concealed.
  # we let it pass through because the GCP_PROJECT_ID, GCP_REGION, and GCP_ZONE do get set despite the crash error.
  # To make sure PROJECT env is always set for gcloud, you can pass CLOUDSDK_CORE_PROJECT variable.
  gcloud config set core/project "$GCP_PROJECT_ID" > /dev/null 2>&1 || true
  not_empty_var GCP_REGION && gcloud config set compute/region ${GCP_REGION} > /dev/null 2>&1 || true
  not_empty_var GCP_ZONE && gcloud config set compute/zone ${GCP_ZONE} > /dev/null 2>&1 || true
  gcloud config set core/disable_usage_reporting False > /dev/null 2>&1
}

### #### ### #### ### #### ### #### ### #### ### #### ### ####

if empty_var REAUTH_LAST_AUTH_FILE
then
   REAUTH_LAST_AUTH_FILE="${HOME}/.otica_last_auth"
fi

if empty_var REAUTH_SECONDS
then
   REAUTH_SECONDS=300
fi

REAUTH_LAST_EPOCH=0
REAUTH_NEEDED=0

# Write the time that we did a gcloud authentication to the file
# REAUTH_LAST_AUTH_FILE.  The time will be in seconds since the epoch.
function write_auth_time() {
    epoch_time=$(date +%s)
    echo "$epoch_time" > "$REAUTH_LAST_AUTH_FILE"
    echo "wrote last auth'ed time to $REAUTH_LAST_AUTH_FILE"
}

# This function sets the global variable REAUTH_LAST_EPOCH to the epoch
# time stored in REAUTH_LAST_AUTH_FILE (if that file exists).
function read_auth_time() {
    if [[ -f "$REAUTH_LAST_AUTH_FILE" ]]; then
       REAUTH_LAST_EPOCH=$(cat "$REAUTH_LAST_AUTH_FILE")
       echo "last auth epoch: $REAUTH_LAST_EPOCH"
    else
       echo "no file $REAUTH_LAST_AUTH_FILE"
    fi
}

# This function sets the global variable REAUTH_NEEDED to 0, if a re-auth
# is needed, or to 1, if a re-auth is not needed.
function reauth_needed() {
    read_auth_time
    CURRENT_EPOCH=$(date +%s)

    LAST_AUTH_SECONDS=$((CURRENT_EPOCH - REAUTH_LAST_EPOCH))
    echo "$LAST_AUTH_SECONDS seconds elapsed since last authentication"

    if (( LAST_AUTH_SECONDS > REAUTH_SECONDS )); then
        echo "more than $REAUTH_SECONDS seconds elapsed, so re-auth is needed"
        REAUTH_NEEDED=1
    else
        echo "fewer than $REAUTH_SECONDS seconds elapsed, so no re-auth is needed"
        REAUTH_NEEDED=0
    fi
}

### #### ### #### ### #### ### #### ### #### ### #### ### ####

function do_auth() {
    progress "about to run gcloud_config"
    gcloud_config
    progress "finished gcloud_config"

    # If GOOGLE_APPLICATION_CREDENTIALS is set, use it for service account auth
    if ( not_empty_var GOOGLE_APPLICATION_CREDENTIALS ) && [ -f "$GOOGLE_APPLICATION_CREDENTIALS" ]
    then
        gcloud_auth_service_account "$GOOGLE_APPLICATION_CREDENTIALS"
        exit 0
    fi

    # Auth with key in GCP_KEY, the value may be base64 encoded
    # Used in case of CI/CD job
    if not_empty_var GCP_KEY
    then
      >&2 echo "Get GCP Key from GCP_KEY."
      key_file=$(mktemp)
      # Default to use GCP_KEY as it is
      echo "$GCP_KEY" > "$key_file"
      if echo "$GCP_KEY" | base64 --decode &> /dev/null ; then
        echo "$GCP_KEY" | base64 --decode > "$key_file"
      fi
      gcloud_auth_service_account "$key_file"
      rm -f "$key_file"
      write_auth_time
      exit 0
    fi

    # Auth with key in GCP_KEY_BASE64, the value must be base64 encoded
    if not_empty_var GCP_KEY_BASE64
    then
      >&2 echo "Get GCP Key from GCP_KEY_BASE64."
      key_file=$(mktemp)
      echo "$GCP_KEY_BASE64" | base64 --decode > "$key_file"
      gcloud_auth_service_account "$key_file"
      rm -f "$key_file"
      write_auth_time
      exit 0
    fi

    # Auth with personal credentials (where does user_auth get defined?)
    if ( not_empty_var user_auth && [ "$user_auth" = true ] ) \
      || ( not_empty_var GCP_USER_AUTH && [ "$GCP_USER_AUTH" = true ] )
    then
      gcloud_auth_user
      write_auth_time
      exit 0
    fi

    # Auth with the key from vault GCP_KEY_PATH
    # Cache the key in local GCP_KEY_PATH to avoid pull from slow vault.
    if not_empty_var GCP_KEY_PATH
    then
      if [ ! -f "$GCP_KEY_FILE" ]
      then
        "${THIS_DIR}/vault-login.sh"
        mkdir -p $(dirname "$GCP_KEY_FILE")
        "${THIS_DIR}/vault-read.sh" "$GCP_KEY_PATH" > "$GCP_KEY_FILE"
      fi
      gcloud_auth_service_account "$GCP_KEY_FILE"
      write_auth_time
      exit 0
    fi

    # Catch All
    gcloud_auth_user
    write_auth_time
}

### #### ### #### ### #### ### #### ### #### ### #### ### ####

GOOGLE_CLOUD_PROJECT=${GOOGLE_CLOUD_PROJECT:-}
GCP_PROJECT_ID=${GCP_PROJECT_ID:-$GOOGLE_CLOUD_PROJECT}
if empty_var GCP_PROJECT_ID
then
  >&2 echo "ERROR: GCP_PROJECT_ID is missing."
  exit 1
fi

NON_INTERACTIVE=${NON_INTERACTIVE:-false}

################################################################
# Go through all auth scenarios in the defined ORDER
################################################################

# Do we need to do a re-auth?
reauth_needed
if [[ "$REAUTH_NEEDED" == "1" ]]; then
    echo "re-auth needed"
    do_auth
else
    echo "re-auth not needed"
fi
