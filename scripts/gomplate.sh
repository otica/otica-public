#!/usr/bin/env bash

###############################################################################
# Call gomplate w/ our custom functions from gomplate-includes
###############################################################################

THIS_DIR=$(dirname "$0")
DRONE_BASE_DIR="${DRONE_BASE_DIR:-.}"

# include functions
source $THIS_DIR/functions.sh

templates_dir=$FRAMEWORK_DIR/gomplate-includes

# fail on error or undeclared vars
trap_errors

# cat includes in templates_dir and stdin together
cat $templates_dir/*.tmpl - | gomplate -d vault="vault://" "$@"