#!/usr/bin/env bash
set -e

# Used by makefile_parts/terraform.mk
# to download the encryption key from
# gs://${GCP_INFRASTRUCTURE_BUCKET}/terraform.key.enc

mkdir -p ${TF_BUILD_DIR}

if [ -f ${TF_BUILD_DIR}/.terraform.key ]; then
	echo ${TF_BUILD_DIR}/.terraform.key exist!
	exit 0
fi

if [ -z ${GCP_INFRASTRUCTURE_KEY_ID} ]; then
	echo KMS GCP_INFRASTRUCTURE_KEY_ID is not defined!
	exit 0
fi

if [ -z ${GCP_INFRASTRUCTURE_BUCKET} ]; then
	echo ERROR: GCP_INFRASTRUCTURE_BUCKET is not defined!
	exit 1
fi

TF_BUILD_DIR=${TF_BUILD_DIR:-build}
TF_BACKEND_PREFIX=${TF_BACKEND_PREFIX:-terraform/${GCP_PROJECT_ID}/${GCP_ENVIRONMENT}/state}
TF_KEY_URI=gs://${GCP_INFRASTRUCTURE_BUCKET}/${TF_BACKEND_PREFIX}/terraform.key.enc

if gsutil ls -p ${GCP_PROJECT_ID} ${TF_KEY_URI}; then
	gsutil cat ${TF_KEY_URI} | \
		base64 --decode | \
		gcloud kms decrypt \
			--key ${GCP_INFRASTRUCTURE_KEY_ID} \
			--ciphertext-file - \
			--plaintext-file ${TF_BUILD_DIR}/.terraform.key
else
	echo ${TF_KEY_URI} not exists
fi
