#!/usr/bin/env python3

# pylint: disable=bad-whitespace
# pylint: disable=superfluous-parens
# pylint: disable=attribute-defined-outside-init
# pylint: disable=bad-continuation
# pylint: disable=too-many-instance-attributes
# pylint: disable=no-self-use
# pylint: disable=len-as-condition

"""
Script to carry out Otica framework deployment tasks.
"""

import argparse
import logging
import os
import pathlib
import shutil
import subprocess
import sys
import tempfile

import semantic_version

# Add './lib' to search path. We need to do this
# so that we can import otica_abstract
MY_DIRECTORY = pathlib.Path(__file__).parent.resolve()
sys.path.append(os.path.join(MY_DIRECTORY, 'lib'))

from otica_abstract import OticaAbstract

class OticaFramework(OticaAbstract):
    """ The class that does framework deployment work.
    """
    env_vars = [
        'GCP_PROJECT_ID',
        'FRAMEWORK_BUCKET',
        'FRAMEWORK_DIR',
        'FRAMEWORK_BUILD_DIR',
        'FRAMEWORK_SOURCE_REPO_URL',
        'FRAMEWORK_SOURCE_REPO_REFSPEC',
        'FRAMEWORK_RELEASE_REPO_URL',
        'FRAMEWORK_RELEASE_REPO_BRANCH',
    ]

    required_env_vars = [
        'GCP_PROJECT_ID',
        'FRAMEWORK_BUCKET',
        'FRAMEWORK_DIR',
        'FRAMEWORK_SOURCE_REPO_URL',
        'FRAMEWORK_RELEASE_REPO_URL',
    ]

    # Some defaults
    env_var_to_default = {
        'FRAMEWORK_SOURCE_REPO_REFSPEC':  'master',
        'FRAMEWORK_RELEASE_REPO_REFSPEC': 'master',
        'FRAMEWORK_BUILD_DIR':            'build',
    }

    deprecated_env_vars = {
    }

    version_file_name = 'OTICA_VERSION'

    def __init__(self,
                 verbose=None,
                 ):
        """Initialize object
        """
        super().__init__(verbose=verbose)

        self.env_vars            = OticaFramework.env_vars
        self.required_env_vars   = OticaFramework.required_env_vars
        self.deprecated_env_vars = OticaFramework.deprecated_env_vars
        self.env_var_to_default  = OticaFramework.env_var_to_default

        # If self.error_on_version_problem is True then exit with an error
        # on any of the "deploy_*" commands if the source version is not
        # larger than the release version. If set to False ignore the versions.
        self.error_on_version_problem = True

        # Set some properties from the environment variables.
        self.build_directory = os.environ['FRAMEWORK_BUILD_DIR']

        # Set the sourdce and release repository URLs.
        self.source_repo_url  = os.environ['FRAMEWORK_SOURCE_REPO_URL']
        self.release_repo_url = os.environ['FRAMEWORK_RELEASE_REPO_URL']

        self.source_repo_refspec  = os.environ['FRAMEWORK_SOURCE_REPO_REFSPEC']
        self.release_repo_refspec = os.environ['FRAMEWORK_RELEASE_REPO_REFSPEC']

        # These should be semantic_version objects.
        self.source_version  = None
        self.release_version = None

        # Construct the full path to FRAMEWORK_BUILD_DIR (IS THIS NEEDED?)
        self.build_dir_full_path = os.path.abspath(os.environ['FRAMEWORK_BUILD_DIR'])

    def framework_dir(self):
        """Return the path to the framework directory. Comes from
        FRAMEWORK_DIR environment variable"""
        return os.environ['FRAMEWORK_DIR']

    def version_file(self, working_directory):
        """Return the path to the Otica version file OTICA_FILE"""
        return os.path.join(working_directory, OticaFramework.version_file_name)

    def show_env(self):
        """Display the environment variables that are specific to the
        framework-develop.mk make module. Also, show the source and release
        repo Otica versions.
        """
        super().show_env()
        print(f"build dir full path: {self.build_dir_full_path}")
        self.show_versions()

    def tarball_path(self):
        """Return the path to where we will put the tarball"""
        if (os.path.isdir(self.build_dir_full_path)):
            return os.path.join(self.build_dir_full_path, 'otica.tar')
        else:
            msg = f"cannot proceed as the build directory '{self.build_dir_full_path}' does not exist"
            self.exit_with_error(msg)

    def gen_tarball(self):
        """Thin wrapper around the tarball method to allow us to print a log
        message."""
        self.tarball()
        msg = (f"left tar file in {os.path.relpath(self.tarball_path())} "
               f"(version: {self.source_version})")
        logging.info(msg)

    def tarball(self):
        """Create a tarball from the Otica source repository.

        The branch or tag used for the export comes from the environment variable
        FRAMEWORK_SOURCE_REPO_REFSPEC.

        To exclude files or directories from export set the
        "export-ignore" git attribute for them in .gitattributes.

        The tarball file is put into the FRAMEWORK_BUILD_DIR directory.

        Returns the version string in the file OTICA_VERSION.
        """

        self.progress("making tarball")

        with tempfile.TemporaryDirectory() as tdir:
            # Step 1. Clone the Otica source repository. This also sets
            # self.source_version.
            self.clone_source_repo(dest_dir=tdir)

            self.progress(f"Otica version in source repo: {self.source_version}")

            # Step 2. Export the Otica source repository using "git archive". Put it into
            # the build directory.
            tarball_file = self.tarball_path()

            export_refspec = os.environ['FRAMEWORK_SOURCE_REPO_REFSPEC']
            self.progress(f"git archive export refspec '{export_refspec}'")

            cmd = [
                'git',
                '-C', tdir,
                'archive',
                '--format=tar',
                f"--output={tarball_file}",
                export_refspec,
            ]
            self.progress(cmd)
            self.run_command(cmd, quiet=(not self.verbose))



    def extract_tarball(self, tarball_path, dest_dir):
        """Extract the tarball at tarball_path extracting into
        dest_dir."""
        self.progress(f"extracting tarball into {dest_dir}")

        cmd = [
            'tar',
            '-x',
            '-f', tarball_path,
            '-C', dest_dir
            ]

        self.progress(f"{cmd}")
        subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)
        self.progress(f"extracted tarball {tarball_path} on top of {dest_dir}")

    def get_version(self, working_directory):
        """Get the Otica version from working_directory."""
        version_file = self.version_file(working_directory=working_directory)

        # CHECK 1: Does the version file exist?
        if (not os.path.isfile(version_file)):
            msg = f"no version file found (should be at {version_file})"
            self.exit_with_error(msg)
        else:
            self.progress(f"version file {version_file} found")

        # Read the lines of the version file.
        with open(version_file) as fhandle:
            lines = fhandle.readlines()

            # CHECK 2: Does the version file have exactly one line?
            if (len(lines) == 0):
                msg = f"version file appears to be empty"
                self.exit_with_error(msg)
            elif (len(lines) > 1):
                msg = f"version file should only have one line (has {len(lines)})"
                self.exit_with_error(msg)

            # CHECK 3: The first line should be a semantic version number.
            first_line = lines[0].strip()

            # This will throw a Value error if first_line is not a valid
            # sematic version.
            sem_version = semantic_version.Version(first_line)

        return sem_version

    def show_versions(self):
        """Print the source and release repo Otica versions."""
        with tempfile.TemporaryDirectory() as tdir:
            self.deploy_prep(release_repo_working_directory=tdir, version_check=False)
            print(f"source  version is {self.source_version}")
            print(f"release version is {self.release_version}")

    def clone_source_repo(self, dest_dir):
        """Clone the source repository and set the Otica version"""
        self.clone_repo(repo_url=self.source_repo_url,
                        dest_dir=dest_dir,
                        branch=self.source_repo_refspec)

        self.source_version = self.get_version(working_directory=dest_dir)

    def clone_release_repo(self, dest_dir):
        """Clone the release repository and set the Otica version"""
        self.clone_repo(repo_url=self.release_repo_url,
                        dest_dir=dest_dir,
                        branch=self.release_repo_refspec)

        self.release_version = self.get_version(working_directory=dest_dir)

    def clone_repo(self, repo_url, dest_dir, branch='master'):
        """Clone repo repo_url into dest_dir. Checkout the branch
        indicated by the branch paramter."""
        self.progress(f"cloning repo {repo_url} into {dest_dir}")

        cmd = [
            'git',
            'clone',
            repo_url,
            dest_dir,
            ]

        self.progress(f"{cmd}")
        self.run_command(cmd, quiet=(not self.verbose))

        self.progress(f"checking out branch {branch}")
        cmd = [
            'git',
            '-C', dest_dir,
            'checkout',
            branch,
            ]

        self.progress(f"{cmd}")
        self.run_command(cmd, quiet=(not self.verbose))


    def rsync_with_delete(self, source, target):
        """Run rsync from source to target with the --delete option, i.e.,
        remove any files in target that are not in source."""
        if (not os.path.isdir(source)):
            msg = f"source directory {source} does not exist"
            self.exit_with_error(msg)

        if (not os.path.isdir(target)):
            msg = f"target directory {target} does not exist"
            self.exit_with_error(msg)

        # Make sure that the source directory has a trailing slash.
        # We need that trailing slash to ensure that rsync copies the
        # contents of source and not the source directory itself.
        source = os.path.join(source, '')

        self.progress(f"rysnc'ing with delete {source} onto {target}")

        # We are careful to exclude the .git drectory since that directory
        # is NOT present in source. Note that we do not attempt to preserve
        # file times. Note that we delete any files in the target not present
        # in the source.
        cmd = [
            'rsync',
            '--recursive',
            '--links',
            '--perms',
            '--group',
            '--owner',
            '--delete',
            '--exclude', '.git',
            source,
            target,
            ]

        self.progress(f"{cmd}")
        subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

    def deploy_prep(self, release_repo_working_directory, version_check=True):
        """This method prepares for the deployment. It does this things:

           1. Creates the tarball from the Otica source repository.
           2. Extracts the tarball into a temporary "extracted source" directory.
           3. Clones the Otica release repository.
           4. Checks that the source version is greater than the release version.
           5. Rsyncs the "extracted source" directory onto the Otica release repository
           6. Cleanup
        """
        # Step 1.
        # Note that the tarball method implicitly clones the source directory and as
        # a result sets the source version.
        self.tarball()

        # Step 2. Extract the tarball into a temporary "extracted source" directory.
        extracted_source_dir = tempfile.TemporaryDirectory()
        self.extract_tarball(tarball_path=self.tarball_path(),
                             dest_dir=extracted_source_dir.name)

        # Step 3.
        self.clone_release_repo(dest_dir=release_repo_working_directory)

        # Step 4. Verify that the source version is strictly greater than the release version.
        if (version_check):
            new_version = self.source_version
            old_version = self.release_version
            self.progress(f"source  version is {new_version}")
            self.progress(f"release version is {old_version}")

            if (new_version <= old_version):
                msg = (f"new (source) version ({new_version}) is not newer "
                       f"than existing (release) version ({old_version})")
                self.exit_with_error(msg)
            else:
                msg = (f"new (source) version ({new_version}) newer "
                       f"than existing (release) version ({old_version})")
                self.progress(msg)
        else:
            self.progress("skipping version check")

        # Step 5. rsync the extracted-source drectory onto the release working directory.
        self.rsync_with_delete(source=extracted_source_dir.name,
                               target=release_repo_working_directory)


        # Step 6. Copy any release-specific files that are not in the source directory.

        # Step 6.1: Copy .gitignore. We need this because Python cache files
        # are generated in the Otica repository when the otica command gets run.
        # These cache files confuse Git into thinking there are some, unversion-controlled
        # files. So, to avoid this, we add "*.pyc" into .gitignore.
        src = os.path.join(MY_DIRECTORY, 'files', 'framework-develop', 'dot.gitignore')
        dst = os.path.join(release_repo_working_directory, '.gitignore')
        shutil.copyfile(src, dst)
        self.progress(f"copied {src} into {dst}")

        # Step 7. Cleanup
        extracted_source_dir.cleanup()

        return

    def deploy_status(self):
        """Deploy source on top of release and then do a 'git status' to
        see which files have changed"""
        with tempfile.TemporaryDirectory() as tdir:
            self.deploy_prep(release_repo_working_directory=tdir,
                             version_check=self.error_on_version_problem)

            self.progress(f"showing git status in {tdir}")

            cmd = [
                'git',
                '-C', tdir,
                'status',
            ]

            self.progress(f"{cmd}")
            subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

    def deploy_diff(self):
        """Deploy source on top of release and then do a 'git diff' to
        see the file differences"""
        with tempfile.TemporaryDirectory() as tdir:
            self.deploy_prep(release_repo_working_directory=tdir,
                             version_check=self.error_on_version_problem)

            self.progress(f"showing git status in {tdir}")

            cmd = [
                'git',
                '-C', tdir,
                'diff',
            ]

            self.progress(f"{cmd}")
            subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

    def deploy_commit(self):
        """Deploy source on top of release and then do a 'git commit', tag the commit
        with the source version, and then push."""
        with tempfile.TemporaryDirectory() as tdir:
            self.deploy_prep(release_repo_working_directory=tdir)

            self.progress(f"source  version is {self.source_version}")
            self.progress(f"release version is {self.release_version}")

            self.progress(f"commit everything")
            cmd = [
                'git',
                '-C', tdir,
                'add',
                '.'
            ]
            self.progress(f"{cmd}")
            subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

            cmd = [
                'git',
                '-C', tdir,
                'commit',
                '-m', f"releasing version {self.source_version}"
            ]
            self.progress(f"{cmd}")
            subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

            self.progress(f"tag with version {self.source_version}")
            cmd = [
                'git',
                '-C', tdir,
                'tag',
                f"release/{str(self.source_version)}",
            ]
            self.progress(f"{cmd}")
            subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

            self.progress(f"push everything")
            cmd = [
                'git',
                '-C', tdir,
                'push',
                '--follow-tags',
            ]
            self.progress(f"{cmd}")
            subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

            self.progress(f"push tags")
            cmd = [
                'git',
                '-C', tdir,
                'push',
                '--tags',
            ]
            self.progress(f"{cmd}")
            subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)


def main():
    """This function is called when this script is run on the command-line."""

    parser = argparse.ArgumentParser(description='Otica framework script.')
    parser.add_argument('--verbose', '-v', action="store_true")
    parser.add_argument('--dryrun', action="store_true")
    parser.add_argument('action', nargs=1,
                        choices=['show-env', 'check-env', 'tarball',
                                 'show-versions', 'deploy-status', 'deploy-diff',
                                 'deploy-commit'],
                        help='create a new project/subproject or update an existing one')


    args = parser.parse_args()

    action = args.action[0]

    fwork = OticaFramework(
        verbose=args.verbose
    )
    fwork.progress(f"action is '{action}'")

    if (action == 'show-env'):
        fwork.show_env()
    elif (action == 'check-env'):
        fwork.check_env()
    elif (action == 'tarball'):
        fwork.gen_tarball()
    elif (action == 'show-versions'):
        fwork.show_versions()
    elif (action == 'deploy-status'):
        # We don't want to error out on version issues.
        fwork.error_on_version_problem = False
        fwork.deploy_status()
    elif (action == 'deploy-diff'):
        # We don't want to error out on version issues.
        fwork.error_on_version_problem = False
        fwork.deploy_diff()
    elif (action == 'deploy-commit'):
        fwork.deploy_commit()
    elif (action == 'gen-build-script'):
        # Write the Kaniko build command to the file "kaniko-build.cmd" in the
        # DOCKER_BUILD_DIR.
        fwork.write_kaniko_build_script()
    elif (action == 'tag-and-push'):
        fwork.tag_and_push()
    else:
        print(f"unknown action '{action}'")
        sys.exit(1)

    fwork.progress('framework-develop.py finished')

if (__name__ == "__main__"):
    main()
