#!/usr/bin/env bash

# Use:
#    datasealer.sh      (creates or adds a new secret to the secret in Vault)
#    datasealer.sh new  (creates a single new datasealer secret and adds to Vault)
#
# The Vault path to the datasealer secret MUST be contained in
# DATASEALER_SEC_PATH.
set -e

THIS_DIR=$(dirname "$0")
source $THIS_DIR/functions.sh


##############################################################################
write_vault_secret_to_file () {
    # Write to the vault secret to the given file.
    vault_path=$1
    destination_path=$2

    set +e
    result=$(vault kv get "$vault_path" 2>&1)
    rc=$?
    set -e
    progress "return code from vault kv list is $rc"

    ## If the rc is 0 this means the Vault path exists. In that case, write
    ## the Vault secret to destination_path.
    if [[ "$rc" == "0" ]]; then
        progress "return code from vault kv is 0 so we will write secret to $destination_path"
        vault-read.sh "$vault_path" > "$destination_path" 2> /dev/null
    else
        progress "return code from vault kv list is non-zero so will not attempt to write secret to $destination_path"
    fi
}

##############################################################################

if [[ -z "$DATASEALER_SEC_PATH" ]]; then
    exit_with_error "missing required variable DATASEALER_SEC_PATH"
else
    progress "DATASEALER_SEC_PATH: $DATASEALER_SEC_PATH"
fi

arg=$1

TEMPDIR=$(mktemp -d)
TMP_DATASEALER_FILE="$TEMPDIR/sealer.keys"

if [[ "$arg" == "new" ]]; then
    NEW="YES"
else
    NEW="NO"
fi

# If arg is not "new" we skip downloading the existing vault secret.
if [[ "$NEW" != "YES" ]]; then
    progress "the 'new' argument was not used"

    write_vault_secret_to_file "$DATASEALER_SEC_PATH" "$TMP_DATASEALER_FILE"

    # If the secret read from Vault is empty, delete $TMP_DATASEALER_FILE.
    # This way shib-seckeygen will create a new secret file staring at
    # version "1:". If the secret read is NOT empty, add an extra carriage
    # return.
    if [[ -s "$TMP_DATASEALER_FILE" ]]; then
        progress "Vault secret was not empty; will add extra carriage return at end of file"
        # This next 'echo' is to deal with the unpleasant fact that vault
        # strips off the trailing carriage return. So, we add it back.
        echo "" >> "$TMP_DATASEALER_FILE"
        progress "added extra carriage return at end of secret file"
    else
        progress "Vault secret was empty; deleting $TMP_DATASEALER_FILE"
        rm -f "$TMP_DATASEALER_FILE"

        # We treat this case as a new datasealer file so we set NEW to
        # "YES".
        NEW="YES"
    fi
else
    progress "the 'new' argument was used"
    progress "will generate new datasealer file"
fi

"$SCRIPTS_DIR"/external/shib-seckeygen -o "$TEMPDIR" -b 256
progress "created/updated new datasealer file"

# Due to the fact that the vault command line client
# strips trailing carriage-returns AND the fact that
# the Shibboleth client cannot handle a datasealer file
# with a single line and no trailing carriage-return, if
# this is a NEW datasealer file we generate an _extra_ secret.
if [[ "$NEW" == "YES" ]]; then
    "$SCRIPTS_DIR"/external/shib-seckeygen -o "$TEMPDIR" -b 256
    progress "created an EXTRA version in the datasealer file as this is a new file"
fi


# Store in vault.
vault-write.sh "$DATASEALER_SEC_PATH" "@$TMP_DATASEALER_FILE"
progress "stored new secret at Vault path $DATASEALER_SEC_PATH"
vault-read.sh  "$DATASEALER_SEC_PATH"
echo ""

rm -rf "$TEMPDIR"
progress "removed temporary files"

progress "finished"

exit 0
