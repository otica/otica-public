#!/usr/bin/env bash

###############################################################################
# apply resource definitions from list of input templates
###############################################################################

THIS_DIR=$(dirname "$0")
PATH="${THIS_DIR}:${PATH}"

# include functions
source $THIS_DIR/functions.sh
if [ -f env.sh ]
then
  source env.sh
fi

# fail on error or undeclared vars
trap_errors

# optional vars
set +u
debug=$debug
set -u

template_files=$@

for template in $template_files; do
  if [ "$debug" = "true" ]; then
    cat $template | render.sh
  else
    echo applying kubernetes resources with $template after dry-run
    cat $template | render.sh | kubectl create -f - --dry-run -o yaml | kubectl apply -f -
  fi
done
