#!/usr/bin/env python3

"""
A Python3 script for managing packages in Otica.

"""

import argparse
import pathlib
import os
import sys

# Add './lib' to search path. We need to do this
# so that we can import package_wrapper
MY_DIRECTORY = pathlib.Path(__file__).parent.resolve()
sys.path.append(os.path.join(MY_DIRECTORY, 'lib'))

from package_wrapper import PackageWrapper

def main():
    """This function is called when this script is run on the command-line."""

    parser = argparse.ArgumentParser(description='Process otica.yaml file.')
    parser.add_argument('--verbose', '-v', action="store_true")
    parser.add_argument('--dryrun', action="store_true")
    parser.add_argument('action', nargs=1,
                        choices=['show-env', 'check-env', 'download',
                                 'build', 'sign', 'push', 'creds-helper'],
                        help='action to take')

    args = parser.parse_args()

    action = args.action[0]

    pwrap = PackageWrapper(
        action,
        verbose=args.verbose,
        dryrun=args.dryrun,
    )
    pwrap.progress(f"action is '{action}'")

    if (action == 'show-env'):
        pwrap.show_env()
    elif (action == 'download'):
        pwrap.download()
    elif (action == 'build'):
        pwrap.build()
    elif (action == 'sign'):
        pwrap.sign()
    elif (action == 'push'):
        pwrap.push()
    elif (action == 'creds-helper'):
        pwrap.run_creds_helper()
    else:
        print(f"unknown action '{action}'")
        sys.exit(1)

if (__name__ == "__main__"):
    main()
