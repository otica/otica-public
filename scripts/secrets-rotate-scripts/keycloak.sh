#!/usr/bin/env bash

###############################################################################
# Creates/rotates a keycloak user password in vault, and updates the user password in keycloak if API is available
# secret rotate script for secrets-rotate.sh
###############################################################################

THIS_DIR=$(dirname "$0")
PATH=$SCRIPTS_DIR:$PATH

# include functions
source $THIS_DIR/../functions.sh

trap_errors

curl_kc() {
curl -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer $token" "${@:2}"
}

mode=$1
password_vault_path=$vault_path
username=$username
keycloak_url=$keycloak_url

if [ "$mode" = "generate" ]; then
    if vault kv put -field value $password_vault_path > /dev/null 2>&1; then
        >&2 echo "- keycloak password already exists in vault"
        exit 0
    fi
        
    >&2 echo "- generating keycloak password in vault"
    pwgen.sh | vault kv put $password_vault_path format=text value=- previous_value=null > /dev/null

elif [ "$mode" = "rotate" ]; then
    previous_password=$(vault kv get -field value $password_vault_path)
    
    >&2 echo "- rotating keycloak password in vault"
    pwgen.sh | vault kv put $password_vault_path format=text value=- previous_value=$previous_password > /dev/null

    new_password=$(vault kv get -field value $password_vault_path)

    token=$(curl --fail -s -X POST "$keycloak_url/auth/realms/master/protocol/openid-connect/token" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -d "username=$username" \
    -d "password=$previous_password" \
    -d 'grant_type=password' \
    -d 'client_id=admin-cli' | jq -er '.access_token' | tr -d "\r")

    # get the user id
    user_id=$(curl_kc $token -s -X GET "$keycloak_url/auth/admin/realms/master/users?username=$username" | jq -er .[0].id)

    # reset
    >&2 echo "- rotating keycloak password for user '$username' on '$keycloak_url'"
    data='{"type": "password", "value": "'${new_password}'"}'
    curl_kc $token -s --fail -d "$data" -X PUT "$keycloak_url/auth/admin/realms/master/users/$user_id/reset-password"
else
    >&2 echo "mode must be 'rotate' or 'generate'"
    false
fi
