
#!/usr/bin/env bash

###############################################################################
# cleans up sql user from 'previous_value'
# secret rotate script for secrets-rotate.sh
###############################################################################

THIS_DIR=$(dirname "$0")
PATH=$SCRIPTS_DIR:$PATH

# include functions
source $THIS_DIR/../functions.sh

trap_errors

# optional args
db_user_vault_path=$db_user_vault_path

username_delete=$(vault kv get -field previous_value $db_user_vault_path)

instance_name=$(cloud-sql-prefix-to-name.sh $db_instance_prefix)

gcloud -q sql users delete --instance $instance_name $username_delete % 
