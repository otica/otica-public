#!/usr/bin/env bash

###############################################################################
# Creates (doesn't rotate) a self-signed TLS keypair (cert & key)
# secret rotate script for secrets-rotate.sh
###############################################################################

THIS_DIR=$(dirname "$0")
PATH=$SCRIPTS_DIR:$PATH

# include functions
source $THIS_DIR/../functions.sh

trap_errors

mode=$1

tls_cert_vault_path=$vault_path
tls_key_vault_path=$tls_key_vault_path

if [ "$mode" = "generate" ]; then
    if vault kv get $tls_cert_vault_path > /dev/null; then
        >&2 echo "- '$tls_cert_vault_path' already generated"
    else
        >&2 echo "- generating tls cert & key"
        openssl req -x509 -newkey rsa:4096 -keyout tls-key.tmp -days 3650 -nodes -subj "/C=US/ST=CA" | vault kv put $tls_cert_vault_path format=text value=-
        vault kv put $tls_key_vault_path format=text value=@tls-key.tmp
        rm tls-key.tmp
    fi
elif [ "$mode" = "rotate" ]; then
    >&2 echo "- NOT rotating long-lived TLS cert/key pair"
else
    >&2 echo "ERROR: mode must be 'rotate' or 'generate'"
    false
fi
