#!/usr/bin/env bash

###############################################################################
# Creates/rotates a basic text secret in vault
# secret rotate script for secrets-rotate.sh
# - the default secret "rotator"
###############################################################################

THIS_DIR=$(dirname "$0")
PATH=$SCRIPTS_DIR:$PATH

# include functions
source $THIS_DIR/../functions.sh

trap_errors

mode=$1

if [ "$mode" = "generate" ]; then
    if vault kv get -field value $vault_path > /dev/null 2>&1; then
        >&2 echo "- secret already exists"
        exit 0
    fi
        
    >&2 echo "- generating secret"
    pwgen.sh | vault kv put $vault_path format=text value=- previous_value=null > /dev/null

elif [ "$mode" = "rotate" ]; then
    previous_value=$(vault kv get -field value $vault_path)
    
    >&2 echo "- rotating secret"
    pwgen.sh | vault kv put $vault_path format=text value=- previous_value=$previous_value > /dev/null
else
    >&2 echo "ERROR: mode must be 'rotate' or 'generate'"
    false
fi
