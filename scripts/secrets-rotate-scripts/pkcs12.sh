#!/usr/bin/env bash

###############################################################################
# Creates/rotates a pkcs12 keystore secret in vault
# secret rotate script for secrets-rotate.sh
###############################################################################

THIS_DIR=$(dirname "$0")
PATH=$SCRIPTS_DIR:$PATH

# include functions
source $THIS_DIR/../functions.sh

trap_errors

keystore_p12_vault_path=$vault_path

vault kv get -field value $bundle_crt_vault_path > bundle.crt
vault kv get -field value $ca_crt_vault_path > ca.crt
vault kv get -field value $server_key_vault_path > server.key
vault kv get -field value $keystore_key_vault_path > keystore.key

openssl pkcs12 -export -password file:keystore.key \
    -in bundle.crt \
    -inkey server.key \
    -chain -CAfile ca.crt \
        | base64 | tr -d '\n' | vault kv put $keystore_p12_vault_path format=base64 value=- > /dev/null

rm -f bundle.crt server.key ca.crt keystore.key
