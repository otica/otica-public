#!/usr/bin/env bash
#
# Usage: image-reports.sh [gcp project id]
#

IFS=$'\n\t'

CRIT='effectiveSeverity": "CRITICAL"'
HIGH='effectiveSeverity": "HIGH"'
MEDIUM='effectiveSeverity": "MEDIUM"'
LOW='effectiveSeverity": "LOW"'

PROJECT_ID=${1:-$GCP_PROJECT_ID}

if [ -z "$PROJECT_ID" ]; then
  echo "Usage $(basename $0) <gcp project id>"
  exit 1
fi

header="\n %-20s %-40s %-40s %-30s\n"
format=" %-20s %-40s %-40s %-30s\n"

printf "$header" "IMAGE NAME" "LAST TAG" "OCCURENCES" "UPDATED"
printf '=%.0s' {1..123}
echo
all_images=$(gcloud container images list --repository=gcr.io/${PROJECT_ID} --format=json --limit=9999 | jq -r '.[].name')

for i in $all_images
do 
  repo=$(basename $i)
  gcloud beta container images list-tags --show-occurrences-from=1 --occurrence-filter='kind="VULNERABILITY"' --format=json  $i > /tmp/image-report.txt
  vuln_counts=$(gcloud beta container images list-tags --show-occurrences --show-occurrences-from=1 --format='get(vuln_counts)' $i )
  if [ -z "$vuln_counts" ]; then
    vuln_counts="None Found "
  else
    # See https://cloud.google.com/container-registry/docs/reference/rest/v1beta1/projects.occurrences
    vuln_crit=$(grep -c $CRIT /tmp/image-report.txt)
    vuln_high=$(grep -c $HIGH /tmp/image-report.txt)
    vuln_medium=$(grep -c $MEDIUM /tmp/image-report.txt)
    vuln_counts="CRITICAL=$vuln_crit;HIGH=$vuln_high;MEDIUM=$vuln_medium"
  fi
  # Get last tag in tags json array
  tags=$(gcloud beta container images list-tags --show-occurrences-from=1 --format=json $i | jq -r '.[].tags[-1]' )
  timestamp=$(gcloud beta container images list-tags --show-occurrences-from=1 \
      --format='table[no-heading](DISCOVERY[0].updateTime)' $i | cut -d '.' -f1)
  
  printf "$format" "$repo" "$tags" "$vuln_counts" "$timestamp"
done
