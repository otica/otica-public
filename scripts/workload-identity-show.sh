#!/bin/sh

source functions.sh

#VERBOSE=1

########################################################
check_var () {
    var="$1"
    value="${!var}"
    if empty_var "$var"; then
        part1="FAIL: $var"
        msg=$(printf '%-24s is NOT set' "$part1")
    else
        part1="pass: $var"
        msg=$(printf '%-24s is set ("%s")' "$part1" "${!var}")
    fi
    progress "$msg"
}

progress () {
    if [[ "$VERBOSE" == "1" ]]; then
        echo "$1"
    fi
}

exit_with_error () {
    echo "error: $1"
    exit 1
}
########################################################

check_var "GSA_PROJECT_ID"
check_var "KUBE_NAMESPACE"
check_var "GSA_NAME"
check_var "KSA_NAME"

