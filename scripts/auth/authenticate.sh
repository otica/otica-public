#!/usr/bin/env bash
set -e

###############################################################################
# Authenticate to a platform.
#
# This is a simple wrapper around the various auth/authenticate-XXX.sh scripts.
# It is the responsibility of each auth/authenticate-XXX.sh script to check for the
# existence of required environment variables.
#
# Environment variables required by all authenticate-XXX.sh scripts:
#
# * CLOUD_PLATFORM: The name of the platform to authenticate to. MUST be one of the
#   the following:
#
#   - "gcp"
#   - "aws"
#   - "azure" (not yet supported)
#
###############################################################################
THIS_DIR=$(dirname "$0")

# include functions
source $THIS_DIR/../functions.sh

if [[ -z "$CLOUD_PLATFORM" ]]; then
    exit_with_error "missing required variable CLOUD_PLATFORM"
fi

progress "CLOUD_PLATFORM: $CLOUD_PLATFORM"

if [[ "$CLOUD_PLATFORM" == "gcp" ]]; then
	"$SCRIPTS_DIR"/auth/authenticate-gcp.sh
elif [[ "$CLOUD_PLATFORM" == "aws" ]]; then
    authenticate-aws.sh
elif [[ "$CLOUD_PLATFORM" == "azure" ]]; then
    exit_with_error "CLOUD_PLATFORM 'azure' not yet supported"
else
    exit_with_error "CLOUD_PLATFORM '$CLOUD_PLATFORM' not recognized"
fi

exit 0
