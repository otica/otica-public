#!/usr/bin/env bash

set -e

###############################################################################
# Configure kubernetes context (platform-independent)
#
# This should be run _after_ getting Kubernetes cluster credentials.
#
# * KUBE_NAMESPACE: the Kubernetes namespace to configure the context to
#
###############################################################################

THIS_DIR=$(dirname "$0")
source "$THIS_DIR/../functions.sh"

progress_ts "entering $(basename $0)"

if [[ -z "$KUBE_NAMESPACE" ]]; then
    exit_with_error "missing required variable KUBE_NAMESPACE"
fi

##############################################################
check_cluster_connect() {
    if ! kubectl version &> /dev/null; then
        progress_ts "kubectl version generated an error"
        kubectl version || true
        echo
        echo ERROR: not connected to a Kubernetes cluster!
        echo
        exit 1
    else
        progress_ts "kubectl version completed successfully"
    fi
}
##############################################################

# STEP 1. Set kubernetes context.
if [[ -n "$KUBE_CONTEXT" ]]; then
    progress_ts "using explicit context from KUBE_CONTEXT"
    ctx="$KUBE_CONTEXT"
else
    progress_ts "getting context from current-context..."
    ctx=$(kubectl config current-context)
    progress_ts "...retrieved context from current-context"
fi

# Set namespace for context and make context current.
progress_ts "setting namespace for context '$ctx'"
kubectl config set-context "$ctx" --namespace="$KUBE_NAMESPACE"

progress_ts "setting '$ctx' to be current context"
kubectl config use-context "$ctx"

# STEP 2. Check that we can connect to the cluster.
#  If cluster connect fails the script will exit
progress_ts "checking that kubectl can connect to Kubernetes cluster..."
check_cluster_connect

# Getting here means the cluster connect succeeded.
progress_ts "... finished checking that kubectl can connect to Kubernetes cluster"

# Step 3. Check that the current context was set properly.

progress_ts "checking context is set correctly"
actual_context=$(kubectl config current-context)
progress_ts "actual context is '$actual_context'"

if [[ "$actual_context" != "$ctx" ]]; then
    exit_with_error "failed to set context to '$ctx'; current context is '$actual_context'"
else
    echo Current Kubernetes Context: "$ctx"
    echo Current Namespace: "$KUBE_NAMESPACE"
fi

exit 0
