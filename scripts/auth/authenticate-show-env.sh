#!/usr/bin/env bash

###############################################################################
# Show authenticate-specific environment variables
#
###############################################################################

set -e

THIS_DIR=$(dirname "$0")

# include functions
source $THIS_DIR/../functions.sh

if [[ -z "$CLOUD_PLATFORM" ]]; then
    exit_with_error "missing required variable CLOUD_PLATFORM"
fi

progress "CLOUD_PLATFORM: $CLOUD_PLATFORM"

if [[ "$CLOUD_PLATFORM" == "gcp" ]]; then
    auth_variables=(CLOUD_PLATFORM VAULT_ADDR \
                    GCP_PROJECT_ID GCP_USER_AUTH \
                    GCP_USER_AUTH_DOMAIN GCP_CONFIGURATION \
                    GCP_KEY_PATH GCP_KEY GCP_KEY_BASE64)
    printf_width="22"
elif [[ "$CLOUD_PLATFORM" == "aws" ]]; then
    auth_variables=(CLOUD_PLATFORM VAULT_ADDR \
                    AWS_CRED_SECRET AWS_DEPLOYMENT_IAM_ROLE)
    printf_width="22"
elif [[ "$CLOUD_PLATFORM" == "azure" ]]; then
    exit_with_error "CLOUD_PLATFORM 'azure' not yet supported"
else
    exit_with_error "CLOUD_PLATFORM '$CLOUD_PLATFORM' not recognized"
fi

for auth_variable in ${auth_variables[@]}; do
    value="${!auth_variable}"

    if [[ -z "$value" ]]; then
        value="<not defined>"
    fi

    printf "%-${printf_width}s %s\n" "${auth_variable}:" "$value"
done

exit 0
