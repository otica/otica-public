#!/usr/bin/env bash

###############################################################################
# Get kubernetes cluster credentials from Vault.
#
# Required environment variables:

# * KUBECONFIG_SEC_PATH: the Vault path to the Kubernetes config string.
#
# * KUBE_CONTEXT: the name of the Kubernetes context in the Kubernetes
#   config string.
#
# This script reads a .kube/config YAML string from Vault and merges it
# with the current .kube/config file. The Vault path must be stored in the
# environment variable KUBECONFIG_SEC_PATH. The context name must be
# stored in the environment variable KUBE_CONTEXT.
#
###############################################################################

set -e

THIS_DIR=$(dirname "$0")
source "$THIS_DIR/../functions.sh"

progress "entering $(basename $0)"

if [[ -z "$KUBECONFIG_SEC_PATH" ]]; then
    exit_with_error "missing required variable KUBECONFIG_SEC_PATH"
fi
progress "KUBECONFIG_SEC_PATH: $KUBECONFIG_SEC_PATH"

if [[ -z "$KUBE_CONTEXT" ]]; then
    exit_with_error "missing required variable KUBE_CONTEXT"
fi
progress "KUBE_CONTEXT: $KUBE_CONTEXT"

KUBECONFIG_DEST="./.kubeconfig"

# STEP 1. Put the vault secret in $KUBECONFIG_DEST
vault-read.sh $KUBECONFIG_SEC_PATH > "$KUBECONFIG_DEST"
progress "wrote vault secret to $KUBECONFIG_DEST"

# STEP 2. Make sure there is a directory ~/.kube
mkdir -p ~/.kube
progress "made sure that ~/.kube exists"

# STEP 3. Merge the kubernetes configuration we just downloaded with
# the default kubernetes configuration. Set DEBUG to empty to
# quiet konfig. The resulting merged kubeconfig file will be in
# the default kubeconfig location (usually ~/.kube/config).
#
# Our modified version of konfig does the merge so that the configuration
# read from Vault is preferred over the default configuration in
# .kube/config. We do this so that if the Vault configuration ever changes
# the updated version will appear in the merged configuration.
DEBUG_SAVE="$DEBUG"
DEBUG=
${SCRIPTS_DIR}/konfig/konfig import --save "$KUBECONFIG_DEST" > /dev/null
DEBUG="$DEBUG_SAVE"
progress "merged $KUBECONFIG_DEST using konfig"

# STEP 4. Set the context.
kubectl config set-context "$KUBE_CONTEXT"
progress "set kube context to '$KUBE_CONTEXT'"

progress "finished with kube-get-credentials-kube-vault.sh"
