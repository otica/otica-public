#!/usr/bin/env bash

###############################################################################
# Get kubernetes cluster credentials from gcloud and verify
#
# Required environment variables:
#
# * KUBE_CLUSTER_NAME: the name of the Kubernetes cluster you are getting
#   credentials for.
#
###############################################################################

set -e

# ### ## ### ## ### ## ### ## ### ## ### ## ### ## ### ## ### ## ### #
set_zone_region() {
    # set zone_region_option to the correct GKE cluster zone or region.
    #
    # If the variable GKE_CLUSTER_AVAILABILITY_TYPE is set use that to
    # define zone_region_option otherwise use "gcloud container clusters list ...".
    progress_ts "entering set_zone_region"

    if [[ "$GKE_CLUSTER_AVAILABILITY_TYPE" == "zonal" ]]; then
        progress_ts "GKE_CLUSTER_AVAILABILITY_TYPE set to zonal"
        zone_region_option="--region=$GKE_CLUSTER_ZONE"
    elif [[ "$GKE_CLUSTER_AVAILABILITY_TYPE" == "regional" ]]; then
        progress_ts "GKE_CLUSTER_AVAILABILITY_TYPE set to regional"
        zone_region_option="--region=$GKE_CLUSTER_REGION"
    else
        progress_ts "GKE_CLUSTER_AVAILABILITY_TYPE not set so will get location using gcloud"
        info=($(gcloud container clusters list --filter "name:${KUBE_CLUSTER_NAME}" --format='value(zone,status)'))
        location=${info[0]}
        status=${info[1]}

        # region name has one '-', otherwise is a zone name
        if [ "$(echo $location | grep -o '-' | wc -l)" -eq "1" ]
        then
            zone_region_option="--region=$location"
        else
            zone_region_option="--zone=$location"
        fi
    fi

    progress_ts "zone_region_option set to '$zone_region_option'"
}


# ### ## ### ## ### ## ### ## ### ## ### ## ### ## ### ## ### ## ### #

THIS_DIR=$(dirname "$0")
source "$THIS_DIR/../functions.sh"

progress_ts "entering $(basename $0)"

progress_ts "getting information for GKE cluster '$KUBE_CLUSTER_NAME'"

if gcloud container clusters list --format=json | jq -r '.[].name' | grep ${KUBE_CLUSTER_NAME} &> /dev/null
then
#    info=($(gcloud container clusters list --filter "name:${KUBE_CLUSTER_NAME}" --format='value(zone,status)'))
#    location=${info[0]}
#    status=${info[1]}
#    # region name has 1 '-', otherwise is a zone name
#    if [ "$(echo $location | grep -o '-' | wc -l)" -eq "1" ]
#    then
#        opt="--region=$location"
#    else
#        opt="--zone=$location"
#    fi
    progress_ts "GKE_CLUSTER_REGION is $GKE_CLUSTER_REGION"

    # set the zone_region_option variable. We used the cached version (if cache
    # exists and has not expired).
    set_zone_region

    progress_ts "running 'gcloud container clusters get-credentials' ..."
    gcloud container clusters get-credentials "${KUBE_CLUSTER_NAME}" "$zone_region_option"
    progress_ts "finished running 'gcloud container clusters get-credentials'"
    echo "Current cluster status is: $status"
else
    echo "WARN: GKE cluster '${KUBE_CLUSTER_NAME}' is not found or status is not RUNNING"
    kubectl config set current-context UNKNOWN &> /dev/null
fi
