#!/usr/bin/env bash
set -e

###############################################################################
#
# AUTHENTICATE TO GCP
#
# Required environment variables:
# * GCP_PROJECT_ID: The GCP project id.
# * GCP_USER_AUTH_DOMAIN: The domain name to append to the username when authenticating.
#
###############################################################################

####
# FUNCTIONS
####

function local_progress() {
    local msg
    msg=$1
    progress_ts "[authenticate-gcp.sh] $msg"
}

function gcloud_auth_user() {
    local_progress "started gcloud_auth_user"
    >&2 echo "Authenticating with personal credentials..."

    # Allow USER override with GCP_USER
    USER=${USER:-unknown}
    user=${GCP_USER:-${USER}}
    local_progress "user is ${user}"

    # Set vars
    local_progress "about to run 'gcloud auth list ...'"
    auth_list_multiline=$(gcloud auth list --format json 2>/dev/null)
    auth_list=$(jq -c <<<$auth_list_multiline '.')
    local_progress "finished running 'gcloud auth list ...'"

    # Get the active user
    auth_user=${user}@${GCP_USER_AUTH_DOMAIN}
    local_progress "getting the active user from auth_list value of '${auth_list}' and auth_user '${auth_user}' ..."
    auth_user_active=$(echo "${auth_list}" | jq -r '.[] | select(.account == "'${auth_user}'") | .status == "ACTIVE"')

    local_progress "auth_user_active flag is '${auth_user_active}'"

    # Login user
    if [[ "${auth_user_active}" == "true" ]] ; then
        local_progress "${user} is active"
    else
        gcloud auth login --brief
    fi

    # Set 'application-default-credentials' with personal credentials if required
    if [[ -n ${REQUIRE_APPLICATION_DEFAULT_CREDENTIALS} ]] \
        && [[ ! -f ~/.config/gcloud/application_default_credentials.json ]]
    then
        >&2 echo "Set application-default-credentials with personal credentials..."
        gcloud auth application-default login
    fi
    local_progress "completed gcloud_auth_user"
}

function gcloud_auth_service_account() {
    ### Purpose: Authenticate to GCP using service account credentials passed
    ###          in as parameter $1.

    local_progress "started gcloud_auth_service_account"
    >&2 echo "Authenticating with service account credentials..."

    # Set vars
    key_file=$1

    # Activate service account
    gcloud auth activate-service-account --key-file "${key_file}"

    # Set 'application-default-credential'
    mkdir -p ~/.config/gcloud
    cp -f "${key_file}" ~/.config/gcloud/application_default_credentials.json
    local_progress "completed gcloud_auth_service_account"
}

#######################################
# Create a GCP configuration from the passed-in argument. Will fail
# if the GCP configuration already exists.
#
# Globals:
#   None
#
# Arguments:
#   $1: the name of the GCP configuration to create
#
# Returns:
#   None
#######################################
function create_gcp_configuration_aux(){
    local gcp_config_name="$1"

    msg="GCP configuration '$gcp_config_name' does not exist so will create"
    local_progress "$msg"
    gcloud config configurations create --activate ${gcp_config_name}
}

#######################################
# Create a GCP configuration from the passed-in argument. Checks first to
# see if the configuration already exists, and skips creation if it does
# already exist.
#
# Globals:
#   None
#
# Arguments:
#   $1: the name of the configuration
#
# Returns:
#   None
#######################################
function create_gcloud_configuration() {
    local gcp_config_name="$1"

    local_progress "creating GCP configuration '$gcp_config_name' (if necessary)"

    configurations=$(gcloud config configurations list --format json)
    configurations=$(jq -r -c <<<$configurations '.[].name')

    cnames=()
    while IFS= read -r line; do
        cnames+=("$line")
    done <<< "$configurations"

    already_exists="NO"
    for name in "${cnames[@]}"; do
        if [[ "$name" == "$gcp_config_name" ]]; then
            already_exists="YES"
        fi
    done

    if [[ "$already_exists" == "YES" ]]; then
        msg="the GCP configuration '$gcp_config_name' already_exists;"
        msg="${msg} no need to create"
        local_progress "$msg"
    else
        create_gcp_configuration_aux "$gcp_config_name"
    fi
}

function gcloud_config() {
    local_progress "started gcloud_config"
    >&2 echo "Setting up gcloud configurations..."

    # Create, if needed, the GCP configuration GCP_CONFIGURATION
    create_gcloud_configuration "$GCP_CONFIGURATION"

    # Set vars
    config_configurations_multiline=$(gcloud config configurations describe ${GCP_CONFIGURATION} --format json 2>/dev/null)
    config_configurations=$(jq -c <<<$config_configurations_multiline '.')

    local_progress "get cloud config from gcloud"
    disable_update_check=$(echo ${config_configurations} | jq -r '.properties.component_manager.disable_update_check')
    disable_usage_reporting=$(echo ${config_configurations} | jq -r '.properties.core.disable_usage_reporting')
    gcp_config_is_active=$(echo ${config_configurations} | jq -r '.is_active')
    gcp_project=$(echo ${config_configurations} | jq -r '.properties.core.project')
    gcp_region=$(echo ${config_configurations} | jq -r '.properties.compute.region')
    gcp_zone=$(echo ${config_configurations} | jq -r '.properties.compute.zone')

    if [[ "${gcp_config_is_active}" == "true" ]] ; then
        # Already activated config
        local_progress "configuration [${GCP_CONFIGURATION}] is already active."
    elif [[ "${gcp_config_is_active}" == "false" ]] ; then
        # Activate config
        gcloud config configurations activate "${GCP_CONFIGURATION}"
    else
        # Create a new gcloud conf if doesn't exist and activate it
        gcloud config configurations create --activate "${GCP_CONFIGURATION}"
    fi

    # Try set config from environment variables (env.sh)
    # config set core/project could fail *after* setting if the cluster conceals metadata, with
    # ERROR: gcloud crashed (MetadataServerException): The request is rejected. Please check if the metadata server is concealed.
    # we let it pass through because the GCP_PROJECT_ID, GCP_REGION, and GCP_ZONE do get set despite the crash error.
    # To make sure PROJECT env is always set for gcloud, you can pass CLOUDSDK_CORE_PROJECT variable.
    if [[ "${gcp_project}" != "${GCP_PROJECT_ID}" ]] ; then
        gcloud config set core/project "${GCP_PROJECT_ID}" > /dev/null 2>&1 || true
    fi
    if [[ "${disable_update_check}" != "${DISABLE_UPGRADE_CHECK}" ]] ; then
        gcloud config set component_manager/disable_update_check "${DISABLE_UPGRADE_CHECK}" > /dev/null 2>&1 || true
    fi
    if [[ "${disable_usage_reporting}" != "False" ]] ; then
        gcloud config set core/disable_usage_reporting False > /dev/null 2>&1 || true
    fi
    if [[ -n ${GCP_REGION} ]] ; then
        if [[ "${gcp_region}" != "${GCP_REGION}" ]] ; then
            gcloud config set compute/region "${GCP_REGION}" > /dev/null 2>&1 || true
        fi
    fi
    if [[ -n ${GCP_ZONE} ]] ; then
        if [[ "${gcp_zone}" != "${GCP_ZONE}" ]] ; then
            gcloud config set compute/zone "${GCP_ZONE}" > /dev/null 2>&1 || true
        fi
    fi
    local_progress "completed gcloud_config"
}

function do_auth() {
    local_progress "started do_auth"
    #
    ## AUTH SCENARIO 1
    #
    # If GOOGLE_APPLICATION_CREDENTIALS is set, use it for service account auth
    if [[ -n ${GOOGLE_APPLICATION_CREDENTIALS} ]] \
        && [[ -f "${GOOGLE_APPLICATION_CREDENTIALS}" ]]
    then
        # Authenticate
        gcloud_auth_service_account "${GOOGLE_APPLICATION_CREDENTIALS}"
        local_progress "completed do_auth"
        exit 0
    fi

    #
    ## AUTH SCENARIO 2
    #
    # Auth with key in GCP_KEY, the value may be base64 encoded
    # Used in case of CI/CD job
    if [[ -n ${GCP_KEY} ]] ; then
        >&2 echo "Get GCP Key from GCP_KEY."

        # Write to key_file
        key_file=$(mktemp)
        echo "${GCP_KEY}" > "${key_file}"
        if echo "${GCP_KEY}" | base64 --decode &> /dev/null ; then
            echo "${GCP_KEY}" | base64 --decode > "${key_file}"
        fi

        # Authenticate
        gcloud_auth_service_account "${key_file}"
        rm -f "${key_file}"
        local_progress "completed do_auth"
        exit 0
    fi

    #
    ## AUTH SCENARIO 3
    #
    # Auth with key in GCP_KEY_BASE64, the value must be base64 encoded
    if [[ -n ${GCP_KEY_BASE64} ]] ; then
        >&2 echo "Get GCP Key from GCP_KEY_BASE64."

        # Write to key_file
        key_file=$(mktemp)
        echo "${GCP_KEY_BASE64}" | base64 --decode > "${key_file}"

        # Authenticate
        gcloud_auth_service_account "${key_file}"
        rm -f "${key_file}"
        local_progress "completed do_auth"
        exit 0
    fi

    #
    ## AUTH SCENARIO 4
    #
    # Auth with personal credentials
    if [[ -n ${GCP_USER_AUTH} ]] && [[ "${GCP_USER_AUTH}" = true ]] ; then
        # Authenticate
        gcloud_auth_user
        local_progress "completed do_auth"
        exit 0
    fi

    #
    ## AUTH SCENARIO 5
    #
    # Auth with the key from vault GCP_KEY_PATH
    # Cache the key in local GCP_KEY_PATH to avoid pull from slow vault.
    if [[ -n ${GCP_KEY_PATH} ]] ; then
        if [[ ! -f "${GCP_KEY_FILE}" ]] ; then
            "${THIS_DIR}/vault-login.sh"
            mkdir -p $(dirname "${GCP_KEY_FILE}")
            "${THIS_DIR}/vault-read.sh" "${GCP_KEY_PATH}" > "${GCP_KEY_FILE}"
        fi
        # Authenticate
        gcloud_auth_service_account "${GCP_KEY_FILE}"
        local_progress "completed do_auth"
        exit 0
    fi

    #
    ## AUTH SCENARIO 6
    #
    # Catch All Authentication
    gcloud_auth_user
    local_progress "completed do_auth"
    exit 0
}

####
# MAIN
####

# Set vars
THIS_DIR=$(dirname "$0")
GCP_CONFIGURATION=${GCP_CONFIGURATION:-default}
DISABLE_UPGRADE_CHECK=${DISABLE_UPGRADE_CHECK:-true}

# Include functions
source ${THIS_DIR}/../functions.sh

local_progress "running auth/authenticate-gcp.sh"

# Required vars
if [[ -z ${GCP_PROJECT_ID} ]] ; then
    exit_with_error "missing required variable GCP_PROJECT_ID"
fi
if [[ -z ${GCP_USER_AUTH_DOMAIN} ]] ; then
    exit_with_error "missing required variable GCP_USER_AUTH_DOMAIN"
fi

# Configure gcloud
gcloud_config

# Perform Authentication
do_auth
