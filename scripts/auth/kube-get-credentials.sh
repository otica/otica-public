#!/usr/bin/env bash
set -e

###############################################################################
# Get Kubernetes credentials
#
# This is a simple wrapper around trhe various auth/kube-get-credentials-XXX.sh scripts.
#
# Required environment variables:
#
# * KUBE_PLATFORM: The name of the Kubernetes platform to authenticate to. MUST be one of the
#   the following:
#
#   - "gke"
#   - "eks"
#   - "kube-vault"
#
###############################################################################
THIS_DIR=$(dirname "$0")

# include functions
source $THIS_DIR/../functions.sh

### Stage 1. Verify that some needed variables are set.
if [[ -z "$KUBE_PLATFORM" ]]; then
    exit_with_error "missing required variable KUBE_PLATFORM"
else
    progress "Kubernetes platform: $KUBE_PLATFORM"
fi

# KUBE_CLUSTER_NAME is not needed by the kube-vault platform but
# IS needed by the other platforms.
if [[ "$KUBE_PLATFORM" != "kube-vault" ]]; then
    if [[ -z "$KUBE_CLUSTER_NAME" ]]; then
        exit_with_error "missing required varaible KUBE_CLUSTER_NAME"
    else
        progress "cluster name: $KUBE_CLUSTER_NAME"
    fi
else
    progress "vault-kube platform does not use KUBE_CLUSTER_NAME"
fi

# APP_NAMESPACE may not be same as the kubectl namespace (but all defaults
# to 'default'):
APP_NAMESPACE=${APP_NAMESPACE:-default}
KUBE_NAMESPACE=${KUBE_NAMESPACE:-$APP_NAMESPACE}
progress "Kubernetes namespace: $KUBE_NAMESPACE"

### Stage 2. Depending on KUBE_PLATFORM autenticate to the cloud and then
### get the kubernetes credentials.
if   [[ "$KUBE_PLATFORM" == "gke" ]]; then
	"$SCRIPTS_DIR"/auth/authenticate-gcp.sh
	"$SCRIPTS_DIR"/auth/kube-get-credentials-gke.sh
elif [[ "$KUBE_PLATFORM" == "eks" ]]; then
	"$SCRIPTS_DIR"/auth/authenticate-aws.sh
	"$SCRIPTS_DIR"/auth/kube-get-credentials-eks.sh
elif [[ "$KUBE_PLATFORM" == "kube-vault" ]]; then
	"$SCRIPTS_DIR"/auth/kube-get-credentials-kube-vault.sh
else
    exit_with_error "KUBE_PLATFORM '$KUBE_PLATFORM' not recognized"
fi


exit 0
