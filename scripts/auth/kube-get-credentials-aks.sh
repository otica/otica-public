#!/usr/bin/env bash

###############################################################################
# Get kubernetes cluster credentials from AWS and verify
#
# Required environment variables:
#
# * KUBE_CLUSTER_NAME: the name of the Kubernetes cluster you are getting credentials for.
#
#
###############################################################################

set -e

THIS_DIR=$(dirname "$0")
source "$THIS_DIR/../functions.sh"

progress "entering $(basename $0)"

AWS_REGION=${AWS_REGION:?'AWS_REGION not set'}

progress "getting information for AKS cluster '$KUBE_CLUSTER_NAME'"

if ! aws eks describe-cluster --name=${KUBE_CLUSTER_NAME} --region=${AWS_REGION} &> /dev/null; then
  echo "Failure running 'aws eks describe-cluster --name=${KUBE_CLUSTER_NAME} --region=${AWS_REGION}'"
  echo "Cannot run aws eks command. Forget to assume ${AWS_ROLE_NAME} role?"
  exit 1
fi

AWS_ROLE_ARN="arn:aws:sts::${AWS_ACCOUNT_ID}:assumed-role/${AWS_ROLE_NAME}"
if aws eks describe-cluster --name=${KUBE_CLUSTER_NAME} --region=${AWS_REGION} | grep ACTIVE &> /dev/null
then
  aws eks update-kubeconfig --name=${KUBE_CLUSTER_NAME} --region=${AWS_REGION}
else
  echo ${KUBE_CLUSTER_NAME} is not running.
fi

exit 0
