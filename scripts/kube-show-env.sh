#!/usr/bin/env bash

set -e

###############################################################################
# Show Kubernetes environment variables.
###############################################################################

THIS_DIR=$(dirname "$0")
source "$THIS_DIR/functions.sh"

progress "entering $(basename $0)"

###############################################################################
echo_result () {
    if empty_var "$1"; then
        msg="<EMPTY>"
    else
        msg="${!1}"
    fi

    printf '%-21s%s\n' "$1:" "$msg"
}
###############################################################################

# We only recognize these values for KUBE_PLATFORM:
declare -A valid_platforms
valid_platforms[gke]=1
valid_platforms[eks]=1
valid_platforms[kube-vault]=1

if [[ -z "${valid_platforms[$KUBE_PLATFORM]}" ]]; then
    exit_with_error "KUBE_PLATFORM '$KUBE_PLATFORM' not supported"
fi

# This next section puts the variables we want to display in the array
# kube_variables. Depending on the platform and other things the variables
# we want to display can vary.
kube_variables=()

# We only care about CLOUD_PLATFORM if KUBE_PLATFORM is _not_ 'kube-vault'
if [[ "$KUBE_PLATFORM" != "kube-vault" ]]; then
    kube_variables+=(CLOUD_PLATFORM)

    if [[ "$KUBE_PLATFORM" = "gke" ]]; then
        kube_variables+=(GCP_PROJECT_ID)
    fi
fi

kube_variables+=(KUBE_PLATFORM)

if [[ "$KUBE_PLATFORM" = "kube-vault" ]]; then
    kube_variables+=(KUBE_CONTEXT KUBECONFIG_SEC_PATH)
else
    kube_variables+=(KUBE_CLUSTER_NAME)
fi

kube_variables+=(KUBE_NAMESPACE)

for variable in "${kube_variables[@]}"
do
    echo_result "$variable"
done

exit 0
