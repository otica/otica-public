#!/usr/bin/env bash

source functions.sh

########################################################
pass_message() {
    progress "pass: $1"
}

check_var () {
    if empty_var $1; then
        exit_with_error "$1 is not set"
    else
        pass_message "$1 is set (\"${!1}\")"
    fi
}

progress () {
    if [[ "$VERBOSE" == "1" ]]; then
        echo "$1"
    fi
}
########################################################

check_var "HELM_BUILD_DIR"
check_var "HELM_REPO_URL"
check_var "HELM_CHART_NAME"

# If HELM_REPO_URL is a 'gs://' repo make sure that the helm gcs plugin
# is installed.
regex='^gs://.*$'
if [[ "$HELM_REPO_URL" =~ $regex ]]; then
    progress "info: HELM_REPO_URL is a Google Gloud Storage (GCS) repository"

    # Is the plugin installed?
    result=$(helm plugin list | grep 'gcs.*Google')
    if [[ -z "$result" ]]; then
        exit_with_error "the Helm GCS plugin is not installed"
    fi
fi

# If HELM_REPO_URL is an 'oci://' repo make sure that the HELM_REPO_NAME
# environment is not set.
regex='^oci://.*$'
if [[ "$HELM_REPO_URL" =~ $regex ]]; then
    progress "info: HELM_REPO_URL is an Open Container Initiative (OCI) repository"

    # Is HELM_REPO_NAME defined?
    if [[ -n "$HELM_REPO_NAME" ]]; then
        exit_with_error "cannot have HELM_REPO_NAME defined with an OCI-based registry"
    else
        pass_message "HELM_REPO_NAME is not set (this is appropriate as HELM_REPO_URL is OCI-based)"
    fi
else
    check_var "HELM_REPO_NAME"
fi

exit 0
