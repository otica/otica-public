#!/usr/bin/env bash
set -e

# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/../functions.sh
source $THIS_DIR/gitlab.sh

function slack_json(){
    cat <<EOF 
#!vault2chart
{
    "active": ${SLACK_ON},
    "commit_events": ${SLACK_COMMIT_EVENTS_ON},
    "confidential_issues_events": ${SLACK_CONFIDENTIAL_ISSUE_EVENTS_ON},
    "confidential_note_events": ${SLACK_CONFIDENTIAL_NOTE_EVENTS_ON},
    "deployment_events": ${SLACK_DEPLOYMENT_EVENTS_ON},
    "issues_events": ${SLACK_ISSUES_EVENTS_ON},
    "job_events": ${SLACK_JOB_EVENTS_ON},
    "merge_requests_events": ${SLACK_MERGE_REQUESTS_EVENTS_ON},
    "note_events": ${SLACK_NOTE_EVENTS_ON},
    "pipeline_events": ${SLACK_PIPELINE_EVENTS_ON},
    "push_events": ${SLACK_PUSH_EVENTS_ON},
    "tag_push_events": ${SLACK_TAG_PUSH_EVENTS_ON},
    "wiki_page_events": ${SLACK_WIKI_PAGE_EVENTS_ON},
    "webhook": "%%${SLACK_WEBHOOK_PATH}%%",
    "channel": "#${SLACK_GITLAB_CHANNEL}",
    "username": "${SLACK_USER}",
    "notify_only_broken_pipelines": ${NOTIFY_ONLY_BROKEN_PIPELINES},
    "branches_to_be_notified": "${BRANCHES_TO_BE_NOTIFIED}",
    "pipeline_channel": "#${SLACK_CICD_CHANNEL}"
}
EOF
}

######
# Main
######

for k in "GITLAB_REPO" "SLACK_WEBHOOK_PATH" "SLACK_GITLAB_CHANNEL"; do
    if empty_var $k; then
        echo "Error: $k is missing"
        exit 1
    fi
done

SLACK_ON=${1:-true}
SLACK_COMMIT_EVENTS_ON=${SLACK_COMMIT_EVENTS_ON:-true}
SLACK_CONFIDENTIAL_ISSUE_EVENTS_ON=${SLACK_CONFIDENTIAL_ISSUE_EVENTS_ON:-false}
SLACK_CONFIDENTIAL_NOTE_EVENTS_ON=${SLACK_CONFIDENTIAL_NOTE_EVENTS_ON:-false}
SLACK_DEPLOYMENT_EVENTS_ON=${SLACK_DEPLOYMENT_EVENTS_ON:-false}
SLACK_ISSUES_EVENTS_ON=${SLACK_ISSUES_EVENTS_ON:-true}
SLACK_JOB_EVENTS_ON=${SLACK_JOB_EVENTS_ON:-false}
SLACK_MERGE_REQUESTS_EVENTS_ON=${SLACK_MERGE_REQUESTS_EVENTS_ON:-true}
SLACK_NOTE_EVENTS_ON=${SLACK_NOTE_EVENTS_ON:-true}
SLACK_PIPELINE_EVENTS_ON=${SLACK_PIPELINE_EVENTS_ON:-true}
SLACK_PUSH_EVENTS_ON=${SLACK_PUSH_EVENTS_ON:-true}
SLACK_TAG_PUSH_EVENTS_ON=${SLACK_TAG_PUSH_EVENTS_ON:-true}
SLACK_WIKI_PAGE_EVENTS_ON=${SLACK_WIKI_PAGE_EVENTS_ON:-true}
SLACK_USER=${SLACK_USER:-$GITLAB_REPO}
SLACK_CICD_CHANNEL=${SLACK_CICD_CHANNEL:-$SLACK_GITLAB_CHANNEL}
BRANCHES_TO_BE_NOTIFIED=${SLACK_BRANCHES_TO_BE_NOTIFIED:-all}
NOTIFY_ONLY_BROKEN_PIPELINES=${SLACK_NOTIFY_ONLY_BROKEN_PIPELINES:-false}

proj_id=$(get_project_id ${GITLAB_REPO})

slack_json | render.sh | gitlab_put_stdin projects/$proj_id/services/slack | grep "Slack notifications" > /dev/null

if [[ "true" == "$SLACK_ON" ]]; then
    echo "Set slack notifications for ${GITLAB_REPO} is ON"
else
    echo "Set slack notifications for ${GITLAB_REPO} is OFF"
fi
