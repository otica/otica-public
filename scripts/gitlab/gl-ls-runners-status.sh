#!/usr/bin/env bash
set -e

# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/gitlab.sh

status=$1
# https://docs.gitlab.com/ee/api/runners.html#list-all-runners
if ! [[ "$status" =~ ^(active|paused|online|offline)$ ]]; then
    echo "$status has to be active|paused|online|offline."
    exit 1
fi
gitlab_get runners/all?status=$status | jq -r '.[]'
