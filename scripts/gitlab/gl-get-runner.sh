#!/usr/bin/env bash
set -e

# get runner by description

# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/gitlab.sh

description=$1
if [ ! -z "$description" ]; then
    id=$(gitlab_get runners/all?per_page=${GITLAB_PER_PAGE_MAX} | jq -r --arg v $description '.[] | select(.description==$v) | .id')
    if [ ! -z "$id" ]; then
        gitlab_get runners/$id | jq -r
    else
        gitlab_get runners/$1 | jq -r
    fi
fi
