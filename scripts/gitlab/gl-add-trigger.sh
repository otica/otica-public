#!/usr/bin/env bash
set -e

# Ref: https://docs.gitlab.com/ee/api/pipeline_triggers.html

# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/../functions.sh
source $THIS_DIR/gitlab.sh

######
# Main
######
proj_id=$(get_project_id ${GITLAB_REPO})
if [ -z "${proj_id}" ]; then
    echo "Error: $GITLAB_REPO doesn't exist or not accessible."
    exit 1
fi

GITLAB_TRIGGER_TOKEN_PATH=${GITLAB_TRIGGER_TOKEN_PATH:-"${SEC_PATH}/cicd/${GITLAB_REPO}/trigger_token"}

if gitlab_get /projects/$proj_id/triggers | grep token > /dev/null; then
    echo "${GITLAB_REPO} trigger exists!"
    exit 0
fi

r=$(gitlab_post projects/$proj_id/triggers -F 'description=gl-trigger')
token=$(echo $r | jq -r '.token')
if is_null_or_empty $token ; then
    echo "Adding ${GITLAB_REPO} trigger is failed: $r"
else
    $THIS_DIR/vault-write.sh ${GITLAB_TRIGGER_TOKEN_PATH} $token > /dev/null
    echo "Added trigger to ${GITLAB_REPO} and token saved to vault://${GITLAB_TRIGGER_TOKEN_PATH}"
fi

