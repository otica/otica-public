#!/usr/bin/env bash
set -e

# Delete all offline runners.

# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/gitlab.sh

token=$(cat $GITLAB_TOKEN_FILE)
url=$GITLAB_SERVER

# If id is given, remove the id and exit.
if [ ! -z "$1" ]; then
   curl --request DELETE --header "PRIVATE-TOKEN:${token}" "${url}/api/v4/runners/$1"
   exit 0
fi
# Remove all offline runners
curl -s --header "PRIVATE-TOKEN:${token}" "${url}/api/v4/runners/all?per_page=${GITLAB_PER_PAGE_MAX}" \
  | jq '.[] | select(.status == "offline") | .id' | tee /tmp/removed-ids \
  | xargs -I runner_id curl --request DELETE --header "PRIVATE-TOKEN:${token}" "${url}/api/v4/runners/runner_id"

if [ -s "/tmp/removed-ids" ]; then
  echo Removed $(cat /tmp/removed-ids)
fi
