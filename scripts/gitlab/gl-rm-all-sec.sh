#!/usr/bin/env bash
set -e

# Ref: https://docs.gitlab.com/ee/api/project_level_variables.html#create-variable
# Remove all gitlab secrets defined in a file, $1
#
# The file format is
#   <sec_name> <sec_value> <sec_options> ....
#   sec_value may be one of:
#           vault://<path>
#           base64vault://<path>
#           file://<path>
#           base64file://<path>
#           string_value
# 
# <sec_options>: 
#     --form variable_type=[env_var|file]
#     --form protected=[yes|no]
#     --form masked=[yes|no]


# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/gitlab.sh

function get_key() {
    echo $1
}

function rm_all() {
    id=$(get_project_id ${GITLAB_REPO})
    grep -v '^#' $1 | grep -v -e '^$' |
    while read -r line; do
        key=$(get_key $line)
        delete_project_var "$id" "$key" > /dev/null
        echo Removed $key from ${GITLAB_REPO} vars
    done
}

if [ ! -f "$1" ]; then 
    echo "Error: .gitlab-ci.sec file is missing"
    exit 1
fi

rm_all $1
