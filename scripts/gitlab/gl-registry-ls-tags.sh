#!/usr/bin/env bash
set -e
set -u

# List registry image tags
# 
# This script requires access rights to the gs://${REGISTRY_BUCKET}/${REGISTRY_ENVIRONMENT}/docker/registry/v2/repositories

# Follow envvars must defined
REGISTRY_BUCKET=${REGISTRY_BUCKET:-prod-my-services-container-registry}
REGISTRY_ENVIRONMENT=${GCP_ENVIRONMENT:-prod}
REGISTRY_BASE_URL=gs://${REGISTRY_BUCKET}/${REGISTRY_ENVIRONMENT}/docker/registry/v2/repositories
REGISTRY_DEFAULT=$(git config --get remote.origin.url | cut -d: -f2)
REGISTRY=${REGISTRY:-$REGISTRY_DEFAULT}

echo gsutil list ${REGISTRY_BASE_URL}/${REGISTRY}/_manifests/tags
gsutil list ${REGISTRY_BASE_URL}/${REGISTRY}/_manifests/tags
