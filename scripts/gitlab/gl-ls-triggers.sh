#!/usr/bin/env bash
set -e

# Ref: https://docs.gitlab.com/ee/api/pipeline_triggers.html

# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/gitlab.sh

# Set default trigger token vault path if not defined
GITLAB_TRIGGER_TOKEN_PATH=${GITLAB_TRIGGER_TOKEN_PATH:-"${SEC_PATH}/cicd/${GITLAB_REPO}/trigger_token"}

proj_id=$(get_project_id ${GITLAB_REPO})
gitlab_get /projects/$proj_id/triggers
