#!/usr/bin/env bash
set -e

# Ref: https://docs.gitlab.com/ee/api/merge_requests.html#accept-mr
# Usage: $0 <merge_request_id> <project_id>

# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/../functions.sh
source $THIS_DIR/gitlab.sh

######
# Main
######
mid=$1
proj=${2:-${GITLAB_REPO}}
proj_id=$(get_project_id $proj)

if [ -z "$mid" ] || [ -z "$proj_id" ]; then
    echo "Usage: $0 <merge_request_id> <project_id>"
    exit 1
fi

gitlab_put /projects/${proj_id}/merge_requests/${mid}/merge

