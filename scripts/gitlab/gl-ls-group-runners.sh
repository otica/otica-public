#!/usr/bin/env bash
set -e

# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/gitlab.sh

gitlab_get groups/$1/runners | jq -r '.[]'
gitlab_get groups/$1/runners
