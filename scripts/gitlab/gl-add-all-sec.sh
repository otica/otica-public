#!/usr/bin/env bash
set -e

# Ref: https://docs.gitlab.com/ee/api/project_level_variables.html#create-variable
# Add all gitlab secrets defined in a file, $1
#
# The file format is
#   <sec_name> <sec_value> <sec_options> ....
#   sec_value may be one of:
#           file://<path>                               get the value from the file
#           base64file://<path>                         get the base64 encoded value from the file
#           vault://<path>                              get the value from vault path
#           base64vault://<path>                        get the base64 encoded value value from vault path
#           render://<template_to_be_rendered>          get the value by render the give template file
#           base64render://<template_to_be_rendered>    get the base64 encoded value by render the give template file       
#           string_value
# 
# <sec_options>: 
#     -d variable_type=[env_var|file]
#     -d protected=[yes|no]
#     -d masked=[yes|no]

# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/gitlab.sh

function get_key() {
    echo $1
}
function get_sec() {
    echo $2
}
function get_options() {
    shift
    shift
    # for backward compatible: need to convert --form to --data in opt
    # what a hack to fix urlencode issue!
    opt=$*
    oldValue="--form"
    newValue="-d"
    opt=${opt//$oldValue/$newValue}
    oldValue="-F"
    newValue="-d"
    opt=${opt//$oldValue/$newValue}
    echo $opt
}

function add_all() {
    id=$(get_project_id ${GITLAB_REPO})
    grep -v '^#' $1 | grep -v -e '^$' | render.sh |
    while read -r line; do
        key=$(get_key $line)
        sec=$(get_sec $line)
        [[ -z "$key" ]] || [[ -z "$sec" ]] && continue
        value=$(get_value $sec)
        update_project_var "$id" "$key" "$value" $(get_options $line)  | grep $key > /dev/null
        echo Added $key=$sec to ${GITLAB_REPO} vars
    done
}

if [ ! -f "$1" ]; then 
    echo "Error: .gitlab-ci.sec file is missing"
    exit 1
fi

add_all $1
