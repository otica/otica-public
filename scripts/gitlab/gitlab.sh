#!/usr/bin/env bash

###############################################################################
# Gitlab API helper functions
# Ref https://docs.gitlab.com/ee/api/api_resources.html
# Required ENVs:
#   GITLAB_SERVER       - no default
#   GITLAB_TOKEN_FILE   - default to ${HOME}/.gitlab_token
###############################################################################
THIS_DIR=$(dirname "$0")
source $THIS_DIR/../functions.sh

if [[ -z "$GITLAB_SERVER" ]]; then
    exit_with_error "missing required variable GITLAB_SERVER"
fi

GITLAB_API=${GITLAB_API:-${GITLAB_SERVER}/api/v4}
GITLAB_TOKEN_FILE=${GITLAB_TOKEN_FILE:-${HOME}/.gitlab-token}
GITLAB_PER_PAGE_MAX=100    # max is 100

# Set gitlab api token
if [[ -z "$GITLAB_TOKEN" ]] && [[ -f "$GITLAB_TOKEN_FILE" ]]; then
    export GITLAB_TOKEN=$(cat $GITLAB_TOKEN_FILE)
fi

if [[ -z "$GITLAB_TOKEN" ]]; then
    echo "Error: GITLAB_TOKEN is missing and it is required by gitlab API calls."
    echo "  Please create one at ${GITLAB_SERVER}/profile/personal_access_tokens"
    echo "  and save it in the GITLAB_TOKEN_FILE ($GITLAB_TOKEN_FILE)."
    exit 1
fi

# Get value from
#           file://<path>                               get the value from the file
#           base64file://<path>                         get the base64 encoded value from the file
#           vault://<path>                              get the value from vault path
#           base64vault://<path>                        get the base64 encoded value value from vault path
#           render://<template_to_be_rendered>          get the value by render the give template file
#           base64render://<template_to_be_rendered>    get the base64 encoded value by render the give template file
#           string_value

function get_value() {
    local value=$1
    if [[ $value == file://* ]]; then
        # get the value from the file,
        # NOTE make sure the file has no unwanted trailing '\n', e.g. added by vim
        cat "${value#file://}"
    elif [[ $value == base64file://* ]]; then
        # get the value from the file and base64 encode it
        cat ${value#base64file://} | base64 | tr -d '\n'
    elif [[ $value == vault://* ]]; then
        # get the value from vault path
        vault-read.sh ${value#vault://}
    elif [[ $value == base64vault://* ]]; then
        # get the value from vault path and base64 encode it
        vault-read.sh ${value#base64vault://} | base64 | tr -d '\n'
    elif [[ $value == properties://* ]]; then
        # Deprecated, should use `render://` instead
        # render the give properties file as the value
        cat "${value#properties://}" | render.sh
    elif [[ $value == render://* ]]; then
        # render the give template file as the value
        cat "${value#render://}" | render.sh
    elif [[ $value == base64render://* ]]; then
        # render the give template file as the value and base64 encoded
        cat "${value#base64render://}" | render.sh | base64 | tr -d '\n'
    else
        echo -n "$value"
    fi
}

# gitlab_xxx <api_path> <options>
function gitlab_get() {
    api_path=$1; shift
    curl -s \
        --request GET \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        "${GITLAB_API}/$api_path"
}

function gitlab_post() {
    api_path="$1"; shift
    curl -s \
        --request POST \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        "${GITLAB_API}/$api_path" $@
}

# post data from stdin
function gitlab_post_stdin() {
    api_path="$1"; shift
    cat /dev/stdin | curl -s \
        --request POST \
        --header "Content-Type: application/json; charset=utf-8" \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        "${GITLAB_API}/$api_path" -d @-
}

function gitlab_put() {
    api_path="$1"; shift
    curl -s \
        --request PUT \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        "${GITLAB_API}/$api_path" $@
}

# put data from stdin
function gitlab_put_stdin() {
    api_path="$1"; shift
    cat /dev/stdin | curl -s \
        --request PUT \
        --header "Content-Type: application/json; charset=utf-8" \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        "${GITLAB_API}/$api_path" -d @-
}

function gitlab_delete() {
    api_path="$1"; shift
    curl -s \
        --request DELETE \
        --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
        "${GITLAB_API}/$api_path" $@

}

# list per_page groups in the page
# eg. list_groups per_page=50 page=2 will return second page of 50 groups
function list_groups() {
    gitlab_get "groups?all_available=true"
}

# get group by name
function get_group() {
    gitlab_get "groups?all_available=true&search=$1" \
        | jq -r --arg v "$1" '.[] | select(.name==$v)'
}

# get group id by full path
function get_group_id() {
    urlencode $1
    # get numeric id
    # get_group $1 | jq -r '.id'
}

# get namespace id by full path
function get_namespace_id() {
    # get numeric id
    get_group $1 | jq -r '.id'
}

# get all subgroups of group id
function get_subgroups() {
    gitlab_get "groups/$1/subgroups?all_available=true"
}

# list per_page projects in the page
# eg. list_projects per_page=50 page=2 will return second page of 50 projects
function list_projects() {
    gitlab_get "projects?simple=true"
}

# get project by full path
function get_project() {
    full_name="$1"
    name=${full_name##*/}
    gitlab_get "projects?simple=true&search=$name&per_page=100&all_available=true" \
        | jq -r --arg p "$full_name" '.[] | select(.path_with_namespace==$p)'
}

# get project id by full path
function get_project_id() {
    urlencode $1

    # get numeric id
    # get_project $1 | jq -r '.id'
}

# get project var by project_id and var name
function get_project_var()  {
    if [[ $# -ge "2" ]]; then
        gitlab_get "projects/$1/variables/$2"
    fi
}

# get project var value by project_id and var name
function get_project_var_value()  {
    if [[ $# -ge "2" ]]; then
        gitlab_get "projects/$1/variables/$2" | jq -r '.value'
    fi
}

function delete_project_var()  {
    if [[ $# -ge "2" ]]; then
        gitlab_delete "projects/$1/variables/$2"
    fi
}

# crate or update_project_var <projct_id> <key> <value> <options>
# Note: need to call curl directly in order to pass some of jwt value
function update_project_var()  {
    if [[ $# -ge "3" ]]; then
        id=$1; shift
        key=$1; shift
        value=$1; shift
        if get_project_var $id $key | grep $key > /dev/null; then
            curl -s \
                --request PUT \
                --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
                "${GITLAB_API}/projects/$id/variables/$key" --data-urlencode "value=$value" $@
        else
            curl -s \
                --request POST \
                --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
                "${GITLAB_API}/projects/$id/variables"  --data-urlencode "key=$key" --data-urlencode "value=$value" $@
        fi
    fi
}

# Testing
# id=$(get_project_id XXXXX/docker-nodeapp)
# delete_project_var $id FOO; echo
# update_project_var $id FOO "test;value&" -d variable_type=env_var
# get_project_var_value $id FOO; echo
# delete_project_var $id FOO2; echo
# echo '{"variable_type":"env_var","key":"FOO2","value":"test2;value&","protected":false,"masked":false}' | gitlab_post_stdin projects/$id/variables
# get_project_var_value $id FOO2; echo
