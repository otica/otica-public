#!/usr/bin/env bash
set -e

# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/gitlab.sh

gitlab_get runners/all?per_page=${GITLAB_PER_PAGE_MAX} | jq -r '.[]'
