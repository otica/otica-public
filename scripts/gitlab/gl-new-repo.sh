#!/usr/bin/env bash
set -e

# Ref: https://docs.gitlab.com/ee/api/pipeline_triggers.html

# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/../functions.sh
source $THIS_DIR/gitlab.sh

######
# Main
######
proj=$(get_project ${GITLAB_REPO})
if ! [ -z "${proj}" ]; then
    echo "Error: $GITLAB_REPO exist."
    exit 1
fi

GITLAB_TRIGGER_TOKEN_PATH=${GITLAB_TRIGGER_TOKEN_PATH:-"${SEC_PATH}/cicd/${GITLAB_REPO}/trigger_token"}

if gitlab_get /projects/$proj_id/triggers | grep token > /dev/null; then
    echo "${GITLAB_REPO} trigger exists!"
    exit 1
fi

GITLAB_REPO_VISIBILITY=${GITLAB_REPO_VISIBILITY:-private}

namespace=$(dirname ${GITLAB_REPO})
namespace_id=$(get_namespace_id $namespace)

r=$(gitlab_post projects \
    -F "name=$(basename ${GITLAB_REPO})" \
    -F "namespace_id=$namespace_id" \
    -F "visibility=$GITLAB_REPO_VISIBILITY"
)
name=$(echo $r | jq -r ".name")
if is_null_or_empty $name ; then
    echo "Creating ${GITLAB_REPO} is failed: $r"
else
    echo "${GITLAB_REPO} is created."
fi

