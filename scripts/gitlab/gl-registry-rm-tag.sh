#!/usr/bin/env bash
set -e
set -u

# Delete image tags from registry
# This script requires access rights to the gs://${REGISTRY_BUCKET}/${REGISTRY_ENVIRONMENT}/docker/registry/v2/repositories
# Usage: $0 <image_tag>

# Follow envvars must defined
REGISTRY_BUCKET=${REGISTRY_BUCKET:-prod-my-services-container-registry}
REGISTRY_ENVIRONMENT=${GCP_ENVIRONMENT:-prod}
REGISTRY_BASE_URL=gs://${REGISTRY_BUCKET}/${REGISTRY_ENVIRONMENT}/docker/registry/v2/repositories
REGISTRY_DEFAULT=$(git config --get remote.origin.url | cut -d: -f2)
REGISTRY=${REGISTRY:-$REGISTRY_DEFAULT}

tag=$1
if [ -z "${tag}" ]; then
    echo "Usage: $0 <image_tag>"
    exit 1
fi

echo "Are you sure to delete ${REGISTRY}:${tag} permanently? [Y/N]: "; read ANSWER; \
if [ ! "$ANSWER" = "Y" ]; then \
    echo "Exiting." ; exit 1 ; \
fi

gsutil rm -r ${REGISTRY_BASE_URL}/${REGISTRY}/_manifests/tags/${tag}
