#!/usr/bin/env bash
set -e

# Fix a corrupted image tag by pushing the alpine to the tag

image_tag=$1
if [ -z "${image_tag}" ]; then
    echo "Usage: $(basename $0) <image_tag>"
    echo "For example: $(basename $0) irt-as/apps/test/test-api:23d1b4ce58123660389863ee04216d4300790ee7"
    exit 1
fi

set -u

if [[ -z "$CI_REGISTRY" ]]; then
    echo "missing required environment variable CI_REGISTRY"
    exit 1
fi

CI_REGISTRY_USER=${CI_REGISTRY_USER:-gitlab-ci-token}
CI_REGISTRY_PASSWORD=${CI_REGISTRY_PASSWORD:-$(cat $HOME/.gitlab-token)}

# Login to registry
echo $CI_REGISTRY_PASSWORD | docker login --username $CI_REGISTRY_USER --password-stdin $CI_REGISTRY

docker pull alpine:latest
docker tag alpine:latest ${image_tag}
docker push ${image_tag}

echo "Now you should be able to replace or delete the <image_tag>."
