#!/usr/bin/env bash
set -e

# get runner token by description

# runnerToken field is remove in GitLab 13.0.0. This code returns nothing.
# include gitlab hepler functions
THIS_DIR=$(dirname "$0")
source $THIS_DIR/gitlab.sh

description=$1
if [ ! -z "$description" ]; then
    id=$(gitlab_get runners/all?per_page=${GITLAB_PER_PAGE_MAX} | jq -r --arg v $description '.| if type=="array" then .[] else . end | select(.description==$v and .status=="online") | .id')
    if [ ! -z "$id" ]; then
        gitlab_get runners/$id | jq -r '.token'
    fi
fi
