#!/usr/bin/env bash
set -e

if [[ -z "$CI_REGISTRY" ]]; then
    echo "missing required environment variable CI_REGISTRY"
    exit 1
fi

CI_REGISTRY_USER=${CI_REGISTRY_USER:-gitlab-ci-token}
CI_REGISTRY_PASSWORD=${CI_REGISTRY_PASSWORD:-$(cat $HOME/.gitlab-token)}

echo $CI_REGISTRY_PASSWORD | docker login --username $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
