#!/usr/bin/env bash

###################################################################################
# Diff kubernetes configurations specified by filename or stdin between the current 
# online configuration, and the configuration as it would be if applied. 
# Require kubernetes cluster 1.13 and above.
###################################################################################

THIS_DIR=$(dirname "$0")
PATH="${THIS_DIR}:${PATH}"

# include functions
source $THIS_DIR/functions.sh
source env.sh

# fail on error or undeclared vars
trap_errors

# optional vars
set +u
debug=$debug
set -u

template_files=$@

for template in $template_files; do
  if [ "$debug" = "true" ]; then
    cat $template | render.sh
  else
    echo Apply diff with online configuration with $template
    cat $template | render.sh | kubectl diff -f -
  fi
done