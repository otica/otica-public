#!/usr/bin/env bash

source functions.sh

########################################################
check_var () {
    if empty_var $1; then
        exit_with_error "$1 is not set"
    else
        progress "pass: $1 is set (\"${!1}\")"
    fi
}

progress () {
    if [[ "$VERBOSE" == "1" ]]; then
        echo "$1"
    fi
}
########################################################

check_var "HELM_REPO_URL"

# Check that HELM_REPO_URL starts with 'gs://'.
regex='^gs://.*$'
if [[ ! "$HELM_REPO_URL" =~ $regex ]]; then
    exit_with_error "HELM_REPO_URL does not start with 'gs://'"
fi
