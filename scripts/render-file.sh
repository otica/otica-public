#!/usr/bin/env bash

# Use:
#   render-file.sh <filename>
#
# Runs the file <filename> through render2.sh.
#
# Value of environment variable RENDER_MODE:
#
# * "NONE": does not run <filename> through render2.sh, i.e.,
#   <filename> remains unchanged.
#
# * "ALL": runs <filename> through render2.sh
#
# * "TMPL": runs <filename> through render2.sh but ONLY if <filename> has
#   the form "SOMETHING.tmpl.EXT"; the resulting file name will be
#   "SOMETHING.EXT". NOTE!! In this case the original ".tmpl" file is
#   is DELETED!
#
# * unset: if RENDER_MODE is unset then behave as if it were set to "ALL".
#
# The resulting file will be in the same directory as <filename>.

THIS_DIR=$(dirname "$0")
source "$THIS_DIR"/functions.sh

# Call the functions.sh function render_file:
render_file "$1"

exit 0
