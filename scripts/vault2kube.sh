#!/usr/bin/env bash
set -e

# Usage:
#   vault2kube.sh <secret.yml>

# Replace %%vault-secret-path%% with the Vault value stored at
# <vault-secret-path>. The output is always Base64-encoded even if the
# secret stored in Vault was stored as plain text.

OLDIFS=$IFS
IFS=''
while read line
do
    path=$(echo $line | grep -v '^#' | grep -o -e '%%.*%%' | tr -d '%')
    if [ -z $path ]
    then
        echo $line
    else
        # Assemble secret from the value retrieved from Vault.
        pre="${line%%%%${path}%%*}"
        post="${line##*%%${path}%%}"
        # set pipefail to catch the vault-read.sh error
        value=$(set -o pipefail; vault-read.sh $path | base64 | tr -d '\n')
        echo "${pre}${value}${post}"
    fi
done < "${1:-/dev/stdin}"
IFS=$OLDIFS
