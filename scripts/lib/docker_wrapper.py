"""A class for Docker development.

"""

# pylint: disable=invalid-name
# pylint: disable=superfluous-parens

import logging
import os
import re
import subprocess
import sys
import yaml

from otica_abstract import OticaAbstract

class DockerWrapper(OticaAbstract):
    """
    The main class for our docker wrapper script.
    """

    docker_env_vars = [
        'DOCKER_SOURCE_DIR',
        'DOCKER_REGISTRY',
        'DOCKER_NAMESPACE',
        'DOCKER_IMAGE',
        'DOCKER_IMAGE_TAGS',
        'DOCKER_IMAGE_TAGS_HOOK',
        'DOCKER_REGISTRY_USERNAME',
        'DOCKER_REGISTRY_PASSWORD_PATH',
        'DOCKER_REGISTRY_LOGIN_TYPE',
    ]

    docker_required_env_vars = [
        'DOCKER_BUILD_DIR',
        'DOCKER_REGISTRY',
        'DOCKER_IMAGE',
        'DOCKER_REGISTRY_USERNAME',
        'DOCKER_REGISTRY_PASSWORD_PATH',
    ]

    docker_deprecated_env_vars = {
    }

    docker_unsupported_env_vars = {
        'DOCKER_IMAGE_TAG': 'use DOCKER_IMAGE_TAGS in place of DOCKER_IMAGE_TAG',
    }

    docker_env_var_to_default  = {
        'DOCKER_REGISTRY_LOGIN_TYPE': 'vault',
        'DOCKER_SOURCE_DIR':          '../common/docker',
    }

    build_args_filename = 'build-args.yaml'

    # The name of the file to write the kaniko command file in
    # DOCKER_BUILD_DIR
    kaniko_cmd_file = 'kaniko-cmd'

    def __init__(self,
                 verbose=False,
                 no_cache=False,
                 dryrun=False,
                 ):
        """Initialize object
        """
        super().__init__(verbose=verbose)

        self._env_vars             = DockerWrapper.docker_env_vars
        self._required_env_vars    = DockerWrapper.docker_required_env_vars
        self._deprecated_env_vars  = DockerWrapper.docker_deprecated_env_vars
        self._unsupported_env_vars = DockerWrapper.docker_unsupported_env_vars
        self._env_var_to_default   = DockerWrapper.docker_env_var_to_default

        # Check for missing required environment variables and set some defaults.
        self.check_env()
        self.set_defaults()

        self.no_cache = no_cache   # pylint: disable=bad-whitespace
        self.dryrun   = dryrun     # pylint: disable=bad-whitespace
        self.tags     = []         # pylint: disable=bad-whitespace

        # Construct properties based on the environment variables.
        self.registry      = os.environ['DOCKER_REGISTRY']
        self.image         = os.environ['DOCKER_IMAGE']
        self.build_dir     = os.environ['DOCKER_BUILD_DIR']
        self.source_dir    = os.environ['DOCKER_SOURCE_DIR']
        self.login_type    = os.environ['DOCKER_REGISTRY_LOGIN_TYPE']
        self.username      = os.environ['DOCKER_REGISTRY_USERNAME']
        self.password_path = os.environ['DOCKER_REGISTRY_PASSWORD_PATH']

        # We ahve some special code to define the namespace and tags
        # properties from the environment.
        if (('DOCKER_NAMESPACE' in os.environ) and os.environ['DOCKER_NAMESPACE']):
            self.namespace = os.environ['DOCKER_NAMESPACE']
        else:
            self.namespace = None

        if (('DOCKER_IMAGE_TAGS' in os.environ) and os.environ['DOCKER_IMAGE_TAGS']):
            self.tags = os.environ['DOCKER_IMAGE_TAGS'].split(',')
        else:
            self.tags = ['latest']

        if (('DOCKER_IMAGE_TAGS_HOOK' in os.environ) and os.environ['DOCKER_IMAGE_TAGS_HOOK']):
            self.tags_hook = os.environ['DOCKER_IMAGE_TAGS_HOOK']
        else:
            self.tags_hook = None

    def all_tags(self):
        """Return both the static and dynamic tags."""
        dynamic_tags = self.docker_tags_hooks()
        return self.tags + dynamic_tags

    def registry_path(self):
        """Construct the path to the Docker registry where we push the Docker image.
        """
        if (self.namespace is not None):
            registry_path = f"{self.registry}/{self.namespace}/{self.image}"
        else:
            registry_path = f"{self.registry}/{self.image}"

        return registry_path

    def show_env(self):
        """
        Print out the DOCKER_* environment variables. Note
        that we do NOT run check_env as we want to show all the
        variable settings even if some of the required ones are
        missing.
        """

        # Skip some of the environment variables if the auth type is "gcloud".
        if (self.login_type == 'gcloud'):
            skip = ['DOCKER_REGISTRY_USERNAME', 'DOCKER_REGISTRY_PASSWORD_PATH']
        else:
            skip = []

        super().show_env(skip=skip)

        # Print out the registry path and the tags.
        try:
            registry_path = self.registry_path()
        except:
            registry_path = '<NOT SET>'

        print(f"{'registry path:':36} {registry_path}")

        # Generate tags list
        tags = self.all_tags()

        print(f"{'tags:':36} {','.join(tags)}")


    def construct_build_args(self):
        """
        If the build-args.yaml exists parse it and use its values
        to define an array. For example, if the file is

          ---
          DEBIAN_CODENAME: buster
          MAINTAINER: johndoe@example.com

        then return the dict
          {'DEBIAN_CODENAME': 'buster', 'MAINTAINER': 'johndoe@example.com'}

        The build-args.yaml file we use is the one in DOCKER_BUILD_DIR that
        has gone through environment variable substitution.
        """
        build_dir       = self.build_dir                  # pylint: disable=bad-whitespace
        build_args_file = os.path.join(build_dir,         # pylint: disable=bad-whitespace
                                       DockerWrapper.build_args_filename)
        self.progress(f"build_args_file is {build_args_file}")

        build_args = {}
        if (os.path.exists(build_args_file)):
            self.progress(f"{build_args_file} exists; will parse for docker build args")
            with open(build_args_file) as file1:
                args = yaml.load(file1, Loader=yaml.FullLoader)
                for arg, value in args.items():
                    if (value is not None):
                        build_args[arg] = value
        else:
            self.progress(f"{build_args_file} does not exist; skipping parsing docker build args")

        return build_args

    def build_cmd(self):
        """
        Generate the docker build command. Takes into account the DOCKER_*
        environment variables, the build-args.yaml file, and the image tag environment
        variables.
        """
        build_dir    = self.build_dir  # pylint: disable=bad-whitespace
        docker_image = self.image      # pylint: disable=bad-whitespace
        self.progress(f"generating build script from directory '{build_dir}'")
        self.progress(f"docker image is                        '{docker_image}'")

        ## STEP 1: Construct tag options
        tags = self.all_tags()

        tags_options = []
        for tag in tags:
            tags_options.append('--tag')
            tags_options.append(f"{docker_image}:{tag}")

        tag_option = ' '.join(tags_options)
        self.progress(f"tags_options: '{tag_option}'")

        ## STEP 2: Construct the --no-cache
        if (self.no_cache):
            no_cache_option = '--no-cache=true'
        else:
            no_cache_option = '--no-cache=false'

        self.progress(f"no_cache_option: '{no_cache_option}'")

        tags_options.append('--progress=plain')

        ## STEP 3: Construct the --build-arg options
        build_args_dict = self.construct_build_args()
        build_args = []
        for key, value in build_args_dict.items():
            build_args.append("--build-arg")
            build_args.append(f"{key}={value}")

        self.progress(f"build_args: '{build_args}'")

        #tags_options.append('--progress=plain')

        ## STEP 4: Construct the docker build command
        cmd = [
            'docker',
            'build',
            no_cache_option,
            '--pull',
        ] + tags_options + [
        ] + build_args + [
            build_dir,
        ]

        return cmd


    def build_kaniko_cmd(self):
        """Generate the kaniko build command. Takes into account the DOCKER_*
        environment variables, the build-args.yaml file, and the image tag environment
        variables.

        If the environment variable DOCKER_KANIKO_NO_PUSH is defined the
        kaniko command will _not_ attempt to push to a destination
        repository and will instear run with the --no-push flag.
        """
        destination_path = self.registry_path()

        if ('DOCKER_KANIKO_NO_PUSH' in os.environ):
            docker_push = False
        else:
            docker_push = True

        ## STEP 1: Construct tag options. Skip this step if we are in
        ## "no-push" mode.
        if (docker_push):
            tags = self.all_tags()

            # Construct the tags options
            tags_options = []
            for tag in tags:
                option = f"--destination={destination_path}:{tag}"
                tags_options.append(option)

            tag_option = ' '.join(tags_options)
            self.progress(f"tags_options: '{tag_option}'")
        else:
            tags_options = [ '--no-push' ]
            self.progress(f'DOCKER_KANIKO_NO_PUSH is set so setting tag options to --no-push')

        ## STEP 2: Construct --context
        context_option = f"--context=$CI_PROJECT_DIR/common/docker"

        ## STEP 3: Construct the --build-arg options
        build_args_dict = self.construct_build_args()
        build_args = []
        for key, value in build_args_dict.items():
            build_args.append(f"--build-arg={key}={value}")

        self.progress(f"build_args: '{build_args}'")

        ## STEP 3: Extra Kaniko flags.
        ##
        ## --cleanup: clean the filesystem at the end of the build. This helps
        ## reduce memory use when doing multiple builds.
        ##
        ## --compressed-caching=false: prevent tar compression for cached
        ## layers; this supposedly reduces memory use during image
        ## build.

        extra_kaniko_flags = ['--cleanup', '--compressed-caching=false']

        ## STEP 4: Construct the docker build command.
        cmd = [
            '/kaniko/executor',
            context_option,
        ] + tags_options + [
        ] + build_args + extra_kaniko_flags

        return cmd

    def build(self):
        """
        Do a "docker build". Takes into account the DOCKER_* environment variables including
        the image tag environment variables. Can do both a cached and un-cached build.
        """
        if (self.no_cache):
            self.progress("building without cache")
        else:
            self.progress("building with cache")

        cmd = self.build_cmd()
        self.progress(cmd)

        # Run the command.
        # cp = subprocess.run(cmd, capture_output = True)
        subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)


    def write_kaniko_build_script(self):
        kaniko_cmd = self.build_kaniko_cmd()

        # Convert array to string.
        kaniko_cmd_str = " ".join(kaniko_cmd)
        self.progress(f"kaniko command: {kaniko_cmd_str}")

        # Write to a file in DOCKER_BUILD_DIR
        docker_build_dir = os.environ['DOCKER_BUILD_DIR']  # pylint: disable=bad-whitespace
        file_path = os.path.join(docker_build_dir, DockerWrapper.kaniko_cmd_file)
        with open(file_path, "w") as text_file:
            text_file.write(kaniko_cmd_str)
            text_file.write("\n")

        logging.info(f"wrote kaniko command to file '{file_path}'")

    def docker_login(self):
        """
        Authenticate to the registry. We don't normally use this as there is
        already a docker-login.sh in the scripts/ directory.
        """
        self.progress("logging into docker")

        if (self.login_type == 'gcloud'):
            # Nothing to do.
            self.progress("login type 'gcloud' uses context for authentication")
        elif (self.login_type == 'vault'):
            vault_path = self.password_path
            self.progress(f"about to read secret from Vault path {vault_path}")

            cmd = ['vault-read.sh', vault_path]
            self.progress(f"about to run command {cmd}")

            result = subprocess.run(cmd, capture_output=True, text=True)
            stdout = result.stdout
            stderr = result.stderr
            rc     = result.returncode
            self.progress(f"stdout: {stdout}")
            self.progress(f"stderr: {stderr}")
            self.progress(f"rc:     {rc}")

            if (rc != 0):
                msg = f"command {cmd} exited with code {rc} and (stdout, stderr): ({stdout}, {stderr})"
                self.exit_with_error(msg)

            password = stdout.strip()
            if ((password is None) or (password == "")):
                msg = 'password undefined or empty'
                self.exit_with_error(msg)

            cmd = ['docker', 'login', f"--username={self.username}", f"--password-stdin"]
            self.progress(f"calling docker login command {cmd}")
            subprocess.run(cmd, input=password, text=True)

    def docker_logout(self):
        self.progress("logging out of docker")

        if (self.login_type == 'gcloud'):
            # Nothing to do.
            self.progress("login type 'gcloud' uses context for authentication so no docker logout needed")
        else:
            cmd = ['docker', 'logout', self.registry]
            self.progress(f"about to run command {cmd}")
            subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

    def tag_and_push_this_tag(self, tag):
        """
        Given a tag name (tag) the local name is DOCKER_IMAGE:tag. Alias this
        image name with DOCKER_REGISTRY/DOCKER_NAMESPACE/DOCKER_IMAGE:tag. Then
        push push this name.
        """
        self.progress(f"about to tag-and-push tag '{tag}'")

        docker_image = self.image

        full_path = f"{self.registry_path()}:{tag}"
        self.progress(f"full path is {full_path}")

        # TAG
        cmd = [
            'docker',
            'tag',
            f"{docker_image}:{tag}",
            full_path
        ]
        self.progress(f"cmd: {cmd}")
        if (self.dryrun):
            self.dry_run_msg(f"tag image with {full_path}")
        else:
            subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

        # PUSH
        cmd = [
            'docker',
            'push',
            full_path
        ]
        self.progress(f"cmd: {cmd}")
        if (self.dryrun):
            self.dry_run_msg(f"push image to {full_path}")
        else:
            #self.docker_login() # Don't need this since make calls scripts/docker-login.sh.
            subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

    def tag_and_push(self):
        """
        Tag the current image and push with all image tags.
        """
        self.progress(f"about to tag-and-push tags {self.all_tags()}")

        for tag in self.all_tags():
            self.tag_and_push_this_tag(tag)

    def docker_tags_hooks(self):
        """Generate tags using DOCKER_IMAGE_TAGS_HOOK script

        If the variable DOCKER_IMAGE_TAGS_HOOK is set it should point
        to a script that generates a comma-delimited list of tags.

        Run this script and return this list as an array.
        """
        if (self.tags_hook is None):
            return []

        # Run the script.
        result = subprocess.run(self.tags_hook, capture_output=True, text=True)
        stdout = result.stdout
        stderr = result.stderr
        rc     = result.returncode

        if (self.verbose):
            print(f"stdout: <{stdout.strip()}>")
            print(f"stderr: <{stderr.strip()}>")
            print(f"rc:     <{rc}>")

        # If there is an error return the empty array.
        if (stderr):
            msg = f"error running tag hook {self.tags_hook}: {stderr}"
            self.progress(msg)
            return []

        # We ONLY TAKE THE LAST LINE of standard ouput for tags.
        last_line = stdout.strip().split("\n")[-1]
        #print(f"last_line is {last_line}")

        # Split the output on the commas and remove any whitespace.
        tags = last_line.split(',')
        tags = [tag.strip() for tag in tags]
        return tags
