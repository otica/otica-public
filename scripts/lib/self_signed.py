"""A class to assist in making self-signed certificates.

"""

import datetime
import git
import os
import tempfile

from cryptography          import x509
from cryptography.x509.oid import NameOID

from cryptography.hazmat.primitives            import hashes
from cryptography.hazmat.primitives            import serialization
from cryptography.hazmat.primitives.asymmetric import rsa

from otica_abstract import OticaAbstract


class SelfSignedWrapper(OticaAbstract):
    """
    The main class for our docker wrapper script.
    """

    package_env_vars = [
        'BUILD_DIR',
        'SSIGNED_COMMON_NAME',
        'SSIGNED_COUNTRY_NAME',
        'SSIGNED_STATE_OR_PROVINCE_NAME',
        'SSIGNED_LOCALITY_NAME',
        'SSIGNED_ORGANIZATION_NAME',
        'SSIGNED_ORGANIZATIONAL_UNIT_NAME',
        'SSIGNED_EMAIL_ADDRESS',
        'SSIGNED_EXPIRE_IN_DAYS',
        'SSIGNED_KEY_LENGTH',
        'SSIGNED_SEC_PATH',
    ]

    package_required_env_vars = [
        'BUILD_DIR',
        'SSIGNED_COMMON_NAME',
        'SSIGNED_STATE_OR_PROVINCE_NAME',
        'SSIGNED_ORGANIZATIONAL_UNIT_NAME',
        'SSIGNED_EXPIRE_IN_DAYS',
    ]

    package_deprecated_env_vars = {
    }

    package_unsupported_env_vars = {
    }

    package_env_var_to_default  = {
        'SSIGNED_COUNTRY_NAME': 'US',
        'SSIGNED_STATE_OR_PROVINCE_NAME': 'California',
        'SSIGNED_KEY_LENGTH': '3072',
    }

    def __init__(self,
                 verbose=False,
                 dryrun=False,
                 ):
        """Initialize object
        """
        super().__init__(verbose=verbose)

        self._env_vars             = SelfSignedWrapper.package_env_vars
        self._required_env_vars    = SelfSignedWrapper.package_required_env_vars
        self._deprecated_env_vars  = SelfSignedWrapper.package_deprecated_env_vars
        self._unsupported_env_vars = SelfSignedWrapper.package_unsupported_env_vars
        self._env_var_to_default   = SelfSignedWrapper.package_env_var_to_default

        # Check for missing required environment variables and set some defaults.
        self.check_env()
        self.set_defaults()

        self.dryrun   = dryrun     # pylint: disable=bad-whitespace

        # Set more attributes.
        self.COUNTRY_NAME = os.environ['SSIGNED_COUNTRY_NAME']
        self.COMMON_NAME  = os.environ['SSIGNED_COMMON_NAME']

        if ('SSIGNED_STATE_OR_PROVINCE_NAME' in os.environ):
            self.STATE_OR_PROVINCE_NAME = os.environ['SSIGNED_STATE_OR_PROVINCE_NAME']
        else:
            self.STATE_OR_PROVINCE_NAME = None

        if ('SSIGNED_LOCALITY_NAME' in os.environ):
            self.LOCALITY_NAME = os.environ['SSIGNED_LOCALITY_NAME']
        else:
            self.LOCALITY_NAME = None

        if ('SSIGNED_ORGANIZATION_NAME' in os.environ):
            self.ORGANIZATION_NAME = os.environ['SSIGNED_ORGANIZATION_NAME']
        else:
            self.ORGANIZATION_NAME = None

        if ('SSIGNED_ORGANIZATIONAL_UNIT_NAME' in os.environ):
            self.ORGANIZATIONAL_UNIT_NAME = os.environ['SSIGNED_ORGANIZATIONAL_UNIT_NAME']
        else:
            self.ORGANIZATIONAL_UNIT_NAME = None

        if ('SSIGNED_EMAIL_ADDRESS' in os.environ):
            self.EMAIL_ADDRESS = os.environ['SSIGNED_EMAIL_ADDRESS']
        else:
            self.EMAIL_ADDRESS = None

        if ('SSIGNED_EXPIRE_IN_DAYS' in os.environ):
            self.EXPIRE_IN_DAYS = int(os.environ['SSIGNED_EXPIRE_IN_DAYS'])
        else:
            self.EXPIRE_IN_DAYS = None

        if ('SSIGNED_KEY_LENGTH' in os.environ):
            self.KEY_LENGTH = int(os.environ['SSIGNED_KEY_LENGTH'])
        else:
            self.KEY_LENGTH = None

        if ('SSIGNED_SEC_PATH' in os.environ):
            self.SEC_PATH = os.environ['SSIGNED_SEC_PATH']
        else:
            self.SEC_PATH = None

        self.cert_filepath = os.path.join(os.environ['BUILD_DIR'], f"{self.COMMON_NAME}-certificate.pem")
        self.key_filepath  = os.path.join(os.environ['BUILD_DIR'], f"{self.COMMON_NAME}-privatekey.pem")

    def show_env(self):
        super(SelfSignedWrapper, self).show_env()
        print(f"Subject: {self.subject()}")
        print(f"File destinations: {self.cert_filepath}, {self.key_filepath}")

    def certificate_vault_path(self):
        if (self.SEC_PATH is None):
            return None
        else:
            return f"{self.SEC_PATH}/certificate"

    def privatekey_vault_path(self):
        if (self.SEC_PATH is None):
            return None
        else:
            return f"{self.SEC_PATH}/privatekey"

    def subject(self):
        """Construct the Subject.

        Will look something like this (some optional components might be missing):

            C=COUNTRY_NAME,ST=STATE_OR_PROVINCE_NAME,L=LOCALITY_NAME,O=ORGANIZATION_NAME,OU=ORGANIZATIONAL_UNIT,CN=COMMON_NAME
        """
        subject_oids = []

        if (self.COUNTRY_NAME is not None):
            subject_oids.append(x509.NameAttribute(NameOID.COUNTRY_NAME, self.COUNTRY_NAME))

        if (self.STATE_OR_PROVINCE_NAME is not None):
            subject_oids.append(x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, self.STATE_OR_PROVINCE_NAME))

        if (self.LOCALITY_NAME is not None):
            subject_oids.append(x509.NameAttribute(NameOID.LOCALITY_NAME, self.LOCALITY_NAME))

        if (self.ORGANIZATION_NAME is not None):
            subject_oids.append(x509.NameAttribute(NameOID.ORGANIZATION_NAME, self.ORGANIZATION_NAME))

        if (self.ORGANIZATIONAL_UNIT_NAME is not None):
            subject_oids.append(x509.NameAttribute(NameOID.ORGANIZATIONAL_UNIT_NAME, self.ORGANIZATIONAL_UNIT_NAME))

        if (self.COMMON_NAME is not None):
            subject_oids.append(x509.NameAttribute(NameOID.COMMON_NAME, self.COMMON_NAME))

        if (self.EMAIL_ADDRESS is not None):
            subject_oids.append(x509.NameAttribute(NameOID.EMAIL_ADDRESS, self.EMAIL_ADDRESS))

        subject = x509.Name(subject_oids)
        return subject

    def destination_files_exist(self):
        """Return True if either file (cert or key) exists.

        We need to know if they exist so we don't overwrite.
        """
        if (os.path.isfile(self.cert_filepath)):
            return True

        if (os.path.isfile(self.key_filepath)):
            return True

        return False

    def cleanup_keypair(self):
        files_to_clean = [
            self.cert_filepath,
            self.key_filepath,
        ]

        for file1 in files_to_clean:
            if (os.path.isfile(file1)):
                os.remove(file1)
                print(f"{file1}: removed")
            else:
                print(f"{file1}: not removed (does not exist)")

    def generate_keypair(self):
        """Make the key-pair
        """

        if (self.destination_files_exist()):
            msg = (
                f"cannot generate keypair: "
                f"one or both of {self.cert_filepath} and {self.key_filepath} exist; "
                f"delete or move to generate a new keypair"
            )
            self.exit_with_error(msg)

        # Generate private key
        msg = "generating RSA private key"
        self.progress(msg)
        private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=self.KEY_LENGTH,
        )

        # Generate public key
        msg = "generating RSA public key"
        self.progress(msg)
        public_key = private_key.public_key()

        # Note that the subject and the issuer are the same (this is self-signed).
        msg = f"generating certificate with Subject {self.subject()}"
        self.progress(msg)
        certificate = x509.CertificateBuilder().subject_name(
            self.subject()
        ).issuer_name(
            self.subject()
        ).public_key(
            public_key
        ).serial_number(
            x509.random_serial_number()
        ).not_valid_before(
            datetime.datetime.utcnow()
        ).not_valid_after(
            # Certificate validity period (e.g., 1 year)
            datetime.datetime.utcnow() + datetime.timedelta(days=self.EXPIRE_IN_DAYS)
        ).sign(private_key, hashes.SHA256())

        msg = "serializing keys"
        self.progress(msg)

        cert_string = certificate.public_bytes(serialization.Encoding.PEM)

        private_key_string = private_key.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.TraditionalOpenSSL,
            encryption_algorithm=serialization.NoEncryption(),
        )

        # Write the files
        with open(self.cert_filepath, "wb") as f:
            f.write(cert_string)
            msg = f"wrote certificate file {self.cert_filepath}"
            self.progress(msg)

        with open(self.key_filepath, "wb") as f:
            f.write(private_key_string)
            msg = f"wrote certificate file {self.key_filepath}"
            self.progress(msg)

        print(f"your keys are in '{self.cert_filepath}' and '{self.key_filepath}'")

    def read_vault(self):
        """Get the keys from Vault
        """
        # Check 1. Are the Vault paths defined?
        if (self.SEC_PATH is None):
            msg = "cannot read from Vault unless SSIGNED_SEC_PATH is defined"
            self.exit_with_error(msg)

        certificate = self.read_from_vault(self.certificate_vault_path())
        privatekey  = self.read_from_vault(self.privatekey_vault_path())

        return (certificate, privatekey)

    def write_vault(self, force=False):
        """Store the generated key-pair files in Vault.

        Only overwrite existing Vault entries when force parameter is True.
        """

        # Check 1. Are the Vault paths defined?
        if (self.SEC_PATH is None):
            msg = "cannot write to Vault unless SSIGNED_SEC_PATH is defined"
            self.exit_with_error(msg)

        # Check 2. Are the files in the build directory?
        if (not self.destination_files_exist()):
            msg = "you must generate keypair files before writing to Vault"
            self.exit_with_error(msg)

        # Check 3. If force is not in effect abort if the keys are already in Vault.
        if (not force):
            # Do the keys already exist in Vault? If so, we abort.
            privatekey, certificate = self.read_vault()

            if ((len(certificate) > 10) or (len(privatekey) > 10)):
                msg = "cannot overwrite Vault secrets unless using force"
                self.exit_with_error(msg)

        # If we get here we are ready to write the secrets to Vault.

        # Read the files into strings
        with open(self.cert_filepath, 'r') as file1:
            certificate = file1.read()

        with open(self.key_filepath, 'r') as file1:
            privatekey = file1.read()

        msg = f"about to write certificate to Vault path {self.certificate_vault_path()}"
        self.progress(msg)
        self.write_to_vault(self.certificate_vault_path(), certificate)
        print(f"wrote certificate to Vault path {self.certificate_vault_path()}")

        msg = f"about to write private key to Vault path {self.privatekey_vault_path()}"
        self.progress(msg)
        self.write_to_vault(self.privatekey_vault_path(), privatekey)
        print(f"wrote private key to Vault path {self.privatekey_vault_path()}")
