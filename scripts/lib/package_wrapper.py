"""A class for building packages.

"""

import git
import os
import tempfile

from otica_abstract import OticaAbstract
from render_files   import RenderFiles

class DockerImage():
    def __init__(self,
                 name,
                 tag,
                 command,
                 ):
        """Initialize object.
        """
        self.name    = name
        self.tag     = tag
        self.command = command

    def __str__(self):
        return f"<name: {self.image_name()}, command: {self.command}>"

    def image_name(self):
        """Return the "full" name of the image, i.e., the "name:tag" string."""
        return f"{self.name}:{self.tag}"

    def valid(self):
        """A docker image is "valid" if none of name, tag, or command is None or empty."""
        if (not self.name):
            return False
        elif (not self.tag):
            return False
        elif (not self.command):
            return False
        else:
            return True

class PackageWrapper(OticaAbstract):
    """
    The main class for our docker wrapper script.
    """

    package_env_vars = [
        'BUILD_DIR',
        'PKG_TYPE',
        'PKG_GIT_URL',
        'PKG_BUILD_DOCKER_IMAGE_NAME',
        'PKG_BUILD_DOCKER_IMAGE_TAG',
        'PKG_BUILD_DOCKER_CMD',
        'PKG_SIGN_DOCKER_IMAGE_NAME',
        'PKG_SIGN_DOCKER_IMAGE_TAG',
        'PKG_SIGN_DOCKER_CMD',
        'PKG_PUSH_DOCKER_IMAGE_NAME',
        'PKG_PUSH_DOCKER_IMAGE_TAG',
        'PKG_PUSH_DOCKER_CMD',
        'PKG_DOCKER_SCRIPTS_DIR',
        'PKG_SIGN_CREDS_HELPER',
        'PKG_CREDS_VAULT_PATH',
    ]

    package_required_env_vars = [
        'BUILD_DIR',
        'PKG_TYPE',
        'PKG_GIT_URL',
        'PKG_BUILD_DOCKER_IMAGE_NAME',
    ]

    package_deprecated_env_vars = {
    }

    package_unsupported_env_vars = {
    }

    package_env_var_to_default  = {
        'PKG_BUILD_DOCKER_IMAGE_TAG': 'latest',
        'PKG_BUILD_DOCKER_CMD': '/root/build-pkg.sh',
        'PKG_SIGN_DOCKER_IMAGE_TAG': 'latest',
        'PKG_SIGN_DOCKER_CMD': '/root/sign-pkg.sh',
        'PKG_PUSH_DOCKER_CMD': '/root/push-pkg.sh',
        'PKG_SIGN_CREDS_HELPER': f"{os.environ['FRAMEWORK_DIR']}/scripts/package/creds-helper.sh",
    }

    package_allowed_types = ['debian', 'redhat', 'python']

    def __init__(self,
                 action,
                 verbose=False,
                 no_cache=False,
                 dryrun=False,
                 ):
        """Initialize object
        """
        super().__init__(verbose=verbose)

        self.action = action

        self._env_vars             = PackageWrapper.package_env_vars
        self._required_env_vars    = PackageWrapper.package_required_env_vars
        self._deprecated_env_vars  = PackageWrapper.package_deprecated_env_vars
        self._unsupported_env_vars = PackageWrapper.package_unsupported_env_vars
        self._env_var_to_default   = PackageWrapper.package_env_var_to_default

        # Check for missing required environment variables and set some defaults.
        self.check_env()
        self.set_defaults()

        self.no_cache = no_cache   # pylint: disable=bad-whitespace
        self.dryrun   = dryrun     # pylint: disable=bad-whitespace
        self.tags     = []         # pylint: disable=bad-whitespace

        ## Define the Docker image based on the action (only for actions
        ## 'build', 'sign' and 'push').
        self.docker_images = {}
        for action in ['build', 'sign', 'push']:
            self.docker_images[action] = self.make_docker_image(action)

        self.pkg_type = os.environ['PKG_TYPE']
        self.git_url  = os.environ['PKG_GIT_URL']

        # Set the full path to BUILD DIR
        self.build_dir_fullpath = os.path.abspath(os.environ['BUILD_DIR'])
        self.progress(f"BUILD_DIR full path is '{self.build_dir_fullpath}'")

        # The directory where we execute the git clone (source):
        self.source_dir  = os.path.join(self.build_dir_fullpath, 'source')
        self.output_dir = os.path.join(self.build_dir_fullpath, 'output')
        self.creds_dir = os.path.join(self.build_dir_fullpath, 'credentials')

        # Check that PKG_TYPE is one of the allowed types.
        if (self.pkg_type not in PackageWrapper.package_allowed_types):
            msg = f"PKG_TYPE '{self.pkg_type}' not recognized; must be one of: "
            msg += ", ".join(PackageWrapper.package_allowed_types)
            self.exit_with_error(msg)

        # Set the package file extension based upon the package type
        if (self.pkg_type == 'redhat'):
            self.pkg_ext = '.rpm'
        elif (self.pkg_type == 'debian'):
            self.pkg_ext = '.deb'

        # Set the source download type.
        self.source_type = 'git'

    def make_docker_image(self, action):
        """Return a DockerImage object based on 'action'.

        Define the Docker image based on the action (only makes sense actions
        'build', 'sign' and 'push').
        """
        if (action not in ['build', 'sign', 'push']):
            msg = f"cannot create a DockerImage object for action '{action}'"
            self.exit_with_error(msg)

        var_image_name = f"PKG_{action.upper()}_DOCKER_IMAGE_NAME"
        var_image_tag  = f"PKG_{action.upper()}_DOCKER_IMAGE_TAG"
        var_cmd        = f"PKG_{action.upper()}_DOCKER_CMD"

        if (var_image_name not in os.environ):
            image_name = None
        else:
            image_name = os.environ[var_image_name]

        if (var_image_tag not in os.environ):
            image_tag = 'latest'
        else:
            image_tag = os.environ[var_image_tag]

        if (var_cmd in os.environ):
            docker_cmd = os.environ[var_cmd]

        docker_image = DockerImage(
            image_name,
            image_tag,
            docker_cmd
        )

        return docker_image

    def download(self):
        """Download the package source into self.source_dir"""
        if (self.source_type == 'git'):
            repo = git.Repo.clone_from(self.git_url, self.source_dir)

    def run_creds_helper(self):
        """Run the creds helper"""
        creds_helper = os.environ['PKG_SIGN_CREDS_HELPER']
        if (not os.path.exists(creds_helper)):
            msg = f"Credential Helper script '{creds_helper}' does not exist.  Exiting..."
            self.exit_with_error(msg)
        else:
            msg = f"running credentials helper {creds_helper}"
            self.progress(msg)
            self.run_command([creds_helper])

    def docker_command(self, docker_image):
        """Create the Docker command array ready for running."""

        # Step 1. Prepare environment variables to be injected in Docker container

        self.validate_scripts_dir()

        # env_vars is a list of individual environment variables.
        env_vars = []
        env_vars += ['--env', 'SOURCE_DIR=/root/pkg/source']
        env_vars += ['--env', 'OUTPUT_DIR=/root/pkg/output']
        env_vars += ['--env', 'VERBOSE=XXX']
        env_vars += ['--env', 'DEBUG=XXX']
        env_vars += ['--env', f"PKG_ACTION={self.action}"]
        if (self.scripts_dir):
            env_vars += ['--env', 'SCRIPTS_DIR=/root/pkg/scripts']

        # env_file is an (optional) file of environment variable definitions.
        # Note that we render this file in case it needs any variable expansions.
        if ('DOCKER_ENV_FILE' in os.environ):
            docker_env_file = os.environ['DOCKER_ENV_FILE']
            if (not os.path.isfile(docker_env_file)):
                msg = f"cannot find Docker env file '{docker_env_file}'"
                self.exit_with_error(msg)
            else:
                files_to_render = [os.environ['DOCKER_ENV_FILE']]
                rendered_files = self.render(files_to_render)
                env_file = ['--env-file', rendered_files[0]]
        else:
            env_file = []

        # Step 2. Directory mounts. We need to mount:
        #  * the source directory (where the package source code lives)
        #  * the output directory (where we read/write the package build artifacts)
        #  * the scripts directory (optional: any scripts from Otica we might need)
        mounts = []

        # The source and output directories are always required.
        mounts += ['--mount', f"type=bind,source={self.source_dir},target=/root/pkg/source"]
        mounts += ['--mount', f"type=bind,source={self.output_dir},target=/root/pkg/output"]

        # The scripts and credentials directories are optional.
        if (self.scripts_dir and os.path.exists(self.scripts_dir)):
            mounts += ['--mount', f"type=bind,source={self.scripts_dir},target=/root/pkg/scripts"]

        if (self.creds_dir and os.path.exists(self.creds_dir)):
            mounts += ['--mount', f"type=bind,source={self.creds_dir},target=/root/pkg/credentials"]

        # Step 3. Construct the "docker run" command from the above components.
        cmd = ['docker', 'run'] + mounts + env_vars + env_file + \
            [docker_image.image_name()] + [docker_image.command]

        return cmd

    def validate_scripts_dir(self):
        if ('PKG_DOCKER_SCRIPTS_DIR' in os.environ):
            self.scripts_dir = os.path.abspath(os.environ['PKG_DOCKER_SCRIPTS_DIR'])
            if (not os.path.exists(self.scripts_dir)):
                msg = f"PKG_DOCKER_SCRIPTS_DIR '{self.scripts_dir}' does not exist.   Please resolve."
                self.exit_with_error(msg)
        else:
            self.scripts_dir = ''

    def build(self):
        """Build the package"""

        self.progress(f"making output directory {self.output_dir}")
        if (not os.path.exists(self.output_dir)):
            os.mkdir(self.output_dir)

        docker_image = self.docker_images['build']
        if (not docker_image.valid()):
            msg = f"cannot build package as docker image {docker_image} is not valid"
            self.exit_with_error(msg)

        self.progress(f"Will execute container build script: {docker_image.command}")

        command = self.docker_command(docker_image)

        self.progress(f"about to run Docker command: {' '.join(command)}")
        self.run_command(command)

        # Show message after build
        msg = "Your build artifacts are in {self.output_dir}; the important files are "

    def sign(self):
        """Sign the built package"""

        # Perform Package Signing Required ENV Variable Validation
        self.check_for_missing_variables(['PKG_SIGN_DOCKER_IMAGE_NAME', 'PKG_SIGN_DOCKER_IMAGE_TAG', 'PKG_CREDS_VAULT_PATH'])

	# If the OUTPUT_DIR does not exist, there is nothing to do, so exit
        if (not os.path.exists(self.output_dir)):
            msg = f"Output directory '{self.output_dir}' does not exist.  Exiting..."
            self.exit_with_error(msg)

        docker_image = self.docker_images['sign']
        if (not docker_image.valid()):
            msg = f"cannot sign package as docker image {docker_image} is not valid"
            self.exit_with_error(msg)

        # Check for valid package files based upon PKG_TYPE
        output_files = os.listdir(self.output_dir)
        pkg_files = [file for file in output_files if file.endswith(self.pkg_ext)]
        if not pkg_files:
            msg = f"No '{self.pkg_type}' package files were found in '{self.output_dir}'.  "
            msg += "Please make sure the PKG_TYPE is set correctly and there are proper "
            msg += f"package files in '{self.output_dir}'."
            self.exit_with_error(msg)

        self.progress(f"making credentials directory {self.creds_dir}")
        if (not os.path.exists(self.creds_dir)):
            os.mkdir(self.creds_dir)

        self.run_creds_helper()

        self.progress(f"Will execute container sign script: {docker_image.command}")

        command = self.docker_command(docker_image)
        self.run_command(command)

        # Show message after signing
        msg = "Your signed artifacts are in {self.output_dir} "

        # Cleanup Credentials
        cmd = ['rm', '-rf', self.creds_dir]
        self.run_command(cmd)

    def push(self):
        """Upload build (and possibly signed) package to package repository

        """
        self.check_for_missing_variables(['PKG_REPO_URL'])
        package_repository = os.environ['PKG_REPO_URL']

        msg = f"PKG_REPO_URL: {package_repository}"
        self.progress(msg)

        self.run_creds_helper()

        docker_image = self.docker_images['push']
        if (not docker_image.valid()):
            msg = f"cannot push package as docker image {docker_image} is not valid"
            self.exit_with_error(msg)

        command = self.docker_command(docker_image)

        self.progress(f"about to run Docker command: {' '.join(command)}")
        self.run_command(command)


    def render(self, files):
        """Render any files that need rendering

        Some files need to be run through the render2.sh script in order
        to expand any environment variables before handing off to the
        docker run command. This method does the rendering.

        The files parameter should be a (possibly empty) list of files to
        render.

        """

        # We need a RenderFiles object so that we can send the files
        # through the rendering script. Create a temporary directory to
        # stash our rendered files. We use the newer render2.sh script.
        tmp_dirpath = tempfile.mkdtemp()
        self.progress('finished creating RenderFiles object')
        scripts_dir = os.environ['SCRIPTS_DIR']
        self.render_files = RenderFiles(
            files=files,
            destination_dir=tmp_dirpath,
            render_script_path=os.path.join(scripts_dir, 'render2.sh'),
            verbose=self.verbose,
        )

        self.progress('creating rendered files')
        self.render_files.create()

        return self.render_files.rendered_file_paths()

    def __del__(self):
        self.progress('starting destructor')

        # If there are any rendered files we no longer need them.
        if hasattr(self, 'render_files'):
            self.progress('deleting rendered files')
            self.render_files.delete()

            # Delete the temporary directory
            tmpdir = self.render_files.destination_dir
            self.progress(f"deleting directory '{tmpdir}'")
            os.rmdir(tmpdir)
        else:
            self.progress('no rendered files to delete')

