"""A class wrapping around the main helm executable.

 Environment Variables:

  - HELM_CHART_NAME
  - HELM_CHART_VERSION
  - HELM_YAML_FILES
  - HELM_RELEASE
  - HELM_REPO_NAME
  - HELM_REPO_URL
  - HELM_REGISTRY_LOGIN_TYPE

1. The environment variable HELM_YAML_FILES should contain an ordered list
of YAML files that can be passed via the "-f" option. This script takes
each such file, runs them through render2.sh, and then supplies them (in
order) to the helm command via "-f" options.

For example, if HELM_YAML_FILES is equal to the string

"../common/data/common/values-local.yaml /tmp/value-local.yaml"

then this script will run the two files though render2.sh and feed the
results into helm using the "-f" option.
"""

# pylint: disable=superfluous-parens,no-else-raise

import os
import re
import subprocess
import sys
import tempfile

# Add './lib' to search path. We need to do this
# so that we can import otica_abstract
#MY_DIRECTORY = pathlib.Path(__file__).parent.resolve()
#sys.path.append(os.path.join(MY_DIRECTORY, 'lib'))

from otica_abstract import OticaAbstract
from render_files   import RenderFiles

class HelmWrapper(OticaAbstract):
    """The main class"""

    helm_env_vars = [
        'HELM_CHART_NAME',
        'HELM_CHART_VERSION',
        'HELM_YAML_FILES',
        'HELM_RELEASE',
        'HELM_REPO_NAME',
        'HELM_REPO_URL',
        'HELM_REGISTRY_LOGIN_TYPE',
        'APP_NAMESPACE',
    ]

    helm_required_env_vars = [
        'HELM_CHART_NAME',
        'HELM_CHART_VERSION',
        'HELM_RELEASE',
        'HELM_REPO_URL',
        'HELM_REGISTRY_LOGIN_TYPE',
        'APP_NAMESPACE',
    ]

    helm_deprecated_env_vars = {
    }

    helm_unsupported_env_vars = {
    }

    def __init__(self,
                 verbose=None,
                 ):
        super().__init__(verbose=verbose)

        self.progress('starting HelmWrapper __init__')

        self._env_vars            = HelmWrapper.helm_env_vars
        self._required_env_vars   = HelmWrapper.helm_required_env_vars
        self._deprecated_env_vars = HelmWrapper.helm_deprecated_env_vars
        self._env_var_to_default  = HelmWrapper.helm_unsupported_env_vars

        # Set some properties from the HELM_* environment variables.
        if (('HELM_YAML_FILES' in os.environ) and (os.environ['HELM_YAML_FILES'])):
            self.yaml_files = os.environ['HELM_YAML_FILES'].split(" ")

        if (('HELM_CHART_VERSION' in os.environ) and (os.environ['HELM_CHART_VERSION'])):
            self.helm_version = os.environ['HELM_CHART_VERSION']
            self.progress(f"using helm version '{self.helm_version}'")

        if (('HELM_RELEASE' in os.environ) and (os.environ['HELM_RELEASE'])):
            self.helm_release = os.environ['HELM_RELEASE']
            self.progress(f"using helm release '{self.helm_release}'")

        if (('HELM_REPO_NAME' in os.environ) and (os.environ['HELM_REPO_NAME'])):
            self.helm_repo_name = os.environ['HELM_REPO_NAME']
            self.progress(f"using helm repo name '{self.helm_repo_name}'")

        if (('HELM_CHART_NAME' in os.environ) and (os.environ['HELM_CHART_NAME'])):
            self.helm_chart_name = os.environ['HELM_CHART_NAME']
            self.progress(f"using helm chart name '{self.helm_chart_name}'")

        # Generate a run-time error if any required environment variables
        # have not been set. It may seem strange that we do this check _after_
        # setting the properties from the environment variables, but we do so
        # in order to print the property values to the screen when run in verbose
        # mode. Otherwise the user would not be able to see the values
        # in the event of an error.
        self.check_env()

        self.helm_repo_name = os.getenv('HELM_REPO_NAME')
        self.helm_repo_url  = os.getenv('HELM_REPO_URL')

        scripts_dir = os.environ['SCRIPTS_DIR']

        # We need a RenderFiles object so that we can send our YAML files
        # through the rendering script. Create a temporary directory to
        # stash our rendered YAML files in. We use the newer render2.sh
        # script.
        tmp_dirpath = tempfile.mkdtemp()
        self.render_files = RenderFiles(
            files=self.yaml_files,
            destination_dir=tmp_dirpath,
            render_script_path=os.path.join(scripts_dir, 'render2.sh'),
            verbose=verbose,
            )
        self.progress('finished creating RenderFiles object')

        self.namespace = os.environ['APP_NAMESPACE']
        self.progress('finished HelmWrapper __init__')

    def __del__(self):
        self.progress('starting destructor')

        # Clean up any rendered files.
        if hasattr(self, 'render_files'):
            self.progress('deleting rendered YAML files')
            self.render_files.delete()

            # Delete the temporary directory
            tmpdir = self.render_files.destination_dir
            self.progress(f"deleting directory '{tmpdir}'")
            os.rmdir(tmpdir)
        else:
            self.progress('no rendered YAML files to delete')

    def is_oci_repo(self):
        """Returns True if the Helm repo is OCI-based."""
        if (os.getenv('HELM_REPO_URL')):
            helm_repo_url = os.getenv('HELM_REPO_URL')
            if (re.match(r'^oci://.+', helm_repo_url)):
                return True
            else:
                return False
        else:
            return False

    def helm_chart_path(self):
        if (self.is_oci_repo()):
            return f"{self.helm_repo_url}/{self.helm_chart_name}"
        else:
            return f"{self.helm_repo_name}/{self.helm_chart_name}"

    def is_gs_repo(self):
        """Returns true if the Helm repository URL defined by the environment
        variable HELM_REPO_URL is a Gogle Cloud Storage repo, i.e., starts
        with 'gs://'. If HELM_REPO_URL is not defined returns False.
        """
        if (os.getenv('HELM_REPO_URL')):
            helm_repo_url = os.getenv('HELM_REPO_URL')
            if (re.match(r'^gs://.+', helm_repo_url)):
                return True
            else:
                return False
        else:
            return False

    def fail_if_oci(self):
        """Exit with an error if the repo is an OCI repo."""
        if (self.is_oci_repo()):
            msg = "cannot run this command with an OCI-based registry"
            self.exit_with_error(msg)
        else:
            return

    def check_env(self):
        super(HelmWrapper, self).check_env()

        if (self.is_gs_repo() and (not self.is_plugin_installed('gcs'))):
            msg = "HELM_REPO_URL is a GCS repository but the gcs Helm plugin is NOT installed!"
            self.exit_with_error(msg)

        if (self.is_oci_repo() and ('HELM_REPO_NAME' in os.environ)):
            msg = "cannot define HELM_REPO_NAME with an OCI repository"
            self.exit_with_error(msg)

    def show_env(self):
        super(HelmWrapper, self).show_env()
        if (self.is_gs_repo()):
            print("info: HELM_REPO_URL is a GCS repository", end='')
            if (not self.is_plugin_installed('gcs')):
                print(" --> WARNING!! gcs Helm plugin is NOT installed!")
            else:
                print("")
        elif (self.is_oci_repo()):
            print("info: HELM_REPO_URL is an OCI repository")
        else:
            print("info: HELM_REPO_URL is not a GCS or OCI repository")

    def list_plugins(self):
        cmd = ['helm', 'plugin', 'list']
        result = subprocess.run(cmd, text=True, capture_output=True)

        stdout = result.stdout
        stderr = result.stderr

        # Look for the plugins. Skip the header line
        plugins = []
        lines = stdout.split("\n")
        for line in lines:
            if (re.match(r'^NAME', line)):
                continue
            elif (not line):
                continue
            else:
                match_obj = re.match(r'^(\S+)\s', line)
                if (match_obj):
                    name = match_obj[1]
                    plugins.append(name)

        return plugins

    def is_plugin_installed(self, plugin_name):
        plugins = self.list_plugins()
        if (plugin_name in plugins):
            return True
        else:
            return False

    def check_gcs_plugin(self):
        # If HELM_REPO_URL starts with "gs://" be sure that the GCS plugin
        # is installed.
        if (os.getenv('HELM_REPO_URL')):
            helm_repo_url = os.getenv('HELM_REPO_URL')
            if (re.match(r'^gs://', helm_repo_url)):
                plugins = self.list_plugins()
                if ('gcs' in plugins):
                    self.progress("the gcs plugin is installed")
                else:
                    msg = f"GCS plugin reqired for HELM_REPO_URL '{helm_repo_url}' but not installed"
                    raise Exception(msg)
            else:
                self.progress("HELM_REPO_URL does not appear to require the Helm GCS plugin")
        else:
            pass

    def get_values_yaml(self):
        """Get the values.yaml file from the Helm chart. This is the values.yaml
        file that comes with the chart _before_ any values-local.yaml file
        are merged in.
        """
        (stdout, stderr, rc) = self.run_helm(['show', 'values'],
                                             use_namespace=False,
                                             include_yaml_files=False)
        return stdout

    def run_helm(self, action, use_namespace,
                 include_yaml_files=True,
                 upsert=False,
                 dryrun=False):
        """Run a helm command.

        The action parameter should be an array, e.g., ['install'], or
        ['get', 'manifest']. Set use_namespace to True when the namespace
        option is needed (e.g., for getting the manifest).

        The "upsert" parameter is only relevant when the action is
        "upgrade". If set to true the option '--install' will be added to
        the "helm update ..." command. This causes the Helm chart to be
        installed if not already installed.

        """
        cmd = ['helm'] + action

        # It is convenient to have an array consisting of just the first two
        # elements of the array action.
        action_short = action[0:2]

        # The "-f" include options
        if (include_yaml_files):
            self.progress('including YAML files')
            self.render()
            yaml_files_rendered = self.render_files.rendered_file_paths()
            for yaml_file in yaml_files_rendered:
                cmd.append('-f')
                cmd.append(yaml_file)

        # namespace
        if (use_namespace):
            cmd.append('--namespace')
            cmd.append(self.namespace)

        # chart version
        if (self.helm_version
            and (self.helm_version != 'latest')
            and (action_short != ['get',  'manifest'])
            and (action_short != ['show', 'values'])
            ):
            cmd.append('--version')
            cmd.append(self.helm_version)
        else:
            self.progress('not using the --version option (i.e., latest)')

        # dry-run
        if (dryrun):
            cmd.append('--dryrun')

        # --install (only for action "upgrade" with upsert flag set).
        if ((action[0] == "upgrade") and upsert):
            cmd.append('--install')

        # the release name (but not for the 'show' action).
        if (action[0] != 'show'):
            cmd.append(self.helm_release)

        # the chart name (only for some of the actions)
        actions_needing_chart_name = {
            'install',
            'template',
            'upgrade',
            'show',
        }
        if (action[0] in actions_needing_chart_name):
            cmd.append(self.helm_chart_path())

        self.progress(f"cmd is '{cmd}'")

        # pylint: disable=subprocess-run-check
        result = subprocess.run(cmd, text=True, capture_output=True)

        stdout = result.stdout
        stderr = result.stderr
        rc     = result.returncode

        return (stdout, stderr, rc)

    def run_helm_exception_text(self, command_text, stderr, rc):
        return f"'{command_text}' generated an error: exit code: {rc}, stderr: {stderr}"

    def render(self):
        """Render self.files using render2.sh. This means that if you want
        environment variables to be expanded you need to put "#!envsubst" at the
        top of your values-local.yaml file(s).

        This message sends all the files in self.render_files.files (which
        is self.yaml_files) through the render script and stores the
        resulting files in self.render_files.destination_dir.
        """
        self.progress(f"self.render_files.files is: {self.render_files.files}")
        self.progress(f"about to render these YAML files: {self.yaml_files}")
        self.render_files.create()

    def merge(self):
        """Show merged values YAML files."""
        self.progress('starting merge')
        self.render()

        # Check that all the files exist. If some do not error out.
        file_list = self.render_files.rendered_file_paths()

        # Get the values.yaml file from the chart and save it in a temporary file.
        values_yaml = self.get_values_yaml()
        values_yaml_file = tempfile.NamedTemporaryFile().name
        with open(values_yaml_file, 'w') as text_file:
            text_file.write(values_yaml)

        # Get the values-local.yaml file(s) and prepend values_yaml_file
        value_yaml_files = self.render_files.rendered_file_paths()
        value_yaml_files.insert(0, values_yaml_file)
        self.progress(f"values yaml files: {value_yaml_files}")

        # Now do a YAML merge of all the value YAML files using yq.
        cmd = [
            'yq',
            'eval-all',
            '. as $item ireduce ({}; . * $item )',
            ] + value_yaml_files
        print(cmd)
        subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

        # Be sure to delete the temporary file.
        #os.remove(values_yaml_file)
        self.progress('ending merge')

    def template(self):
        """Run 'helm template'. This renders the Helm templates _locally_.
        Returns the results from standard output."""
        (stdout, stderr, rc) = self.run_helm(action=['template'], use_namespace=False)
        if (rc != 0):
            raise Exception(self.run_helm_exception_text('helm template', stderr, rc))
        else:
            return stdout

    def install(self):
        """Run 'helm install'. Returns the results from standard output."""
        (stdout, stderr, rc) = self.run_helm(action=['install'], use_namespace=True)
        if (rc != 0):
            raise Exception(self.run_helm_exception_text('helm install', stderr, rc))
        else:
            return stdout

    def upgrade(self):
        """Run 'helm upgrade'. Returns the results from standard output."""
        (stdout, stderr, rc) = self.run_helm(action=['upgrade'], use_namespace=True)
        if (rc != 0):
            raise Exception(self.run_helm_exception_text('helm upgrade', stderr, rc))
        else:
            return stdout

    def upsert(self):
        """Run 'helm upgrade --install'. Returns the results from standard output."""
        (stdout, stderr, rc) = self.run_helm(action=['upgrade'], use_namespace=True, upsert=True)
        if (rc != 0):
            raise Exception(self.run_helm_exception_text('helm upsert', stderr, rc))
        else:
            return stdout

    def get_manifest(self):
        """Run 'helm get manifest'."""
        return self.run_helm(action=['get', 'manifest'],
                             use_namespace=True,
                             include_yaml_files=False)

    def file_diff(self, file1, file2):
        """Return the file diff between file1 and file2."""
        cmd = [
            'diff',
            '--context=3',
            file1,
            file2,
        ]

        # pylint: disable=subprocess-run-check
        self.progress(f"running '{cmd}'")
        result = subprocess.run(cmd, text=True, capture_output=True)
        return (result.stdout, result.stderr)


    def diff(self):
        """Return the file difference between the running manifest and 'helm manifest'."""
        (stdout, stderr, rc) = self.get_manifest()
        with tempfile.NamedTemporaryFile() as fp_old:
            fp_old.write(stdout.encode())
            stdout = self.template()
            with tempfile.NamedTemporaryFile() as fp_new:
                fp_new.write(stdout.encode())
                (stdout, stderr) = self.file_diff(fp_old.name, fp_new.name)
                if (stderr):
                    raise Exception(f"file_diff had an error: {stderr}")
                else:
                    print(stdout)
