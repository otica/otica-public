import logging
import os
import re
import subprocess
import sys

class OticaAbstract:

    def __init__(self,
                 verbose=None,
                 ):
        """Initialize object.
        """
        self.set_verbose(verbose_value=verbose)

        if (self.verbose):
            logging.basicConfig(level=logging.DEBUG, format='%(message)s',)
        else:
            logging.basicConfig(level=logging.INFO, format='%(message)s',)

        self._env_vars             = []
        self._required_env_vars    = []
        self._deprecated_env_vars  = []
        self._unsupported_env_vars = {}
        self._env_var_to_default   = {}

        # It is an error to have an environment variable in both
        # self._required_env_vars and self._env_var_to_default. So, check
        # this has not happened.
        for required_env_var in self.required_env_vars:
            if (required_env_var in self.env_var_to_default):
                msg = (f"you cannot have required variable {required_env_var} "
                       "in the env_var_to_default list")
                self.exit_with_error(msg)

        # Check that all the required variables are set.
        self.check_env()

        # Set defaults.
        self.set_defaults()

    ### PROPERTIES
    @property
    def verbose(self):
        return self._verbose

    @verbose.setter
    def verbose(self, value):
        self._verbose = value

    @property
    def env_vars(self):
        return self._env_vars

    @env_vars.setter
    def env_vars(self, value):
        self._env_vars = value

    @property
    def required_env_vars(self):
        return self._required_env_vars

    @required_env_vars.setter
    def required_env_vars(self, value):
        self._required_env_vars = value

    @property
    def deprecated_env_vars(self):
        return self._deprecated_env_vars

    @deprecated_env_vars.setter
    def deprecated_env_vars(self, value):
        self._deprecated_env_vars = value

    @property
    def unsupported_env_vars(self):
        return self._unsupported_env_vars

    @unsupported_env_vars.setter
    def unsupported_env_vars(self, value):
        self._unsupported_env_vars = value

    @property
    def env_var_to_default(self):
        return self._env_var_to_default

    @env_var_to_default.setter
    def env_var_to_default(self, value):
        self._env_var_to_default = value

    @property
    def dry_run(self):
        """ Getter method for the "dry_run" property.
        """
        return self._dry_run

    @dry_run.setter
    def dry_run(self, value):
        """ Setter method for the "dry_run" property.
        """
        self._dry_run = value
    ### END OF PROPERTIES

    def progress(self, msg):
        """Output a progress message (only if the verbose property is True)."""
        if (self.verbose):
            print(f'[progress] {msg}')

    def dry_run_msg(self, msg):
        # pylint: disable=no-self-use
        """ Print a dry-run message.
        """
        print(f"dry-run: {msg}")

    def exit_with_error(self, msg):
        """
        Print a message and then exit.
        """
        # pylint: disable=no-self-use
        print(f"ERROR: {msg}")
        sys.exit(1)

    def set_verbose(self, verbose_value=None):
        if (('DEBUG' in os.environ) and (os.environ['DEBUG'])):
            debug_env_value = os.environ['DEBUG']
            if (len(debug_env_value) > 1):
                self.verbose = True
            else:
                self.verbose = False
        else:
            # Not defined in the environment so use the passed in value.
            if (verbose_value):
                self.verbose = True
            else:
                self.verbose = False

    @staticmethod
    def normalize_boolean(value):
        """Given a string value convert to True or False.
        """
        # pylint: disable=no-else-return
        if (not value):
            return False
        elif (re.match(r'^(yes|true)$', value, re.IGNORECASE)):
            return True
        elif (re.match(r'^(no|false)$', value, re.IGNORECASE)):
            return False
        else:
            return False

    def warn_deprecated_variables(self):
        for env_var in self.deprecated_env_vars:
            if ((env_var in os.environ) and (os.environ[env_var])):
                dep_msg = self.deprecated_env_vars[env_var]
                print(f"DEPRECATED: {dep_msg}")
                time.sleep(1)

    def show_env(self, skip=[]):
        """
        Print out the environment variables. Note
        that we do NOT run check_env as we want to show all the
        variable settings even if some of the required ones are
        missing.

        Any environment variables that appear in the `skip` parameter
        are not shown.
        """

        #print(skip)
        for env_var in self.env_vars:
            if (env_var in skip):
                continue

            value = '<NOT SET>'
            result = 'pass'
            if (env_var in os.environ):
                value = '"' + os.environ[env_var] + '"'
            else:
                if (env_var in self.required_env_vars):
                    result = 'FAIL'

            print(f"{result}: {env_var:30} ({value})")

        # Warn about any deprecated environment variables.
        self.warn_deprecated_variables()

    def check_for_missing_variables(self, variable_names):
        """Raise an error if any environment variables in `variable_names` are missing.

        Iterate through the list of environment variable names in `variable_names`
        and if any are missing or are the empty string exit with an error.

        Example:

            required_variables = ['PATH', 'HOME']
            self.check_for_missing_variables(required_variables)
        """
        for env_var in variable_names:
            if (env_var not in os.environ):
                msg = f"required variable {env_var} not defined"
                self.exit_with_error(msg)
            else:
                value = os.environ[env_var]
                if (value == ""):
                    msg = f"required variable {env_var} is empty"
                    self.exit_with_error(msg)

    def check_env(self):
        """Check that all required environment variables are present.

        Check all variables in self.required_env_vars and if any are
        missing or are the empty string exit with an error. Also, exit with
        an error if any variables in self.unsupported_env_vars are
        present.
        """
        self.check_for_missing_variables(self.required_env_vars)

        # If any unsupported environment variables are in the environment
        # exit with an error.
        if (self.unsupported_env_vars):
            for env_var in self.unsupported_env_vars:
                if (env_var in os.environ):
                    unsupported_msg = self.unsupported_env_vars[env_var]
                    self.exit_with_error(unsupported_msg)

    def set_defaults(self):
        for env_var in self.env_var_to_default:
            if (env_var not in os.environ):
                default = self.env_var_to_default[env_var]
                self.progress(f"setting {env_var} to default value '{default}'")
                os.environ[env_var] = default

    def run_command(self, cmd, quiet=False):
        """Run the command specified by the array cmd.
           If quiet is set to True, run without showing any output.
           Otherwise, run and show output as the process proceeds
        """
        if (quiet):
            subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        else:
            subprocess.check_call(cmd, stdout=sys.stdout, stderr=subprocess.STDOUT)

    def run_command_full(self, command, input_string=None):
        msg = f"about to run command {command}"
        self.progress(msg)

        if (input_string is not None):
            result = subprocess.run(command, capture_output=True, text=True, input=input_string,)
        else:
            result = subprocess.run(command, capture_output=True, text=True)

        stdout     = result.stdout
        stderr     = result.stderr
        returncode = result.returncode

        msg = f"stdout: {stdout}"
        self.progress(msg)
        msg = f"stderr: {stderr}"
        self.progress(msg)
        msg = f"return code: {returncode}"
        self.progress(msg)

        return (stdout, stderr, returncode)

    def read_from_vault(self, vault_path):
        """Read secret from Vault at path ``vault_path``
        """

        command = ['vault', 'kv', 'get', '-format=json', vault_path]

        stdout, stderr, returncode = self.run_command_full(command)

        if (stdout is None):
            return None

        # Extract data using jq.
        command = ['jq', '-r', '.data.format//.data.data.value']
        stdout, stderr, returncode = self.run_command_full(command, input_string=stdout)

        return stdout.strip()


    def write_to_vault(self, vault_path, content):
        """Write ``content`` to Vault path ``vault_path``
        """
        command = ['vault', 'kv', 'put', vault_path, f"value={content}", f"format=text"]
        stdout, stderr, returncode = self.run_command_full(command)
