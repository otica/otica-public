"""
Given a filter (e.g., render.sh) and a list of file paths, send each file in the
list of files through the filter. Take each resulting transformed file and
put it into a new file whose name is the MD5 checksum of the file path.

Actions:

 * create
 * list
 * delete
 * exists
"""

# pylint: disable=superfluous-parens,no-else-raise

import hashlib
import os
import subprocess
import sys

from otica_abstract import OticaAbstract

class RenderFiles(OticaAbstract):
    """The main class for rendering files.

    The following parameters are required:
      - files
          a list of paths of files to render
      - destination_dir
          the path to a directory where the rendered files
          are to be put
      - render_script_path
          the name of the render script; must be in the PATH
    """
    def __init__(self,
                 files:              list,
                 destination_dir:    str,
                 render_script_path: str,
                 verbose:            bool = None,
                 skip_missing_files: bool = True,
                 ):
        super().__init__(verbose=verbose)

        self.progress(f"render script:         {render_script_path}")
        self.progress(f"files to render:       {files}")
        self.progress(f"destination directory: {destination_dir}")
        self.progress(f"skip_missing_files:    {skip_missing_files}")

        self.render_script_path = render_script_path
        self.destination_dir    = destination_dir
        self.skip_missing_files = skip_missing_files

        # Populate self.files. Start by setting it to the empty array.
        self.files = []

        # If we are skipping missing files remove any missing files now.
        if (self.skip_missing_files):
            for cur_file in files:
                if (os.path.isfile(cur_file)):
                    self.progress(f"file '{cur_file}' exists so add it to our list")
                    self.files.append(cur_file)
                else:
                    self.progress(f"file '{cur_file}' does not exist so skip")
        else:
            self.progress('adding list of files (no skipping needed)')
            self.files = files

        self.validate()


    def validate(self):
        """Check the validity of several properties. Raise an exception
           for ones that are not valid.
        """
        self.progress('starting validation')
        # Check 1. Does the render script exist?
        if (not os.path.isfile(self.render_script_path)):
            raise Exception(f"render script '{self.render_script_path}' does not exist")
        else:
            self.progress(f"render script '{self.render_script_path}' exists")

        # Check 2. Does the destination directory exist?
        if (not os.path.isdir(self.destination_dir)):
            raise Exception(f"destination directory '{self.destination_dir}' does not exist")
        else:
            self.progress(f"destination directory '{self.destination_dir}' exists")

        # Check 3. Ensure that all the files to be rendered exist.
        for file1 in self.files:
            if (not os.path.isfile(file1)):
                raise Exception(f"file to be rendered '{file1}' does not exist")
            else:
                self.progress(f"file to be rendered '{file1}' exists")

    def create(self):
        """Render all the files in self.files by sending them through
        the render script and storing them in self.destination_dir with
        names that are MD5 hashes of their path names.
        """
        self.progress('starting rendering')
        for file1 in self.files:
            self.render_file(file1)

    @staticmethod
    def hashed_filename(filename):
        """Given the path (absolute or relative) generate the has of that path."""
        hasher =  hashlib.md5()
        hasher.update(filename.encode())
        return hasher.hexdigest()

    def destination(self, filename):
        """Return the path to the destination file of the rendered output. Will be
        in self.destination_dir and have as its name the hashed value of
        the source path.
        """
        return os.path.join(self.destination_dir, RenderFiles.hashed_filename(filename))

    def render_file(self, file1):
        """Given a file run ithrough the renderer and store in it self.destination_dir.
        """
        self.progress(f"rendering file '{file1}'")

        # Calculate destination.
        destination = self.destination(file1)
        self.progress(f"hashed name for '{file1}' is {destination}")

        cmd = [
            'bash',
            '-c',
            f"{self.render_script_path} < {file1} > {destination}",
        ]

        self.progress(f"{cmd}")
        try:
            subprocess.run(cmd, capture_output=True, check=True)
        except subprocess.CalledProcessError as e:
            print(f"command '{cmd}' failed")
            print(f"stderr: {e.stderr}")
            sys.exit(1)

    def rendered_file_paths(self):
        """Return a list of the pathnames of the destination files, that is, the
        paths of the _rendered_ files. Note that if the rendering has not
        yet happened this list may include files that do not yet exist.
        """
        self.progress('starting list')

        results = []
        for file1 in self.files:
            destination = self.destination(file1)
            results.append(destination)

        return results

    def exists(self):
        """Returns True if ALL the rendered files exist, False otherwise."""
        self.progress('starting exists')

        for rendered_file_path in self.rendered_file_paths():
            # pylint: disable=no-else-return
            if (not os.path.isfile(rendered_file_path)):
                self.progress(f"file '{rendered_file_path}' does not exist (or is not a file)")
                return False
            else:
                self.progress(f"file '{rendered_file_path}' exists and is a file")

        return True

    def delete(self):
        """Delete all the destination (i.e., rendered) files."""
        self.progress('starting delete')

        for rendered_file_path in self.rendered_file_paths():
            if (not os.path.isfile(rendered_file_path)):
                self.progress(f"file '{rendered_file_path}' does not exist (or is not a file) so skipping")
            else:
                os.remove(rendered_file_path)
                self.progress(f"file '{rendered_file_path}' deleted")
