#!/usr/bin/env bash

# Use:
#   render-file-dir.sh <source-directory> [<target-directory>]

# Send every file in <source-directory> (recursively) through
# render-file.sh. If <target-directory> is given, the files in
# <source-directory> are copied into <target-directory> and then
# the files in <target-directory> are sent through render-file.sh

# This script respects the RENDER_MODE environment variable. In particular,
# if RENDER_MODE is set to the string "NONE", this script exits without doing
# anything.

# WARNING: this script does NOT remove any files already in
# <target-directory> before running, so you may want to run "rm -rf
# <target-directory>/*" first.


set -e

progress_l() {
    local msg
    msg=$1
    progress "$msg" "progress (render-file-dir.sh): "
}

FILE_COUNTER=0
SHOW_FILE_PROGRESS=false # Will be enabled if large number of files need to be processed.
file_progress() {
    FILE_COUNTER=$(( FILE_COUNTER + 1 ))

    local remainder_00
    local remainder_000

    if [[ "$SHOW_FILE_PROGRESS" = true ]]; then
        remainder_00=$(( FILE_COUNTER % 100 ))
        remainder_000=$(( FILE_COUNTER % 1000 ))

        if (( remainder_00 == 0 )); then
            echo -n "."
        fi

        if (( remainder_000 == 0 )); then
            echo " [$FILE_COUNTER files processed]"
        fi
    fi
}

THIS_DIR=$(dirname "$0")
source $THIS_DIR/functions.sh

# Step 0: Initialization
usage() {
    echo "Usage: render-file-dir.sh <source-directory>"
}

SECONDS=0

SOURCE_DIR=$1

if [[ -z "$SOURCE_DIR" ]]; then
    echo "error: missing SOURCE_DIR argument"
    usage
    exit 1
fi
progress_l "source-directory is $TARGET_DIR"

TARGET_DIR=$2

# If TARGET_DIR was given copy everything from SOURCE_DIR to TARGET_DIR:
if [[ -n "$TARGET_DIR" ]]; then
    # Get number of files in source directory
    number_files_to_copy=$(number_files_in_dir "$SOURCE_DIR")
    if (( number_files_to_copy > 500 )) && [[ "$RENDER_MODE" != "NONE" ]]; then
        msg="WARNING: you are processing ${number_files_to_copy} files"
        msg="${msg}; this may take a while..."
        echo "$msg"
        SHOW_FILE_PROGRESS=true
    fi

    progress_l "copying files from source (${SOURCE_DIR}) into target (${TARGET_DIR})"
    cp -pR "${SOURCE_DIR}/." "${TARGET_DIR}"
    RENDER_DIR="$TARGET_DIR"
else
    progress_l "target-directory not provided so not copying files before rendering"
    RENDER_DIR="$SOURCE_DIR"
fi

if [[ "$RENDER_MODE" == "NONE" ]]; then
    progress_l "RENDER_MODE is set to 'NONE' so nothing to render; exiting"
   exit 0
fi

# Convert everything in SOURCE_DIR (recursively) with render-file.sh.
cd "${RENDER_DIR}"


shopt -s globstar
for file in ./**/*
do
    # a file but not a link
    if [[ -f "$file" ]] && [[ ! -L "$file" ]]; then
        progress_l "processing $file"

        render_file "$file"

        progress_l "finished processing $file"
        file_progress
    fi
done

elapsed_time="$SECONDS"
echo "render-file-dir.sh elapsed time: $elapsed_time seconds ($FILE_COUNTER files processed)"
