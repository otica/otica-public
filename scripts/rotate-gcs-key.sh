#!/usr/bin/env bash
set -e

# rotate the customer supplied encryption key of GCS

THIS_DIR=$(dirname "$0")

usage() {
	echo "Rotate the customer supplied encryption key of GCS:"
	echo "Usage: $0 <old_key> <new_key> <bucket> <object>"
	exit 1
}

vpath=$1
if [ -z "$vpath" ]; then
	usage
fi

old_key=$1
old_sha=$(echo -n $old_key | base64 --decode | sha256sum | base64 | tr -d '\n\r')
new_key=$2
new_sha=$(echo -n $old_key | base64 --decode | sha256sum | base64 | tr -d '\n\r')
bucket=$3
object=$4
token=$(gcloud beta auth application-default print-access-token)

curl -X POST --data-binary @[OBJECT] \
  -H "Authorization: Bearer $token" \
  -H "x-goog-encryption-algorithm: AES256" \
  -H "x-goog-encryption-key: $new_key" \
  -H "x-goog-encryption-key-sha256: $new_sha" \
  -H "x-goog-copy-source-encryption-algorithm: AES256" \
  -H "x-goog-copy-source-encryption-key: $old_key" \
  -H "x-goog-copy-source-encryption-key-sha256: $old_sha" \
  "https://www.googleapis.com/storage/v1/b/${bucket}/o/${object}/rewriteTo/b/${bucket}/o/${object}"
