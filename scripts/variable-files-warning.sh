#!/usr/bin/env bash

# Show any variable files out-of-sync

set -e

THIS_DIR=$(dirname "$0")
source $THIS_DIR/functions.sh

progress "checking for any variable files out-of-sync"

cd ..
if [[ ${SKIP_OTICA_CHECK} == true ]] ; then
    exit 0
fi
otica check
