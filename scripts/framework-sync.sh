#!/usr/bin/env bash
set -e

# Update the files in FRAMEWORK_DIR. Whether or not the update happens
# depends on FRAMEWORK_DIR_SYNC. If FRAMEWORK_DIR_SYNC is:
#
# "NO_SYNC": no update happens
#
# a positive integer: the update happens if more than this number of
# seconds has passed since the last update.
#
# anything other value: the update will happen.

# Ignore issues with following sourced external files:
# shellcheck disable=SC1090

# include functions
THIS_DIR=$(dirname "$0")
source "$THIS_DIR"/functions.sh

FRAMEWORK_LAST_SYNC_FILE=${FRAMEWORK_LAST_SYNC_FILE:-${COMMON}/.last_sync_epoch}
progress "FRAMEWORK_LAST_SYNC_FILE is $FRAMEWORK_LAST_SYNC_FILE"

################################################################################

# Local version of exit_with_error (includes sleep)
exit_with_error () {
    echo "error syncing framework code: $1"
    sleep 2
    exit 1
}

# Write the time that we did a framwrok directory sync.
# The time will be in seconds since the epoch.
function write_sync_time() {
    epoch_time=$(date +%s)
    echo "$epoch_time" > "$FRAMEWORK_LAST_SYNC_FILE"
    progress "wrote last synced'ed time to $FRAMEWORK_LAST_SYNC_FILE"
}

# This function sets the global variable SYNC_LAST_EPOCH to the epoch
# time stored in FRAMEWORK_LAST_SYNC_FILE (if that file exists).
function read_sync_time() {
    if [[ -f "$FRAMEWORK_LAST_SYNC_FILE" ]]; then
       SYNC_LAST_EPOCH=$(cat "$FRAMEWORK_LAST_SYNC_FILE")
       progress "last auth epoch: $SYNC_LAST_EPOCH"
    else
       progress "no last framework sync file $FRAMEWORK_LAST_SYNC_FILE"
    fi
}

# This function sets the global variable REAUTH_NEEDED to 0, if a re-auth
# is needed, or to 1, if a re-auth is not needed.
function sync_needed() {
    read_sync_time
    CURRENT_EPOCH=$(date +%s)

    LAST_SYNC_SECONDS=$((CURRENT_EPOCH - SYNC_LAST_EPOCH))
    progress "$LAST_SYNC_SECONDS seconds elapsed since last sync"

    if (( LAST_SYNC_SECONDS > FRAMEWORK_DIR_SYNC )); then
        progress "more than $FRAMEWORK_DIR_SYNC seconds elapsed, so sync is needed"
        SYNC_NEEDED=1
    else
        progress "fewer than $REAUTH_SECONDS seconds elapsed, so no sync is needed"
        SYNC_NEEDED=0
    fi
}

function do_sync() {
    progress "doing the sync"
    if [[ -d "${FRAMEWORK_DIR}/.git/" ]]; then
        # IMPORTANT! Do the checkout FIRST and the "git pull" SECOND. Otherwise
        # you could merge one branch with another which is probably not what
        # you want to do.
        echo "Updating Otica cloud framework from Git into ${FRAMEWORK_DIR}..."

        # Step 1. Checkout the branch or tag pointed to by FRAMEWORK_GIT_BRANCH.
        progress "checking out $FRAMEWORK_GIT_BRANCH"
        git -C "${FRAMEWORK_DIR}" checkout "$FRAMEWORK_GIT_BRANCH"

        # Step 2. Merge the FRAMEWORK_GIT_BRANCH with remote.
        progress "merging $FRAMEWORK_GIT_BRANCH with remote"
        git -C "${FRAMEWORK_DIR}" pull "${FRAMEWORK_GIT_REMOTE}" "${FRAMEWORK_GIT_BRANCH}"
    else
        echo "Updating Otica cloud framework in ${FRAMEWORK_DIR}..."
        mkdir -p "${FRAMEWORK_DIR}"

        # We append a string that changes each time to get around retry slow-downs (is this true???)
        date_string=$(date +%s)
        framework_url="https://storage.googleapis.com/${FRAMEWORK_BUCKET}/framework.tar.gz?random=${date_string}"
        curl --retry 3 -s "$framework_url" | tar -xzf - -C "${FRAMEWORK_DIR}"

        framework_version=$(cat "${FRAMEWORK_DIR}"/sha.txt)
        echo "framework version: $framework_version"
    fi

    # Update the file keeping track of the last sync time:
    write_sync_time
}

################################################################################

progress "deciding whether to sync FRAMEWORK_DIR"
progress "FRAMEWORK_DIR:        $FRAMEWORK_DIR"
progress "FRAMEWORK_DIR_SYNC:   $FRAMEWORK_DIR_SYNC"
progress "FRAMEWORK_GIT_BRANCH: $FRAMEWORK_GIT_BRANCH"

################################################################################

# Case 1: NO_SYNC
if [[ "$FRAMEWORK_DIR_SYNC" == "NO_SYNC" ]]; then
    progress "FRAMEWORK_DIR_SYNC set to 'NO_SYNC' so skipping sync"
    exit 0
fi

# Case 2: a number of seconds.
REGEX_NUMBER='^[0-9]+$'
if [[ "$FRAMEWORK_DIR_SYNC" =~ $REGEX_NUMBER ]]; then
    progress "FRAMEWORK_DIR_SYNC set to a number"
    sync_needed

    if [[ "$SYNC_NEEDED" == 1 ]]; then
        do_sync
    else
        progress "skipping sync"
    fi
    exit 0
fi

# Case 3: all other cases. Do the sync.
progress "not using sync timeouts so will sync"
do_sync
exit 0
