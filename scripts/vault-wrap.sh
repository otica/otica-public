#!/usr/bin/env bash
# Get the kv sec wrap token from path
# Usage:
#   vault-wrap <path> <ttl>

THIS_DIR=$(dirname "$0")

# include functions
source $THIS_DIR/functions.sh

if [[ -z "$VAULT_ADDR" ]]; then
    exit_with_error "missing required environment variable VAULT_ADDR"
fi

export GCP_ENVIRONMENT=${GCP_ENVIRONMENT:-dev}
export SEC_ENV=${SEC_ENV:-$GCP_ENVIRONMENT}

if ! vault --version | grep 'v1.' &> /dev/null
then
    (>&2 echo "The vault version is too old, please upgrade the vault cmd.")
    exit 1
fi

path=$(echo $1 | ${THIS_DIR}/render.sh)

# set default ttl to 5m
ttl=${2:-5m}

# Get the kv sec wrap token from path and return the unwrap url
if value=$(vault kv get -wrap-ttl=$ttl -format=json $path) ; then
    token=$(echo $value | jq -r '.wrap_info.token')
    if [ ! -z "$token" ] ; then
        echo $token
        exit 0
    fi
fi

exit 2
