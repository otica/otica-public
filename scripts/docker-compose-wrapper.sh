#!/usr/bin/env bash

# A wrapper around the DOcker compose executable used by the
# docker-compose.mk makefile module to get around some of the
# unpleasantness of formatting complex commands in a make file.

. functions.sh

export DEBUG=

VERSION_OPTION=""
DRYRUN_OPTION=""

####################################################################################

do_docker_compose_command () {
    ${DOCKER_COMPOSE_BIN} -f ${DOCKER_COMPOSE_FILE} $1
}

####################################################################################

if [[ -z "$COMPOSE_PROJECT_NAME" ]]; then
    echo "missing required variable COMPOSE_PROJECT_NAME"
    exit 1
fi

ACTION=$1

progress "ACTION is '$ACTION'"

cd ${DOCKER_COMPOSE_SOURCE_DIR}
progress "changed directory to '${DOCKER_COMPOSE_SOURCE_DIR}'"

if [[ "$ACTION" == "config" ]]; then
    do_docker_compose_command "config"
fi

if [[ "$ACTION" == "ps" ]]; then
    do_docker_compose_command "ps"
fi

if [[ "$ACTION" == "up" ]]; then
    do_docker_compose_command "up --detach"
fi

if [[ "$ACTION" == "down" ]]; then
    do_docker_compose_command "down --remove-orphans"
fi

if [[ "$ACTION" == "logs" ]]; then
    do_docker_compose_command "logs --follow"
fi

if [[ "$ACTION" == "build" ]]; then
    do_docker_compose_command "build --pull"
fi

exit 0
