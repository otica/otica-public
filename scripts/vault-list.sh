#!/usr/bin/env bash
set -e

# Usage:
#   vault-read <path>

# include functions
source $THIS_DIR/functions.sh

# include functions
THIS_DIR=$(dirname "$0")

if [[ -z "$VAULT_ADDR" ]]; then
   exit_with_error "missing required environment variable VAULT_ADDR"
fi

export GCP_ENVIRONMENT=${GCP_ENVIRONMENT:-dev}
export SEC_ENV=${SEC_ENV:-$GCP_ENVIRONMENT}

path=$(echo $1 | ${THIS_DIR}/render.sh)

vault kv list $path
