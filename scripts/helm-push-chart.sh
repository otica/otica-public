#!/usr/bin/env bash

# Push a Helm chart. This wrapper script can push to a 'gs://' Helm chart
# repository using the GCS (Google Cloud Storage) plugin (but the plugin had
# better be installed). It can also push to OCI (Open Container Initiative)
# registries.

THIS_DIR=$(dirname "$0")
source $THIS_DIR/functions.sh


# 1. Check that the necessary environment variables are defined.
if [[ -z "$HELM_REPO_URL" ]]; then
    exit_with_error "HELM_REPO_URL is not defined"
fi

if [[ -z "$HELM_CHART_NAME" ]]; then
    exit_with_error "HELM_CHART_NAME is not defined"
fi

# 2.a. Is this a gs:// repo? or an oci:// repo?
regex_gs='^gs://.*$'
regex_oci='^oci://.*$'
if [[ "$HELM_REPO_URL" =~ $regex_gs ]]; then
    IS_GCS_REPO="YES"
    IS_OCI_REPO=""
elif [[ "$HELM_REPO_URL" =~ $regex_oci ]]; then
    IS_GCS_REPO=""
    IS_OCI_REPO="YES"
else
    IS_GCS_REPO=""
    IS_OCI_REPO=""
fi

# 3. $HELM_REPO_NAME had better not be not defined if this is an OCI-based registry.
if [[ -n "$IS_OCI_REPO" && -n "$HELM_REPO_NAME" ]]; then
    msg="cannot have HELM_REPO_NAME defined with an OCI-based registry"
    exit_with_error "$msg"
fi

# 4. Define the Helm repository destination. If this is OCI-based
# we use HELM_REPO_URL otherwise we use HELM_REPO_NAME.
if [[ -n "$IS_OCI_REPO" ]]; then
    HELM_REPO_DESTINATION="$HELM_REPO_URL"
else
    HELM_REPO_DESTINATION="$HELM_REPO_NAME"
fi

# 5. Make sure there is EXACTLY one chart
NUM_FILES=$(ls "${HELM_CHART_NAME}"*.tgz | wc -l)

if (( NUM_FILES == 0 )); then
    exit_with_error "no packaged HELM charts found"
elif (( NUM_FILES > 1 )); then
    exit_with_error "more than one packaged HELM chart found"
fi

# 6. Are we pushing to a 'gs://' Helm repository?
if [[ -n "$IS_GCS_REPO" ]]; then
    gcs_subcommand=" gcs"
else
    gcs_subcommand=""
fi

# 7. Are we forcing the push?
if [[ -n "$HELM_PUSH_FORCE" ]]; then
    echo "forcing helm push..."
    force_option=" --force"
else
    force_option=""
fi

# 8. We have a single Helm package, so we can now upload it.
helm $gcs_subcommand push *.tgz "$HELM_REPO_DESTINATION" $force_option

exit 0
