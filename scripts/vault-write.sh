#!/usr/bin/env bash
# Usage:
#   vault-write <path> [<secret vaule> | @<secret file>]

THIS_DIR=$(dirname "$0")

source $THIS_DIR/functions.sh

if [[ -z "$VAULT_ADDR" ]]; then
    exit_with_error "missing required environment variable VAULT_ADDR"
fi

export GCP_ENVIRONMENT=${GCP_ENVIRONMENT:-dev}
export SEC_ENV=${SEC_ENV:-$GCP_ENVIRONMENT}

if ! vault --version | grep 'v1.' &> /dev/null
then
    (>&2 echo "The vault version is too old, please upgrade the vault cmd.")
    exit 1
fi

path="$1"

if [[ "$2" =~ ^@ ]];
then
    # Case A: The data to be added to Vault is in a file.
    src=$(echo $2 | cut -c 2-)
    if file -b --mime-encoding $src | grep -s binary > /dev/null
    then
        # If the data in the file is binary, base64 encode it and set
        # format=base64.
        cat $src | base64 | vault kv put $path value=- format="base64"
    else
        # If the data in the file is NOT binary set format=text.
        cat $src | vault kv put $path value=- format="text"
    fi
else
    # Case B: The data to be added to Vault is on the command line. This
    # only works when the secret to be added is a text string.
    vault kv put $path value="$2" format="text"
fi

exit 0

DOCS=<<__END_OF_DOCS__

=head1 NAME

vault-write.sh -- Write secrets into Hashicorp Vault

=head1 USAGE

B<vault-write.sh> I<vault-path> I<secret>

B<vault-write.sh> I<vault-path> I<@filename>

=head1 DESCRIPTION

Given a I<vault-path> B<vault-write.sh> writes a secret into
Hashicorp's Vault. The script gets the Vault address from the environment
variable C<VAULT_ADDR>; if C<VAULT_ADDR> is not defined B<vault-write.sh>
will exit with an error.

There are two ways you pass the secret to B<vault-write.sh>. If your secret
is a text string you can pass it directly from the command line:

    $ vault-write.sh secret/projects/mygroup/password "p@ssW@rd"

The second method is to put the secret into a file. This method allows adding
both text and non-text passwords. Here is an example where we generate some
random binary characters and then add them to Vault:

    $ head -c32 </dev/urandom > random.bin
    $ vault-write.sh secret/projects/mygroup/password '@random.bin'

Note the leading C<@>; this tells B<vault-write.sh> that the data should be
read from the file F<random.bin>.

=head1 BASE64 ENCODING

Secrets added by B<vault-write.sh> will be stored with an extra key-value pair.

If the secret added is detected to be binary then the secret is
Base64-encoded and the key-value pair "(format,base64)" is added along
with the data.

If the secret added is not binary then the secret is added without
transformation and the key-value pair "(format,text)" is added along with
the data.

It is best to use read Vault secrets stored with B<vault-write.sh> with its
sister script B<vault-read.sh>. This is because vault-read.sh knows to look
at the "format" key-value pair and correctly decode (if necessary) the
secret.

=head1 OPTIONS

There are no options.

=head1 EXIT VALUE

Returns C<0> on success, C<1> on any failure.

