#!/usr/bin/env bash
# Revoke login token and all its children
# and cleanup local cache

THIS_DIR=$(dirname "$0")

source $THIS_DIR/functions.sh

if [[ -z "$VAULT_ADDR" ]]; then
    exit_with_error "missing required environment variable VAULT_ADDR"
fi

VAULT_CACHE=${VAULT_CACHE:-$HOME/.vault-local}

echo "Logout ${VAULT_ADDR}"
vault token revoke -self &> /dev/null
rm -f ${HOME}/.vault-token

# Cleanup local cache
rm -rf ${VAULT_CACHE}
