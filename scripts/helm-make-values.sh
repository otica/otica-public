#!/usr/bin/env bash

# Given a list of YAML files
# envsubst.

# Use: helm-make-values.sh COMMON|APP_ENV [write]
#
# The first argument is required. If equal to "COMMON" acts on the file
# $COMMON/values-local.yaml; if equal to "APP_ENV" acts on the file
# $APP_ENV/value-local.yaml.
#
# The second argument is optional. If it exists and is equal to the string
# "write" sends the results to a file, otherwise sends the result to
# standard output.

if [[ -z "$APP_ENV" ]]; then
    echo "missing required APP_ENV"
    exit 1
fi

if [[ -z "$COMMON" ]]; then
    echo "missing required COMMON"
    exit 1
fi

DATADIR="${COMMON}/data"

DEST=$1
WRITE=$2

if [[ "$DEST" == "COMMON" ]] ; then
    if [[ "$WRITE" == "write" ]]; then
        # COMMON
        mkdir -p "${DATADIR}/common"
        # If there is a "common" values-local.yaml file run that though envsubst
        if [[ -e "${COMMON}/values-local.yaml" ]]; then
            cat "${COMMON}/values-local.yaml" | envsubst > "${DATADIR}/common/values-local.yaml"
        else
            # No such file, so create an empty YAML file so that the helm command is happy
            rm -f "${DATADIR}/common/values-local.yaml"
            touch "${DATADIR}/common/values-local.yaml"
        fi
    else
        if [[ -e "${COMMON}/values-local.yaml" ]]; then
            echo "COMMON:"
            cat "${COMMON}/values-local.yaml" | envsubst
        fi
    fi
elif [[ "$DEST" == "APP_ENV" ]] ; then
    if [[ "$WRITE" == "write" ]]; then
        # Environment Instance
        mkdir -p "${DATADIR}/${APP_ENV}"
        if [[ -e "values-local.yaml" ]]; then
            mkdir -p "${DATADIR}/${APP_ENV}"
            cat values-local.yaml | envsubst > "${DATADIR}/${APP_ENV}/values-local.yaml"
        else
            # No such file, so create an empty YAML file so that the helm command is happy
            rm -f "${DATADIR}/${APP_ENV}/values-local.yaml"
            touch "${DATADIR}/${APP_ENV}/values-local.yaml"
        fi
    else
        if [[ -e "values-local.yaml" ]]; then
            echo "APP_ENV:"
            cat values-local.yaml | envsubst
        fi
    fi
else
    echo "missing second argument"
    exit 1
fi

echo

exit 0
