#!/usr/bin/env bash
set -e

###############################################################################
# get kubernetes cluster credentials and switch kubeconfig context to the cluster
###############################################################################

THIS_DIR=$(dirname "$0")
KUBE_CLUSTER_NAME=${KUBE_CLUSTER_NAME:?'KUBE_CLUSTER_NAME not set'}
AWS_REGION=${AWS_REGION:?'AWS_REGION not set'}

KUBE_NAMESPACE=${KUBE_NAMESPACE:-default}
if [ -z ${KUBE_CLUSTER_NAME} ]
then 
  echo "WARNING: KUBE_CLUSTER_NAME is missing."
  exit 1
fi

if ! aws eks describe-cluster --name=${KUBE_CLUSTER_NAME} --region=${AWS_REGION} &> /dev/null; then
  echo "Failure running 'aws eks describe-cluster --name=${KUBE_CLUSTER_NAME} --region=${AWS_REGION}'"
  echo Cannot run aws eks command. Forget to assume ${AWS_ROLE_NAME} role?
  exit 1
fi
AWS_ROLE_ARN="arn:aws:sts::${AWS_ACCOUNT_ID}:assumed-role/${AWS_ROLE_NAME}"
if aws eks describe-cluster --name=${KUBE_CLUSTER_NAME} --region=${AWS_REGION} | grep ACTIVE &> /dev/null
then
  aws eks update-kubeconfig --name=${KUBE_CLUSTER_NAME} --region=${AWS_REGION}
  ctx=$(kubectl config current-context)
  kubectl config set-context $ctx --namespace=$KUBE_NAMESPACE
else
  echo ${KUBE_CLUSTER_NAME} is not running.
fi
