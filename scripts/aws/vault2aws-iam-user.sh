#!/usr/bin/env bash
set -e
set -u

VAULT_AWS_BACKEND=${VAULT_AWS_BACKEND}
AWS_PROFILE=${AWS_PROFILE}
AWS_REGION=${AWS_REGION}
REFRESH=${REFRESH:-false}

function check_profile_ok() {
    # Check if the profile ${AWS_PROFILE} exsits
    if ! aws configure list --profile ${AWS_PROFILE} &> /dev/null; then
        echo " " >> $HOME/.aws/credentials
        echo "[${AWS_PROFILE}]" >> $HOME/.aws/credentials
        return 1
    fi
    # Check the credentails 
    aws --profile ${AWS_PROFILE} iam get-user &> /dev/null
    return $?
}

if ! check_profile_ok || [[ "${REFRESH}" = "true" ]]; then
    echo "Getting a new credential for ${AWS_PROFILE} from  ${VAULT_AWS_BACKEND}"
    creds=$(vault read -format json ${VAULT_AWS_BACKEND})
    access_key=$(echo $creds | jq -r '.data.access_key')
    secret_key=$(echo $creds | jq -r '.data.secret_key')

    aws configure --profile ${AWS_PROFILE} set aws_access_key_id $access_key
    aws configure --profile ${AWS_PROFILE} set aws_secret_access_key $secret_key
    aws configure --profile ${AWS_PROFILE} set region $AWS_REGION
    # for AWS IAM keys are eventually consistent
    sleep 15
fi

aws --profile ${AWS_PROFILE} iam get-user
