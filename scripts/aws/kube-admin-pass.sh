#!/usr/bin/env bash

###############################################################################
# get kubernetes cluster credentials
###############################################################################

THIS_DIR=$(dirname "$0")
# include functions
source $THIS_DIR/functions.sh

source env.mk

# fail on error or undeclared vars
trap_errors

if [ -z ${KUBE_CLUSTER_NAME} ]
then 
  echo "WARNING: KUBE_CLUSTER_NAME is missing."
  exit 1
else
  kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | \
    grep eks-admin | awk '{print $1}')
fi