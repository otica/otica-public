#!/usr/bin/env bash
set -e
set -u

if [[ -z "$VAULT_ADDR" ]]; then
    echo "missing required environment variable VAULT_ADDR"
    exit 1
fi

../vault-login.sh
(return 2>/dev/null) && sourced=1 || sourced=0

usage(){
    echo
    echo "WARNING: this script modifies AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_SESSION_TOKEN"
    echo "environment variables, and thus must be 'sourced'. Do not call directly."
    echo
    echo "Generate time-bound AWS STS credentials"
    echo
    echo "Usage:"
    echo "source ${0} <vault_aws_backend> <iam_role>"
    echo
    echo "Required parameters:"
    echo "<vault_aws_backend>: the aws secret engine path in Vault"
    echo "<iam_role>:  the name of the IAM role to be attached to the generated credentials"
}

assume_role () {
    REFRESH=${REFRESH:-false}
    AWS_ACCOUNT_NAME=$1
    AWS_ROLE_NAME=$2
    SESSION_DURAION=${3:-120m}

    if ! aws sts get-caller-identity > /dev/null 2>&1 || \
        ! aws sts get-caller-identity | grep  "arn:aws:sts::${AWS_ACCOUNT_ID}:assumed-role/${AWS_ROLE_NAME}"
    then
        echo "Getting new cluster-admin role."
        vault read --format=json aws-${AWS_ACCOUNT_NAME}/sts/${AWS_ROLE_NAME} -ttl=${SESSION_DURAION} > /tmp/sts.json
        AWS_ACCESS_KEY_ID=$(jq -r '.data.access_key' /tmp/sts.json)
        AWS_SECRET_ACCESS_KEY=$(jq -r '.data.secret_key' /tmp/sts.json)
        AWS_SESSION_TOKEN=$(jq -r '.data.security_token' /tmp/sts.json)
        export_envars
    fi
}

export_envars () {
    export AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
    export AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
    export AWS_SESSION_TOKEN=${AWS_SESSION_TOKEN}
    echo "Assumed the following IAM role"
    aws sts get-caller-identity
}

if [[ $sourced -eq 0 && -z has_run ]]; then
    usage
    exit
fi

has_run="true"

assume_role "$@"
