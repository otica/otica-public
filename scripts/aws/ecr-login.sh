#!/usr/bin/env bash

###############################################################################
# Login to AWS ECR cluster credentials and verify
###############################################################################

THIS_DIR=$(dirname "$0")
ECR_REGION=${ECR_REGION:-'us-west-2'}

if ! aws ecr get-authorization-token --region=${ECR_REGION} &> /dev/null; then \
  echo "Cannot run aws get-authorization-token command." ; \
  echo Forget to run "source $HOME/bin/ps-cloud-framework/scripts/aws/vault2aws-sts.sh ${AWS_ACCOUNT_NAME} ${AWS_ROLE_NAME}"?
  exit 1
fi

ECR_ENDPOINT="$(aws ecr get-authorization-token --region=${ECR_REGION} | jq -r '.authorizationData[].proxyEndpoint')"
aws ecr get-login-password --region=${ECR_REGION} | \
  docker login --username AWS --password-stdin ${ECR_ENDPOINT}
