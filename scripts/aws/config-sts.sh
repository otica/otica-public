#!/usr/bin/env bash
set -e
set -u

function cloud_auth_user() {
    if ! aws sts get-caller-identity > /dev/null 2>&1 || \
        ! aws sts get-caller-identity | grep -q "arn:aws:sts::${AWS_ACCOUNT_ID}:assumed-role/${AWS_ROLE_NAME}" > /dev/null 2>&1
    then
        echo; echo "Please cut-and-paste the following command to get AWS STS token and try make config again."; echo
        echo "source ${FRAMEWORK_DIR}/scripts/aws/vault2aws-sts.sh ${AWS_ACCOUNT_NAME} ${AWS_ROLE_NAME}"
        echo
        exit 1
    fi
}
# Auth with cluster-admin credentials
cloud_auth_user
