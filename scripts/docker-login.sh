#!/usr/bin/env bash
set -e

# Login to the Docker remote registry DOCKER_REGISTRY.

# include functions
THIS_DIR=$(dirname "$0")
source "$THIS_DIR"/functions.sh

############################################################
missing_required_variable () {
    exit_with_error "missing value for required variable $1"
}

# Take the passed in string and from it construct the image repo string.
# That is, return nothing if the string is "docker.io", otherwise return
# the string.
#
# Sets the variable REPO_SERVER_URL
REPO_SERVER_URL=unknown
construct_docker_repo_url () {
    repo=$1

    if [[ "$repo" =~ docker.io ]]; then
        REPO_SERVER_URL=""
    else
        REPO_SERVER_URL="$repo"
    fi
}

############################################################


if [[ -z "$DOCKER_REGISTRY" ]]; then
    missing_required_variable "DOCKER_REGISTRY"
fi


# If DOCKER_REGISTRY_LOGIN_TYPE is not set default to "vault".
if [[ -z "$DOCKER_REGISTRY_LOGIN_TYPE" ]]; then
    progress "DOCKER_REGISTRY_LOGIN_TYPE not set; defaulting to 'vault'"
    DOCKER_REGISTRY_LOGIN_TYPE="vault"
fi

progress "DOCKER_REGISTRY:               $DOCKER_REGISTRY"
progress "DOCKER_REGISTRY_LOGIN_TYPE:    $DOCKER_REGISTRY_LOGIN_TYPE"
progress "DOCKER_REGISTRY_PASSWORD_PATH: $DOCKER_REGISTRY_PASSWORD_PATH"
progress "DOCKER_REGISTRY_USERNAME:      $DOCKER_REGISTRY_USERNAME"

if [[ "$DOCKER_REGISTRY_LOGIN_TYPE" == 'gcloud' ]]; then
    progress "running 'auth configure-docker $DOCKER_REGISTRY'"
    gcloud auth configure-docker $DOCKER_REGISTRY

    progress "running 'docker login https://$DOCKER_REGISTRY'"
    docker login "https://$DOCKER_REGISTRY"
else
    if [[ -z "$DOCKER_REGISTRY_USERNAME" ]]; then
        missing_required_variable "DOCKER_REGISTRY_USERNAME"
    fi

    if [[ -z "$DOCKER_REGISTRY_PASSWORD_PATH" ]]; then
        missing_required_variable "DOCKER_REGISTRY_PASSWORD_PATH"
    fi

    if [[ "$DOCKER_REGISTRY_LOGIN_TYPE" == 'vault' ]]; then
        progress "getting credentials from Vault"

        # Set REPO_SERVER_URL from DOCKER_REGISTRY.
        construct_docker_repo_url "$DOCKER_REGISTRY"

	    vault-read.sh "$DOCKER_REGISTRY_PASSWORD_PATH" \
		    | docker login -u "$DOCKER_REGISTRY_USERNAME" --password-stdin "$REPO_SERVER_URL"
    elif [[ "$DOCKER_REGISTRY_LOGIN_TYPE" == 'file' ]]; then
        progress "getting credentials from file $DOCKER_REGISTRY_PASSWORD_PATH"
        if [[ ! -f "$DOCKER_REGISTRY_PASSWORD_PATH" ]]; then
            exit_with_error "no file at DOCKER_REGISTRY_PASSWORD_PATH '$DOCKER_REGISTRY_PASSWORD_PATH'"
        fi

	    cat "$DOCKER_REGISTRY_PASSWORD_PATH" \
		    | docker login -u "$DOCKER_REGISTRY_USERNAME" --password-stdin "$REPO_SERVER_URL"
    else
        exit_with_error "unrecognized value '$DOCKER_REGISTRY_LOGIN_TYPE' for DOCKER_REGISTRY_LOGIN_TYPE"
    fi
fi

progress "docker login complete"
exit 0
