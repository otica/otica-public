#!/usr/bin/env bash
set -e

function send_notification() {
  export TITLE STATUS MESSAGE
  slack-tmpl.sh > /tmp/slack.json
  curl -X POST -H 'Content-type: application/json' \
    -d "@/tmp/slack.json" ${SLACK_HOOK}
}
SLACK_HOOK=${SLACK_HOOK:-$1}

if [[ -z "$TITLE" ]] || [[ -z "$STATUS" ]] || [[ -z "$MESSAGE" ]] || [[ -z "$SLACK_HOOK" ]] ;
then
  echo missing TITLE STATUS MESSAGE or SLACK_HOOK
  exit 1
fi
send_notification
