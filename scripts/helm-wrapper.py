#!/usr/bin/env python3

"""
Command-line interface to the helm_wrapper Python 3 class.
"""

# pylint: disable=superfluous-parens,no-else-raise

import argparse
import pathlib
import os
import sys

# Add './lib' to search path. We need to do this
# so that we can import helm_wrapper
MY_DIRECTORY = pathlib.Path(__file__).parent.resolve()
sys.path.append(os.path.join(MY_DIRECTORY, 'lib'))

from helm_wrapper import HelmWrapper

def main():
    """This function is called when this script is run on the command-line."""

    parser = argparse.ArgumentParser(description='Send a list of files ' \
                                     'through a filter and save with MD5 sum filenames')
    parser.add_argument('--verbose', '-v', action="store_true")
    parser.add_argument('--dryrun', action="store_true")
    parser.add_argument('action', nargs=1,
                        choices=['show-env',
                                 'check-env',
                                 'install',
                                 'install-dryrun',
                                 'upgrade',
                                 'upsert',
                                 'merge',
                                 'template',
                                 'diff',
                                 'fail-if-oci',
                                 ],
                        help='action to take')
    parser.add_argument('--destination-dir', dest='destination_dir', default='/tmp',
                        help='directory to save the rendered files')

    args = parser.parse_args()

    helm_wrapper = HelmWrapper(
        verbose=args.verbose,
    )

    action = args.action[0]
    helm_wrapper.progress(f"action is '{action}'")

    if (action == 'show-env'):
        helm_wrapper.show_env()
    elif (action == 'check-env'):
        helm_wrapper.check_env()
    elif (action == 'merge'):
        helm_wrapper.merge()
    elif (action == 'template'):
        print(helm_wrapper.template())
    elif (action == 'diff'):
        helm_wrapper.diff()
    elif (action == 'install'):
        helm_wrapper.install()
    elif (action == 'upgrade'):
        helm_wrapper.upgrade()
    elif (action == 'upsert'):
        helm_wrapper.upsert()
    elif (action == 'fail-if-oci'):
        helm_wrapper.check_env()
        helm_wrapper.fail_if_oci()
    else:
        raise Exception('how did I get here?!?')

if (__name__ == "__main__"):
    main()
