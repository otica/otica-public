#!/usr/bin/env bash
set -e

# Usage:
#   vault2text.sh <secret.yml>

# Replace %%vault-secret-path%% with the value stored at
# <vault-secret-path> in its ORGINAL secret value format. Unlike
# vault2kube.sh the secret is NOT Base64-encoded. This means that the
# output might contain non-printable characters.

OLDIFS=$IFS
IFS=''
while read line
do
    path=$(echo $line | grep -v '^#' | grep -o -e '%%.*%%' | tr -d '%')
    if [ -z $path ]
    then
        echo $line
    else
        # Assemble secret from the value retrieved from Vault.
        pre="${line%%%%${path}%%*}"
        post="${line##*%%${path}%%}"
        echo "${pre}$(vault-read.sh $path)${post}"
    fi
done < "${1:-/dev/stdin}"
IFS=$OLDIFS
