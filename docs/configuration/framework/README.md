[[_TOC_]]

# Framework-level configuration

## System variables

The environment variables we describe here are "system" variables and
should almost certainly _never_ be changed. If you do any makefile module
development you should use these variables.

Keep in mind that relative file paths in variable files are always
relative to the directory where the make command is run, that is, to an
environment directory.

### Otica source repository variables

These variables refer to directories in the Otica source code and are typically
absolute paths.  If you installed Otica in `~bin/` these paths will be
subdirectories of `~bin/otica/`.

* `SCRIPTS_DIR`: the absolute path to the Otica `scripts/` directory
containing all the scripts used by Otica make modules.

* `BUILD_DIR`: the subdirectory of an environment directory that holds
intermediate files, usually files run through the `render.sh` script.
This is, by default, set to `build`.

#### `FRAMEWORK_*` variables

When you [installed the Otica source code](???) you cloned it
using Git. Whenever you run a `make` command the [`framework.mk`](../../make-modules/framework/) module
will, potentially, update the code by doing a `git pull`. The
`FRAMEWORK_*` variables tell `framework.mk` where to find the cloned
repository and how often to update.

These variables are set via set in `framework.var`. Be sure to include
`framework.var` in the `environment:toplevel` section of `otica.yaml`.
This way the subproject copy of `framework.var` will be updated from the
toplevel `common/env_variables/` directory copy.

Note: you will **rarely** need to change the value of any of these environment
variables.

* `FRAMEWORK_DIR`: Where the Otica code is installed.
Default: `${HOME}/bin/otica`.

* `FRAMEWORK_DIR_SYNC`: Every time you run a make command the code in
`FRAMEWORK_DIR` (might) get synced with the upstream source. The variable
`FRAMEWORK_DIR_SYNC` controls if and when this syncing happens. If
`FRAMEWORK_DIR_SYNC` is set to "NO_SYNC" then `FRAMEWORK_DIR` will never
be synced. If set to a positive integer, then `FRAMEWORK_DIR` is synced if
at least this number of seconds has passed since the last sync. Finally,
if `FRAMEWORK_DIR_SYNC` is set to any other value (or not set at all)
`FRAMEWORK_DIR` will be synced everytime any make command (including make
with no target) is run. Default: unset

* `FRAMEWORK_GIT_REMOTE`: The Git remote for the Otica code.
Default: `origin`.

* `FRAMEWORK_GIT_BRANCH`: The Git branch to use when accessing the Otica
code.
Default: `master`.

* `FRAMEWORK_TOOLS`: A list of programs that will be checked when
the `make check-tools` target is run. The makefile modules append to this
list programs those makefile modules need.


### Toplevel directory variables

These variables point to directories in the Otica toplevel project. They
are automatically set in the
[`framework.mk`](../../make-modules/framework/) make module and in most
circumstances should not be changed. In the following descriptions
`<toplevel>` refers to the absolute path to the Otica toplevel project
directory.

* `TL_BASEDIR`: the directory `<toplevel>`. This variable must be set or
  `otica` commands such as `otica create` and `otica update` will not
  work.

The following other `TL_*` variables will be set only if `TL_BASEDIR` is
defined.

* `TL_COMMON`: the directory `<toplevel>/common`.

* `TL_ENV_VARIABLES`: the directory `<toplevel>/env_variables`.

* `TL_MAKEFILE_PARTS`: the directory
`<toplevel>/makefile_parts`.

* `TL_SCRIPTS`: the directory `<toplevel>/scripts`.

* `TL_TEMPLATES`: the directory `<toplevel>/templates`.

#### Overriding the `TL_BASEDIR` variable

Normally the top-level directory is calculated by traversing the directory
from the current directory until the directory containing the file
`.otica-top-level` is found indicating the top-level directory.

However, this may not always work if, for example, you have an Otica
subproject in a different directory structure or symbolically linked from
another directory location.

Because `otica` commands such as `otica create` and `otica update` will
**fail** unless `TL_BASEDIR` is set there needs to be a way to get Otica
to work when the top-level directory cannot be found. In these
circumstances you can force `TL_BASEDIR` to a particular static value by
setting the environment variable `TL_BASEDIR_OVERRIDE` to point to the
top-level directory.

**CAUTION!** Use of `TL_BASEDIR_OVERRIDE` to get around the usual
directory structure of an Otica project with caution as this can sometimes
lead to confusing and unexpected behavior.

### Subproject directory variables

These variables refer to directories in a subproject. They are usually
releative paths.

* `COMMON`: the directory `../common/`, that is, the `common` directory in
  an Otica toplevel project or subproject.

* `TEMPLATES`: the `../common/templates` which is where to put Kubernetes
templates. This is only relevant when the subproject is a Kubernetes project
_not_ based on a Helm chart.
