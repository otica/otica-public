[[_TOC_]]

# Variables and variable files

## Introduction

Otica is configured almost entirely using environment variables. (To save
time we will refer to these throughout as "variables" rather than
"environment variables".) These variables are set in "variable files",
that is, a file with the extension `.var` that can be included in a GNU
makefile or in a bash shell script. Similar to CSS style sheets variable
settings can be "cascaded", that is, the same variable can be set in
multiple files and its value is set to the definition in the last variable
file included.

Variables in variable files can derive their values from other variables.

## Variable Files

A variable file is a file whose name ends in `.var` and contains
environment variable settings. Variable files are intended to be included in both
GNU makefiles and Bash shell scripts, so variable files should contain only
comments and variable definitions.

Example:
```
# common.var
export GCP_PROJECT_ID=my-project-id
export GCP_BUCKET_NAME=${GCP_PROJECT_ID}-bucket
...
```

## How variable files are read

The `environment` section of `otica.yaml` file defines which variable
files are included and in which order. Here is part of a typical `otica.yaml` file:
```
environment:
  toplevel:
    - framework.var
    - gcp.var
  common:
    - common.var
    - vault.var
  local:
    - local.var
```

The variable files are loaded in this order:

1. From `environment:toplevel`.

1. From `environment:common`.

1. From `environment:local`.

Within each section the files are loaded in the order they appear in the list.
So, for the above example, the variable files would be loaded in this order:
```
common/env_variables/framework.var
common/env_variables/gcp.var
common/env_variables/vault.var
common/env_variables/common.var
<environment>/local.var (for any environments containing local.var)
```

Variables in files loaded _later_ override the settings from
files loaded _earlier_.

### The `environment:toplevel` section

Variable files listed in the `environment:toplevel` section are copied
from the toplevel Otica project's `common/env_variables/` directory into
the subproject's `common/env_variables` directory. This gives you a
mechanism for setting certain variables in one place but having them sync
to every subproject.

This copying happens _only_ when running an `otica update` or
`otica create`.

**Note.** The variable file `framework.var` _must_ be included in the
`environment:toplevel` section. If it is not included `otica update` will
display an error and refuse to run. Furthermore, we recommend that it be
the first one listed.

#### Environment-specific toplevel variable files (optional)

As described above each Otica [subproject environment
directory](/docs/basics#subproject-environments) will read in the same
variable files listed on in the `otica.yaml` file `environment:toplevel`
section. However, there may be circumstances where you want to read in
_different_ top-level variable files for different subproject environments.

Let's assume your Otica project directory layout looks like this (some files
omitted for simplicity):

```
kube-myapp/
|
|-- otica.yaml
|
|-- common/
|
|-- dev/
|   |
|   '-- local.var
|
|-- uat/
|   |
|   '-- local.var
|
'-- prod/
    |
    '-- local.var
```

The subproject environments are

* `dev`: a development instance you want to run in an on-premise
Kubernetes cluster

* `uat`: a user-acceptance testing instance you want to run on AWS (Amazon
  Web Service) [Elastic Kubernete Service (EKS)][2] service

* `prod`: a production instance you want to run on [Google's Kubernetes
  Engine (GKE)][3] service

Assume further that your top-level directory has these Kubernetes configuration files:

* `onprem.var`: configuration for Otica projects running in the on-premise Kubernetes cluster

* `aws.var`: configuration for Otica projects running in AWS EKS service

* `gcp.var`: configuration for Otica projects using GCP

* `gke.var`: configuration for Otica projects running in the GCP GKE service

How do you tell Otica to load `onprem.var` in the `dev` environment,
`aws.var` in the `uat` environment, and `gcp.var` and `gke.var` in the
`prod` environment?

To do this use a YAML hash rather than simple entry in the
`environment:toplevel` section:

```
environment:
  toplevel:
    - framework.var
    - dev:
        - onprem.var
      uat:
        - aws.var
      prod:
        - gcp.var
        - gke.var
      default:
        - onprem.var
  common:
    - common.var
    - vault.var
  local:
    - local.var
```

As you can see from the above, the YAML hash has as its keys the
subproject environment directory name and as its values the top-level variable
file name (or names). You can provide a single variable file (as in `dev`)
or multiple variable files (as in `prod`).

If the `default` entry is provided, then any subproject not specified will
get those top-level environment variable files.

Keep these points in mind when using environment-specific toplevel
variable files:

1. All files listed _must_ exist in the toplevel directory
`common/env_variables`.

1. Variable-files that are not environment-specific are loaded first, and
only after all those are loaded are environment-specific files loaded.

1. On an `otica update` all top-level variable files, both
environment-specific and non environment-specific, are copied to the
subproject's `common/env_variables` directory.

### The `environment:common` section

Variable files listed in the `environment:common` section are meant for
configuration that applies to every environment within a subproject. For
example, if the Docker image is the same for all your enviroments (`dev`,
`test`, and `prod`) in your subproject you would put the Docker image
definition in `common/env_variables/common.var`.

**Note.** Listing the same filename in both the `environment:otica` and
`environment:common` sections is not allowed and will cause `otica update`
to throw an exception.

**Best practice.** Have only the one entry `common.var` in the the
`environment:common` section and put all your common configuration
settings into `common/env_variables/common.var`. Only break
`common/env_variables/common.var` into multiple files if it gets too long.

### The `environment:local` section

Variable files listed in this section live in the environment directories.
For example, `test/local.var`. When you run `make` inside an environment
directory all the files in that directory listed in the
`environment:local` section are loaded. Override environment-specific
settings in this file.

**Best practice.** Leave `environment:local` set to the single element
`local.var`. Only add additional local variable files if the project
configuration becomes complex.

## Variable cascading

Otica is designed so that a variable can be defined in multiple places and
only the "last" definition is used. Recall that variable files are loaded
first from the toplevel, then from the subproject's `common/env_variables`
directory, and finally from the environment directory. This allows you
to define defaults for variables but to override when necessary.

Example: you have this `otica.yaml` file in a subproject:
```
---
platform: none

environment:
  toplevel:
    - framework.var
    - gcp.var
  common:
    - common.var
  local:
    - local.var

  <rest of otica.yaml excluded for reasons of brevity>
```

Let's say you define `DOCKER_REGISTRY` in three files: `framework.var`,
`common.var`, and `dev/local.var`:
```
# common/env_variables/framework.var
export DOCKER_REGISTRY=gcr.io

# common/env_variables/common.var
export DOCKER_REGISTRY=my_account_id.dkr.ecr.us-west-2.amazonaws.com

# dev/local.var
export DOCKER_REGISTRY=mylocal.repo.com
```

Given these definitions, when you run any make commands in the `dev/`
environment directory of your subproject the value of `DOCKER_REGISTRY`
will be `mylocal.repo.com`. If you had the other environment `test` and
did not define `DOCKER_REGISTRY` in `test/local.var`, then the value of
`DOCKER_REGISTRY` would be the value as defined in `common.var`, i.e.,
`my_account_id.dkr.ecr.us-west-2.amazonaws.com`.

## Variable expansion

Variables in variable files can use other variables in their definition.
Recall that in Otica (almost) everything that happens is via GNU make.
This is important to keep in mind because GNU make has its own rules about
variable expansion. In particular, when GNU make sees a variable
definition like `export MYVARIABLE=${OTHER_VARIABLE}-123` GNU make
_defers_ the expansion of `MYVARIABLE` until the variable is used in a
make target. See also [GNU make's "How make Reads a Makefile" page][1].

This has some counter-intuitive implications, namely, that
variables defined in variables files loaded _later_ can be used in
variables files loaded earlier.

To see this more clearly, consider the above example `otica.yaml` file.
Let's say that the two files `common.var` in `common/env_variables/` and
`local.var` in `dev/`, have these contents:
```
# common.var
export MYVARIABLE1=${MYVARIABLE2}
```
and
```
# dev/local.var
export MYVARIABLE2=abc
```
Even though the variable file `local.var` is loaded after
`common.var` the variable `MYVARIABLE1` will have the
value `abc`.

Furthermore, if we change `local.var` to be this:
```
# common/extra.var
export MYVARIABLE2=def
export MYVARIABLE1=123
```
then` MYVARIABLE1` will have the value `123`.

## Unsetting a variable

If you want to _unset_ a variable in a variable file use `unexport`:
```
unexport DOCKER_NAMESPACE
```
However, note that `unexport` is _not_ recognized by bash so if you are
including a variable file containing an `unexport` in a bash script bash
will generate an error.

You _can_ set the variable to the empty string:
```
export DOCKER_NAMESPACE=
```
This does not undefine the variable but it does set it to the empty string
which, if the scripts are written properly, should be good enough.


[1]: https://www.gnu.org/software/make/manual/html_node/Reading-Makefiles.html

[2]: https://aws.amazon.com/eks/

[3]: https://cloud.google.com/kubernetes-engine
