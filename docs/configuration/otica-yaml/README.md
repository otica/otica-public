[[_TOC_]]

# The `otica.yaml` file configuration

Every Otica project and subproject must contain an `otica.yaml` file. This
file defines basic configuration options and makes accessible various make
modules.

Here is an example:
```
---
environment:
  toplevel:
    # variable files in the environment:toplevel list are synced from the
    # toplevel project's common/env_variables/ directory into the
    # subproject's common/env_variables directory.
    - framework.var
    - gcp.var
  common:
    # variable files listed here are included from the
    # common/env_variables directory in the current subproject.
    - common.var
  local:
    # variable files listed here are included from the
    # directory running the make command.
    - local.var
  local-envs:
    # OPTIONAL. If you want to load files 

makefile_parts:
  otica:
    # makefile modules listed here are included from the Otica
    # source repository.
    - framework.mk
    - autenticate.mk
    - vault.mk
  toplevel:
    # makefile modules listed here are included from the toplevel's
    # common/makefile_parts directory.
    # - project.mk
  common:
    # makefile modules listed here are included from the current
    # subproject's common/makefile_parts directory.
    # - app.mk
```

The `otica.yaml` file is used by the `otica` script when configuring
the project.

* `environment`: names of files that contain environment variables. The
files are loaded in the order given:

  1. Variables files listed in `toplevel`

  1. Variables files listed in `common`

  1. Variables files listed in `local`

  Within each of the above sections the files are loaded in the order
  listed. For example, in the example `otica.yaml` file listing above,
  `framework.var` is loaded before `gcp.var`.

  For more details on how variable files listed in this section work see
  ["Variables and variable files"](../variables/)

* `makefile_parts`: makefile modules you want enabled for this
project.

  * `otica`: These are make modules that come with Otica. The module
    `framework.mk` is _mandatory_ and should be listed first. You will
    normally also have the `vault.mk` module. For others you can enable,
    see the [Make Modules](../make-modules/) documentation.

  * `toplevel`: These are make modules that live in the
     `common/makefile_parts` directory of your toplevel project. Put make
     modules that are specific to your multiple projects that are not yet
     included with Otica. If you have a make module you feel would be
     useful to others consider contributing it to the Otica project.

  * `common`: You can also have make modules that are local to a specific
    subproject. These should be put in the directory
    `common/makefile_parts`.

  As with the `environment:` section makefiles are loaded in the order
  they appear in the `otica.yaml` file.

# Running the `otica` script

The `otica` script reads the `otica.yaml` and generates the
`common/makefile.mk` file. It will also create directories and copy
variable files listed in the `environment:toplevel` section from the
`common/env_variables` directory of your toplevel directory.


# Templating the `otica.yaml` file (advanced topic)

When you create a new subproject a "starter" version of `otica.yaml` is
provided. Typically you then edit this initial version of `otica.yaml` to
add your project's specific toplevel environment files and, perhaps,
custom toplevel make modules.

If you create many subprojects this manual editing can become tedious. To
help automate this process Otica provides a "templating" feature for new
subprojects. Note that this is an optional feature and only applies to
_new_ subproject creation; it does not affect subproject updates.


The file that controls `otica.yaml` templating is
`<toplevel-dir>/common/subproject-platforms.yaml`. Here is an example:

```
---
ALL:
  environment:
    toplevel:
      # variable files in the environment:toplevel list are synced from the
      # toplevel project's common/env_variables/ directory into the
      # subproject's common/env_variables directory.
      - framework.var
    common:
      # variable files listed here are included from the
      # common/env_variables directory in the current subproject.
      - common.var
    local:
      # variable files listed here are included from the
      # directory running the make command.
      - local.var

  makefile_parts:
    otica:
      # makefile modules listed here are included from the Otica
      # source repository.
      - framework.mk
      - vault.mk
      - authenticate.mk
    toplevel:
      # makefile modules listed here are included from the toplevel's
      # common/makefile_parts directory.
      # - project.mk
    common:
      # makefile modules listed here are included from the current
      # subproject's common/makefile_parts directory.
      # - app.mk

gcp:
  environment:
    toplevel:
      - gcp.var
  makefile_parts:
    toplevel:
      - custom.mk
```

In the above, if you create a new subproject with the command `otica
create subproject generic` then the `otica.yaml` file will have the same
elements as under `ALL:`.

On the other hand, if you create a new subproject with the command `otica
create subproject gcp` then the `otica.yaml` file will have the same
elements as under `ALL:` *plus* the elements under `gcp:`:
```
---
environment:
  toplevel:
    - framework.var
    - gcp.var
  common:
    - common.var
  local:
    - local.var

makefile_parts:
  otica:
    - framework.mk
    - vault.mk
    - authenticate.mk
  toplevel:
    - custom.mk
  common: []
```

