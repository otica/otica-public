[[_TOC_]]

# Rendering configuration (`RENDER_MODE`)

## Quick start

The values of `RENDER_MODE` and their effect:

* `ALL`: **all** files will be run through `render2.sh`.

* `NONE`: **no** files will be run through `render2.sh`.

* `TMPL`: only files that end in `.tmpl.<ext>` will be run through
  `render2.sh`. The result will be written to a file with the same name
  but with `.tmpl` removed. Example: if the file is called
  `start.tmpl.sh`, then that file will be run through `render2.sh` and the
  resulting file will be named `start.sh`.

* unset: if `RENDER_MOD` is not defined (or set to the empty string) then
  this is the same as setting `RENDER_MOD` to `ALL`.


## Introduction

Set the environment variable `RENDER_MODE` to affect how `render-file.sh`,
[`render-file-dir.sh`](../../manpages/render-file-dir.sh) (and those make-modules that use these scripts) will
render files when run through [`render2.sh`](../../filters/render2).

In particular, the [`docker-develop`
make-module](../../make-modules/docker-develop) uses `render-file-dir.sh`
in its pre-build processing.

Here are the possible values of `RENDER_MODE` and the effect of each:

* `ALL`: all files will be run through `render2.sh`.

* `NONE`: no files will be run through `render2.sh`.

* `TMPL`: only files that end in `.tmpl.<ext>` will be run through
  `render2.sh`. The result will be written to a file with the same name
  but with `.tmpl` removed. Example: if the file is called
  `start.tmpl.sh`, then that file will be run through `render2.sh` and the
  resulting file will be named `start.sh`.

* unset: if `RENDER_MOD` is not defined (or set to the empty string) then
  this is the same as setting `RENDER_MOD` to `ALL`.

* any other value: this will result in an error.

* **Note**: Files containing `.tmpl` in their filename will be converted
to files without the `.tmpl` in their name only when `RENDER_MODE` is set
to `TMPL`.

## History

The variable `RENDER_MODE` was added in Otica version 1.12. Prior to this,
the `docker-develop` make-module operated as if `RENDER_MODE` was set to
`ALL`. This led to the `docker-develop` make-module taking a very long
time when it had to pre-process many files.

## Recommended use

When using `render-file.sh`, or `render-file-dir.sh` without a target
directory, if `RENDER_MODE` is set to `TMPL` or `ALL` then source files are
**replaced** by their rendered versions. This may not be what you want.

As such it is recommended that you not use `render-file.sh` and instead
only use [`render-file-dir.sh`](../../manpages/render-file-dir.sh) with a
source _and_ a target directory.

Example: `render-file-dir.sh <source-dir> <target-dir>`. This will
first **copy** all files from `<source-dir>` into `<target-dir>` and
then render all the files in `<target-dir>`. This way no files
in `<source-dir>` are ever changed.

For more information see the [`render-file-dir.sh` man
page](../../manpages/render-file-dir.sh).
