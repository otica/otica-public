[[_TOC_]]

# Configuration

* [Variables and variable files](variables/)

* [Framework-level configuration](framework/)

* [The `otica.yaml` file configuration](otica-yaml/)

* [Rendering configuration (`RENDER_MODE`)](render/)
