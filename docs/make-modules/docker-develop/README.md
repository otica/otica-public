[[_TOC_]]

# Developing with Docker

The [`docker-develop.mk`](../../makefile_parts/docker-develop.mk) Makefile
module supports Docker development including builds and pushes.

## Quick start

To build:

        $ make docker-build

To push to the container repository:

        $ make docker-push

To see this help page:

        $ make docker-help

## Configuration

### Directory structure

This module expects that all Dockerfiles, support files, etc., are in
`common/docker`. Here is an example layout:

    docker-myapp/
    |
    |-- otica.yaml
    |
    |-- common/
    |   |
    |   |-- docker/
    |   |   |
    |   |   |-- Dockerfile
    |   |   |
    |   |   `-- any other files used in the Docker build
    |   |
    |   .
    |   .
    |   .
    |
    `-- test/
        |
        `-- local.var

### Environment variables

As in other parts of Otica environment variables for this module should be
set in one of the `*.var` files.

#### Required

The following environment variables _must_ be set in order for this module
to work properly:

* `DOCKER_IMAGE`: the name of the Docker image (e.g., "myapp"). This is
usually set in the variable file `common/common.var` or in the project
environment `local.var` variable file.

* `DOCKER_REGISTRY`: the hostname of the Docker registry where the Docker
image will be pushed. Two points to observe: (1) omit the `https://`, and
(2) be sure to use `docker.io` when pushing to Docker Hub. Examples:
`gcr.io`, `docker.io`, `123456789012.dkr.ecr.us-west-2.amazonaws.com`.

* `DOCKER_REGISTRY_USERNAME`: the username to use when authenticating to
the remote Docker registry.

* `DOCKER_REGISTRY_LOGIN_TYPE`: must have one of three values, either
`gcloud`, `vault`, or `file`. See the section "Image registry
authentication" for more details on how `DOCKER_REGISTRY_LOGIN_TYPE` is
used when accessing a Docker image registry. If
`DOCKER_REGISTRY_LOGIN_TYPE` is not set it defaults to `vault`.

* `DOCKER_REGISTRY_PASSWORD_PATH`: this is interpreted differently depending
on the value of `DOCKER_REGISTRY_LOGIN_TYPE`.

  - if `DOCKER_REGISTRY_LOGIN_TYPE` is set to `vault` this must be the Vault
    path containing the password needed when authenticating to the remote
    Docker registry;

  - if `DOCKER_REGISTRY_LOGIN_TYPE` is set to `file` this must be the absolute
    path to a file containing the password needed when authenticating to the remote
    Docker registry;

  - if `DOCKER_REGISTRY_LOGIN_TYPE` is set to `gcloud` this variable is ignored.

#### Optional or defaulted

The following environment variables either are not required or come with reasonable
defaults.

* `DOCKER_IMAGE_TAGS`: the tags to apply after a Docker build. If this
environment variable is not set then the tag `latest` will be used.
Example: `DOCKER_IMAGE_TAGS=dev,buster` will tag the image with `dev`, and
`buster`.
Usually set in the project variable file `local.var`.

* `DOCKER_IMAGE_TAG`: this tag is no longer recognized and if set will
raise an exception.

* `DOCKER_IMAGE_TAGS_HOOK`: if this environment variable is defined it
should point to an executable that generates a comma-delimited list of
tags. This allows the user of `docker-develop.mk` to generate Docker image
tags _dynamically_. For more information on using this feature,
see ["Dynamic image tags"](#dynamic-image-tags).

* `DOCKER_NAMESPACE`: the container namespace; normally does not need to
be set as it defaults to `GCP_PROJECT_ID`.

* `DOCKER_SOURCE_DIR`: the directory containing `Dockerfile` and any other
files needed for the Docker image build; defaults to `${COMMON}/docker`.

* `DOCKER_BUILD_DIR`: the directory where the Docker build takes place.
All the files from `DOCKER_SOURCE_DIR` are copied into
`DOCKER_BUILD_DIR` including `Dockerfile` itself.
Defaults to `build`.

* `DOCKER_KANIKO_NO_PUSH`: normally the kaniko script that `make
docker-kaniko-script` generates will include the `--destination` options
to push the build Docker image to a repository. If you set
`DOCKER_KANIKO_NO_PUSH` then the `--destination` tags are omitted and the
`--no-push` option is used instead. This can be useful when you want to
know only if the image builds correctly, or when debugging the image
build. Defaults to undefined.

* `DEBUG`: set to a non-empty value to turn on extra debugging output.
Defaults to undefined.

### How your Docker image is pushed to the remote registry

Your Docker image is pushed to the URL
```
https://DOCKER_REGISTRY/DOCKER_NAMESPACE/DOCKER_IMAGE
```
or, if `DOCKER_NAMESPACE` is empty,
```
https://DOCKER_REGISTRY/DOCKER_IMAGE
```

The image is tagged with each tag in the comma-delimited list
`DOCKER_IMAGE_TAGS` (including any tags generated by the optional
`DOCKER_IMAGE_TAGS_HOOK` script).

See ["Image registry authentication"](#image-registry-authentication)
below for details on how the `docker-develop.mk` module authenticates to
the registry.

### Vault configuration

The `docker-develop.mk` module looks up remote registry authentication
credentials in Vault, so before being able to push your Docker images you
must save the password in Vault. Set `DOCKER_REGISTRY_PASSWORD_PATH` to
the Vault path of the password.

## How your image is built

Here is what happens when you run `make docker-build`:

1. The `DOCKER_*` environment variables are checked and if any required
environment variables are missing the build will halt with an error
message.

1. All the files in `../common/docker` are copied into `DOCKER_BUILD_DIR`
(unless overridden `DOCKER_BUILD_DIR` will be the environment directory's
`build/` directory).

1. The [`render-file-dir.sh` script](../../manpages/render-file-dir.sh) is run against `DOCKER_BUILD_DIR`.
Depending on the value of `RENDER_MODE`, this converts some of the files
in `DOCKER_BUILD_DIR` via the [`render2.sh`](../../filters/render2) filter. For more information on
how the value `RENDER_MODE` affects file rendering, see the ["Rendering
configuration (RENDER_MODE)" information
page](../../configuration/render). Setting `RENDER_MODE` to the string
"NONE" causes this step to be skipped.

1. As part of the above step, if the file
`../common/docker/build-args.yaml` exists it will be sent through the
`render2.sh` script to have any environment variables replaced and the
results copied into `DOCKER_BUILD_DIR`. See also section ["ARG: Using
external variables during image
build"](#arg-using-external-variables-during-image-build).

1. The Docker image is built using `DOCKER_BUILD_DIR` as the Docker build
directory.

## Image registry authentication

You need credentials when pushing your image to the remote registry
specified by `DOCKER_REGISTRY`. The credentials are in the form of a
username and a secret, i.e., password or access token. The
`docker-develop.mk` make module supports three types of registry
authentication:

* `gcloud`: authentication is achieved via the [gcloud command][3] `gcloud
auth configure-docker`; so, in this case, no secrets need to be managed.

* `vault`: access is gained by logging as user `DOCKER_REGISTRY_USERNAME`
with the password coming from the Vault path stored in
`DOCKER_REGISTRY_PASSWORD_PATH`.

* `file`: access is gained by logging as user `DOCKER_REGISTRY_USERNAME`
with the password coming from the local file with absolute path stored in
`DOCKER_REGISTRY_PASSWORD_PATH`.

To indicate which type of authentication set the environment variable
`DOCKER_REGISTRY_LOGIN_TYPE` to one of the above three strings. For
example, to use a password stored in Vault set
`DOCKER_REGISTRY_LOGIN_TYPE` to `vault` and set
`DOCKER_REGISTRY_PASSWORD_PATH` equal to the Vault path where you stored
the password.

## Docker tags

### Local image names

The local image name is `DOCKER_IMAGE` followed by a tag.
There are two ways to specify which Docker tags to apply to a Docker build set:

1. (static) Set `DOCKER_IMAGE_TAGS` to a comma-delimited list of tags. These tags
will be applied to the build and pushed when using `make docker-push`.

2. (dynamic) Set `DOCKER_IMAGE_TAGS_HOOK` to a script that generates a list of
tags dynamically.

The final list of Docker tags to apply is the union of the static and
dynamic tags with any duplicates removed. Thus, you can use either static
tagging, dynamic tagging, or **both**.

If you want the tag `latest` to be applied be sure to add it explicitly.
If `DOCKER_IMAGE_TAGS` is undefined or is left blank, then the list of
tags defaults to the single tag `latest`.

**Example**: `DOCKER_IMAGE` is set to `myapp` and `DOCKER_IMAGE_TAGS` is set to the string
`dev,buster-dev`. After `make docker-build` is run the resulting image will have have
two names: `myapp:dev` and `myapp:buster-dev`:
```
$ make docker-build
$ docker images
REPOSITORY         TAG           IMAGE ID       CREATED         SIZE
myapp              dev           15401f825243   1 minute ago    246MB
myapp              buster-dev    15401f825243   1 minute ago    246MB
```

### Remote image names

When you run `docker-push` you add "remote aliases" and push those to the
remote repository.

**Example**: `DOCKER_IMAGE` is set to `myapp` and `DOCKER_IMAGE_TAGS` is set
to the string `dev,buster-dev`. `DOCKER_REGISTRY` is set to `gcr.io` and
`DOCKER_NAMESPACE` is set to `my-gcp-project`. Running `make docker-push` will
create two additional image aliases `gcr.io/my-gcp-project/myapp:dev` and
`gcr.io/my-gcp-project/myapp:buster-dev` and push those two image names to the
remote repository:
```
$ make docker-push
$ docker images
REPOSITORY                      TAG           IMAGE ID       CREATED          SIZE
myapp                           dev           15401f825243   2 minutes ago    246MB
myapp                           buster-dev    15401f825243   2 minutes ago    246MB
gcr.io/my-gcp-project/myapp     dev           15401f825243   2 minutes ago    246MB
gcr.io/my-gcp-project/myapp     buster-dev    15401f825243   2 minutes ago    246MB
```

HINT: run `make docker-push-dryrun` to see what the remote aliases will
look like before actually pushing them.

### Dynamic image tags

The list of tags in the environment variable `DOCKER_IMAGE_TAGS` are
applied during image builds and pushes. However, this list is _static_.
The `docker-develop.mk` make-module also supports the _dynamic_ generation
of Docker tags. To make use of this feature create an executable that
generates a comma-delimited list of strings. Once you have done that set
the environment variable `DOCKER_IMAGE_TAGS_HOOK` to the path to that
executable.

The `docker-develop.mk` make-module applies tags from both the static list
as defined by `DOCKER_IMAGE_TAGS` and any tags generated dynamically.

#### Example

Here is a script that generates two extra tags: one with the
current date and one that extracts a Python package version number.
```
#!/bin/bash
set -e

version_tag=$(pip show typer | grep 'Version:' | cut -d' ' -f2)
date_tag=$(date --iso-8601)

echo "$version_tag,$date_tag"
```

Putting the above script in `common/scripts/make-tags.sh` we would set
`DOCKER_IMAGE_TAGS_HOOK` to `$COMMON/scripts/make-tags.sh`. To see
the tags generated by this script run `make docker-show-env`:
```
$ make docker-show-env
pass: DOCKER_SOURCE_DIR              ("../common/docker")
...
pass: DOCKER_IMAGE_TAGS              ("dev")
pass: DOCKER_IMAGE_TAGS_HOOK         ("../common/scripts/make-tags.sh")
...
tags:                                dev,0.9.0,2023-08-28
```

The two tags at the end (`0.9.0` and `2023-08-28`) are the ones generated
by the script pointed to by `DOCKER_IMAGE_TAGS_HOOK`. The tag `dev`
comes from `DOCKER_IMAGE_TAGS`.

**Note** The docker-develop make-module derives the version strings from
the *last* line of the `DOCKER_IMAGE_TAGS_HOOK` script, so feel free to
send any text to standard output just so long as your last line is the
comma-delimited version string.


## ARG: Using external variables during image build

### Overview

If you want to have values in your `Dockerfile` updated at build time but
*don't* want these values to be set as environment variables in the built
image, use the Dockerfile `ARG` directive. To specify the value to be used
by the `ARG` directive you put the values in the `build-args.yaml` file in
the same directory as `Dockerfile`. For more information on the `ARG`
instruction see [the `ARG` instruction in the "Dockerfile reference"
page][1].

**Note.** The `ENV` Dockerfile instruction can also be used in the
Dockerfile and can be set to a default value. But keep in mind that the
`ENV` is meant to be changed during a docker _run_ not a docker _build_.
In particular, Otica environment variables cannot be used to set `ENV`
values in the Dockerfile during docker build; use `ARG` and
`build-args.yaml` (see below) for that.


### Example

Assume we have a standard docker otica project with this layout:
```
    docker-myapp/
    |
    |-- otica.yaml
    |
    |-- common/
    |   |
    |   |-- docker/
    |   |   |
    |   |   |-- Dockerfile
    |   |   |
    |   |   `-- any other files used in the Docker build
    |   |
    |   .
    |   .
    |   .
    |
    `-- dev/
    |   |
    |   '-- local.var
    |
    `-- prod/
        |
        '-- local.var
```

Assume that the `Dockerfile` starts like this:
```
FROM debian:bullseye-slim
LABEL maintainer="johndoe@example.com"
...
```

and that for the `dev` Docker build you want the maintainer `LABEL` to
be `johndoe@example.com` but for the `prod` Docker build you want the
maintainer `LABEL` to be `janeroe@example.com`. You can do this using the
`ARG` instruction. Change your `Dockerfile` so it looks like this:
```
FROM debian:bullseye-slim
ARG MAINTAINER_LABEL=johndoe@example.com
LABEL maintainer="${MAINTAINER_LABEL}"
...
```

The above tells the Docker build that unless overridden the value of the
maintainer LABEL should be `johndoe@example.com`. How do you override the value?
When using the `docker build` directly you override the value of the ARG using
`--build-arg`:
```
docker build --build-arg MAINTAINER_LABEL=janeroe@example.com .
```

With Otica we do this slightly differently. For `ARG` instructions in the
`Dockerfile` put the key-value pairs in a YAML file with the special name
`build-args.yaml` in the the `common/docker` directory:
```
---
# common/docker/build-args.yaml
MAINTAINER_LABEL: janeroe@example.com
```

This tells the `docker-develop.mk` make module to add the `--build-arg
MAINTAINER_LABEL=janeroe@example.com ` argument when running the `docker
build` command.

However, we said we want the maintainer label to be different for the
`prod` environment. How do accomplish that? By using Otica-level
evironment variable substitution.

### Otica-level evironment variable substitution

While you can "hard-code" values in the `build-args.yaml` file, it is
better to leverage Otica's use of environment variables instead. In the
above example it is more common to construct the `build-args.yaml` files
as follows:
```
#!envsubst
---
# common/docker/build-args.yaml
MAINTAINER_LABEL: ${MAINTAINER_LABEL}
```

When Otica detects the presence of the file
`common/docker/build-args.yaml` it sends the file through the `render2.sh`
filter. The `#!envsubst` line at the top of the file tells `render2.sh` to
send the file through the `envsubst` script, that is, replace all the
environment variable references (i.e., the ones preceded by a dollar sign)
with the values specifed in the various variable files (`common.var` or
`local.var`).

In our example, we want the `dev` build to set the label to
`johndoe.example.com` so we set the variable `MAINTAINER_LABEL` to
that value in `dev/local.var`:
```
---
# dev/local.var
...
export MAINTAINER_LABEL=johndoe.example.com
...
```

Likewise, we set that variable for the `prod` environment:
```
---
# prod/local.var
...
export MAINTAINER_LABEL=jane.example.com
...
```

With this setup, running `make docker-build` in the `dev/` directory will replace
the value in the Dockerfile with `johndoe.example.com` during the build.

Running `make docker-build` in the `prod/` directory will replace
the value in the Dockerfile with `janeroe.example.com` during the build.

**Remember.** The Otica-level environment variable subsititution happens
in the `build-args.yaml` file _not_ in the `Dockerfile`.

**Note.** In older versions of the `docker-develop.mk` make module the
`build-args.yaml` was always run through `envsubst`; this has **changed**
and now, if you want environment-variable substitution to happen to
`build-args.yaml`, you **must** explicitly put the line `#!envsubst` at
the top of the `build-args.yaml` file.

**Note.** The `ARG` instructions only affect Docker builds and do _not_ persist
as environment variables in the Docker image.

**Note.** The `build-args.yaml` file can only be in `common/docker`. Any such
files in environment directories will be ignored.

### Using ARG with the FROM instruction

You can use `ARG` with the `FROM` instruction:
```
# Dockerfile
ARG DEBIAN_CODENAME=buster
FROM debian:${DEBIAN_CODENAME}-slim
LABEL maintainer="${MAINTAINER_LABEL}"
...
```

However keep in mind that `DEBIAN_CODENAME` is defined for the `FROM` instruction
but **not** for instructions **after** the `FROM` instruction. This is because
`ARG` instructions are local to the compile stage and the `FROM` instruction is
considered a separate stage from the rest of the file.

If you want the `ARG` definition used in `FROM` to apply to later instructions
do this:
```
# Dockerfile
ARG DEBIAN_CODENAME=buster
FROM debian:${DEBIAN_CODENAME}-slim
ARG DEBIAN_CODENAME
LABEL maintainer="${MAINTAINER_LABEL}"
...
```
By putting `ARG DEBIAN_CODENAME` after the `FROM` instruction we tell the
docker build process to use the previously defined value in this stage.
See also ["Understand how ARG and FROM interact" from the Dockerfile
reference page][2].

## Make targets

### Main make targets

* `docker-help`: show this README file formatted as plain text.

* `docker-build`: build the Docker image and tag with the tags in
`DOCKER_IMAGE_TAGS` (uses Docker build cache; run `make docker-build-nocache`
to build without the Docker build cache).

* `docker-build-nocache`: same as `docker-build` but does *not* use the
Docker build cache; use this target when ready to push.

* `docker-push`: tag the image the tags in `DOCKER_IMAGE_TAGS` and push to
the registry pointed to by the environment variable `DOCKER_REGISTRY`.

* `docker-push-dryrun`: show what tags would be pushed by the
`docker-push` make target.

* `docker-check`: check that the needed environment variables are set to
non-empty values.

* `docker-show-env`: show the values of the `DOCKER_*` environment variables.

### Auxilliary make targets

These are targets called by the main make targets. Normally you do not
need to use these make targets directly.

* `docker-login`: login to the Docker repository; needed when pushing the
image.

* `docker-kaniko-script`: generates the [Kaniko][4]-compatible build
command to create the Docker image and puts the command in the file
`build/kaniko-cmd`. See also the section "Building in CI/CD" below.

## Building in CI/CD

### Make target `docker-kaniko-script`

The make target `docker-kaniko-script` generates the [Kaniko][4]-compatible
build command in the environment driectory where the make
command is run. For example, assume you have a `dev` environment directory.

```
$ cd dev
$ make docker-kaniko-script
render-file-dir.sh elapsed time: 1 seconds (4 files processed)
wrote kaniko command to file 'build/kaniko-cmd'
```

The file `build/kaniko-cmd` can now be run to build and upload the Docker
image.

### The `make-kaniko-cmds` script

The make target `docker-kaniko-script` generates the
[Kaniko][4]-compatible build command but only does it for the single
environment in which it is run. To generate a file with Kaniko build
commands for multiple images use `make-kaniko-cmds`. This script lives in
the Otica repository `bin/` directory.

This is especially useful when using CI/CD to generate Docker images.

Like `make docker-push`, if `common/docker/build-args.yaml` exists the make
target `docker-kaniko-script` will perform environment variables
substitution on `common/docker/build-args.yaml`.

**Example:** Assume you have an Otica Docker subproject with
three environment directories `prod`, `uat`, and `dev`. To generate a script
file containing the three Kaniko commands to generate the three images you would
change to the subproject's base directory and run
```
OTICA_ENVIRONMENTS="prod uat dev" ~/bin/otica/bin/make-kaniko-cmds
```
This would create the file `kaniko-cmds` with three lines, one line for
each Kaniko-build command. To put the file somewhere other than lse use
`KANIKO_CMD_FILE`. For example,
```
KANIKO_CMD_FILE=/tmp/kaniko-cmds OTICA_ENVIRONMENTS="prod uat dev" ~/bin/otica/bin/make-kaniko-cmds
```
works like the previous example except the resulting command file will be
put in `/tmp/kaniko-cmds`.

If you are having problems with the push portion of the kaniko build you
can set `DOCKER_KANIKO_NO_PUSH`. This has the effect that the image will
be built but not pushed to any repositories.

[1]: https://docs.docker.com/engine/reference/builder/#arg

[2]: https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact

[3]: https://cloud.google.com/sdk/gcloud

[4]: https://github.com/GoogleContainerTools/kaniko
