[[_TOC_]]

# Python Virtual Environment management

## Overview

The Python Virtual environment (`python/pvenv.mk`) Makefile module manages a
local [Python virtual environment][2] in your Otica subproject.

The virtual environment is created in the subproject's `BUILD_DIR`.

## Quick start

1. Add `python/pvenv.mk` to the `makefile_parts:otica:` section
of your subproject's `otica.yaml` file.

2. Update: `otica update`.

3. Create the Python virtual environment: `make pv-create`.

4. Add Python packages to install in the Python virtual environment to the
file `pv-requirements.txt`.

5. Install packages in `pv-requirements.txt`: `make pv-install`.

6. To activate the Python virtual environment you have to run a command
_outside_ of the Makefile. To see this command run `make pv-activate`.

## Configuration

### Environment variables

* `PVENV_SYSTEM_SITE_PACKAGES`: by default the `pv-create` action creates
the Python virtual environment configured so as to _ignore_ any operating
system-installed Python packages. If you do not want to ignore these
packages set the environment `PVENV_SYSTEM_SITE_PACKAGES` to a non-empty
value before running `pv-create`.


## Example

The file `pv-requirements.txt` should contain all Python modules needed when running
pvenv make targets. Put your `pv-requirements.txt` file in the base directory of your subproject.
This file should follow the usual [pip requirements file][1] format.

Once you have created the Python virtual environment via `make pv-create` you
will see the virtual environment directory in `BUILD_DIR/venv`:

```
my-subproject/
|
|-- otica.yaml
|
|-- pv-requirements.txt
|
`-- test/
    |
    |-- local.var
    |
    `-- build/venv/
```

To enable this Python virtual environment you type `source
build/venv/bin/activate` (assuming you are using the Bash shell). If you
forget this command type `make pv-activate` to display the necessary
command. To deactivate type `dactivate`.

You can use the virtual environment in your own Makefiles. For example,
say you have a file of Python unit tests in the file `tests.py` in the
base of your subproject. To run these unit tests in the context of the
Python virtual environment use this Makefile fragment:

```
.PHONY: test
test:  ## Run unit tests
    source ${BUILD_DIR}/venv/bin/activate \
    && cd .. \
    && PYTHON tests.PY
```


## Make target

### Main make targets

* `pv-create`: creates the Python virtual environment in `BUILD_DIR/venv`.

* `pv-install`: reads the file `pv-requirements.txt` in the subproject's main
directory and installs into the Python virtual environment the Python
packages listed `pv-requirements.txt`.

* `pv-list`: lists all Python packages installed in the Python virtual environment.

* `pv-activate`: displays the command to activate the Python virtual environment.

* `pv-deactivate`: displays the command to deactivate the Python virtual environment.

[1]: https://pip.pypa.io/en/stable/reference/requirements-file-format/

[2]: https://docs.python.org/3/library/venv.html
