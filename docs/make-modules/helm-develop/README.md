[[_TOC_]]

# Developing with Helm

The [`helm-develop.mk`](../../makefile_parts/helm-develop.mk) Makefile
module supports Helm development including packaging and uploading to a
Helm repository. This module uses the [`helm-gcs` Helm plugin][1] to
support pushing to a Helm repository in Google Cloud Storage.

**NOTE.** The `helm-develop.mk` is for _developing_ a Helm chart. When
_using_ a Helm chart in a Kubernetes project use the [`helm.mk` make
module instead](../makefile_parts/helm/).

## Quick start

1. Set `HELM_REPO_URL` and `HELM_CHART_NAME`.

1. If using an OCI-based registry skip to the next step. Otherise
   set `HELM_REPO_NAME` and add the Helm repository with `make dhelm-repo-add`.

1. Show the relevant Helm variables with `make dhelm-show-env`.

1. To package the Helm chart without pushing: `make dhelm-package`.

1. To push the Helm chart: `make dhelm-push`.

## Configuration

### Directory structure

This module expects that all Helm chart files (Chart.yaml, values.yaml,
etc.), are in `common/docker`. Here is an example layout:

    docker-myapp/
    |
    |-- otica.yaml
    |
    |-- common/
    |   |
    |   |-- helm/
    |   |   |
    |   |   |-- Chart.yaml
    |   |   |
    |   |   |-- values.yaml
    |   |   |
    |   |   |-- templates/
    |   |   |
    |   |   `-- any other files used to define Helm chart
    |   |
    |   .
    |   .
    |   .
    |
    `-- dev/
        |
        '-- local.var


### Environment variables

As with other parts of Otica, environment variables for this module should be
set in one of the `*.var` files.

#### Required

The following environments variable _must_ be set in order for this module
to work properly:

* `HELM_REPO_URL`: points to the URL where your Helm chart is hosted.
If your `HELM_REPO_URL` starts with `gs://` see ??? below.

* `HELM_CHART_NAME`: the name of the Helm chart. This is usually
the same as the `name` attribute in `Chart.yaml`. No default.

#### Optional

* `HELM_REPO_NAME`: the name given to the local Helm repository that points
to `HELM_GCS_BUCKET_PATH`. Default: `iedo-helm-public`. This should only be
set for non-OCI-based repositories.

## Helm registry authentication

The `helm-develop.mk` module uses the same authentication mechanism to
access protected Helm registries as the `helm.mk`. For further information
see section "Helm registry authentication" of the [`helm.mk` project's
documentation](../helm).

## Make target

### Main make targets

* `dhelm-help`: show this README file formatted as plain text.

* `dhelm-package`: create a Helm chart package. The resulting package file
is put in the current environment directory.

* `dhelm-push`: package the Helm chart and upload to the GCS bucket
pointed to by `HELM_GCS_BUCKET_PATH`. Important: this target assumes that
you already have the authentication context and credentials necessary to
push the Helm package (hint: if pushing to GCP run `make config-gcloud`
first).

* `dhelm-push-force`: package the Helm chart and upload to the GCS bucket
forcing the push, if necessary. This allows you to overwrite an uploaded
Helm package with the same version number.

* `dhelm-show-env`: display the Helm development relevant environment variables.


### GCS (Google Cloud Storage) Helm chart repository

If your are pushing to a GCS bucket, you will need to have [`helm-gcs`
Helm plugin][1] installed. To enable this support add the `helm-gcs.mk`
make module to `otica.yaml` file. See [`helm-gcs.mk` documentation](??)
for more information.

[1]: https://github.com/hayorov/helm-gcs

