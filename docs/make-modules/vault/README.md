[[_TOC_]]

# Hashicorp's Vault

## Overview

The `vault.mk` make module integrates [Hashicorp's Vault][3] product with
Otica supporting Vault login, logout as well as reading and writing to
Vault.

## Reading and writing Vault secrets

MORE LATER.

## Configuration

* `VAULT_ADDR`: [REQUIRED] The IP address or hostname of the Vault server.

* `VAULT_SEC_PATH`: [REQUIRED] The path in Vault where your secrets are
stored. This typically changes for each Otica subproject and each
subproject environments.
Defaults to `auth/token/lookup-self`.

* `VAULT_AUTH_PATH`: [REQUIRED] remote path in Vault where the auth method
is enabled. Defaults to `ldap`.

* `VAULT_AUTH_METHOD`: [REQUIRED] Type of authentication to use when
authenticating to Vault. Defaults to `ldap`

* `VAULT_USER`: [REQUIRED] Login to Vault as this user. Defaults to the
vaule of the environment variable `USER`.


## Make targets

* `vault-login`: Login to Vault. This login will probably trigger a Duo
push.

* `vault-logout`: Logout of Vault.

* `vault-info`: Show Vault configuration information by running `vault
status` followed by `vault read auth/token/lookup-self`.

* `vault-token-audit`: Get information about your local authentication
[Vault token][1].

* `vault-cap`: List the capabilities for your local Vault token on
the Vault path `SEC_PATH`.

* `vault-revoke`: Revoke yout Vault authentication token.

* `vault-help`: Show some useful Vault commands.

* `help-approle`: Show some useful Vault commands for approle.



[1]: https://www.vaultproject.io/docs/commands/token

[2]: https://www.vaultproject.io/docs/commands/login

[3]: https://www.vaultproject.io/
