[[_TOC_]]

# Overview

The package.mk make-module was designed to be a flexible set of make commands to build, sign,
and push linux packages of varying operating systems or type.  For all of these functions,
the specific details required for each step is managed via a set of Environment Variables.
The ability to set these values are handled in the same way that all other Enviroment Variables
are set in Otica.

Below is a table of supported package types and the supported functions of each type.

| Package type  | Build? | Sign? | Push? |
|---------------|--------|-------|-------|
| RedHat (.rpm) | YES    | YES   | NO    |
| Debian (.deb) | YES    | YES   | NO    |
| Python        | YES    | NO    | YES   |

## Environment Variables

Here are all of the Environment Variables that can be set for package.mk

### Global

These are variables that are applied and can be used by all 3 functions: Build, Sign, and Push

#### BUILD_DIR
*(required)*

This is the local directory external to the container that is used to store all files related to
package building, signing, and pushing.  This local directory is used as the parent directory
for the following subdirectories: source, output, credentials

##### source dir

This directory is used to store the build source data which is retrieved by package.mk based upon
the source_type.  Currently the only supported source_type is git and so the source_dir is populated
by the value set by PKG_GIT_URL.  Within the container, this directory is mounted to: /root/pkg/source

##### output dir

This directory is used to store the software package(s) and all related build files as determined
by the package builder used in the docker container.  Within the container, this directory is mounted
to: /root/pkg/output.

##### credentials dir

This directory is used to store the credentials that are needed in order to sign the built package
as well as push it to a desired location.  The intention is for these packaging functions to be
fully automated, so these credentials aid in the ability to perform these protected actions in an
automated fashion.

#### PKG_TYPE

This sets the type of package that is being built.  Current accepted values are: 'debian', 'redhat',
and 'python'.  Currently PKG_TYPE is being used to set the expected output package file extension
so that the existance of those files can be verified before anything is signed.

#### PKG_DOCKER_SCRIPTS_DIR

This Environment Variable sets a local directory external to the container that will contain scripts
that can be called from within the container.  This local directory is mounted to: /root/pkg/scripts
and so all scripts executed from within the container will need to be referenced from that path.

#### PKG_SIGN_CREDS_HELPER
*(default: '{otica}/scripts/package/creds-helper.sh')*

This Environment Variable sets the script that is called to retrieve credential information from a
secure location.  The default script is contained within the otica directory and is written to pull
package building credentials from the vault location specified in PKG_CREDS_VAULT_PATH.  The script
reads the credential information from Vault and stores it into BUILD_DIR/credentials for use in signing
debian and entlinux packages.  This Environment Variable can also be used to pull the relevant credentials
needed to push the packages to a target location.

#### PKG_CREDS_VAULT_PATH

As mentioned above, this variable is used by the default creds-helper.sh script to pull package signing
credentials.  However, it can also be used to pull any other credentials stored in Vault for any signing
or pushing operation.

### Building

For building, package.mk relies upon a Docker container and a package code repository.  As mentioned
above, currently only git is supported as a package code repository source, however if needs change
in the future, package.mk can surely be extended in order to support other source_type options.

A default command is used to execute the build process, but can be overwritten as explained below

#### PKG_GIT_URL
*(required)*

As mentioned above, git is currently the only supported repository source type.  This Environment
Variable determines which source code repository is pulled and placed into the /root/pkg/source
directory inside the container.  This is the source code that is used to build the package.

#### PKG_BUILD_DOCKER_IMAGE_NAME
*(required)*

This defines the location and name of the Docker Image that package.mk will use to execute the
package build.

<br><br>
The Docker Image used for Python packaging can be found here:
INSERT PYTHON DOCKER IMAGE HERE

#### PKG_BUILD_DOCKER_IMAGE_TAG
*(default: 'latest')*

This defines the Docker Image Tag that package.mk will use to execute the package build.

#### PKG_BUILD_DOCKER_CMD
*(default: '/root/build-pkg.sh')*

This is the command that is run once the Build Container has been loaded.  It is assumed that the
Docker Image specified by PKG_BUILD_DOCKER_IMAGE_NAME will include a script at the default path.
If not, then the command can be overwritten with this Environment Variable.

### Signing

For signing, package.mk relies upon a Docker container and credentials

#### PKG_SIGN_DOCKER_IMAGE_NAME

This defines the location and name of the Docker Image that package.mk will use to execute the
package sign.

#### PKG_SIGN_DOCKER_IMAGE_TAG
*(default: 'latest')*

This defines the Docker Image Tag that package.mk will use to execute the package sign.

#### PKG_SIGN_DOCKER_CMD
*(default: '/root/sign-pkg.sh')*

This is the command that is run once the Sign Container has been loaded.  It is assumed that the
Docker Image specified by PKG_SIGN_DOCKER_IMAGE_NAME will include a script at the default path.
If not, then the command can be overwritten with this Environment Variable.

### Pushing

#### PKG_PUSH_DOCKER_IMAGE_NAME

This defines the location and name of the Docker Image that package.mk will use to execute the
package push.  As mentioned above, python packaging are the only ones that support pushing.

#### PKG_PUSH_DOCKER_IMAGE_TAG

This defines the Docker Image Tag that package.mk will use to execute the package push.

#### PKG_PUSH_DOCKER_CMD
*(default: '/root/push-pkg.sh')*

This is the command that is run once the Push Container has been loaded.  It is assumed that the
Docker Image specified by PKG_PUSH_DOCKER_IMAGE_NAME will include a script at the default path.
If not, then the command can be overwritten with this Environment Variable.

#### DOCKER_ENV_FILE

Set `DOCKER_ENV_FILE` to the path of a file containing Docker environment
variable settings, that is, environment variable names and values that
will be passed to the Docker file performing the Docker push. For
example, assume `DOCKER_ENV_FILE` is set to `docker-envs.txt` and
`docker-envs.txt` is a file in the environment directory with the
following contents:
```
#!envsubst
PKG_REPO_URL==https://upload.pypi.org/legacy/
```
When the Docker container runs that does package push
the container will see the environment variable `PKG_REPO_URL` set to
`https://upload.pypi.org/legacy/`. Note that the `package.mk` Make-module
runs the file pointed to by `DOCKER_ENV_FILE` through the render script so
you can configure it (as the above example does) to do local environment variable
subsitution.

# package.mk Functionality

The logic flow for build, sign, and push are largely identical with a few differences in
prerequisites.  This section will document the elements that all of the functions have in common,
while the sections below will identify and describe the prerequisities that are unique to each
function.  From a high level point of view, package.mk, utilizes a variety of Environment Variables
to determine which Docker Container to load, how to set up the Docker Environment and which script
to run to execute the desired function.  The majority of the logic and execution of building,
signing, and pushing is dependant upon the Docker Container that is selected.

1. Prepare environment variables to be injected into the Docker Container
    1. Validate Scripts Dir
        1. Checks for PKG_DOCKER_SCRIPTS_DIR Variable and Path existance.  The scripts_dir is set with the value if all validation checks are passed.
    1. Setup Environment Variables to pass into the Container.  Note: these values are meant to be used by the Container Scripts in order to perform whatever function is desired.
        1. SOURCE_DIR is set to /root/pkg/source
        1. OUTPUT_DIR is set to /root/pkg/output
        1. PKG_ACTION is set to one of: build, sign, push
        1. Optionally add SCRIPTS_DIR set to /root/pkg/scripts if PKG_DOCKER_SCRIPTS_DIR passed validation
    1. An optional Environment Variable File is processed
        1. If DOCKER_ENV_FILE is availabe, then its path location is validated.  If everything checks out, then it is rendered and configured to be run with docker
1. Set up the mount commands for Docker
    1. Mount the BUILD_DIR/source dir to /root/pkg/source to align with SOURCE_DIR
    1. Mount the BUILD_DIR/output dir to /root/pkg/output to align with OUTPUT_DIR
    1. Optionally mount the BUILD_DIR/scripts dir to /root/pkg/scripts if scripts_dir exists
    1. Optionally mount the BUILD_DIR/credentials dir to /root/pkg/credentials if creds_dir exists
1. Execute Docker run
    1. Combine the docker run command with the Mounts, Environment Variables, Environment File, Docker Image, and the script to execute the function

## Secrets

All secret path environment variables should end in `_SEC_PATH`.

## Building

For building, package.mk makes sure:

1. That the output_dir (BUILD_DIR/output) exists if not, then create it.
1. Makes sure that all required Building Docker Container information has been provided.

## Signing

For signing, package.mk makes sure:

1. That the output_dir (BUILD_DIR/output) exists, because without it, there are no packages to sign
1. Makes sure that all required Signing Docker Container information has been provided.
1. That the output_dir contains packages of the correct specified PKG_TYPE.
1. That the creds_helper script is executed.  Creds_helper is executed outside of the container because it is assumed that an Otica context is needed in order to access the credentials in their protected store.
1. After the docker command is run, the credentials directory is cleaned up.

## Pushing

For pushing, package.mk makes sure:

1. That the PKG_REPO_URL has been provided
1. That the creds_helper script is executed.  Creds_helper is executed outside of the container because it is assumed that an Otica context is needed in order to access the credentials in their protected store.
1. Makes sure that all required Pushing Docker Container information has been provided.

## Package Type Specific Information

Below are the details that are specific to the functions available for each Package Type

### RPM/RedHat Packaging

Set `PKG_TYPE` to `redhat`.

Currently the Otica `package.mk` make-module supports RedHat class package building and signing
but not pushing for the Rocky Linux and Oracle Linux Operating Systems.  Building is performed
against the specific desired Operating System.

The Otica `package.mk` make-module expects the Docker container to follow
the standard as described in section [package.mk
Functionality](#packagemk-functionality) above.

### Debian Packaging

Set `PKG_TYPE` to `debian`.

Currently the Otica `package.mk` make-module supports Debian package building and signing
but not pushing.

The Otica `package.mk` make-module expects the Docker container to follow
the standard as described in section [package.mk
Functionality](#packagemk-functionality) above.

### Python Packaging

Set `PKG_TYPE` to `python`.

Currently the Otica `package.mk` make-module supports Python package building and pushing
but not signing.

Otica provides the script
[`scripts/package/python-build-push.sh`](../../../scripts/package/python-build-push.sh) to help
you build and push Python packages using the standard [Python Docker image][4].

To use this script add thes settings:
```
export PKG_DOCKER_SCRIPTS_DIR=${SCRIPTS_DIR}/package

export PKG_BUILD_DOCKER_IMAGE_NAME=python
export PKG_BUILD_DOCKER_IMAGE_TAG=3.11-slim-bullseye
export PKG_BUILD_DOCKER_CMD=/root/pkg/scripts/python-build-push.sh

export PKG_PUSH_DOCKER_IMAGE_NAME=${PKG_BUILD_DOCKER_IMAGE_NAME}
export PKG_PUSH_DOCKER_IMAGE_TAG=${PKG_BUILD_DOCKER_IMAGE_TAG}
export PKG_PUSH_DOCKER_CMD=${PKG_BUILD_DOCKER_CMD}

export DOCKER_ENV_FILE=envfile.txt # Needed for pushing
```

#### Building

We recommend you use
[`scripts/package/python-build-push.sh`](../../../scripts/package/python-build-push.sh)
for Python package building. This script will do a `python -m build` on
your source files leaving the output in `build/output`.

If you only need to build and not push the Docker environment file is not
needed so you can omit setting `DOCKER_ENV_FILE`.

#### Pushing

For pushing (i.e., uploading) to a Python package repository you will need
to make some extra environment variable settings. These settings depend on
where you are pushing. See the subsections below for details.

##### Pushing to TestPyPI

Make sure you have a [TestPyPi][1] account. Once you have that create
an [API token][3] and store that token in Vault.

In `local.var` make these settings:
```
export PKG_REPO_URL=https://test.pypi.org/legacy/
export PKG_REPO_USERNAME=__token__
export PKG_REPO_PASSWORD_SEC_PATH=<Vault path to API token secret>
export DOCKER_ENV_FILE=envfile.var
```

In the same directory as `local.var` create the file `envfile.var` with
this content:
```
#!envsubst
PKG_REPO_URL=${PKG_REPO_URL}
```

##### Pushing to PyPi

Use the same configuration as for [TestPyPi above](#pushing-to-testpypi)
with the following change:
```
export PKG_REPO_URL=https://upload.pypi.org/legacy/
```

##### Pushing to Gitlab

Create a Gitlab deploy token in the Gitlab repository that you want to push to.
If the path to the project is `group1/group2/projectname`, find the
URL encoded version of `group1/group2/projectname`. Set the variables as follows:
```
export PKG_REPO_URL=https://gitlab.example.com/api/v4/projects/<URL-encoded path>/packages/pypi/simple
export PKG_REPO_USERNAME=<deploy-token-name>
export PKG_REPO_PASSWORD_SEC_PATH=${PACKAGING_SEC_PATH}/path/to/deploy-token-secret
```

See also [the Gitlab documentation][5].

##### Pushing to GCP's Artifact Registry

In `local.var` make these settings:
```
export PKG_REPO_URL=<path to your Google Artifact Registry Python repository>
export PKG_REPO_USERNAME=__token__
export GOOGLE_APPLICATION_CREDENTIALS_SEC_PATH=<vault path to Google service account>
export DOCKER_ENV_FILE=envfile.var
```

The Google service account mentioned above must have permissions to write
to the Python Artifact Registry.

In the same directory as `local.var` create the file `envfile.var` with
this content:
```
#!envsubst
PKG_REPO_URL=${PKG_REPO_URL}
GOOGLE_APPLICATION_CREDENTIALS_SEC_PATH=${GOOGLE_APPLICATION_CREDENTIALS_SEC_PATH}
```



[1]: https://test.pypi.org/

[2]: https://pypi.org/

[3]: https://pypi.org/help/#apitoken

[4]: https://hub.docker.com/_/python

[5]: https://docs.gitlab.com/ee/user/packages/pypi_repository/
