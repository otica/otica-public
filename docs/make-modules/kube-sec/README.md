[[_TOC_]]

# Kubernetes Secrets Management with Vault

## Overview

The `kube-sec.mk` make module uses the `vault2kube.sh` script to populate
the Kubernetes secrets file with secrets from Vault and store them in
Kubernetes' secrets store. This way you can store your Kubernetes secrets
file in Git separate from your secrets in Vault. This module handles the details
Base64-encoding the secret before storage.

## The secrets file

The `kube-sec.mk` make module assumes that the Kubernetes secrets file is
in `{TEMPLATES}/secrets.yaml` (although this can be overridden; see
below). It is best practice to _not_ put any `#!`-strings (including
`#!vault2kube`) at the top of the secrets file as `kube-sec.mk`
*ignores* `#!`-strings and implicitly runs the file through `envsubst` and
`vault2kube.sh` for you.

Here is an example `secrets.yaml` file suitable for use with `kube-sec.mk`. Note
the string delimited by `%%`; that is the string that will be replaced with a Vault
secret by `vault2kube.sh`.
```
apiVersion: v1
kind: Secret
metadata:
  namespace: ${APP_NAMESPACE}
  name: ${APP}-saml-key
  labels:
    app: ${APP}
type: Opaque
data:
  saml-key.pem: %%${APP_SEC_PATH}/saml-key.pem%%
```


## Dependencies

These make modules _must_ be included in the `makefile_parts:otica`
section of your `otica.yaml` file.

* `kube.mk`: needed for Kubernetes configuration

* `vault.mk`: for reading secrets from Vault.


## The Kubernetes namespace

It is recommended that you specify the Kubernetes namespace in the secrets
file. If the namespace is not specified in the secrets file the Secrets
will be written to the current Kubernetes namespace.


## Configuration

### Environment Variables

* `KUBE_SEC_DEF_FILE`: the path to the secrets file. This defaults to
`{TEMPLATES}/secrets.yaml`. In the rest of this document when we say
"secrets file" we mean the file pointed to by `KUBE_SEC_DEF_FILE`.

## Make targets

### `kube-sec`

Expand environment variables and merge Vault secrets in the secrets file,
then store as Kubernetes Secrets. For an example of how to format the
secrets file to pull in secrets from Vault, see the documentation for the
[filter `vault2kube`](../../filters/vault2kube).

### `check-kube-sec`

Render the secrets file by replacing environment variables with their
values but do not insert the Vault secrets.

### `check-kube-sec-values`

Render the secrets file replacing environment variables with their values
_and_ replacing secret paths with their Base-64 values.
Since this make action displays secrets use with **caution**.

### `destroy-kube-sec`

Delete the Kubernetes secrets specified in the secrets file.


