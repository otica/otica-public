[[_TOC_]]

# Repo subprojects (`sub-projects`)

All repositories are managed as sub-directories of the directory `sub-projects/`.

## The `repos.yaml` file

Configuring the subprojects is via the file `repos.yaml`. All repositories are
entries under `sub-projects`. Each subproject can have these attributes:
```
name
url
category
two-step
description
```

The **only required** attribute is `url`.

### The `url` attribute

Only `url` is required:
```
---
sub-projects:
  - url: code.example.com:et-iedo/cloud/kube-cloud-sql
  - url: code.example.com:et-iedo/cloud/kube-gitlab-runner
  - url: code.example.com:et-iedo/cloud/kube-patchman
  - url: git@code.example.com:et-iedo/cloud/kube-puppet
```

Note that the URL does not need to have the prefix `git@`; if `git@` is
omitted it will be added during processing.

### The `category` attribute

If you want to indicate a "sub-subproject" use `category`. When running an
`update` will clone repo into `sub-projects/<category>`.

```
---
sub-projects:
  - url: code.example.com:et-iedo/cloud/kube-cloud-sql
    category: aws
  - url: code.example.com:et-iedo/cloud/kube-kpuppet
```

In the above example, the `kube-cloud-sql` will be cloned into
`sub-projects/aws/kube-cloud-sql` while `kube-kpuppet` will be cloned into
`sub-projects/kube-kpuppet`.


### The `name` attribute

The `name` property is normally derived from `url` and corresponds to
the directory name, but you can override this. For example,
the following will clone into `sub-projects/aws/apache-webapp`:
```
---
sub-projects:
  - url:  git@code.example.com:et-iedo/debian-packages/apache-webapp.git
    category: aws
```

However, the following will clone into `sub-projects/aws/kremctl`:
```
---
sub-projects:
  - url:  git@code.example.com:et-iedo/debian-packages/apache-webapp.git
    name: kremctl
    category: aws
```

### The `two-step` attribute

Defaults to false.

### The `description` attribute

Useful when generating reports of the subprojects to give more detail
on the purpose of the subproject.

### Example

```
sub-projects:
  - name: apache-webapp
    url:  git@code.example.com:et-iedo/debian-packages/apache-webapp.git
    category: aws
    two-step: true
    description: |-
      GKE cluster creation
```

## Operations

### `repos-help`

Display this help file.

### `repos-check`

Check the validity of the `repos.yaml` file.

### `repos-list`

List the subprojects in the `repos.yaml` file.

### `repos-details`

Display the details for each subproject listed in the `repos.yaml` file.

### `repos-update [filter=<regex>]`

Update (or create) each subproject listed in the `repos.yaml` file.  If
you want to update a only _subset_ of the subprojects then add `filter=<regex>` to
the end of the make command. For example, to update only those subprojects
whose path contains "email" run the command like this:

        $ make repos-update filter=email

The string `<regex>` can be any Python-compatible regular
expression. Example:

        # Update all repos whose name contains a number
        $ make repos-update filter='\d'

### `repos-update-dryrun [filter=<regex>]`

Explains what would happen if you run `make repos-update` but do not
actually do any of the creates or updates. See `repos-update` above
for information on how to use `filter=<regex>`.

### `repos-orphaned`

The `repos-update` operation updates/creates entries in `repos.yaml`.
However, it ignores any directories in `sub-projects/` (or any category
directories) that are not listed in `repos.yaml`. To get a list of such
orphaned directories run `make repos-orphaned`.

### `repos-show-status`

The `repos-show-status` target lists all repositories followed by their status.
This status will be one of:

* `clean`: repo has no uncommited changes and no untracked files

* `has uncommited changes`: repo has either some uncommited changes or
files that are not being tracked (or both)

* `ERROR`: some error that prevents getting status for the repository; one
common cause for this error is that the repository was never cloned
