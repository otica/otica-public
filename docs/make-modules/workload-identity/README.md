[[_TOC_]]

# GCP Workload Identity (`gcp/workload-identity.mk`)

## Overview
Google Cloud Platform has a feature called Workload Identity that allows
the binding of a Kubernetes service account to a GCP service account. This
allows a Kubernetes Pod to access and change GCP resources without any
special configuration on the part of the software inside of the Pod.
The `gcp/workload-identity.mk make module helps you set up this binding.

How to do it:

1. If not already created, create a GCP service account with the
access you want the Kubernetes application to have.

1. Set `GSA_NAME` to the name of this GCP service account.

1. Set the other variables (see "Configuration" below).

1. Create the Kubernetes service account with `make wi-create-ksa`.

1. Create the binding between the Kubernetes and GCP service accounts with
`wi-add-binding`.


## Configuration

All the following environment variables are *required*.

* `GCP_PROJECT_ID`: the GCP project identity.

* `KUBE_NAMESPACE`: The Kubernetes namespace.

* `KSA_NAME`: The Kubernetes service account name being mapped to the
  Google service account.

* `GSA_NAME`: The Google service account being mapped to.

## Make targets

## `wi-show-env`

Show the variables needed by the `gcp/workload-identity.mk`. Useful
starting place when trouble-shotting.

## `wi-create-ksa`

Create a Kubernetes service account with the name `KSA_NAME` in the
current Kubernetes namespace.

## `wi-add-binding`

Bind the Kubernetes service account `KSA_NAME` to the GCP service account
`GSA_NAME`. Note that the GCP service account must already exist for this
target to succeed.

## `wi-rm-binding`

Unbind the the Kubernetes service account `KSA_NAME` from the GCP service account
`GSA_NAME`.

## `wi-get-binding`

Show the IAM policy of the bound GCP service account `GSA_NAME`. Note that
if the GCP service account `GSA_NAME` is not bound to a Kubernetes
service account this target will generate an error.


