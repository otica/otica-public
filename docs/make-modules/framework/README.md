[[_TOC_]]

# Framework setup

The `framework.mk` make module sets several important [system
variables](../../configuration/framework) including the [`FRAMEWORK_*` and
`TL_*` variables](../../configuration/framework). It should be the first
makefile module in the `makefile_parts:otica` list in `otica.yaml`.

It also has a few useful make targets:

* `check-tools`: run the [`check-tools`](../../manpages/check-tools)
script that verifies software Otica needs to run is installed.

* `show-env`: show all the environment variables set in Otica.

* `otica-update`: update a subproject from the `otica.yaml` file.



