[[_TOC_]]

# Shibboleth Datasealer Secrets Management

## Overview

The Shibboleth client can support a simple form of clustering using
persisted attributes and a shared secret. To understand the contents of
the rest of this document first familiarize yourself with how
Shibboleth datasealer-based clustering works by reading these
pages:

* [Service Provider Clustering page][2]

* [Service Provider SessionCache][3]

* [Service Provider DataSealer][1]

The `datasealer.mk` make-module uses the [`seckeygen`][4] script to create
and update [versioned datasealer files][5] that the Shibboleth client uses for
clustering. These files are stored in Vault.

## Quick Start

1. Define the environment variable `DATASEALER_SEC_PATH` to be the Vault
path where you store the datasealer secret.

2. Add `datasealer.mk` to your `otica.yaml` file in the
`makefile_parts:otica` section.

3. Update your configuration by running `otica update`.

4. Run `make datasealer-update` to create the datasealer file and store it
in Vault.

5. [Optional] If this datasealer file is part of a Kubernetes project, add
the secret to your `common/templates/secrets.yaml` and run `make kube-sec`
(see also the [`kube-sec.mk`](../kube-sec/) make-module).

## Prerequisites

The [`seckeygen`][4] script which is the script that actually creates the
secrets requires the command-line `openssl` client, so if `openssl` is not
installed the `datasealer.mk` make-module will not work.

## Configuration

The environment variable `DATASEALER_SEC_PATH` tells the `datasealer.mk`
make-module where to store your datasealer secrets file. If this variable
is not set the `datasealer.mk` make-module will raise an error.

## The datasealer file

Resetting or updating the datasealer file creates a datasealer file that looks
something like this:
```
1:XC19sejRyFTCEe8XI+szMn9Jb52DCHmIIWzcvGsTh38=
2:jWoMYB0lPQvAUzTyhbHnIyuDdGRVl7xALwQSzoy5K/U=
...
```

This file is referred to as "versioned" because it contains multiple
secrets where the last secret in the file is the one that is used.

## Make targets

* `datasealer-update`: create (if the Vault object does not already exist)
or update the datasealer file. Updating the file adds a new secrets with a
new number at the end of the file. Thus, each update makes the datasealer
longer. After the file gets 14 entries subsequent updates "roll", that is,
the oldest entry is deleted before the new one is added.

* `datasealer-reset`: reset the Vault object to a new datasealer file. All
the old secrets are removed and two new ones are put in their place.

* `datasealer-show`: show the datasealer Vault object on the terminal. *Be
cautious* when using this make target as it displays secrets on your screen.






[1]: https://shibboleth.atlassian.net/l/cp/FdbCicV1

[2]: https://shibboleth.atlassian.net/l/cp/yX7vR3AV

[3]: https://shibboleth.atlassian.net/l/cp/8zUfXhS8

[4]: https://shibboleth.atlassian.net/l/cp/qaapLDkX

[5]: https://shibboleth.atlassian.net/l/cp/mToJ9rh2
