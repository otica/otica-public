[[_TOC_]]

# Using docker-compose

## Introduction

The `docker-compose.mk` make module integrates several `docker compose`
commands with Otica. This integration allows the use of Otica variables in
the `docker-compose.yml` file. Following Otica conventions, the file
`docker-compose.yml`, `Dockerfile`, and other supporting files must be
stored in `common/docker`.

## Recommended Workflow

1. Edit your `docker-compose.yml` and `Dockerfile` files.

1. View the `docker-compose.yml` file after variable substitution
by running `make dc-config`.

1. Run the service: `make dc-up`.

1. Look at the service logs `make dc-logs`.

1. Stop the service: `make dc-down`.


## Environment variables

As in other parts of Otica environment variables for this module should be
set in one of the `*.var` files, e.g., `common/env_variables/common.var`
or in `local.var` in the environment subdirectories.

#### Required

The following environment variables are specific to the
`docker-compose.mk` make module.

* `COMPOSE_PROJECT_NAME`: the name of your docker-compose project. This
needs to be different than any other docker-compose project running on the
same machine. Recomendation: give it a name like `my-project-dev`, or
`my-project-prod`. No default.

* `DOCKER_COMPOSE_SOURCE_DIR`: the directory containing
`docker-compose.yml`; defaults to `${COMMON}/docker`.

* `DOCKER_COMPOSE_FILE`: the `docker compose` source file name;
defaults to `docker-compose.yml`.

