# Cloud Platform Authentication

## Overview

The `authenticate.mk` module allows you to authenticate to one of the
supported cloud platforms. The scripts used in the module are also used by
the `kube.mk` make module.

The supported cloud platforms are:

* AWS ([Amazon Cloud Services][2])

* GCP ([Google Cloud Platform][1])

Support for [Azure][3] will be added in the near future.


## Configuration

Some of the configuration for authentication against a cloud platform is
independent of the platform while some depends on the cloud platform you
are authenticating against.

### Global configuration

The variables in this section are used when authenticating by all the
different cloud platforms.

* `CLOUD_PLATFORM`: This indicates which cloud platform you are
authenticating against. It is required and must have one of the following
values: `aws`, `gcp`.

### Platform-specific configuration

Each cloud platform has specific variables that affect authentication.
Click on the links below for details of each platform.

* [AWS configuration](aws/)

* [GCP configuration](gcp/)


## Make targets

* `auth`: Authenticate against the cloud platform pointed to by
`CLOUD_PLATFORM`. For this to work you must have set up the
Platform-specific configuration (see above section).

* `auth-show-env`: Show the environment variables that are relevant to the
`authenticate.mk` module. Note that the variables displayed will be
different depending on the value of `CLOUD_PLATFORM`.


[1]: https://cloud.google.com/

[2]: https://aws.amazon.com/

[3]: https://azure.microsoft.com/
