# Cloud Platform Authentication: AWS

All cloud platform authentication and configuration variables for the AWS
platform start with `AWS_`. These settings should probably be put in
variable file in the toplevel directory `common/env_variables` and then
included in each Otica subprojects via the `otica.yaml` file.

The script used to authenticate to AWS is
`scripts/auth/authenticate-aws.sh`. This script generates time-bound AWS
credentials using the Vault secrets engine and caches credentials in the
Vault token cubbyhole to avoid generating new credentials when previously
generated credentials have not expired. Thus you must have the `vault.mk`
make module variables configured properly. See also the [Vault AWS secrets
engine documentation][1].


* `VAULT_ADDR`: The Vault server's address (see [`vault.mk`][???]).

* `AWS_CRED_SECRET`: Because Vault can be set up for multiple tenants, not
every user of Vault can get their AWS secrets from the same Vault endpoint
"aws/creds". In light of this the script sets the first part of the AWS
Vaults secrets endpoint path to `AWS_CRED_SECRET`.
You will get this value from your Vault administrator.

* `AWS_DEPLOYMENT_IAM_ROLE`: This is the Vault user that has the access to
create the AWS IAM credentials. You will get this value from your Vault
administrator.


[1]: https://www.vaultproject.io/docs/secrets/aws
