# Cloud Platform Authentication: GCP

All cloud platform authentication and configuration variables for the GCP
platform start with `GCP_`. These settings should probably be put in
variable file in the toplevel directory `common/env_variables` and then
included in each Otica subprojects via the `otica.yaml` file.

* `GCP_PROJECT_ID`: [REQUIRED] This is the GCP [project ID][1].

* `GCP_USER_AUTH_DOMAIN`: [REQUIRED] The domain to append to user names when
authenticating to GCP. For example, if the user authenticating has
`USER` set to `johndoe` and  `GCP_USER_AUTH_DOMAIN` is `example.com`, then
when authenticating the username `johndoe@example.com` will be used.

* `GCP_USER`: this can be used to override the username constructed from
`USER` and `GCP_USER_AUTH_DOMAIN` in those cases where they differ.

* `GCP_USER_AUTH`: if set to `true` will use personal GCP credentials to
log in.

* `GCP_CONFIGURATION`: [REQUIRED] A name to give this GCP [gcloud
configuration][3]. This can be changed when switching among GCP projects
or GCP configurations. Its default value is `default`, but we
strongly recommend you not use the default value and instead
use a name derived from `GCP_PROJECT_ID`.

* `GCP_REGION`: The GCP [region][2] to use when managing GCP resources.
Example: `us-west1`. Required for most uses of GCP.

* `GCP_ZONE`: The GCP [zone][2] to use.
This is usually set to something like `${GCP_REGION}-a` or
`${GCP_REGION}-b`.
Required for most uses of GCP.



[1]: https://cloud.google.com/resource-manager/docs/creating-managing-projects

[2]: https://cloud.google.com/compute/docs/regions-zones

[3]: https://cloud.google.com/sdk/gcloud/reference/config
