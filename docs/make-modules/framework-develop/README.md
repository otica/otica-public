[[_TOC_]]

# Developing the Otica Framework

## Introduction

The `framework-develop` module assists in develping and deploying a new
release of the otica framework.

The Otica release process is to maintain two separate but related
repositories. The Otica "source" repository, where development happens,
and a "release" repository that everyone else has access to. The release
repository has a scrubbed history, that is, the only commits it has are
single commits containing all the changes for that release.

This make module requires that the `rsync` utility be available.
The [Python semantic_version][2] module must also be installed.

## Terminology

* _Otica source repository:_ This is the private Git repository where
changes are made and committed by the principal Otica developers. Access
to this repository is granted on an individual basis. Currently, this
repository is at [https://code.stanford.edu/otica/otica][1]

* _Otica source repository refspec:_ The branch or tag of the
Otica source repository you want to release. This is
set via the variable `FRAMEWORK_SOURCE_REPO_REFSPEC`.

* _Otica release repository:_ This is a Git repository comprising a _subset_ of the
files in the Otica source repository. In particular, the
Git history from the Otica source repository is _not_ duplicated to an
Otica release repository. Note that there can be more than one Otica
release repository.

* _Public Otica release repository:_ This the Otica release repository
that end-users should use when using the Otica project. This repository
will be world-readable.

## How a release works

Here is an outline of what happens when you run `make fw-release`:

1. The Otica _source_ repository is cloned and checked-out to the Otica
source repository refspec as specified by `FRAMEWORK_SOURCE_REPO_REFSPEC`.
When releasing to `stage` we set the refspec to `master`, while when
releasing to `public` we set the refspec to `release/latest`.

1. The files in the clonsed Otica source repository working directory are
exported using `git archive` into a tarball with certain of the files and
directories excluded.

1. The Otica _release_ repository is cloned.

1. The tarball from the above step is un-tarred onto a temporary directory
and the files in that temporary directory are rsync'ed to the Otica release
repository working directory.

1. All the files in the Otica release repository working directory are
committed.

1. The commit is tagged with the Otica version number.

1. The commits and tags in the Otica release repository working directory
are pushed.


## Recommended Workflow

1. Do your Otica framework development in the Otica source repository.

1. When ready to release changes change the version in `OTICA_VERSION`.
Commit and push all changes. Tag the commit you want to release as
`release/latest`.

1. Change to your Otica framework subproject (the one that has this
`framework-develop.mk` make module). Change into the `staging`
environment.

1. Make sure that the variable `FRAMEWORK_SOURCE_REPO_REFSPEC` is set to
`release/latest`.

1. To see what files are changing with your new release:

        $ make fw-deploy-status

1. To see the file differences between your new release and the existing
`staging` release:

        $ make fw-deploy-diff

1. When you are happy with the changes, commit and push your changes to
the `staging` repository:

        $ make fw-deploy-commit

1. The above will tag the release with the version number in `OTICA_RELEASE`.

1. When ready for a public release repeat Steps 2 through 7 but change
into the `public` rather then `stagin` environment.


## Configuring an Otica framework development project

* `FRAMEWORK_SOURCE_REPO_URL`: the Git URL of the Otica source repository
you are releasing.
No default.

* `FRAMEWORK_SOURCE_REPO_REFSPEC`: the branch or tag of the Otica source repository
you are releasing.
Default: `master`.

* `FRAMEWORK_RELEASE_REPO_URL`: the Git URL to the repsitory you want to
release the Otica project to.

* `FRAMEWORK_RELEASE_REPO_REFSPEC`: the branch of
`FRAMEWORK_RELEASE_REPO_URL` you want to release the Otica project to.
Default: `master`.

* `FRAMEWORK_BUILD_DIR`: this is the directory where the Otica tarball
file is generated. Default: `build`.


## The Otica version number

The Otica version is the first (and only) line of the file
[`OTICA_VERSION`](OTICA_VERSION). The Otica version number follows the
[semantic versioning][3] (semi)-standard.

When releasing a new version using this module, the make targets will
throw an error if the version in the Otica source repository is not
strictly larger than the version in the Otica release repository.

## What is copied to an Otica release repository?

All the files in the Otica source repository are copied to an Otica
release repository _except_ those excluded via the `export-ignore` Git
attribute in the [`.gitattributes`](.gitattributes) file.

We currently do not release files in the archive directories and the
`helm-charts-legacy/` directory. To see the exact list of files not exported see
[`.gitattributes`](.gitattributes).



[1]: https://code.stanford.edu/otica/otica

[2]: https://pypi.org/project/semantic-version/

[3]: https://semver.org/
