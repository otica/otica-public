[[_TOC_]]

# Creating and managing self-signed certificates

## Overview

The `self-signed.mk` Make module creates self-signed keypairs and (optionally)
storing them in Vault. Useful wherever self-signed certificates are
needed, for example, when deploying SAML-based web applications.

The certificate created uses RSA encrpytion and a random serial number.

## Quick start

1. Install the [cryptography Python module][1].

1. Define some required environment variables:

        export SSIGNED_COMMON_NAME=myapp.example.com
        export SSIGNED_STATE_OR_PROVINCE_NAME=California
        export SSIGNED_ORGANIZATIONAL_UNIT_NAME=Central IT
        export SSIGNED_EXPIRE_IN_DAYS=365

2. Generate a self-signed keypair:

        $ make ss-gen-keypair
        your keys are in 'build/myapp.example.com-certificate.pem' and 'build/myapp.example.com-privatekey.pem'

3. (Optional) Store these keys in Vault:

        $ make ss-gen-keypair
        your keys are in 'build/myapp.example.com-certificate.pem' and 'build/myapp.example.com-privatekey.pem'

        # Assumes that SSIGNED_SEC_PATH is set to secret/projects/your-project/your-app/saml
        $ make ss-write-vault
        wrote certificate to Vault path secret/projects/your-project/your-app/saml/certificate
        wrote certificate to Vault path secret/projects/your-project/your-app/saml/privatekey

## Configuration

All configuration is via environment variables. All `self-signed.mk` environment
variables start `SSIGNED_`.

### Certificate subject

The `self-signed.mk` Make module uses these environment variables to set
the certificate Subject. The required components are marked with a `[*]`.
```
C=SSIGNED_COUNTRY_NAME            [*]  (defaults to "US")
ST=SSIGNED_STATE_OR_PROVINCE_NAME [*]  (defaults to "California")
L=SSIGNED_LOCALITY_NAME
O=SSIGNED_ORGANIZATION_NAME
OU=SSIGNED_ORGANIZATIONAL_UNIT    [*]
CN=SSIGNED_COMMON_NAME            [*]
emailAddress=SSIGNED_EMAIL_ADDRESS
```

For example, if
`SSIGNED_COUNTRY_NAME=US`,
`SSIGNED_STATE_OR_PROVINCE_NAME=California`,
`SSIGNED_ORGANIZATIONAL_UNIT=Central IT`,
`SSIGNED_COMMON_NAME=myapp.example.com`, then the self-signed certificate will have
Subject
```
C=US, ST=California, OU=Central IT, CN=myapp.example.com
```

### Expiration date

You tell `self-signed.mk` when the certificate expires by defining
`SSIGNED_EXPIRE_IN_DAYS`. For example, setting `SSIGNED_EXPIRE_IN_DAYS` to
"365" will create a certificate that expires in one year.

The `SSIGNED_EXPIRE_IN_DAYS` environment variable is *required*.

### Key length

The certificate `self-signed.mk` creates uses a 3072-bit RSA key.
To use a different key length define `SSIGNED_KEY_LENGTH`, e.g.,
`SSIGNED_KEY_LENGTH=4096` will create 4096-bit keys.

### Encryption algorithm

RSA is the only encryption algorithm `self-signed.mk` supports.

### Vault path

You have the option to store the keypair in Vault. If you do you must set
the `SSIGNED_SEC_PATH` environment variable. For more details on Vault
integration, see section ["Vault integration"](#vault-integration) below.

## Vault integration

An optional feature of `self-signed.mk` is the ability to
write the generated keypair values into Vault. To do this you
must set `SSIGNED_SEC_PATH` to the Vault path where the values
are to be written.

*Example.* If `SSIGNED_SEC_PATH` is set to `secret/projects/your-project/your-app/saml`, then
the following commands will generate the keypair and write it to Vault:
```
$ make ss-gen-keypair
your keys are in 'build/myapp.example.com-certificate.pem' and 'build/myapp.example.com-privatekey.pem'

$ make ss-write-vault
wrote certificate to Vault path secret/projects/your-project/your-app/saml/certificate
wrote certificate to Vault path secret/projects/your-project/your-app/saml/privatekey
```

The `make ss-vault-write` command will _not_ save the keypair to Vault if
there are already values at that path. To get around this you can either
delete the existing Vault values manually or else use `make
ss-value-write-force` which forces the update.


## Make targets

### `ss-show-env`

Show the `self-signed.mk` environment variables.
```
$ make ss-show-env
pass: BUILD_DIR                      ("build")
pass: SSIGNED_COMMON_NAME            ("myapp-example.com-saml")
pass: SSIGNED_COUNTRY_NAME           ("US")
pass: SSIGNED_STATE_OR_PROVINCE_NAME ("California")
pass: SSIGNED_LOCALITY_NAME          ("Anytown")
pass: SSIGNED_ORGANIZATION_NAME      ("State University")
pass: SSIGNED_ORGANIZATIONAL_UNIT_NAME ("Central IT")
pass: SSIGNED_EMAIL_ADDRESS          (<NOT SET>)
pass: SSIGNED_EXPIRE_IN_DAYS         ("3650")
pass: SSIGNED_KEY_LENGTH             ("3072")
pass: SSIGNED_SEC_PATH               ("secret/projects/your-project/your-app/saml")
Subject: <Name(C=US,ST=California,L=Anytown,O=State University IT,OU=CVentral IT,CN=myapp-example.com-saml)>
File destinations: build/myapp-example.com-saml-certificate.pem, build/myapp-example.com-saml-privatekey.pem
```

### `ss-gen-keypair`

Generate the self-signed keypair writing output to `BUILD_DIR`.
Display the names of the output files by running the `ss-show-env` target.

Running `make ss-gen-keypair` will _not_ overwrite existing keypair
files; if you need to regenerate do a `make ss-clean` first.

### `ss-clean`

Remove the keypair files from `BUILD_DIR`. Run if you need to regenerate
the keypair files. Does not affect Vault.

### `ss-write-vault`

Write the generated certificate files into Vault. Requires that
`SSIGNED_SEC_PATH` be set. See section ["Vault integration"](#vault-integration) for more
details on Vault integration.

Note that `make ss-write-vault` will _not_ overwrite existing vault
values; if you need to update the Vault values either remove the existing
Vault values manually, or use `make ss-write-vault-force` instead.

### `ss-write-vault-force`

The same as `ss-write-vault` except will overwrite existing Vault values.

[1]: https://github.com/pyca/cryptography
