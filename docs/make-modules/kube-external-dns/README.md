[[_TOC_]]

# ExternalDNS for Kubernetes

## Overview

The `kube-external-dns.mk` make module provides a simple way to manage
an [ExternalDNS][1] instance using Bitnami's [ExternalDNS Helm chart][2].

The `kube-external-dns.mk` make module uses the [`kube.mk`](../kube)'s
`KUBE_PLATFORM` environment variable to determine which cloud provider's
Kubernetes system you are deploying to. Currently this make module
supports deploying ExernalDNS to GKE only, that is, `KUBE_PLATFORM` must
be set to `gke`. If there is demand, support for other Kubernetes
platforms can be added.


## Requirements

* `authenticate.mk`: needed in order to authenticate to your cloud
  provider.

* `kube.mk`: needed in order to authenticate to your Kubernetes cluster.

* `vault.mk`: needed for fetching secrets from Vault.

The management of the ExternalDNS instance is via Helm, so you
must have the Helm program installed.

## Configuration

### General

* `KUBE_PLATFORM` [REQUIRED]: this variable is part of the
[`kube.mk`](../kube) make module and must be set. Currently
`kube-external-dns` only supports Google Kubernetes Engine so this
variable must be set to `gke`.

* `EXTERNAL_DNS_VERSION` [REQUIRED]: this is the version of the Bitnami
Helm chart you want to install. To see a list of available versions run
the make target `exdns-repo-list-versions`. Example: `6.1.5`.

* `HELM_YAML_FILES`: this environment variable is used by the
`helm-wrapper.py` script and is a space-delimited list of Helm
`values.yaml` files that are loaded when deploying the ExternalDNS Helm
chart. Each version of ExternalDNS supported by `kube-external-dns` has an
associated values.yaml file. By default the `HELM_YAML_FILES` environment
variable includes three locations: the ExternalDNS values.yaml file , the
subproject's common directory, and the subproject's environment directory.
See the [`helm.mk`](../helm) make module for more information on
`HELM_YAML_FILES`.

* `EXTERNAL_DNS_KUBE_NAMESPACE`: the namespace in which to install
ExternalDNS. Default: `kube-system`.

* `TXT_OWNER_ID`: A name that identifies this instance of ExternalDNS.
The Helm chart value `txtOwnerId` is set to this value.
Default: `KUBE_CLUSTER_NAME` (see also [`kube.mk`](../kube)).

* `EXTERNAL_DNS_EVENTS`: the Helm chart value `triggerLoopOnEvent` is set
to this value. Set to "true" to enable triggers to run on
create/update/delete events in addition to regular intervals.
Default: "true".

* `EXTERNAL_DNS_INTERVAL`: how often to poll the ExternalDNS and sync DNS.
Default: "30m".

### Platform-specific: GKE

* `EXTERNAL_DNS_GOOGLE_PROJECT`: the Google Project ID where the GKE
instance is running. Default: `GCP_PROJECT_ID` (see also the [GCP
configuration for the `authenticate.mk` make
module](../../authenticate/gcp).

* `EXTERNAL_DNS_GCP_CREDENTIALS_PATH`: the Vault path to the JSON string
containing the secret for a GCP service account that can manage your GCP
DNS instance. The Helm chart `google.serviceAccountKey` is set to the
value of this JSON string.


## Make targets

### `exdns-show-env`

Show the environment variables relevant to this module. Useful for
verifying that you have all your variables set up properly.

### `exdns-template`

Generate the Helm template files that would be installed when running
`make exdns-install` or `make exdns-upgrade`.

### `exdns-show-diff`

Show the difference between the templates for what is currently running
and what _would_ be running if you were to run `make exdns-upgrade`. Run
this before any upgrade to see what is changing.

### `exdns-repo-update`

### `exdns-repo-list-versions`

### `exdns-upgrade`

### `exdns-create-ns`

### `exdns-list`

List the current running Helm release of ExternalDNS.

### `exdns-show-details`

### `exdns-install`

### `exdns-uninstall`

### `exdns-values-merge`



[1]: https://github.com/kubernetes-sigs/external-dns

[2]: https://artifacthub.io/packages/helm/bitnami/external-dns

[3]: https://cloud.google.com/kubernetes-engine
