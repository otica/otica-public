[[_TOC_]]

# Make Modules

## Overview

The "intelligence" of Otica is contained in its scripts and its
makefiles. These makefiles are based on GNU Make and come in "modules". A
make module is a set of related make targets. Examples of make modules are
`vault.mk`, `helm.mk`, and `docker-develop.mk`.

## Supported Modules
For more information on each make module click on its documentation in the
list below.

### Authentication modules

* [Cloud Platform Authentication (`authenticate.mk`)](authenticate/)

* [AWS Configuration and Authentication (`config-aws.mk`)](config-aws/)

* [GCP Configuration and Authentication (`config-gcp.mk`)](config-gcp/)

* [Vault (`vault.mk`)](vault/)

* [GCP Workload Identity (`gcp/workload-identity.mk`)](workload-identity/)


### Development and deployment modules

* [Docker Compose (`docker-compose.mk`)](docker-compose/)

* [Docker Development (`docker-develop.mk`)](docker-develop/)

* [Function-as-a-Service (`faas.mk`)](faas/)

* [Helm (`helm.mk`)](helm/)

* [Helm Development (`helm-develop.mk`)](helm-develop/)

* [Otica Framework Development (`framework-develop.mk`)](framework-develop/)

* [Subproject Repo Management (`sub-projects.mk`)](repos/)

* [Terraform (`terraform.mk`)](terraform/)


### Kubernetes modules

* [Kubernetes Configuration and Authentication (`kube.mk`)](kube/)

* [Kubernetes Secrets Management with Vault (`kube-sec.mk`)](kube-sec/)

* [Manage ExternalDNS in Kubernetes (`kube-external-dns.mk`)](kube-external-dns/)


### System modules

* [Framework setup (`framework.mk`)](framework/)

### Miscellaneous modules

* [Shibboleth datasealer management (`datasealer.mk`)](datasealer/)

## Unsupported Modules

We list the make modules in Otica that are no longer supported here.
Use at your own risk.

* `gcp/audit.mk`: Set up audit logging for a GCP project.

* `cert-manager-helm3.mk`: Add an ACME certificate manager application using Helm.

* `deps.mk`: Install needed software and command-line tools.

* `envvars.mk`: generate and include envvars from `envvars.sec`.

* `gcp/cloudsql.mk`: Manage a GCP Cloud SQL instance.

* `gcp/nat.mk`: Manage a NAT in GCP.

## How to enable a make module

To enable a particular make module add it to the `makefile_parts/otica` section of
`otica.yaml`. For example, to enable the `helm` make module:

```
---
environment:
  toplevel:
    - framework.var
  common:
    - common.var
  local:
    - local.var

makefile_parts:
  otica:
    - framework.mk
    - authenticate.mk
    - vault.mk
    - helm.mk    ## <<< PUT IT HERE
```

