[[_TOC_]]

# Kubernetes Configuration and Authentication

## Overview

The `kube.mk` makefile module supports Kubernetes cluster authentication and
configuration as well as some other useful Kubernetes actions.

This module supports these on-premise Kubernetes clusters as well as
clusters on Cloud provider platforms:

* GKE (Google Kubernetes Engine)

* EKS (AWS Elastic Kubernetes Service)

* AKS (Azure Kubernetes Service) (NOT YET SUPPORTED)

* On-premise Kubernetes clusters

## Configuration

### Environment Variables: Cloud Platforms

**Note.** For Kubernetes clusters that are part of a cloud platform you must
configure that cloud platform's variables before the
`kube.mk` make module will work properly. See the [Cloud Platform
Authentication (`authenticate.mk`) make module](../authenticate/) for how
to configure a cloud platform.

For kubernetes on a cloud platform the following environment variables
**must** be set:

* `KUBE_PLATFORM`: the cloud platform your Kubernetes cluster runs in. Must be
one of `gke`, `eks`, or `aks` (`aks` support not yet finished).

* `KUBE_CLUSTER_NAME`: the name of the Kubernetes cluster you want to
manage. Examples: `stage-us-west1`, `prod-us-west1`.

* `KUBE_NAMESPACE`: the Kubernetes namespace. This is typically set
in an Otica environment directory.
Examples: `myapp-dev`, `myapp-prod`.

### Environment Variables: On-Premise Platforms

We currently support the `kube-vault` on-premise authentication mechanism.
This mechanism assumes that you have a `.kube/config` string in Vault and
extracts it in order to authenticate. This configuration string must
correspond to a _single_ cluster. The string will look something like this:
```
apiVersion: v1
kind: Config
preferences: {}
clusters:
  - cluster:
      certificate-authority-data: LS0tLS1...
      server: https://192.168.0.123:443
    name: kube-onpremise-dev
contexts:
  - context:
      cluster: kube-onpremise-dev
      user: kube-onpremise-dev
    name: kube-onpremise-dev
users:
  - name: kube-onpremise-dev
    user:
      client-certificate-data: LS0tLS...
    client-key-data: LS0tLS1...
```

When you run `make kube-config` with this mechanism the string is
extracted from Vault and merged with the [Kubernetes configuration][2] in your
home directory (usually `./kube/config`).

To use the `kube-vault` platform the following environment variables
**must** be set:

* `KUBE_PLATFORM`: the on-premise platform your Kubernetes cluster runs
in. This must be set to `kube-vault`.

* `KUBECONFIG_SEC_PATH`: the Vault path to the Kubernetes config string.

* `KUBE_CONTEXT`: the Kubernetes context. This must correspond to a
`context` element of the kubernetes configuration YAML string.

* `KUBE_NAMESPACE`: the Kubernetes namespace. This is typically set
in an Otica environment directory.
Examples: `myapp-dev`, `myapp-prod`.

### Environment Variables: Platform-specific variables

* `GKE_CLUSTER_AVAILABILITY_TYPE`: a Kubernetes cluster in GKE is either a
[zonal cluster or a regonal cluster][1]. If `KUBE_PLATFORM` is `gke` and
`GKE_CLUSTER_AVAILABILITY_TYPE` is set to either "regional" (respectively
"zonal") then when authenticating to the GKE Kubernetes cluster the
location will be set to `GCP_REGION` (respectively `GCP_ZONE`). While
Otica can infer the availability type using `gcloud` tools, setting
`GKE_CLUSTER_AVAILABILITY_TYPE` to the correct value will improve
Otica Kubernetes configuration speed.

## Make targets

### `kube-config`

Authenticates to the platform, gets Kubernetes cluster credentials, and
sets up the Kubernetes context. This is the command you run whenever first
changing into an Otica environment directory so that you can manage the
correct Kubernetes namespace.

### `kube-namespace-create`

Creates the Kubernetes namespace specified by `KUBE_NAMESPACE`.

### `kube-namespace-show`

Shows the current Kubernetes namespace.

### `kube-cluster-info`

Displays Kubernetes cluster information by running `kubectl cluster-info`.

### `kube-show-resources`
Lists all Kubernetes resources in the current namespace by running
`kubectl get all`.

### `kube-failed-pods-ls`

List all Pods with phase "Failed" in the Kubernetes namespace
`KUBE_NAMESPACE`.

### `kube-failed-pods-rm`

Delete all Pods with phase "Failed" in the Kubernetes namespace
`KUBE_NAMESPACE`.

### `kube-deploy-render`

Take all files in `common/templates` that have an extension of `.yaml`,
`.yml`, or `.json` and "render" them, that is, run them through the
`render.sh` script. The results are put in the `BUILD_DIR/templates`
subdirectory (usually `build/templates`) of the current directory.

### `kube-deploy-apply`

Render all files via `kube-deploy-render` (see above) and then
do a `kubectl apply` on all these rendered files.

### `kube-deploy-delete`

Delete all the Kubernetes resources created by `kube-deploy-apply`.

### `ssh`

Use this target to "login" to a container running on one of your Pods
using the `pod-login` script.

For this to work you need to set some extra environment variables; see the
[`pod-login` man page](/docs/manpages/pod-login/) for details.

[1]: https://cloud.google.com/kubernetes-engine/docs/concepts/types-of-clusters#availability

[2]: https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/
