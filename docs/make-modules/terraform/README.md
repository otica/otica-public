[[_TOC_]]

# Terraform (`terraform.mk`)

## Overview

The [Terraform][1] make module `terraform.mk` provides a unified way to manage
Terraform resources. It also allows the flexibility of multiple terraform "stacks".

The `terraform.mk` make module expects the usual Otica directory structure
and expects your Terraform code to be in `../common/terraform/` (although
see ["The `TF_*` variables"](#the-tf_-variables) below on how to override
this). You can also have Terraform code in your local environment
directories.

The `terraform.mk` make module will [run through Otica's filter
system](../../filters) any files with an extension matching the file glob
`.tmpl*` before running `terraform`. In particular, you can use
`#!envsubst` at the top of a `.tf.tmpl` file to interpolate any variables
you set in your variable files.

## The `TF_*` variables

(Note: for simple Terraform projects there is usually no need to
override any of these variables.)

The `terraform.mk` make module has defaults that expect
the terraform code to be `../common/terraform/` and will build
in `build/`. However, these can be overridden.

* `TF_BUILD_DIR`: this is the directory relative to where `make` is called
  where all the files in `TF_DIR` are copied. This should only be changed
  in unusual circumstances. Defaults to `build/`.

* `TF_DIR`: the directory where Terraform files (`.tf`), Terraform
  definition files (`.tfvars`), and any template files (`*.tmpl*`) live.
  These files are copied to `TF_BUILD_DIR` and rendered before `terraform`
  is called.

  `TF_DIR` is a computed value and is equal to
  `${TF_CODE_BASEDIR}/${TF_CATEGORY}`. This defaults to
  `../common/terraform/`.

* `TF_CATEGORY`: the directory _name_ that contains the Terraform code.
  This value should normally not be a path but a single string. Default:
  `terraform`.

* `TF_CODE_BASEDIR`: the directory that contains the Terraform code
  directory `TF_CATEGORY`. Default: `../common`.

* `TF_MODULES_DIR`: the directory containing your terraform modules.
  This directory is soft-linked into `TF_BUILD_DIR` prior to running
  `terraform`; note that the files in `TF_BUILD_DIR` are _not_ rendered prior
  to running `terraform`.
  Default: `${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules`


## Workflow

This `terraform.mk` make module does its Terraform tasks by calling the
script `tf-sync.sh` and then running `terraform`. Here is an summary of what
happens when you run one of the `make tf-*` targets:

1. [tf-sync.sh] Copy all the files in `TF_DIR` into `TF_BUILD_DIR`.

1. [tf-sync.sh] Copy any _local_ `.tf` or `.tfvar files` into `TF_BUILD_DIR`.

1. [tf-sync.sh] If it exists, link the terraform modules directory pointed
to by `TF_MODULES_DIR` into `TF_BUILD_DIR`.

1. [tf-sync.sh] Render any  template files (`*.tmpl*`) in `TF_BUILD_DIR`.

1. Run `terraform` in `TF_BUILD_DIR`.


## Examples of directory layouts

### Default layout (single terraform stack)

```
my-subproject/
|
|-- otica.yaml
|
|-- common/ (TF_CODE_BASEDIR)
|   |
|   |-- env_variables/
|   |
|   |-- makefile_parts/
|   |
|   |-- scripts/
|   |
|   |-- terraform/ (TF_CATEGORY)
|   |   |
|   |   |- example1.tf
|   |   |
|   |   |- example2.tf
|   |   |
|   |   |- example3.tmpl.tf
|   |
|   '-- makefile.mk
|
|-- <env1>/
|   |
|   |-- local.var
|   |
|   |-- build/  (TF_BUILD_DIR)
|   |
|   '-- makefile.mk
|
```

### Multiple terraform stacks

This is best illustrated with an example. Let's say your subproject
manages via Terraform both computer resources and load-balancer resources.
However, want to keep the two terraform "stacks" separate, that is, keep
separated Terraform code directories and separate Terraform state files.
So you put the computer resource terraform code in
`TF_CODE_BASEDIR/compute` and the load-balancer resources in
`TF_CODE_BASEDIR/loadbalancer`. In your local make files
(`common/makefile_parts`) you would have targets like this:

```
.PHONY: compute-remove
compute-remove: export TF_CATEGORY=compute
compute-remove: aws-auth ## compute resources removal
	@$(MAKE) tf-destroy

.PHONY: loadbalancer-remove
loadbalancer-remove: export TF_CATEGORY=loadbalancer
loadbalancer-remove: aws-auth ## loadbalancer resources removal
	@$(MAKE) tf-destroy
```

Your directory structure would look like this:

```
my-subproject/
|
|-- otica.yaml
|
|-- common/ (TF_CODE_BASEDIR)
|   |
|   |-- env_variables/
|   |
|   |-- makefile_parts/
|   |
|   |-- scripts/
|   |
|   |-- compute/
|   |
|   |-- loadbalancer/
|   |
|   '-- makefile.mk
|
.
.
.
```

Further, if you want to put the `compute/` and `loadbalancer/` directories
under a common directory change the value of `TF_CODE_BASEDIR` in
`common/common.var`. For example, if we set `TF_CODE_BASEDIR` to the value
`${COMMON}/terraform-code` then the above directory structure should look
like this:

```
my-subproject/
|
|-- otica.yaml
|
|-- common/
|   |
|   |-- env_variables/
|   |
|   |-- makefile_parts/
|   |
|   '-- terraform-code/  (TF_CODE_BASEDIR)
|   |   |
|   |   '-- compute/
|   |   |
|   |   '-- loadbalancer/
|   |
|   |-- scripts/
|   |
|   '-- makefile.mk
|
.
.
.
```

## Make targets

Most of `terraform.mk`'s make targets correspond directly with `terraform`'s actions.
We list those first.

* `tf-plan`: run `terraform tf-plan`.

* `tf-apply`: run `terraform tf-apply`.

* `tf-destroy`: run `terraform tf-destroy`.

* `tf-show`: run `terraform tf-show`.

* `tf-refresh`: run `terraform tf-refresh`.

* `tf-output`: run `terraform tf-output`.

* `tf-validate` run `terraform tf-validate`.


There are several targets that are specific to `terraform.mk`:

* `tf-sync`: Sync the files in `TF_DIR` into `TF_BUILD_DIR`. See also
["Workflow"](#workflow).

* `tf-clean`: delete the files in `TF_BUILD_DIR`.

* `tf-init`: do a `terraform init -get`, validate, and put the results(?)
in `resources.json.

* `tf-re-init`: do a `make tf-clean` followed by a `make tf-init`.

* `tf-output-json`: send the state file in JSON format to standard output.


[1]: https://www.terraform.io/
