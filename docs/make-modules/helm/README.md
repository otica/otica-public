[[_TOC_]]

# Using Helm charts in an Otica project

(Note: To see this help page from the command-line type `make helm-helm`.
For information on _developing_ with Otica see the ["Developing with Helm"
`helm-develop.mk` module](../helm-develop).)

## Overview

The `helm.mk` Makefile module provides simple Helm chart support. To use this
Makefile module the following must be true:

1. Your project uses only a single Helm chart.

1. Your Helm chart is hosted in a Helm chart repository accessible by your
project.

Furthermore, this module depends on the `kube.mk` module, so `kube.mk`
must be included in your `otica.yaml` file.

## Traditional Helm Chart repositories vs OCI-based registries

There are two common ways to store and retrieve Helm charts:

* a traditional Helm Chart repository

* an [OCI (Open Container Initiative) registry][2]

The `helm.mk` Makefile module supports both. However, some of the make
targets are only applicable to one or another. In particular, the
following targets only work with traditional chart repository and either do
nothing or, in some cases, will generate an error if used with an OCI-based registry:

* `helm-repo-add`

* `helm-repo-update`

* `helm-repo-remove`

* `helm-repo-list-charts`

* `helm-repo-list-versions`


## Tips

* To see what your Kubernetes templates would look like _before_
upgrading the chart, use `make helm-templates`. To see the difference
between the current Helm charts and a proposed change, use `make helm-diff`.

## Configuration


### Environment variables

To use this module you **must** set the following environment variables:

* `APP_NAMESPACE`: the Kubernetes namespace for your project. The
`helm.mk` module assumes this namespace already exists.

* `APP_ENV`: the name of one of your project's environment directories
(e.g., "dev" or "prod").

* `HELM_REPO_URL`: points to the URL where your Helm chart is hosted.

* `HELM_CHART_NAME`: the name of the chart in `HELM_REPO_URL` you are
deploying.

The following are optional (meaning they assume reasonable defaults):

* `HELM_REPO_NAME`: the local name you want to use for the HELM repo
pointed to by `HELM_REPO_URL`. This is *required* for traditional Helm
chart repositories and Google storage repositories. Do *not* define
`HELM_REPO_NAME` if using an [OCI-based registry][2]; if you do define
`HELM_REPO_NAME` when using an OCI-based registry Otica will raise an
error.

* `HELM_RELEASE`: the name to use for the running Helm instance; defaults
to the value of `HELM_CHART_NAME`.

* `HELM_CHART_VERSION`: the version of the Helm chart to use. If omitted,
or set to the string "latest", will use the latest version of the
Chart. This is useful when testing Helm chart releases.
See also the `--version` flag of `helm install`.

* `HELM_YAML_FILES`: a space-separated list of values.yaml files that are
to be included via `helm`'s `-f/--values` option when installing or
upgrading the Helm release. The _order_ that the files in this variable
appear is **important**: values in files that appear later in the list
override values set by files that appear earlier in the list. The default
value for `HELM_YAML_FILES` is
```
../common/values-local.yaml values-local.yaml
```
See also the section ["The `values-local.yaml` files"](#the-valuesyaml-and-values-localyaml-files) below.

#### Examples

##### Traditional public chart repository

This example subproject uses the `gcloud-sqlproxy` Helm chart hosted at
the chart repository https://charts.rimusz.net/. We use the name `rimusz`
to refer to the local cache of the chart repository https://charts.rimusz.net/.
We will have two subproject environments: `stage` and `prod`. So, we would set
the above environment variables as follows:
```
APP_NAMESPACE:    sql-proxy
APP_ENV:          stage (or prod)
HELM_REPO_URL:    https://charts.rimusz.net/
HELM_CHART_NAME:  gcloud-sqlproxy
HELM_REPO_NAME:   rimusz
```

##### OCI-based chart registry

This example subproject uses the `apache-oauth2` Helm chart
hosted at the Artifact Registry path
oci://us-west1-docker.pkg.dev/uit-et-iedo-services/docker-testing/charts/apache-oauth2.
We set the above environment variables as follows (note that this being an OCI-based
registry we do NOT set `HELM_REPO_NAME`):
```
APP_NAMESPACE:    apache-oauth2-dev
APP_ENV:          stage
HELM_REPO_URL:    oci://us-west1-docker.pkg.dev/uit-et-iedo-services/docker-testing/charts
HELM_CHART_NAME:  apache-oauth2
```

### The `values.yaml` and `values-local.yaml` files

Helm chart values are set via the `values.yaml` file. A default
`values.yaml` comes with the chart from the Helm chart repository.
However, you almost always need to override some of the entries in the
default `values.yaml`. You do this by specifying your own values.yaml
files. All `values-local.yaml` files are sent through
[`render2.sh`](../../filter/render2) before being used.

By default there are two `values-local.yaml` files where you can set chart values:
one in `common/` and one in (each) subproject environment:
```
my-subproject/
|-- README.md
|-- common
|   |-- var.mk
|   |-- framework-var.mk
|   |-- values-local.yaml
|-- stage
|   |-- Makefile
|   |-- values-local.yaml
|   `-- var.mk
...
```

These files cascade: first the chart's `values.yaml` file is read, then
the file `common/values-local.yaml` is read (if it exists), and finally,
the file `values-local.yaml` in the environment directory is read (if it
exists).

So the settings in the environment instance (i.e., in
`stage/values-local.yaml`) take precedence over the values in
`common/values-local.yaml` which, in turn, take precedence over the
chart's `values.yaml` file.

This cascading allows you to set subproject-wide settings in
`common/values-local.yaml` and override only those you need to change in
each subproject environment.

#### Rendering of `values-local.yaml`

The `values-local.yaml` files are run through the
[`render2.sh`](../filters/render2/) filter before being used. If you
want environment variables in your `values-local.yaml` file to be expanded
put `#!envsubst` at the top.

**Example** For our example `my-subproject`, let's define the common
`values-local.yaml` file first:
```
#!envsubst
# common/values-local.yaml
# Chart settings common to all environment instances of gcloud-sqlproxy
enabled: true
cloudsql:
  instances:
    - project: ${GCP_PROJECT_ID}
      region: ${GCP_REGION}
      instance: ${GCP_CLOUD_SQL_PROXY_MYSQL_INSTANCE}
      port: 3306
    - project: ${GCP_PROJECT_ID}
      region: ${GCP_REGION}
      instance: ${GCP_CLOUD_SQL_PROXY_POSTGRES_INSTANCE}
      port: 5432
replicasCount: 1
serviceAccount:
  create: false
```

We want two replicas for `stage` and three for `prod`, so
for `stage/values-local.yaml` we have
```
replicasCount: 2
```
and in `prod/values-local.yaml` we have
```
# prod/values-local.yaml
replicasCount: 3
```


#### Notes on using `values-local.yaml`

1. You can have just the common `values-local.yaml` file, just the
subproject environment `values-local.yaml` file, both, or neither.

1. The subproject environment `values-local.yaml` file takes precedence
over `common/values-local.yaml`.

1. You can use additional values.yaml files, or change the files that are
used for values.yaml by changing the value of the `HELM_YAML_FILES`
environment variable.

1. Each `values-local.yaml` file is sent through the `render2.sh`, so if
you want some form of text substitution applied be sure to put the desired
`#!`-string at the top of the YAML file.

1. To see what final result of all the files is after cascading and
environment substitution run `make helm-merge`.

1. You can accomplish the same "cascading" values effect by using just the
single `common/values-local.yaml` file and setting the values via
environment variables. Which method you use (two YAML files
vs. environment variables) is a matter of taste.

### Using a Google Cloud Storage hosted Helm repository

If your Helm chart repository is hosted in a Google Cloud Storage (GCS)
bucket, then the URL (`HELM_REPO_URL`) will start with the string `gs://`.
The `helm.mk` module supports these repositories but only if the [Helm GCS
plugin][1] is installed. To check that the plugin is installed run `make
helm-show-env`; if the plugin is not installed you will see an error
message to that effect.

To install the plugin:
```
helm plugin install https://github.com/hayorov/helm-gcs.git
```

## Helm registry authentication

To pull Helm charts you need to have access to the registory where the
Helm chart is hosted. If the registry is public there is nothing to do.
Otherwise, you will need to provide some sort of credentials for access.

You need to set `HELM_REGISTRY_LOGIN_TYPE` to the appropriate string to
indicate which form of authentication to use. The `helm.mk` module
supports the following types of authentication:

* `gcloud`: authentication is achieved via the [gcloud command][3] `gcloud
auth configure-docker`; so, in this case, no secrets need to be managed.
Use this form for accessing Google's Artifact Registry using the `gcloud`
command.

The variable `HELM_REGISTRY_LOGIN_TYPE` is set to `gcloud` by default.


## Managing the lifecycle of a Helm installation

1. Check that the proper environment variables are set:

        $ make helm-show-env

1. Check that your `values-local.yaml` file(s) are correct:

        $ make helm-values-merge

1. (Only for non-OCI registries.)
If not already done, add the Helm chart's repository and do a list to make
sure it was added:

        $ make helm-repo-add
        $ make helm-repo-list

1. Look at the Helm chart's templates and verify they look good:

        $ make helm-template

1. Do a dry-run install:

        $ make helm-install-dryrun


1. Do a **real** install:

        $ make helm-install

1. Check the install:

        $ make helm-list

1. (Only for non-OCI registries.) If the Helm chart is updated refresh the local chart cache:

        $ make helm-repo-update

1. Later, if you change the configuration or the Helm chart is upgraded,
do a Helm "diff" and if everything is OK upgrade your Helm installation:

        $ make helm-diff
        $ make helm-upgrade

## Operations

### `helm-help`

This make target displays this file. If the `pandoc` program is installed it
renders the Markdown into plain text.

### `helm-check`

To check that the your variables are set, run `make helm-check`. Note that
this check does _not_ verify that the Kubernetes namespace has been
created.

### `helm-show-env`

This target displays the values of environment variables described in
section "Configuration" above. Will also display an error if your Helm
chart repository is hosted in Google Cloud Storage but the [Helm GCS
plugin][1] is not installed.

### `helm-values-merge`

Load all the values.yaml files specified in `HELM_YAML_FILES` and produce
a list of the merged values.

### `helm-get-values`

Get the values file from the running Helm chart. This is different than
`helm-values-show` in that `helm-values-show` displays the file that will
be used when installing or upgrading the Helm chart, while
`helm-get-values` shows the values from the actual _running_ Helm
chart. These are usually the same, but, in some circumstances, might
differ.

### `helm-diff`

This diff downloads the templates from the currently running chart (the
"running") and compares this to the templates that would be generated (the
"potential") from the current `values-local.yaml` file. This way you can
see what the changes will be when you run `helm-update` before you
actually run it. The output will be in "diff --context=3" meaning that
three lines of context around changes will be displayed while changed,
new, and removed lines are indicated by `!`, `+`, and `-`, repectively.

### `helm-repo-add` (not for oci://)

Add the Helm repo where the Helm chart is hosted. That is, in the local
Helm repo create an entry with name `HELM_REPO_NAME` pointing to URL
`HELM_REPO_URL`.

### `helm-repo-remove` (not for oci://)

Remove the local repo pointer to `HELM_REPO_URL`.

### `helm-repo-list` (not for oci://)

List the local Helm repository.

### `helm-repo-list-charts` (not for oci://)

List _all_ the charts in repo `HELM_REPO_URL`.

### `helm-repo-list-versions` (not for oci://)

List all versions of chart `HELM_REPO_NAME` in repo `HELM_REPO_URL`.

### `helm-repo-update` (not for oci://)

Update the local Helm repository cache. This is necessary when the chart
hosted at `HELM_REPO_URL` is updated (similar to an `apt-get update`).

### `helm-install-dryrun`

Do a Helm install but in dry-run mode.

### `helm-install`

Do a Helm install. Run `make helm-list` afterwards to confirm that the
Helm chart is installed.

### `helm-upgrade`

Do a Helm upgrade. This should be done if the Helm chart changes or your
configuration changes.

### `helm-upsert`

Do a Helm upgrade of a running Helm chart, or install if the Helm chart is
not installed.
(This target simply adds the `--install` option to the `helm-uprade` target.)

### `helm-uninstall`

Remove the running Helm chart.

### `helm-restart`

There is NO helm-restart target.


[1]: https://github.com/hayorov/helm-gcs.git

[2]: https://helm.sh/docs/topics/registries/

[3]: https://cloud.google.com/sdk/gcloud
