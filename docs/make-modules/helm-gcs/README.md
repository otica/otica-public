[[_TOC_]]

# Managing the Helm GCS (Google Cloud Storage) plugin

## Quick start

1. Add this make module (`helm-gcs.mk`) to your `otica.yaml` file
and run `otica update`.

1. Change into an environment directory.

1. Check that the necessary environment variables are set:

        $ make helm-gcs-check

1. Install the plugin:

        $ make helm-gcs-install

1. In your repository has not yet been set up in GCS initialize it now:

        $ make helm-gcs-init

1. You can now go to your Helm development project and add this repository
using `make dhelm-repo-add`.

## Configuration

As with other parts of Otica, environment variables for this module should be
set in one of the `*.var` files.

* `HELM_REPO_URL`: points to the URL where your Helm chart is hosted. As
this module is managing a Helm chart repository in a Google Cloud Storage
bucket, `HELM_REPO_URL` _must_ start with `gs://`.


## Initializing a new GCS Helm chart repository

If your GCS Helm chart repository has not yet been created you need to
initialize it. Set your `HELM_REPO_URL` environment variable to the correct
`gs://` path and then run `make helm-gcs-init`.
