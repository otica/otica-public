[[_TOC_]]

# Kubernetes Port Forwarding

## Quick start

1. Add `kube-pf.mk` to the `makefile_parts:otica` section of the
`otica.yaml` file and run `otica update`.

2. To connect to port 80 via local port 80 on the first running Pod whose name contains "web":

        make kube-pf-connect PORT=80 FILTER=web &
        wget 127.0.0.1:80/metrics


## Overview

The `kube-pf.mk` make module is a convenient wrapper around the `kubectl
port-forward` command making it easy to connect to a running Pod's
port from your local machine.

For more information on port-forwarding with kubectl see ["Use Port
Forwarding to Access Applications in a Cluster"][1].

## Usage

The `kube-pf-connect` make target will look for the first
running-and-ready Pod and attempt to connect to that Pod. There are two ways
to tell `kube-pf.mk` which Pod to connect to and which ports to use.

The first is to set the following environment variables:
```
KUBE_PF_LOCAL_PORT
KUBE_PF_REMOTE_PORT
KUBE_PF_FILTER
```

For example, to set up a port-forwarder that listens on local port 81 and
forwards to port 80 on the first running-and-ready Pod whose name contains
the string "web", set the environment variables like this:
```
export KUBE_PF_LOCAL_PORT=81
export KUBE_PF_REMOTE_PORT=80
export KUBE_PF_FILTER=web
```

The second way will work if the local and remote ports are the same. In
this case indicate the ports via the `PORT` variable from the command
line. For example, to connect to Pod port 80 from local port 80 on the
first Pod whose name contains the string "web", run this:
```
make kube-pf-connect PORT=80 KUBE_PF_FILTER=web
```

## Pod selection

The Pod selected will be the first Pod that is "running-and-ready" and
whose name matches the `KUBE_PF_FILTER` regular expression. If no such Pod
exists the connection exit with an error.

## Environment variables

### `KUBE_PF_LOCAL_PORT`

Required. Sets the _local_ port to connect to. Is overridden by the `PORT` environment
variable.
Example: `80`.

### `KUBE_PF_REMOTE_PORT`

Sets the _remote_ port on the Pot to connect to. If omitted will use
the value of `KUBE_PF_LOCAL_PORT`.
Example: `80`.

### `KUBE_PF_FILTER`

Used to pick which Pod to connect to. Set to a Perl-compatible regular
expression. Defaults to '`.`' (i.e., matches everything). Is overridden by
the `FILTER` environment variable. If more than one Pod matches the first
Pod matching will be chosen. Example: `web` (matches any Pod containing
the string `web`).

## Make targets

### kube-pf-show-env

Show the environment variables relevant to the `kube-pf.mk` make-module.

### kube-pf-connect

Connect to a running Pod using port-forwarding. In other words, runs this command:
```
kubectl port-forward <running Pod matching filter> ${KUBE_PF_LOCAL_PORT}:${KUBE_PF_REMOTE_PORT}
```



[1]: https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/
