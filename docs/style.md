# Style Guide for Otica Documentation

* Use "top-level" when using as an adjective; use "toplevel" when
used as a noun. Example:

  - "The top-level directory contains the repos-yaml directory..."

  - "Change directory into the toplevel."

* Use "subproject" when describing a directory inside the directory
`sub-projects` (Yes, there is an inconsistentcy between the name
`sub-projects` and the workd "subdirectory" but it's too late to change it
now.)

* All scripts and code should respect the `DEBUG` environment variable: if
`DEBUG` is set to a non-empty value this means debug mode is enabled,
otherwise debug mode is disabled.

* Environment variables should be set in monospace font: "Set PATH to `/home/user`." (wrong),
"Set `PATH` to `/home/user`." (right).

* Section titles should use use lower case:
```
### Explanation of the environment variables  (right)
### Explanation of the Environment Variables  (wrong)
```
