[[_TOC_]]

# CI/CD support

This page documents any special CI/CD support that Otica provides. CI/CD
support files are put in the [`cicd`](../../cicd) directory.

## GitLab CI/CD

* [GitLab: Docker build and upload to DockerHub](gitlab/otica-kaniko-dockerhub)

* [GitLab: Docker build and upload to GAR (Google Artifact Registry)](gitlab/otica-kaniko-gar)

* [GitLab: Docker build and upload to GCR (Google Container Registry)](gitlab/otica-kaniko-gcr) [DEPRECATED]
