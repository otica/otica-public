# GitLab: Docker build and upload to GAR (Google Artifact Registry)

## Overview

The GitLab-compatible CI/CD configuration file
`cicd/gitlab/otica-kaniko-gar.yaml` provides an easy way to build and
upload your Otica Docker projects to [Google's Artifact Registry][1]
(GAR). This build uses the full Otica framework ensuring that the
resulting images are identical to the build done via a manual `make
docker-build`.
Otica uses [Kaniko][3] to do the Docker image build.

This CI/CD can build and push multiple subproject enviroments in a single
run; see `OTICA_ENVIRONMENTS` in the "Instructions" section below.

## Instructions

1. Your Otica Docker project must be configured to upload to GAR. This
means that `DOCKER_REGISTRY` should be set to your project's Artifact
Registry path and `DOCKER_NAMESPACE` should be set to your [GCP project
id][2].

1. Create a `.gitlab-ci.yml` in your Otica Docker subproject with the
following content:

        variables:
          OTICA_ENVIRONMENTS: dev test prod

        include:
          remote: https://code.stanford.edu/et-iedo-public/misc/-/raw/master/otica-kaniko-gar.yml

1. Change the line above containing `OTICA_ENVIRONMENTS` to be a list of
the subproject environment directories you want to build and upload
against.

1. (If your GitLab Runners are using Google's Workload Identity you can
**skip** this step.) Create a GitLab CI/CD Variable with the name
`GOOGLE_APPLICATION_CREDENTIALS` and put into it a Google service acount
JSON key that has the permissions to push to GAR.

1. **Remember:** If you mark the GitLab CI/CD Variable
`GOOGLE_APPLICATION_CREDENTIALS` as "Protected" then you must mark the
branch where you want the CI/CD to run as Protected as well. If you do not
then the upload will fail.


## Technical detail: how it all works

The CI/CD configuration `otica-kaniko-gar.yml` has two steps. The first
step creates a file that is a series of [Kaniko][3] build commands. The
second step launches a Kaniko Docker container that executes the build
commands generated in the first step.

How does the first step generate this set of Kaniko build commands? Here
is a summary of the GitLab CI/CD steps:

1. Launch a [special Docker container][4] that has enough of the software
needed by Otica to generate the file of Kaniko build commands.

1. In this container clone the public version of Otica.

1. Run the Otica script
[`make-kaniko-cmds`](../../../../bin/make-kaniko-cmds). This script
generates a Kaniko build command for each Otica environment listed in the
environment variable `OTICA_ENVIRONMENTS`.

1. Store the output of the previous step in a GitLab "artifact", that is,
in a file that can be used by the actual Kaniko build step.



[1]: https://cloud.google.com/artifact-registry

[2]: https://support.google.com/googleapi/answer/7014113?hl=en

[3]: https://github.com/GoogleContainerTools/kaniko

[4]: https://code.stanford.edu/et-iedo/docker/docker-otica
