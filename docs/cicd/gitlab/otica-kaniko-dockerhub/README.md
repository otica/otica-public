# GitLab: Docker build and upload to Docker Hub

## Overview

The GitLab-compatible CI/CD configuration file
`cicd/gitlab/otica-kaniko-dockerhub.yaml` provides an easy way to build
and upload your Otica Docker projects to [Docker Hub][1]. This build uses
the full Otica framework ensuring that the resulting images are identical
to the build done via a manual `make docker-build`.
Otica uses [Kaniko][2] to do the Docker image build.

This CI/CD can build and push multiple subproject enviroments in a single
run; see `OTICA_ENVIRONMENTS` in the "Instructions" section below.

## Instructions

1. Your Otica Docker project must be configured to upload to Docker Hub.
This means that `DOCKER_REGISTRY` should be set to `docker.io` and
`DOCKER_NAMESPACE` should be set to the Docker Hub account name you are
pushing to.

1. Create a `.gitlab-ci.yml` in your Otica Docker subproject with the
following content:

        variables:
          OTICA_ENVIRONMENTS: dev test prod

        include:
          remote: https://code.stanford.edu/et-iedo-public/misc/-/raw/master/otica-kaniko-dockerhub.yml

1. Change the line above containing `OTICA_ENVIRONMENTS` to be a list of
the subproject environment directories you want to build and upload
against.

1. For the Docker Hub upload to work you need to provide Docker Hub
account credentials that have access to push Docker images. To do this you
need to set two GitLab CI/CD File variables `DOCKER_HUB_USERNAME` and
`DOCKER_HUB_PASSWORD`. For `DOCKER_HUB_PASSWORD` it is probably best to
use a Docker Hub access token.

1. **Remember:** If you mark either of the CitLab CI/CD File variables
`DOCKER_HUB_USERNAME` or `DOCKER_HUB_PASSWORD` as "Protected" then you
must mark the branch where you want the CI/CD to run as Protected as well.
If you do not then the upload will fail.


## Technical detail: how it all works

The CI/CD build and push to Docker Hub works in much the same way as the
CI/CD build and push to Google's Container Registry, so for for more
information on the technical details see the section "Technical detail:
how it all works" in the ["Docker build and upload to GCR" documentation
page](../otica-kaniko-gcr).


[1]: https://hub.docker.com/

[2]: https://github.com/GoogleContainerTools/kaniko
