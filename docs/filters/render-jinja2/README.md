[[_TOC_]]

# Render a Jinja2 template (`render-jinja2`)

## Overview

The [`render-jinja2`](/scripts/render-jinja2) script takes standard input,
runs it through the [Jinja2 templating engine][1], and sends the results
to standard output.

## Environment variables

When sending a Jinja2 file through `render-jinja2` environment variables
are _not_ expanded as in envsubst. If you need to access an environment
variable use the Jinja2 `env` function; see the section "Examples" below
for more information.


## Examples of use

Let the following be stored in the file `config.yaml.jinja2`:
```
---
nagios-hosts:
  {%- set nagios_servers = ['nagios01', 'nagios02', 'nagios-dev'] -%}
  {%- for server in nagios_servers %}
  - {{ server }}.example.com
  {%- endfor %}
send-timeout-secs: {{ env['TIMEOUT_SECS'] }}
```

Here is the result of running that file through `render-jinja2`:
```
$ export TIMEOUT_SECS=5
$ cat config.yaml.jinja2 | render-jinja2
---
nagios-hosts:
  - nagios01.example.com
  - nagios02.example.com
  - nagios-dev.example.com
send-timeout-secs: 5
```


[1]: https://jinja.palletsprojects.com/en/3.1.x/
