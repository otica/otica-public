[[_TOC_]]

# Populate file with Vault secrets (`vault2text.sh`)

## Overview

The `vault2text.sh` filter replaces specially formatted strings with secrets
from Vault. This is especially useful when rendering [Kubernetes secrets
file](../../make-modules/kube-sec/).

The `vault2text.sh` filter is almost the same as
[`vault2kube.sh`](../vault2kube/) except that whereas `vault2kube.sh`
outputs Vault secrets Base64-encoded, `vault2text.sh` outputs the Vault
secret unencoded.

## Base64-encoding

The `vault2text.sh` filter does *not* do a Base64-encoding of the Vault
secret before outputting it. If you need the Vault secret Base64-encoded,
use the [`vault2text.sh`](../vault2text/) script instead.

## Environment variable expansion

The `vault2text.sh` filter does not, on its own, expand environment variables
in the file it is transforming. However, if you put `#!vault2text` at the
top of a file and run that file through [`render2.sh`](../render2/) you
_will_ have the environment variables expanded.

In other words, if the file `file.txt` has `#!vault2text` as its first
line, these two commands will produce the same output:
```
$ cat file.txt | envsubst | vault2text.sh | tail --lines=+2
$ cat file.txt | render2.sh
```

## Example of use

Here is a file:
```
My secret is [%%secret/projects/mygroup/key%%]
```

The double-percentage signs tell `vault2text.sh` to take the string
between the pair of double-percentage signes, look up that string in
Vault, and replace the entire string with the value from Vault. So,
`vault2text.sh` will look up the secret stored at the Vault path
`secret/projects/mygroup/key` and put that secret between the square
brackets.

Remember that `vault2text.sh` does _not_ Base64-encode the string before
outputting.
