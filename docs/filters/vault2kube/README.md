[[_TOC_]]

# Populate file with Vault secrets (`vault2kube.sh`)

## Overview

The `vault2kube.sh` filter replaces specially formatted strings with secrets
from Vault. This is especially useful when rendering [Kubernetes secrets
file](../../make-modules/kube-sec/).

**Best Practice** You can use [`render2.sh`](../render2/) to call the
`vault2kube.sh` filter by putting `#!vault2kube` at the top of the file.
However, do not do this with the Kubernetes secrets file managed by
[`kube-sec.mk`](../../make-modules/kube-sec/) as `kube-sec.mk` ignores
all `#!`-strings.

## Base64-encoding

The `vault2kube.sh` filter does a Base64-encoding of the Vault secret
before outputting it. If you need the Vault secret as stored and not
Base64-encoded, use the [`vault2text.sh`](../vault2text/) script instead.

## Environment variable expansion

The `vault2kube.sh` filter does not, on its own, expand environment variables
in the file it is transforming. However, if you put `#!vault2kube` at the
top of a file and run that file through [`render2.sh`](../render2/) you
_will_ have the environment variables expanded.

In other words, if the file `file.txt` has `#!vault2kube` as its first
line, these two commands will produce the same output:
```
$ cat file.txt | envsubst | vault2kube.sh | tail --lines=+2
$ cat file.txt | render2.sh
```

## Example of use

Here is a Kubernetes secrets file:
```
---
apiVersion: v1
kind: Secret
metadata:
  namespace: webserver-dev
  name: webserver
  labels:
    app: main
type: Opaque
data:
  htpasswd: %%secret/projects/my-project/webserver/htpasswd%%
```

Note the line containing
`%%secret/projects/my-project/webserver/httpasswd%%`. The
double-percentage signs tell `vault2kube.sh` to take the string between, look
up that string in Vault, and replace the entire string with the value from
Vault. So, in this example `vault2kube.sh` will look up the secret stored at
the Vault path `secret/projects/my-project/webserver/httpasswd` and put
that secret after the colon in `htpasswd:`.
