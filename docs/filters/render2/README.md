[[_TOC_]]

# The `render2.sh` script

## Overview

The script `scripts/render2.sh` is the primary filter script and is the one
used most often. It reads text from standard input and (potentially) transforms that text.
How it transforms is determined by the first line of the input. If the
first line contains a recognized `#!`-string it will send the rest of the
input through another filter script.

1. If the first line is not a recognized `#!`-string then the output of
`render2.sh` will be simply the text itself, that is, the text is not
changed.

1. If the first line _is_ a recognized `#!`-string then the first line of
the input is removed from the output; the rest of the text is then sent
through an auxiliary filter determined by the `#!`-string.

## Usage

```
# normal mode:
$ cat file.txt | render2.sh [--no-envsubst] > file-filtered.txt

# legacy mode:
$ export RENDER_LEGACY_MODE=YES
$ cat file.txt | render2.sh [--no-envsubst] > file-filtered.txt
```

The option `--no-envsubst` *disables* environment variable substitution no
matter what `#!`-string is at the top of the file.

Setting the environment variable `RENDER_LEGACY_MODE` to "YES" enables
legacy mode; see ["Legacy mode"](#legacy-mode) for more detail on this
mode.

## Example: `envsubst`

If the file `test.txt` contains the text

        #!envsubst
        My path is $PATH

then

        $ cat test.txt | render2.sh

will cause the contents of `test.txt` to be run through `envsubst` and the
output will be something like

        My path is /usr/local/bin:/usr/bin:/bin:/usr/bin/X11

Note that the first line of the input (`#!envsubst`) does not appear in
the output.

## Environment variable substitution

Depending on which `#!`-string is used, environment variables in the
file being transformed may, or may not, be expanded. See [the table below](#recognized-strings)
to see which `#!`-strings cause environment variables to be expanded.

If you want to disable environment variable substitution _completely_, even when the
top line is `#!envsubst`, use the `--no-envsubst` option.

## Upgrade Note

The `render2.sh` script is based on the `render.sh` script and is
intended to be used in its place. The main difference between `render2.sh`
and `render.sh` is how the two scripts treat files without a `#!`-string
header. The older `render.sh` would expand the environment variables in a
file even if the file did not have any `#!`-string. However, we have
learned that it is better practice to leave a file untouched when
rendering unless there is an explicit `#!`-string indicating what the
transformation should be. Thus, `render2.sh` does precisely that.

If you are still using `render.sh` you should switch to `render2.sh`. Of
course, keep in mind that any files that need to have their environment
variables expanded must now have an explicit `#!envsubst` as their first
line.

If you _need_ `render2.sh` to behave like `render.sh` with regards to
environment variable expansion in the absence of a recognized `#!`-string,
use legacy mode. See section ["Legacy mode"](#legacy-mode) below for more
information.

## Recognized `#!`-strings

As mentioned above, the `render2.sh` script looks at the first line
of the input text for a `#!`-string it recognizes. Here are all of the
recognized `#!`-strings and what they do.

| Name              | Description                                      | Expands environment variables?|
| ---               | ---                                              |  ---                          |
|`#!envsubst`       | replace environment variables with their values  | YES|
|`#!gomplate`       | render a [gomplate template][3]                  | NO |
|`#!gomplate-vault` | render a [gomplate template][3] and replace Vault secrets | NO |
|`#!vault2kube`     | insert Vault secrets (Base64-encoded)            | YES|
|`#!vault2text`     | insert Vault secrets (not Base64-encoded)        | YES|
|`#!vault2chart`    | synonym for `#!vault2text`                       | YES|
|`#!render-jinja2`  | render a [Jinja2][2] template                    | NO |

Note 1. The `render2.sh` script will leave a file untouched if does not have
a `#!`-string as the first line, or it the `#!`-string is not one that it
recognizes.

Note 2. Using the `--no-envsubst` option disables environment variable expansion
completely.

## Legacy mode

As mentioned in the ["Upgrade Note"](#upgrade-note) `render2.sh` behaves differently from
the original `render.sh` when there is no recognized `#!`-string at the top of the file.
If you _want_ `render2.sh` to act like `render.sh` in this circumstance set the
environment variable `RENDER_LEGACY_MODE` to the string "YES".

Example. Assume that the file `file.txt` has only one line:
```
The PATH environment variable is $PATH.
```

Here is what happens when we run `file.txt` through `render2.sh` in
non-legacy-mode and then in legacy mode:
```
$ cat file.txt | render2.sh
The PATH environment variable is $PATH.
$ cat file.txt | RENDER_LEGACY_MODE=YES render2.sh
The PATH environment variable is /usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/sbin:/sbin.
```

Note. Using `--no-envsubst` in legacy mode will result in a non-zero exit.

## Detail on each subfilter

### Subfilter: `#!envsubst`

The `#!envsubst` subfilter will simply run the file through the
[envsubst][1] environment variable substitution script.

### Subfilter: `#!gomplate`

Runs the file through the `gomplate` template renderer. For more information,
see [gomplate's documentation pages][3].

### Subfilter: `#!gomplate-vault`

This subfilter is similar to `#!gomplate` in that it runs the file through
the `gomplate` template renderer, but in addition it will also replace
Vault secrets as long as the secret is referred to using the `vault-kv`
gomplate template identifier. This is most easily explained with an
example.

Assume you have a file with this content:
```
#!gomplate-vault
my secret "{{ tmpl.Exec  "vault-kv"  "secret/projects/mygroup/key"}}"
```

If the Vault path `secret/projects/mygroup/key` contains the value `a_special_secret`, then
running the above file through `render2.sh` will result in the output

```
#!gomplate-vault
my secret base64 encoded "{{ tmpl.Exec  "vault-kv"  "a_special_secret"}}"
```

Note that if the Vault secret is stored in Base64 format the
`#!gomplate-vault` filter will do a Base64 decode before outputting.

### Subfilter: `#!vault2kube`

Runs the file through `envsubst` and then through `vault2kube.sh`.
For more information, see the [`vault2kube.sh` documentation](../vault2kube/).

### Subfilter: `#!vault2text`

Runs the file through `envsubst` and then through `vault2text.sh`. For
more information, see the [`vault2text.sh` documentation](../vault2text/).

### Subfilter: `#!render-jinja2`

The `#!render-jinja2` filter renders your page as a [Jinja2][2] template using the
`render-jinja2` script.
For more information, see the [`render-jinja2` documentation](../render-jinja2).


[1]: https://linux.die.net/man/1/envsubst

[2]: https://jinja.palletsprojects.com/en/3.1.x/

[3]: https://docs.gomplate.ca/
