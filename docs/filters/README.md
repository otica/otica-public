# Filters

Otica "filters" are scripts that you run files through to change the file
in some manner. The filters send to standard output what was supplied via
standard input but with specific changes. For example, the filter
`envsubst` replaces environment variables with their values.

* [`render2.sh`](render2/): the primary filter script. This script can be used
to call other filter scripts. It is a best practice to put a "hash-bang"
(`#!`) string at the top of any file to be transformed and then use
`render2.sh` to transform it.

* [`envsubst`][1]: replace variable files with their values.

* [`vault2kube.sh`](vault2kube): populate file with Vault secrets.

* [`vault2text`](vault2text): insert Vault secrets (base64 decoded)

* [`render-jinja2`](render-jinja2/): render a [Jinja2][2] template.

**Best Practice** Rather than using one of the filters directly it is a
best practice to put a `#!`-string at the top of any file to be
transformed and then use [`render2.sh`](render2/) to transform it.

[1]: https://linux.die.net/man/1/envsubst

[2]: https://jinja.palletsprojects.com/en/3.1.x/

