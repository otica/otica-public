# Otica Documentation

* [Basics](basics/)

* [Installation](tutorials/installing/)

* [Tutorials](tutorials/)

* [Configuration](configuration/)

* [Secrets](secrets/)

* [Filters](filters/)

* [Make Modules](make-modules/)

* [Script Man Pages](manpages/)

* [CI/CD Suppport](cicd/)

* [FAQ](faq/)

