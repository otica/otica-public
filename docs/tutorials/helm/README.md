# Setting up and using an Otica Helm project

1. If not already installed, [install Otica](../installing). Change into
the `sub-projects/` directory.

        $ cd my-otica
        $ cd sub-projects

1. We will assume that the Helm subproject is called `myapp` and that we have one
project environment `dev`.

1. Create a new directory. We typically prefix directory names with the
type of project. In this case we use `helm`:

        $ mkdir helm-myapp

1. Change into the new directory:

        $ cd helm-myapp

1. Initilize this directory as an Otica Helm subproject:

        $ otica create subproject generic --subtype=helm
        creating common/ directory
        Creating common/helm
        initialized Helm chart in common/helm
        wrote otica.yaml
        creating ./common/env_variables/ directory
        creating ./common/makefile_parts/ directory
        creating ./common/scripts/ directory
        creating common/makefile.mk
        common/env_variables/framework.var does not exist, so copying
        copied file framework.var from your toplevel Otica project into common/env_variables/framework.var
        copied .gitignore

1. The previous command will have created the directory `common/helm`
where all your Helm files are kept and placed in it some "starter" Helm
files. You are free to change any or all of the files in `common/helm`.

1. The `otica.yaml` file just created will look something like this:

        ---
        platform: none

        environment:
          toplevel:
            # variable files in the environment:toplevel list are synced from the
            # toplevel project's common/env_variables/ directory into the
            # subproject's common/env_variables directory.
            - framework.var
          common:
            # variable files listed here are included from the
            # common/env_variables directory in the current subproject.
            - common.var
          local:
            # variable files listed here are included from the
            # directory running the make command.
            - local.var

        makefile_parts:
          otica:
            # makefile modules listed here are included from the Otica
            # source repository.
            - framework.mk
            - authenticate.mk
            - vault.mk
            - helm-develop.mk
          toplevel:
            # makefile modules listed here are included from the toplevel's
            # common/makefile_parts directory.
            # - project.mk
          common:
            # makefile modules listed here are included from the current
            # subproject's common/makefile_parts directory.
            # - app.mk

1. Make the `dev` project environment directory:

        $ otica create environment --name dev
        copied dev/Makefile
        created new environment directory 'dev'

1. Change into the `dev` directory.

        $ cd dev

1. Type `make` and look at the output:

        $ make

1. As part of the output you should see something like this:

        -------------------------------------------------------------------------------
        helm-develop.mk (~/bin/otica/makefile_parts/helm-develop.mk)
        -------------------------------------------------------------------------------
        dhelm-clean                    remove package tar files
        dhelm-gcs-init                 initialize the GCS Helm plugin
        dhelm-gcs-install              install the GCS Helm plugin
        dhelm-gcs-uninstall            remove the GCS Helm plugin
        dhelm-gcs-update               update the GCS Helm plugin
        dhelm-help                     show help for this module
        dhelm-package                  package the Helm chart into a tar-ball
        dhelm-push                     upload the Helm package to its GCS repository
        dhelm-render-templates         display templates with values substituted in
        dhelm-repo-add                 add the Helm repository
        dhelm-repo-list                list all the Helm repositories
        dhelm-show-env                 show the Helm make module environment variables

1. The above are all the `helm-develop.mk` make targets. Let's run `make helm-show-env` to
look at the Helm-specific variables:

        $ make dhelm-show-env
        pass: HELM_BUILD_DIR is set ("../common/helm")
        pass: HELM_GCS_BUCKET_PATH is set ("gs://helm-charts-public")
        pass: HELM_REPO_NAME is set ("iedo-helm-public")
        error: HELM_CHART_NAME is not set
        make: *** [/srv/scratch/adamhl/gcp/bin/otica/makefile_parts/helm-develop.mk:42: dhelm-show-env] Error 1

1. Note the error concerting `HELM_CHART_NAME`. This is because the Otica
`helm-develop.mk` make module needs to know the name of your Helm chart.
Set this in `common/env_variables/common.var`:

        # common/env_variables/common.var
        export HELM_CHART_NAME=myapp

1. Now run `make helm-show-env` again:

        $ make dhelm-show-env
        pass: HELM_BUILD_DIR is set ("../common/helm")
        pass: HELM_GCS_BUCKET_PATH is set ("gs://helm-charts-public")
        pass: HELM_REPO_NAME is set ("iedo-helm-public")
        pass: HELM_CHART_NAME is set ("myapp")

1. To create a tar-ball of your Helm chart run `make dhelm-package`. The
tar-ball will be put in the directory where you ran the make command.

1. To push the Helm chart to the repository run `make dhelm-push`.

1. In this tutorial we are assuming that the Helm chart is being pushed to
a Google Cloud Storage bucket. For more instructions on how to do this as
well as other configuration options, see the page ["Developing with
Helm"](../../make-modules/helm-develop) that documents the
`helm-develop.mk` make module.

