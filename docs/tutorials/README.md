# Tutorials

* [Installing Otica](installing/)

* [Setting up a **top-level** Otica project](toplevel/)

* [Setting up and using an Otica **Docker** subproject](docker/)

* [Setting up and using an Otica **Kubernetes** project](kube/)

* [Setting up and using an Otica **Helm** project](helm/)

* [Setting up and using an Otica **Serverless** project](serverless/)

* [Setting up and using an Otica **Generic** subproject](generic/)
