# Setting up and using a Otica Docker project

1. If not already installed, [install Otica](../installing). Change into
the `sub-projects/` directory.

        $ cd my-otica
        $ cd sub-projects

1. We will assume that the Docker subproject is called `myapp` and that we have one
project environment `dev`.

1. Create a new directory. We typically prefix directory names with the
type of project. In this case we use `docker`:

        $ mkdir docker-myapp

1. Change into the new directory:

        $ cd docker-myapp

1. Initilize this directory as an Otica Docker subproject:

        $ otica create subproject generic --subtype=docker
        creating common/docker/ directory
        writing a default Dockerfile for you at common/docker/Dockerfile
        wrote otica.yaml
        creating ./common/env_variables/ directory
        creating ./common/makefile_parts/ directory
        creating ./common/scripts/ directory
        creating common/makefile.mk
        common/framework.var does not exist, so copying
        copied file framework.var from your toplevel Otica project into common/framework.var
        copied .gitignore

1. The `otica.yaml` file created will look something like this:

        ---

        # Explanation of the different sections.
        #
        # environment:
        #   toplevel:
        #     # variable files in the environment:toplevel list are synced from the
        #     # toplevel project's common/env_variables/ directory into the
        #     # subproject's common/env_variables directory.
        #     - framework.var
        #   common:
        #     # variable files listed here are included from the
        #     # common/env_variables directory in the current subproject.
        #     - common.var
        #   local:
        #     # variable files listed here are included from the
        #     # directory running the make command.
        #     - local.var
        #
        # makefile_parts:
        #   otica:
        #     # makefile modules listed here are included from the Otica
        #     # source repository.
        #     - framework.mk
        #     - vault.mk
        #     - authenticate.mk
        #   toplevel:
        #     # makefile modules listed here are included from the toplevel's
        #     # common/makefile_parts directory.
        #     # - project.mk
        #   common:
        #     # makefile modules listed here are included from the current
        #     # subproject's common/makefile_parts directory.
        #     # - app.mk

        environment:
          toplevel:
          - framework.var
          common:
          - common.var
          local:
          - local.var
        makefile_parts:
          otica:
          - framework.mk
          - vault.mk
          - authenticate.mk
          - docker-develop.mk
          toplevel: []
          common: []


1. Make the `dev` project environment directory:

        $ otica create environment --name dev
        copied dev/Makefile
        created new environment directory 'dev'

1. Change into the `dev` directory.

        $ cd dev

1. Type `make` and look at the output:

        $ make

1. As part of the output you should see something like this:

        -------------------------------------------------------------------------------
        docker-develop.mk (~/bin/otica/makefile_parts/docker-develop.mk)
        -------------------------------------------------------------------------------
        docker-build-nocache           build Docker image (does NOT use Docker cache)
        docker-kaniko-script           generate Kaniko build script
        docker-build                   build Docker image (uses Docker cache)
        docker-check                   check that needed environment variables are set
        docker-help                    show help for this module
        docker-login                   login to private docker registry
        docker-push-dryrun             tag image the tags in DOCKER_IMAGE_TAGS and push (DRY RUN)
        docker-push                    tag image the tags in DOCKER_IMAGE_TAGS and push
        docker-show-env                show docker-relevant environment variables


1. The above are all the `docker-develop` make targets. Let's run `docker-check` to
see if we are missing any needed variables:

        $ make docker-check
        ERROR: required variable DOCKER_REGISTRY is not set

1. You should see an error. The error says that the `DOCKER_REGISTRY`
variable is not set. Let's look at all the `DOCKER_*` variables:

        $ make docker-show-env
        pass: DOCKER_SOURCE_DIR              ("../common/docker")
        FAIL: DOCKER_REGISTRY                (<NOT SET>)
        pass: DOCKER_IMAGE                   ("myapp")
        pass: DOCKER_NAMESPACE               ("")
        FAIL: DOCKER_REGISTRY_USERNAME       (<NOT SET>)
        FAIL: DOCKER_REGISTRY_PASSWORD_PATH  (<NOT SET>)
        FAIL: DOCKER_IMAGE_TAG               (<NOT SET>)
        FAIL: DOCKER_IMAGE_TAGS              (<NOT SET>)

1. In the `make docker-show-env` any variable with a `FAIL` next to it is
_required_ and must be set. Several of these variables will be set in a
cloud variable file (like `gcp.var`) managed at the top-level so that they
can be used by all of your Otica Docker projects.

1. To set these missing variables you will need to know the URL of the
Docker repository you are pushing to. You will also need a username and
password for an account that allows you to push to this repository. The
password must be stored in Vault. You set `DOCKER_REGISTRY_PASSWORD_PATH`
to this Vault path of that passoword. Once you have that setup you can
move to the next step.

1. The `DOCKER_IMAGE` variable should be set to the name of your Docker
image. For this tutorial we chose to call it `my-app`.

1. For this tutorial we will assume your Docker registry is in Google
Cloud Platform (GCP). Now that you have the credentials provisioned you
can set your variables. Several of the variables will be the same for all
of your Docker projects, so let's set them at the top-level.

1. Change directories so that you are the `common/env-variables/` directory of your
top-level Otica project. Edit the file `gcp.var` and add these lines
(putting in the appropriate values for your environment):

        export GCP_PROJECT_ID=my-project-id
        export DOCKER_NAMESPACE=${GCP_PROJECT_ID}
        export DOCKER_REGISTRY=gcr.io
        export DOCKER_REGISTRY_USERNAME=<the username of your Docker repo account>
        export DOCKER_REGISTRY_PASSWORD_PATH=<the path to your Docker repo account password>

1. Of course, replace `my-project-id` with your actual GCP project name.

1. Chagne directory back into your `docker-myapp` subproject.

1. Configure the required DOCKER environment variables in `common/common.var`:

        export DOCKER_IMAGE=myapp

1. Go back to `dev/` and run `make docker-check` again:

        $ make docker-check

1. No errors should have appeared. Let's look at all the relevant
`DOCKER_*` variables:

        $ make docker-show-env
        pass: DOCKER_SOURCE_DIR              ("../common/docker")
        pass: DOCKER_REGISTRY                ("gcr.io")
        pass: DOCKER_IMAGE                   ("myapp")
        pass: DOCKER_NAMESPACE               ("uit-et-iedo-services")
        pass: DOCKER_REGISTRY_USERNAME       ("_json_key")
        pass: DOCKER_REGISTRY_PASSWORD_PATH  ("secret/projects/et-iedo/uit-et-iedo-services/common/gcr-key")
        pass: DOCKER_IMAGE_TAG               (<NOT SET>)
        pass: DOCKER_IMAGE_TAGS              (<NOT SET>)

1. Configure the Docker tags in the `dev/local.var` directory (use
whatever tags you want):

        export DOCKER_TAGS=dev,0.1

1. When ready to build go back to the `dev` project environment directory:

        $ cd dev/
        $ make docker-build

1. Push to GCP:

        $ make docker-push
