# Setting up and using a **generic** Otica subproject

1. These instructions are for setting up a generic Otica subproject, that
is, one that does not use any of the specialized make modules.

1. If not already installed, [install Otica](../installing).

1. If not already done so [set up your toplevel Otica
project](../otica-project).

1. Change directory into your Otica project's `sub-projects`
directory. Create and change into a new directory to hold your new
subproject.

        $ cd sub-projects
        $ mkdir new-sub-project
        $ cd new-sub-project

1. When creating a new subproject you need to specify a _platform_

1. Create the support files needed for the new subproject:

        $ otica create subproject generic
        copied subproject.yaml.template
        creating 'common/' directory
        creating 'common/makefile_parts/' directory
        creating common/makefile.mk
        copied common/framework-env.sh
        ...

1. Turn this directory into a Git repository

        $ git init .
        $ git add .
        $ git commit -m "initial commit"

1. Normally you need at least one environment. Let's assume you want one
called `dev`. Create it now.

        $ otica create environement
        be sure to RENAME new-env to something more appropriate
        $ mv new-env dev
        $ ls -l dev
        total 0
        lrwxrwxrwx 1 joeuser root 21 Aug 26 14:38 Makefile -> ../common/makefile.mk
        -rw-r--r-- 1 joeuser root  0 Aug 26 14:38 env.sh

1. Add the new environment directory to Git and commit.

        $ git add dev/
        $ git commit -m "add dev/ environment"

1. Change into the `dev` environment and run `make` to see what you can do.

        $ cd dev
        $ make
