# Setting up and using a Kubernetes project

1. If not already installed, [install Otica](../installing). Change into
the `sub-projects/` directory.

        $ cd my-otica
        $ cd sub-projects

1. We will assume that the Kubernetes subproject is called `myapp` and that we have one
project environment `dev`.

1. Create a new directory. We typically prefix directory names with the
type of project. In this case we use `kube`:

        $ mkdir kube-myapp

1. Change into the new directory:

        $ cd kube-myapp

1. When creating a new subproject you must specify a "platform". To
see your available platforms use the "platforms" otica subcommand:

        $ otica platforms
        gcp,aws,generic (sample output)

1. We assume that you there is already a "gcp" platform setup in the
top-level directory. If not you can replace "gcp" with "generic" in the
next step.

1. Initilize this directory as an Otica subproject:

        $ otica create subproject gcp
        creating common/ directory
        wrote otica.yaml
        creating ./common/env_variables/ directory
        creating ./common/makefile_parts/ directory
        creating ./common/scripts/ directory
        creating common/makefile.mk
        common/env_variables/framework.var does not exist, so copying
        copied file framework.var from your toplevel Otica project into common/env_variables/framework.var
        common/env_variables/gcp.var does not exist, so copying
        copied file gcp.var from your toplevel Otica project into common/env_variables/gcp.var
        copied .gitignore
        copied ./common/env_variables/common.var

1. The `otica.yaml` file created will look something like this:

        ---

        # Explanation of the different sections.
        #
        # environment:
        #   toplevel:
        #     # variable files in the environment:toplevel list are synced from the
        #     # toplevel project's common/env_variables/ directory into the
        #     # subproject's common/env_variables directory.
        #     - framework.var
        #   common:
        #     # variable files listed here are included from the
        #     # common/env_variables directory in the current subproject.
        #     - common.var
        #   local:
        #     # variable files listed here are included from the
        #     # directory running the make command.
        #     - local.var
        #
        # makefile_parts:
        #   otica:
        #     # makefile modules listed here are included from the Otica
        #     # source repository.
        #     - framework.mk
        #     - vault.mk
        #     - authenticate.mk
        #   toplevel:
        #     # makefile modules listed here are included from the toplevel's
        #     # common/makefile_parts directory.
        #     # - project.mk
        #   common:
        #     # makefile modules listed here are included from the current
        #     # subproject's common/makefile_parts directory.
        #     # - app.mk

        environment:
          toplevel:
          - framework.var
          - gcp-uit-et-iedo-services.var
          common:
          - common.var
          local:
          - local.var
        makefile_parts:
          otica:
          - framework.mk
          - vault.mk
          - authenticate.mk
          toplevel: []
          common: []

1. As this is a Kubernetes project add the `kube.mk` make module to Otica; change
`otica.yaml` to look like this:

        ---

        environment:
          toplevel:
          - framework.var
          - gcp-uit-et-iedo-services.var
          common:
          - common.var
          local:
          - local.var
        makefile_parts:
          otica:
          - framework.mk
          - vault.mk
          - authenticate.mk
          - kube.mk
          toplevel: []
          common: []

1. Whenever you change `otica.yaml` you must run `otica udpate`:

        $ otica update
        updating common/makefile.mk

1. Make the `dev` project environment directory:

        $ otica create environment --name dev
        copied dev/Makefile
        copied dev/local.var
        setup new environment directory 'dev'

1. Change into the `dev` directory.

        $ cd dev

1. Type `make` and look at the output:

        $ make

1. You will see this *error*:

        missing env var(s):

         KUBE_NAMESPACE

1. To fix this error edit the file `common/env-variables/common.var` and
add a line like the following (substitute your namespace for the one
shown):

        KUBE_NAMESPACE=my-namespace

1. Type `make` again. This time there should be no error.

1. As part of the `make` output you should see something like this:

        -------------------------------------------------------------------------------
        kube.mk (~/bin/otica/makefile_parts/kube.mk)
        -------------------------------------------------------------------------------
        kube-config                    authenticate and configure Kubernetes cluster
        kube-delete-failed-pods        delete all failed pods in the current namespace
        kube-images-get                list all images running in the cluster
        kube-namespace-create          kube create namespace using KUBE_NAMESPACE
        kube-namespace-show            show current Kubernetes namespace
        kube-show-env                  show Kubernetes environment variables
        kube-show-resources            show all Kubernetes resources in the current namespace


