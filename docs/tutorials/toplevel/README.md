# Setting up a top-level Otica project

This tutorial goes through the setting up of a top-level Otica
project. This top-level project is the one containing the `repos.yaml`
file and the `sub-projects` directory.  (For more on the differences
among "top-level projects", "subprojects", "environments", etc., see the
"Vocabulary" section of the ["Otica: the basics" page](../../basics).)

1. Create an empty directory where your Otica top-level project will live.

        $ mkdir my-otica

1. Change directory into this new directory.

        $ cd my-otica

1. Create the skeleton files needed (note: the `otica` program is in the
`bin/` directory of the Otica repository):

        $ otica create toplevel

1. You should see several new files and directories including `Makefile`,
`sub-projects/`, `common/`, and `repos.yaml`.

1. Initialize this directory as a Git repository.

        $ git init .
        $ git add .
        $ git commit -m "initial commit"

1. Test that `make` works. You should see several make targets displayed.

        $ make

1. One of the make targets is `check-tools`. To run this target type

        $ make check-tools

1. Any errors displayed  means some of the required software is not
installed.

1. Let's look at the `common/env_variables/` directory. This directory is where
project-wide configuration settings are stored. If you do an `ls common/env_variables` you
will see some files including `framework.var`, `gcp.var.example` and
`aws.var.example`. These files are provided as a guide for configuring GCP
and AWS resources in your subprojects. The file `framework.var` will be
synced to each of your subprojects.

1. Your top-level Otica project needs to know where the Otica source code is
installed. If you installed it in `${HOME}/bin/otica` then there is
nothing more to do. If you did not you need to edit `framework.var` and
change the setting for `FRAMEWORK_DIR` to point to the directory where
Otica is installed.

1. This new top-level project comes with a single platform called
`generic`. You may want to add more platforms. See section ["Templating
the otica.yaml"](../../configuration/otica-yaml/README.md) on how to add
more platforms.

1. You are now ready to add a subproject; for an example of this, see
["Setting up and using a **generic** Otica subproject"](../generic).



