[[_TOC_]]

# Installing Otica

A system supports Otica if it can run the [`bin/check-tools`
script](../../manpages/check-tools) without error. The following systems
are known to be able to run Otica (albeit with some of the software
components that Otica requires installed manually):

* Debian "bullseye"

* MacOS "Big Sur" and later (newer versions of required software can be
  installed using Homebrew)


## Prereqisites

The following software is *required* to run Otica:

* git

* bash

* [envsubst][2]

* [GNU Make][5]

* Python 3 (version 3.8 or later)

* The following Python 3 modules:

  * shutil
  * hashlib
  * yaml (PyYAML; version 5.x or later)
  * jinja2


The following software is optional but _is_ required for
certain of the Otica [make modules](../../make-modules/):

* [gcloud][8] (needed when working with Google Cloud Platform)

* [kubectl][3] (needed when using Kubernetes)

* [vault][4]  (needed when storing secrets in Hashicorp's Vault)

* [jq][1] (needed by various modules for parsing JSON files)

* [yq][6] (needed by various modules for parsing YAML files)

If [pandoc][7] is installed it will be used to display Markdown
documentation.

## Installation

1. Ensure that you have, at mimimum, the required software installed (see
"Prerequisites" above for more information). Hint: it is a good idea to install
the optional software as well. You can also check for the required software
by running the program `check-tools` provided in Otica's `bin/` directory.

1. Clone Otica's git repository into a local directory. While you can do
this clone into any directory, Otica assumes that you have installed it in
`${HOME}/bin`. If not, you will have to make one small configuration
change when you [set up the top-level Otica project](../toplevel).

1. Let's assume that you are currently in the directory `/home/joeuser/bin`.

        /home/joeuser> git clone <Otica Git URL> otica

1. Verify that you have the minimum necessary software installed. Change into
the directory where you just cloned Otica and run `check-tools`:

        $ cd otica
        $ bin/check-tools -v

1. Set up a top-level Otica project by following the instructions on
the ["Setting up a top-level Otica project"](../toplevel) page.


[1]: https://stedolan.github.io/jq/

[2]: https://linux.die.net/man/1/envsubst

[3]: https://kubernetes.io/docs/reference/kubectl/overview/

[4]: https://www.vaultproject.io/

[5]: https://www.gnu.org/software/make/

[6]: https://github.com/mikefarah/yq

[7]: https://pandoc.org/

[8]: https://cloud.google.com/sdk/gcloud
