# Frequently Asked Questions

## How do you pronounce "Otica"?

Otica comes from the Portugese "ótica" (optics/optical) with the accent on the
first syllable.

## How do I enable "debug" or "verbose" mode?

Set the environment variable `DEBUG` to a non-empty value.
