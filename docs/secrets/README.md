[[_TOC_]]

# Otica and Secrets

## Overview

Otica integrates with two secret managers: [Hashicorp's Vault][2] and the
[Kubernetes native Secrets manager][3].

For best results _please_ follow these two rules:

**Rule 1**: Write secrets to Vault using
[`vault-write.sh`](../manpages/vault-write.sh/) and read them using
[`vault-read.sh`](../manpages/vault-read.sh/).

**Rule 2**: Save Kuberenetes Secrets using the [`kube-sec.mk`
make-module](../make-modules/kube-sec/).


## Kubernertes Secrets and Base64 encoding

When using Otica it is recommended that you store Kubernetes secrets in the
`data` field. Secrets stored in the `data` field must be Base64-encoded, but if
you use the `vault2kube.sh` filter Base64-encoding is handled for you.
Even better is to use the [`kube-sec.mk`
make-module](../make-modules/kube-sec/) which uses the `vault2kube.sh`
script.

## Vault and Base64 encoding

It is best practice when storing binary data to Vault to Base64-encode the
data before storing it. The sister scripts `vault-write.sh` and
`vault-read.sh` take care of Base64-encoding and -decoding for you, so it is recommended
that you always use these two scripts when reading and writing secrets to
Vault.

### Technical details

When `vault-write.sh` is writing to Vault it first checks to see if the
data being written is in binary format. If it is, `vault-write.sh`
Base64-encodes it and then writes the result to Vault. In addition,
`vault-write.sh` adds a key-value pair to the Vault secret. This pair is
either "(format, text)" or "(format, base64)", depending on whether
`vault-write.sh` Base64-encoded the data or not. This key-value pair
enables `vault-read.sh` to know whether to Base64-decode the Vault secret.


[1]: https://kubernetes.io/docs/concepts/configuration/secret/

[2]: https://www.vaultproject.io/

[3]: https://kubernetes.io/docs/concepts/configuration/secret/
