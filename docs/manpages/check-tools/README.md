# NAME

check-tools - check that software Otica needs is installed

# SYNOPSIS

check-tools

check-tools --minimal

check-tools --verbose

check-tools --help

# DESCRIPTION

**check-tools** verifies that the software needed to run Otica is
installed. It exits with code 0 if software required is installed,
non-zero if something is missing.

The **check-tools** script checks for all the software components that
might get used by part of the Otica framework. Thus it may complain that
some excutable is missing, but if you know that you are not going to use
that part of Otica you can ignore that warning. For example,
**check-tools** looks for the `kubectl` executable and issues a warning if
it is missing. If you only want to verify that the software needed to run
the **otica** script is installed use the **--minimal** option.


# OPTIONS

- **--minimal**

    Will check only for those software componenets necessary to run the **otica** program.

- **--help**

    Show the help screen.

- **--verbose**

    Show extra information while running.

# EXIT STATUS

Returns `0` on success, `1` on any failure.

# LICENSE AND COPYRIGHT

The MIT License (MIT)

Copyright (c) 2022 University IT, Stanford University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

