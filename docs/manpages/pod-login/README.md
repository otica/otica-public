# NAME

pod-login -- Easy "login" to a running Pod's container

# USAGE

**pod-login**

# DESCRIPTION

The **pod-login** script makes it quick and easy to start an interactive
shell on a running Pod's container. Normally you would have to find the name
of a running Pod and then type something like this:

    kubectl exec -ti pod-name -c container-name -- bash

By setting some appropriate environment variables, instead of the above you
can simply type **pod-login** (or **make ssh** if using the **kube.mk** Make
module).

# HOW IT WORKS

There are four environment variables that **pod-login** uses:

    KUBE_LOGIN_POD_NAME
    KUBE_LOGIN_CONTAINER_NAME
    KUBE_LOGIN_SHELL
    NODE

**pod-login** constructs a list of all running Pods whose name
contains `KUBE_LOGIN_POD_NAME` as a substring, chooses one of the
Pods from that list, and finally runs

    kubectl exec -ti -c $KUBE_LOGIN_CONTAINER_NAME podname -- $KUBE_LOGIN_SHELL

If there are more than one running Pod that contains the string
`KUBE_LOGIN_POD_NAME` it chooses the `NODE`+1'st Pod. If `NODE` is not
defined (or is not a non-negative integer), then **pod-login** chooses the
first Pod.

If no Pods match, **pod-login** exits with an error.

If the Pod chosen does not have a running container containing
`KUBE_LOGIN_CONTAINER_NAME` as a substring the **kubectl** command will
fail.

# ENVIRONMENT VARIABLES

- `KUBE_LOGIN_POD_NAME`

    This string is used to find a running Pod. **pod-login** constructs a list
    all Pods containing this string as a substring. It then executes an
    interactive shell on one of the Pods in the list. (See also **NODE** for
    choosing a specific Pod in the list of running Pods.)

    If `KUBE_LOGIN_POD_NAME` is not set then the environment variable `APP`
    is used. If neither `KUBE_LOGIN_POD_NAME` nor `APP` is set then
    **pod-login** exits with an error.

- `KUBE_LOGIN_CONTAINER_NAME`

    When using "kubectl exec" you must supply the name of container. The
    **pod-login** script uses the environment variable
    `KUBE_LOGIN_CONTAINER_NAME` as the container name; **pod-login** will fail
    unless `KUBE_LOGIN_CONTAINER_NAME` is set.

- `KUBE_LOGIN_SHELL`

    When **pod-login** launches **kubectl exec** it must tell **kubectl** which
    executable to run. If `KUBE_LOGIN_SHELL` is defined then it is used as
    the executable. If `KUBE_LOGIN_SHELL` is not defined, **pod-login** uses
    **bash** as the default.

    Hint: set `KUBE_LOGIN_SHELL` to "ash" when connecting to Alpine-based
    containers.

- `NODE=n`

    **pod-login** picks one of the running Pods. If `NODE` is defined and is
    equal to a non-negative ineger `n` then **pod-login** will choose the
    `n`th matching Pod. This can be useful when you want to iterate through
    all the Pods.

    If you set `NODE` to an integer so that `NODE`+1 is larger than the
    number of Pods, **pod-login** exits with an error.

# EXAMPLE

Assume that your are in a Kubernetes namespace with the following
four Pods running:

    $ kubectl get pods
    NAME                      READY   STATUS    RESTARTS   AGE
    vemail-59dd6b875b-l2vnw   1/1     Running   0          11h
    vemail-59dd6b875b-p48fw   1/1     Running   0          11h
    vemail-59dd6b875b-sc9xj   1/1     Running   0          11h

Assume further that you want to login to the "web" container that runs on
each Pod. Set `KUBE_LOGIN_POD_NAME` equal to the string "vemail" and set
`KUBE_LOGIN_CONTAINER_NAME` equal to "web". Running **pod-login** will pick
one of the above Pods, e.g., `vemail-59dd6b875b-l2vnw` and run

    kubectl exec -ti vemail-59dd6b875b-l2vnw -c web -- bash

If you set `NODE=0` then **pod-login** does the same thing (since
`vemail-59dd6b875b-l2vnw` is the first Pod). Setting `NODE=2` will make
**pod-login** connect to `vemail-59dd6b875b-sc9xj`. Setting `NODE=3` will
generate an error since there is no fourth Pod.

# OPTIONS

There are no options.

# EXIT VALUE

Returns `0` on success, `1` on any failure.
