# NAME

vault-read.sh -- Read secrets from Hashicorp Vault

# USAGE

**vault-read.sh** _vault-path_

# DESCRIPTION

Given a _vault-path_ **vault-read.sh** reads the Vault secret at that path
and writes the results to standard out. The script gets the Vault address
from the environment variable `VAULT_ADDR`; if `VAULT_ADDR` is not
defined **vault-read.sh** will exit with an error.

# BASE64 ENCODING

The `vault-read.sh` script uses the "format" key-value pair written by
`vault-write.sh` and associated with the Vault secret to determine if the
value should be Base64-decoded or not. As such you should always use
`vault-write.sh` and `vault-read.sh` together: if a secret is written by
`vault-write.sh` use `vault-read.sh` to read it.

You _can_ use `vault-read.sh` to read a secret that was not written by
`vault-write.sh` but `vault-read.sh` will not attempt to Base64-decode the
result.

# OPTIONS

There are no options.

# EXIT VALUE

Returns `0` on success, `1` on any failure.
