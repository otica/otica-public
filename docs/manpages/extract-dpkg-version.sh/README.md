# NAME

extract-dkg-version.sh -- Extract the Debian package version from a changelog file

# USAGE

**extract-dkg-version.sh | tail -n1**

# DESCRIPTION

The **extract-dkg-version.sh** script clones the Git repository specified by the
`DEBIAN_PACKAGE_GIT_URL` environment variable and extracts the Debin
version from the file `debian/changelog`. It does this by running the
`dpkg-parsechangelog` command that is part of the
macrotex/debian-package:sid Docker image.

The output of this script is the Debian packages's version number. The
recommended way to run this script is **extract-dkg-version.sh | tail
-n1**; the last line output is the version number.

If `DEBIAN_PACKAGE_GIT_URL` is not defined or points to an inaccessible
repository this script will exit with an error.

If the cloned repository does not have the directory `debian/changelog` at
its root, this script will exit with an error.

# EXAMPLE

```
$ DEBIAN_PACKAGE_GIT_URL=https://github.com/macrotex/debian-native-package.git extract-dpkg-version.sh | tail -n1
1
```

# EXIT VALUE

Returns `0` on success, `1` on any failure.
