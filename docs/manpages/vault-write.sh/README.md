# NAME

vault-write.sh -- Write secrets into Hashicorp Vault

# USAGE

**vault-write.sh** _vault-path_ _secret_

**vault-write.sh** _vault-path_ _@filename_

# DESCRIPTION

Given a _vault-path_ **vault-write.sh** writes a secret into
Hashicorp's Vault. The script gets the Vault address from the environment
variable `VAULT_ADDR`; if `VAULT_ADDR` is not defined **vault-write.sh**
will exit with an error.

There are two ways you pass the secret to **vault-write.sh**. If your secret
is a text string you can pass it directly from the command line:

    $ vault-write.sh secret/projects/mygroup/password "p@ssW@rd"

The second method is to put the secret into a file. This method allows adding
both text and non-text passwords. Here is an example where we generate some
random binary characters and then add them to Vault:

    $ head -c32 </dev/urandom > random.bin
    $ vault-write.sh secret/projects/mygroup/password '@random.bin'

Note the leading `@`; this tells **vault-write.sh** that the data should be
read from the file `random.bin`.

# BASE64 ENCODING

Secrets added by **vault-write.sh** will be stored with an extra key-value pair.

If the secret added is detected to be binary then the secret is
Base64-encoded and the key-value pair "(format,base64)" is added along
with the data.

If the secret added is not binary then the secret is added without
transformation and the key-value pair "(format,text)" is added along with
the data.

It is best to use read Vault secrets stored with **vault-write.sh** with its
sister script **vault-read.sh**. This is because vault-read.sh knows to look
at the "format" key-value pair and correctly decode (if necessary) the
secret.

# OPTIONS

There are no options.

# EXIT VALUE

Returns `0` on success, `1` on any failure.
