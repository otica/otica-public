# NAME

pods-running-and-ready - return list of ready and running Kubernetes Pods

# SYNOPSIS

pods-running-and-ready [options]

pods-running-and-ready --help

# DESCRIPTION

The **pods-running-and-ready** script returns a list of ready and running
Pods in the current Kubernetes namespace. Some situations where this is
useful:

* you have a Deployment and you need to run a command on one of the
Deployment's Pod's containers

* you have a web service running in a Deployment and you need to
_temporarily_ update the web configuration; you do this by updating some
file on all of the running Pods

What do we mean by "ready and running"?

This script considers a Pod to "ready and running" if all of the following
are true:

1. The Pod's phase is "Running"

1. If the Pod has any PodConditions in the following list, those
PodConditions have staus "True": `Initialized`, `Ready`,
`ContainersReady`, `PodScheduled`.

For an explanation of "phase" and "PodConditions" see the ["Pod Lifecycle"][1]
page.


# OPTIONS

- **--filter**=PYTHON_REGEX

    By default this script returns all Pods that are ready and running. If
    you want only those Pods that are ready and running _and_ whose name
    matches a regular expression, use **--filter**=PYTHON_REGEX.
    **Important:** There is an implicit begin and end string anchor that
    surrounds PYTHON_REGEX, that is, the search is on the Python regular
    expression `^PYTHON_REGEX$`. Thus, if you want to filter on all Pod
    names that contain the string "web" you would use the option
    `--filter='.*web.*'`;

- **--invert**

    Using the **--invert** option returns all Pods that are _not_ in the ready and running
    state.

- **--namespace**=NAMESPACE

    This script will normally get the list of Pods from the current Kubernetes namespace. If you
    need to specify the namespace explicitly use the **--namespace** option.

- **--help**

    Show the help screen.

- **--verbose**

    Show extra information while running.

# EXAMPLES

Show help:

    $ pods-running-and-ready --help
    usage: pods-running-and-ready [-h] [--verbose] [--invert] [--namespace NAMESPACE] [--filter NAME_FILTER]

    List all Kubernetes Pods that are "ready" (running and all PodStatus conditions are True); see also
    https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/

    optional arguments:
      -h, --help            show this help message and exit
      --verbose, -v         show extra processing information
      --invert              return all non-ready Pods
      --namespace NAMESPACE, -n NAMESPACE
                            specify Kubernetes namespace scope (defaults to current namespace)
      --filter NAME_FILTER  return only Pods whose names match the supplied regular expression


Return all ready and running Pods in the current Kubernetes namespace:

    $ pods-running-and-ready
    www-scheduler-crondist-7bf4c79df6-v9fgv
    www-scheduler-web-7fd64fd47f-drl6n
    www-scheduler-web-7fd64fd47f-dsvzx
    www-scheduler-web-7fd64fd47f-wh5m4


Return all ready and running Pods in the current Kubernetes namespace
whose name contains the string "web":

    $ pods-running-and-ready --filter='.*web.*'
    www-scheduler-web-7fd64fd47f-drl6n
    www-scheduler-web-7fd64fd47f-dsvzx
    www-scheduler-web-7fd64fd47f-wh5m4

Return all ready and running Pods in a _different_ Kubernetes namespace
(the following will only work if your current Kubernetes credentials have
access to the cluster with the specified namespace):

    $ pods-running-and-ready --namespace=shared-email-dev
    shared-email-web-7f9748cb78-qxnkn

Return all Pods in the current Kubernetes namespace that are _not_ ready and running:

    $ pods-running-and-ready --invert
    www-scheduler-web-75f478ff9c-tnn6n
    www-scheduler-crondist-b5bfbbfbc-lvbdc

# EXIT STATUS

Returns `0` on success, `1` on any failure.

# LICENSE AND COPYRIGHT

The MIT License (MIT)

Copyright (c) 2022 University IT, Stanford University

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.



[1]: https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/
