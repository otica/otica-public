# Script Man Pages

* [`check-tools`](check-tools): check that software Otica needs is installed

* [`extract-dpk-version.sh`](extract-dpkg-version.sh): extract a Debian package version number

* [`pods-running-and-ready`](pods-running-and-ready): return list of ready and running Kubernetes Pods

* [`render-file-dir.sh`](render-file-dir.sh): Send all files in a directory through `render2.sh`

* [`vault-read.sh`](vault-read.sh): Read secrets from Hashicorp Vault

* [`vault-write.sh`](vault-write.sh): Write secrets into Hashicorp Vault


