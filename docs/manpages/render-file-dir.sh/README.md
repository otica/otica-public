# NAME

render-file-dir.sh -- Send a directory of files through render2.sh respecting `RENDER_MODE`

# USAGE

**render-file-dir.sh** _source-directory_

**render-file-dir.sh** _source-directory_ _target-directory_

# DESCRIPTION

The **render-file-dir.sh** script conditionally sends all files in a
directory (and subdirectories, recursively) through the
[`render2.sh`](../../filters/render2) filter. The "conditionally" refers
to the fact the the `RENDER_MODE` environment variable affects which files
will go through `render2.sh`.

There are two ways to call this script:

1. Calling **render-file-dir.sh** with _source-directory_ but no
_target-directory_. In this case the each file in _source-directory_ will be
replaced by the result of its conversion by `render2.sh`.

1. Calling **render-file-dir.sh** with _source-directory_ *and*
_target-directory_. In this case the files in _source-directory_ are first
copied to _target-directory_ and then the files in _target-directory_ are
converted.


## How `RENDER_MODE` affects conversion

The environment variable `RENDER_MODE` affects which files are processed.

1. If `RENDER_MODE` is set to `ALL` then *all* files are sent through
`render2.sh`.

1. If `RENDER_MODE` is set to `NONE` then *no* files are sent through
`render2.sh`.

1. If `RENDER_MODE` is set to `TMPL` then only files whose names contain
`.tmpl` will be rendered.

1. `RENDER_MODE` not set (or set to the empty string) is the same as
setting `RENDER_MODE` to `ALL`.

For more information on the `TMPL` mode, see ["Rendering configuration
(`RENDER_MODE`)"](../../configuration/render).


# OPTIONS

There are no options.

# EXIT VALUE

Returns `0` on success, `1` on any failure.
