[[_TOC_]]

# Otica: the basics

Otica is a set of Makefiles, scripts, and conventions to help manage
modern service deployment technlogies including Kubernetes and cloud
resources. This document discusses the conventions and terminology used by
Otica.

## Vocabulary

* _top-level project:_ An Otica _top-level project_ is the directory that
  contains the `repos.yaml` file, the `sub-projects` directory, and
  variable files containing settings common to multiple subprojects.
  We sometimes refer to this simply as the "top-level directory" or
  just the "toplevel".

* _subproject:_ An Otica _subproject_ is a stand-alone collection of
  configuration and code containing a specific [directory
  structure](#otica-subprojects) that manages a single piece of
  infrastrucure such as a Docker image, Kubernetes application, serverless
  function, or Helm chart. Otica subprojects are Git repositories and are
  cloned inside the `sub-projects` directory of an Otica top-level project.

* _subproject environment:_ An Otica subproject is structured so that there is
  a section of common code and separate directories that use this common code
  to create specific instances of the infrastructure.  These separate directories
  are called _subproject environments_ and typically have names like "dev", "test", or
  "prod" (although they can be named anything you want).
  These directories are the place where all `make` commands are  run.

  Example: you have a subproject "myapp" with subdirectories "dev" and
  "prod". When managing the "dev" instance of "myapp" you change directory
  into "dev" and run your make commands; likewise, when you want to manage
  the "prod" instance of "myapp" you change into the "prod" directory.

* _make module:_ Almost all the action that happens in Otica is via
  `make`. Different functions are divided into separate makefile
  files. These files are called "make modules". For example, there is a
  "helm" make module, a "docker-develop" make module, a "vault" make module,
  etc. For more information on make modules see the [Make Modules](../make-modules/)
  documentation.

* _variable files:_ These are files containing environment variable
  definitions that can be included in both GNU `make` files and Bash shell
  scripts. These variables are available to all the make modules and any
  shell scripts called by those make modules. Variable files should have the
  file extension `.var`. Examples of variable files are `common.var`,
  `local.var`, `framework.var`, etc. Each line of a variable file should be
  of the form `export VAR_NAME=some_value`. The expression on the right
  side of the equals sign can use other variables defined earlier.  Add
  these files to the "environment" section of `otica.yaml` to have them
  included. Here is an example:

        # common/framework-var.mk
        # Environment variables for the Otica framework (scripts, shared config, etc.).
        export FRAMEWORK_DIR=${HOME}/bin/otica
        export FRAMEWORK_BUCKET=otica
        export SCRIPTS_DIR=${FRAMEWORK_DIR}/scripts
        export FRAMEWORK_GIT_REMOTE=origin
        export FRAMEWORK_GIT_BRANCH=master

  For more information on how Otica uses variables and variable files see
  "[Variables and variable files](../configuration/variables)".

* _(subproject) platform:_ When creating a subproject you must supply the
  name of a _platform_. The platform helps Otica configure your
  `otica.yaml` file with appropriate environment variable files and make
  modules. Otica comes with one built-in platform called `generic`. You
  can add more platforms by editing the file
  `common/subproject-platforms.yaml` in the top-level directory For more
  details on defining additional platforms see ["The `otica.yaml` file
  configuration"](../configuration/otica-yaml#templating-the-oticayaml-file-advanced-topic).

* _Otica source code:_ This is the local copy of the Otica project
  source repository containing the makefile modules, scripts, and the
  `otica` command. It is typically a Git clone into your local file
  system.

## The Otica top-level project

An Otica _top-level project_ is the directory containing your Otica
subprojects and configuration information used by these subprojects.
Otica requires that a top-level project directory have a **specific
directory structure**:
```
my-toplevel/
|
|-- Makefile
|
|-- otica.yaml
|
|-- repos.yaml
|
|-- common/
|   |
|   |-- framework.var
|   |
|   |-- makefile.mk
|   |
|   |-- env_variables/
|   |   |
|   |   |-- framework.var
|   |   |
|   |   |-- aws.var
|   |   |
|   |   '-- gcp.var
|   |
|   |-- makefile_parts/
|   |
|   |-- scripts/
|   |
|   '-- subproject-platforms.yaml
|
|-- sub-projects/
|   |
|   |-- docker/
|   |
|   |-- helm/
|   |
|   '-- kube/
|
```

### The `otica.yaml` file

This file is used to generate the file `common/makefile.mk`. The
`platform` entry for this file should be set to `otica`. For more
information on how an `otica.yaml` file is structured see the
["Configuration" documentation](../configuration/).

### The `repos.yaml` file

This file is used to manage the subproject repositories in the
`sub-projects` directory. For more information on the format of
`repos.yaml` see ["Repo subprojects
(sub-projects)"](../make-modules/repos).

### The `sub-projects/` directory

This directory contains subproject directories and is populated and
updated by the `repos.yaml` file.


### The `common/` directory

The `common/` directory contains files that are used by subprojects.

#### The `common/env_variables/` directory

This directory should contain variable files that are synced to
subprojects. As such these variable files should contain settings that are
common to a large number of subprojects, e.g., cloud vendor settings. See
["Variables and variable files"](../configuration/variables) for how to
configure a subproject to sync files from `common/env_variables/`. One of
these variable files _must_ be `framework.var`; when creating a new
top-level project with the `otica` script a generic version of
`framework.var` will be provided; for more details on
`framework.var` see the page
["Framework-level configuration"](../configuration/framework).

#### The `common/makefile_parts/` directory

Put makefile modules in this directory that you want to make available to
multiple subprojects.

#### The `common/scripts/` directory

Put scripts in this directory that you want to make available to
multiple subprojects.

#### The `common/subproject-platforms.yaml` file

This file is used by the `otica` script when creating new subprojects. For
more details on this file and how it is used see ["Templating the
`otica.yaml` file"](../configuration/otica-yaml#templating-the-oticayaml-file-advanced-topic).

## Otica subprojects

An Otica _subproject_ is an independent set of resources in its own
directory managed by Otica. For example, a subproject might manage a
Docker container, a Helm chart, or a Kubernetes application.

Otica requires that each subproject have a **specific directory
structure**. Let's look at the directory structure of the
Otica subproject `kube-myapp`:

```
kube-myapp/
|
|-- otica.yaml
|
|-- common/
|   |
|   |-- env_variables/
|   |   |
|   |   '-- framework.var (synced from toplevel)
|   |   |
|   |   '-- gcp.var (synced from toplevel)
|   |   |
|   |   '-- common.var
|   |
|   |-- makefile_parts/
|   |
|   |-- scripts/
|   |
|   |-- templates/
|   |
|   '-- makefile.mk
|
|-- <env1>/
|   |
|   '-- local.var
|   |
|   '-- makefile.mk (soft link)
|
|-- <env2>/
|   |
|   '-- local.var
|   |
|   '-- makefile.mk (soft link)
```

### The `otica.yaml` file

This file is used to generate the file `common/makefile.mk`. For more
information on how it is structured see ["The otica.yaml
file"](../configuration/otica-yaml).

### The `common` directory

This directory contains the heart of the application. The files in this
directory are intended to be used by every subproject environment with the
subproject environment only making changes to the configuration appropriate for
that environment.

#### The `common/env_variables` directory

Contains variable files synced from the toplevel as well as variable files
specific to this subproject. The `framework.var` variable file is required.
See "[Variables and variable files](../configuration/variables)" for more
information on these files.

#### The `common/scripts` directory

Put any scripts local to the subproject in this directory.
In most subprojects this directory is empty.

#### The `common/makefile_parts` directory

Makefile local to this subproject go in this directory.
In most subprojects this directory is empty.

#### The `common/templates` directory

This directory is used primarily as the place to put Kubernetes YAML
files.  These files are often run through a [filter](../filters) before being
applied by `kubectl`.
In most subprojects this directory is empty.

#### The `makefile.mk` file

This file is automically generated when the otica script is run in the
top-level directory. The configuration file `otica.yaml` controls the
layout of `makefile.mk`. You should _not_ edit `makefile.mk` directly.

### Subproject environments

The directories `<env1>`, `<env2>`, etc., represent project
environments. The directory names should be things like `dev`, `test`,
`prod`, etc. Every Otica subproject has at least one project environment. All
`make` commands are run from within one of these directories.

Each project environment directory should contain the file `makefile.mk`
which is just an `include` to the file `../common/makefile.mk`.

Each project environment should also have a `local.var` file for
configuration overrides. See ["Variables and variable
files"](../configuration/variables) for more details on configuration
cascading.

### Project-type directories

Different project types will have their own specific directories in the
`common` directory.

#### Docker projects

The [`docker.mk` make module](../make-modules/docker-develop/) expects the
`Dockerfile` as well as other Docker image build support files to be in
the `common/docker` directory.

#### Helm projects

The [`helm-develop.mk` make module](../make-modules/helm-develop/) expects
all the Helm files (`Chart.yaml`, `values`, `templates`, etc.) to be in
the `common/helm` directory.

## Getting Started

A good way to get familiar with Otica projects and subprojects is to go
through the [tutorials](../tutorials).
