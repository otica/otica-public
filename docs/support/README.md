# Support

If you are not an Otica developer report issues by filing an issue at the
[Otica public repository][1].

If you _are_ an Otica developer file issues or merge requests at the [Otica
source repository][2].


[1]: https://code.stanford.edu/otica/otica-pubic/-/issues

[2]: https://code.stanford.edu/otica/otica
