################################################################################
## kube-sec.mk
# Targets for kubernetes secret ops
################################################################################

ifndef KUBE_SEC_DEF_FILE
	export KUBE_SEC_DEF_FILE=${TEMPLATES}/secrets.yaml
endif

.PHONY: kube-sec
kube-sec: vault-login kube-config  ## create kubernetes secret from ${TEMPLATES}/secrets.yaml
	@if [ -a ${KUBE_SEC_DEF_FILE} ]; then \
		cat ${KUBE_SEC_DEF_FILE} | envsubst | ${SCRIPTS_DIR}/vault2kube.sh | kubectl apply --overwrite=true -f - ; \
	fi

.PHONY: check-kube-sec
check-kube-sec: ## check the generated secrets.yaml
	@if [ -a ${KUBE_SEC_DEF_FILE} ]; then \
		cat ${KUBE_SEC_DEF_FILE} | envsubst ; \
	fi

.PHONY: check-kube-sec-values
check-kube-sec-values: vault-login kube-config ## check the generated secrets.yaml with secrets!
	@if [ -a ${KUBE_SEC_DEF_FILE} ]; then \
		cat ${KUBE_SEC_DEF_FILE} | envsubst | ${SCRIPTS_DIR}/vault2kube.sh ; \
	fi

.PHONY: destroy-kube-sec
destroy-kube-sec: vault-login kube-config ## destroy kubernetes secret defined by ${TEMPLATES}/secrets.yaml
	@if [ -a ${KUBE_SEC_DEF_FILE} ]; then \
		cat ${KUBE_SEC_DEF_FILE} | envsubst | ${SCRIPTS_DIR}/vault2kube.sh  \
			| kubectl delete --ignore-not-found -f -  ; \
	fi


.PHONY: kube-sec-reg
kube-sec-reg: destroy-kube-sec-reg
	@kubectl create secret docker-registry ${DOCKER_REGISTRY} \
		-n ${APP_NAMESPACE} \
		--docker-server=${DOCKER_REGISTRY} \
		--docker-username=${DOCKER_REGISTRY_USERNAME} \
		--docker-password=$$(vault-read.sh ${DOCKER_REGISTRY_PASSWORD_PATH})  \
		--docker-email=${DOCKER_REGISTRY_USERNAME}

.PHONY: destroy-kube-sec-reg
destroy-kube-sec-reg: kube-config
	@kubectl delete secret ${DOCKER_REGISTRY} \
		--ignore-not-found \
		-n ${APP_NAMESPACE} \
