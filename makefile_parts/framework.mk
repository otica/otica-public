################################################################################
## framework.mk
################################################################################

# set shell options, debug variable allows debugging of makefile
ifeq ($(debug),true)
	SHELL := /usr/bin/env bash -e -O extglob -O nullglob -o pipefail -x
else
	SHELL := /usr/bin/env bash -e -O extglob -O nullglob -o pipefail
endif

# set BUILD_DIR if not set
ifndef BUILD_DIR
	BUILD_DIR=build
endif

# set SCRIPTS_DIR if not set
ifndef SCRIPTS_DIR
	SCRIPTS_DIR=${FRAMEWORK_DIR}/scripts
endif

# set FRAMEWORK_TOOLS if not set
ifndef FRAMEWORK_TOOLS
	FRAMEWORK_TOOLS=gcloud kubectl vault jq envsubst gomplate python3 git pwgen
endif

# set SKIP_OTICA_CHECK if not set
ifndef SKIP_OTICA_CHECK
	SKIP_OTICA_CHECK=false
endif

# add scripts to PATH
PATH := ${SCRIPTS_DIR}:${PATH}

OTICA_VERSION := $(shell head -n1 ${FRAMEWORK_DIR}/OTICA_VERSION)

### TL (top-level) environment variables
TL_BASEDIR=$(shell ${SCRIPTS_DIR}/top-level-dir.sh)

# It can happen that the above shell script sets TL_BASEDIR to the empty
# string. In that case, unset TL_BASEDIR altogether.
ifeq ($(TL_BASEDIR),)
    TL_BASEDIR :=
endif

#ifndef TL_BASEDIR
#  missing_vars := ${missing_vars} TL_BASEDIR
#endif

# Only set the other TL_* environment variables if TL_BASEDIR is set.
ifdef TL_BASEDIR
  ifndef TL_COMMON
      TL_COMMON=${TL_BASEDIR}/common
  endif

  ifndef TL_ENV_VARIABLES
      TL_ENV_VARIABLES=${TL_COMMON}/env_variables
  endif

  ifndef TL_SCRIPTS
      TL_SCRIPTS=${TL_COMMON}/scripts
  endif

  ifndef TL_MAKEFILE_PARTS
      TL_MAKEFILE_PARTS=${TL_COMMON}/makefile_parts
  endif

  ifndef TL_TEMPLATES
      TL_TEMPLATES=${TL_COMMON}/templates
  endif
endif
### END OF TL (top-level) environment variables


ifneq ($(missing_vars),)
	_ := $(info )
	_ := $(info missing env var(s):)
	_ := $(info )
	_ := $(info ${missing_vars}))
	_ := $(info )
	_ := $(info you must populate the required env vars before continuing)
	_ := $(info )
	_ := $(error )
endif

function_reverse = $(if $(1),$(call function_reverse,$(wordlist 2,$(words $(1)),$(1)))) $(firstword $(1))

all: help

.PHONY: help
help: grep := '.*'
help: ## show this help page, use "help=<filter>" for more specific helps
	@# adapted from https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@echo
	@printf ">>>>>>  MAKEFILE TARGETS FOR DIRECTORY: $$(basename $$PWD)/ <<<<<<"
	@echo
	@printf ">>>>>>  Otica Version: %-22s<<<<<<" $$OTICA_VERSION
	@for f in ${MAKEFILE_LIST}; do\
	  show-make-help.sh $$f ; \
	done
	@variable-files-warning.sh
	@#make check-tools no_pull=true

.PHONY: show-makefile
show-makefile: ## show complete makefile w/ includes
	@cat $(MAKEFILE_LIST)

.PHONY: check-tools
check-tools: ## check if required tools are installed and accessible
	@${FRAMEWORK_DIR}/bin/check-tools --verbose

.PHONY: show-env
show-env: grep := '.*'
show-env: ## show all env var values, use "grep=<filter>" for specific envs
	@env | grep -i ${grep} | sort

.PHONY: clean
clean: ## remove the build directory
	@rm -rf ${BUILD_DIR}

.PHONY: update-framework
update-framework: ## Update ${FRAMEWORK_DIR}
	@if [ -d ${FRAMEWORK_DIR}/.git ] ; then \
		cd ${FRAMEWORK_DIR}; git pull; \
	else \
		rm -rf ${FRAMEWORK_DIR} ; \
	fi

.PHONY: otica-update
otica-update: ## run "otica update"
	cd .. && ${FRAMEWORK_DIR}/bin/otica update

