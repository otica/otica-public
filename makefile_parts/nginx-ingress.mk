################################################################################
# nginx-ingress.mk
# Ref: https://github.com/kubernetes/ingress-nginx/tree/master/charts/ingress-nginx
################################################################################

ifndef HELM
	export HELM := helm
endif

ifndef NGINX_INGRESS_CHART
	export NGINX_INGRESS_CHART := ingress-nginx/ingress-nginx
endif

ifndef NGINX_INGRESS_NS
	export NGINX_INGRESS_NS := nginx-ingress
endif

ifndef NGINX_INGRESS_VALUES_FILE
	export NGINX_INGRESS_VALUES_FILE := ${FRAMEWORK_DIR}/helm-charts/nginx-ingress/stable/values.tmpl.yaml
endif

ifndef NGINX_INGRESS_RELEASE
	export NGINX_INGRESS_RELEASE = nginx-ingress
endif

.PHONY: create-nginx-ingress-ns
create-nginx-ingress-ns: config-kube  ## create nginx-ingress namespace
	@${HELM} repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
	@if ! kubectl get namespace ${NGINX_INGRESS_NS} &> /dev/null ; then \
		kubectl create namespace ${NGINX_INGRESS_NS} ; \
	fi

.PHONY: deploy-nginx-ingress
deploy-nginx-ingress: create-nginx-ingress-ns ## provisioning the nginx-ingress
	@if ! ${HELM} ls --namespace ${NGINX_INGRESS_NS} | grep ${NGINX_INGRESS_RELEASE} | grep deployed &>/dev/null ; then \
		${HELM} repo update; \
		cat ${NGINX_INGRESS_VALUES_FILE} | render.sh | ${HELM} install \
			--namespace ${NGINX_INGRESS_NS} \
			${NGINX_INGRESS_RELEASE} ${NGINX_INGRESS_CHART}; \
	fi

.PHONY: upgrade-nginx-ingress
upgrade-nginx-ingress: config-kube ## update the nginx-ingress
	@if ${HELM} ls --namespace ${NGINX_INGRESS_NS} | grep ${NGINX_INGRESS_RELEASE} | grep deployed &>/dev/null ; then \
		${HELM} repo update; \
		cat ${NGINX_INGRESS_VALUES_FILE}| render.sh | ${HELM} upgrade \
			--namespace ${NGINX_INGRESS_NS} \
			${NGINX_INGRESS_RELEASE} ${NGINX_INGRESS_CHART}; \
	else \
		echo "ERROR: ${NGINX_INGRESS_RELEASE} is not deployed"; \
		echo "To deploy: make deploy-nginx-ingress"; \
	fi

.PHONY: destroy-nginx-ingress
destroy-nginx-ingress: config-kube ## destroy the nginx-ingress
	@if ${HELM} ls --namespace ${NGINX_INGRESS_NS} | grep ${NGINX_INGRESS_RELEASE} | grep deployed  &>/dev/null; then \
		${HELM} delete --namespace ${NGINX_INGRESS_NS} ${NGINX_INGRESS_RELEASE}; \
	fi

## end of nginx-ingress.mk
