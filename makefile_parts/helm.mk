# Basic Helm chart support.
#
# Assuptions:

# 1. APP_NAMESPACE environment variable is set to the correct Kubernetes
# namespace.
#
# 2. The Kubernetes namespace specified by APP_NAMESPACE already exists.
#
# 3. The application environment ("dev", "test", etc.) is specified by the
# environment variable APP_ENV.
#
# 4. The Helm repo and name are specified by HELM_REPO_NAME and APP,
# respectively.
#
# 5. The HELM repository URL is specified by HELM_REPO_URL.
#
# 6. The values.yaml settings are overridden in the file
# ${APP_ENV}/values-local.yaml (uses environment variable substitution).

ifndef HELM_BIN
  HELM_BIN=helm
endif

ifndef HELM_RELEASE
	export HELM_RELEASE=${HELM_CHART_NAME}
endif

ifndef HELM_CHART_VERSION
	export HELM_CHART_VERSION=latest
endif

ifndef HELM_REGISTRY_LOGIN_TYPE
	export HELM_REGISTRY_LOGIN_TYPE=gcloud
endif

ifndef HELM_CHART_NAME
	missing_vars := ${missing_vars} HELM_CHART_NAME
endif

ifndef HELM_REPO_URL
	missing_vars := ${missing_vars} HELM_REPO_URL
endif

# In HELM_YAML_FILES order matters!
ifndef HELM_YAML_FILES
	export HELM_YAML_FILES=../common/values-local.yaml values-local.yaml
endif

ifneq ($(missing_vars),)
	_ := $(info )
	_ := $(info missing env var(s):)
	_ := $(info )
	_ := $(info ${missing_vars}))
	_ := $(info )
	_ := $(info you must populate the required env vars before continuing)
	_ := $(info )
	_ := $(error )
endif

.PHONY: helm-check
helm-check: ## check that the correct environment variables are set
	@helm-wrapper.py check-env

.PHONY: helm-show-env
helm-show-env: helm-check ## show Helm-relevant environment variables
	@helm-wrapper.py show-env

##
## helm-repo-* targets (only for non-OCI repos)

.PHONY: helm-fail-on-oci
helm-fail-on-oci:
	@helm-wrapper.py fail-if-oci

.PHONY: helm-repo-add
helm-repo-add: helm-fail-on-oci helm-check kube-config ## add the Helm repo where the Helm chart is hosted (no oci://)
	${HELM_BIN} repo add ${HELM_REPO_NAME} ${HELM_REPO_URL}
	${HELM_BIN} repo update

.PHONY: helm-repo-update
helm-repo-update: helm-check kube-config ## update the local Helm repo cache (no oci://)
	@# Only run this target if HELM_REPO_NAME is defined.
	@if [ -n "$(HELM_REPO_NAME)" ]; then \
	  ${HELM_BIN} repo update ${HELM_REPO_NAME} ; \
	fi

.PHONY: helm-repo-remove
helm-repo-remove: helm-fail-on-oci helm-check kube-config ## remove the Helm repo where the Helm chart is hosted (no oci://)
	${HELM_BIN} repo remove ${HELM_REPO_NAME}
	${HELM_BIN} repo update

.PHONY: helm-repo-list
helm-repo-list: helm-check kube-config ## list all Helm repos
	${HELM_BIN} repo list

.PHONY: helm-repo-list-charts
helm-repo-list-charts: helm-fail-on-oci helm-check kube-config ## list all Helm charts in HELM_REPO_URL (no oci://)
	${HELM_BIN} search repo ${HELM_REPO_NAME}

.PHONY: helm-repo-list-versions
helm-repo-list-versions: helm-fail-on-oci helm-check kube-config ## list all versions of this Helm chart in HELM_REPO_URL (no oci://)
	#${HELM_BIN} search repo ${HELM_REPO_NAME} -l | grep ${HELM_CHART_NAME}
	${HELM_BIN} search repo ${HELM_CHART_NAME} -l

##
## Other targets

.PHONY: helm-list
helm-list: helm-check kube-config ## list Helm installation
	${HELM_BIN} list

.PHONY: helm-install
helm-install: helm-check  helm-login kube-config ## do a Helm install
	@helm-wrapper.py install

.PHONY: helm-install-dryrun
helm-install-dryrun: helm-check kube-config ## do a Helm install dry-run
	@helm-wrapper.sh install-dryrun

.PHONY: helm-uninstall
helm-uninstall: helm-check kube-config ## uninstall the Helm chart
	release="${HELM_RELEASE}"; \
	${HELM_BIN} uninstall \
	$${release}

.PHONY: helm-template
helm-template: helm-check ## render Helm templates
	@helm-wrapper.py template

.PHONY: helm-upgrade
helm-upgrade: helm-check kube-config helm-login helm-repo-update ## upgrade running Helm installation
	@helm-wrapper.py upgrade

.PHONY: helm-upsert
helm-upsert: helm-check kube-config helm-login helm-repo-update ## upgrade running Helm (or install if not installed)
	@helm-wrapper.py upsert

#.PHONY: helm-show-all
#helm-show-all: helm-check kube-config ## ???
#	${HELM_BIN} show all ${HELM_REPO_URL}/${HELM_CHART_NAME}

.PHONY: helm-get-values
helm-get-values: helm-check kube-config ## show the values for the current Helm release
	${HELM_BIN} get values "${HELM_RELEASE}"

.PHONY: helm-help
helm-help: ## show help page for this module
	@show-module-help.sh helm

.PHONY: helm-diff
helm-diff: kube-config ## generate a Helm diff
	@helm-wrapper.py diff

.PHONY: helm-values-merge
helm-values-merge: ## show merged values.yaml files
	@helm-wrapper.py merge

.PHONY: helm-login
helm-login: helm-check ## login to Helm registry
	@helm-login.sh



