###########################################
## NOTE: FOR BACKWARD COMPATIBILITY ONLY ##
## Please use sub-projects.mk instead    ##
###########################################

THIS_DIR := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

_ := $(shell >&2 ${SCRIPTS_DIR}/deprecated.sh "sub-projects-new.mk is deprecated; run 'otica update'")

include ${THIS_DIR}/sub-projects.mk
