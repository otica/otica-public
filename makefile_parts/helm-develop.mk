# Support for DEVELOPING Helm charts.
#
# This make module is useful when working on, packaging, and
# uploading a Helm chart. If you want to USE a Helm chart look at the
# helm.mk make module instead.

ifndef HELM_BUILD_DIR
  HELM_BUILD_DIR=${COMMON}/helm
endif

ifndef HELM_BIN
  HELM_BIN=helm
endif

ifndef HELM_REGISTRY_LOGIN_TYPE
	export HELM_REGISTRY_LOGIN_TYPE=gcloud
endif

# For Helm development we do not need to use an existing KUBECONFIG file,
# so we set it to an out-of-the-way path so helm does not use an
# inappropriate one by mistake. In the empty-kubeconfig target we ensure that the
# file pointed to by KUBECONFIG exists, is empty, and has the correct
# permissions.
KUBECONFIG=/tmp/.helm-develop-kubeconfig-$(shell id -u)

.PHONE: empty-kubeconfig
empty-kubeconfig:
	@rm -f $$KUBECONFIG; touch $$KUBECONFIG; chmod 600 $$KUBECONFIG

.PHONY: dhelm-login
dhelm-login: ## login to Helm repository
	@helm-login.sh

.PHONY: dhelm-help
dhelm-help: ## show help for this module
	@show-module-help.sh helm-develop

.PHONY: dhelm-package
dhelm-package: dhelm-clean empty-kubeconfig ## package the Helm chart into a tar-ball
	${HELM_BIN} package ${HELM_BUILD_DIR}

.PHONY: dhelm-push
dhelm-push: dhelm-check-env dhelm-login dhelm-package ## upload the Helm package to its repository
	${SCRIPTS_DIR}/helm-push-chart.sh

.PHONY: dhelm-push-force
dhelm-push-force: dhelm-check-env dhelm-login dhelm-package ## upload the Helm package to its repository (forced)
	@HELM_PUSH_FORCE=YES ${SCRIPTS_DIR}/helm-push-chart.sh

.PHONY: dhelm-show-env
dhelm-show-env: ## show the Helm make module environment variables
	@VERBOSE=1 helm-develop-variable-check.sh

.PHONY: dhelm-check-env
dhelm-check-env:
	helm-develop-variable-check.sh

.PHONY: dhelm-repo-list
dhelm-repo-list: ## list all the Helm repositories
	${HELM_BIN} repo list

.PHONY: dhelm-repo-add
dhelm-repo-add: ## add the Helm repository (no oci://)
	@# Only run this target if HELM_REPO_NAME is defined.
	@if [ -n "$(HELM_REPO_NAME)" ]; then \
	  ${HELM_BIN} repo update ${HELM_REPO_NAME} ; \
	else \
	  echo "cannot run dhelm-repo-add with an OCI-based registry"; exit 1; \
	fi

.PHONY: dhelm-clean
dhelm-clean: ## remove package tar files
	rm -f *.tgz

.PHONY: dhelm-render-templates
dhelm-render-templates: dhelm-dependency-update ## display templates with values substituted in
	${HELM_BIN} template --debug xxx ${HELM_BUILD_DIR}

.PHONY: dhelm-dependency-update
dhelm-dependency-update: ## update Chart dependencies
	${HELM_BIN} dependency update ${HELM_BUILD_DIR}

.PHONY: dhelm-dependency-list
dhelm-dependency-list: ## list Chart dependencies
	${HELM_BIN} dependency list ${HELM_BUILD_DIR}
