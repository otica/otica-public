# Makefile to manage datasealer files used with Shibboleth clients.

# Configuration:
#
# DATASEALER_SEC_PATH: the Vault path to the datasealer file.

ifndef DATASEALER_SEC_PATH
	missing_vars := ${missing_vars} DATASEALER_SEC_PATH
endif

.PHONY: datasealer-update
datasealer-update: vault-login ## Add or update the Vault datasealer secret
	@${SCRIPTS_DIR}/datasealer.sh

.PHONY: datasealer-reset
datasealer-reset: vault-login ## Reset the Vault datasealer secret
	@${SCRIPTS_DIR}/datasealer.sh new

.PHONY: datasealer-show
datasealer-show: vault-login ## Show datasealer Vault secret (WARNING: displays secrets on terminal!)
	@vault-read.sh ${DATASEALER_SEC_PATH}

