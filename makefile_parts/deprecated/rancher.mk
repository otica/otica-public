## rancher.mk
# Ref https://rancher.com/docs/rancher/v2.x/en/installation/k8s-install/helm-rancher/

ifndef RANCHER_NS
	export RANCHER_NS = rancher
endif

ifndef RANCHER_CHART_VERSION
	export RANCHER_CHART_VERSION = 2.4.6
endif


.PHONY: helm-repo-add-rancher
helm-repo-add-rancher:
	${HELM} repo add rancher-latest https://releases.rancher.com/server-charts/latest
	${HELM} repo update

.PHONY: create-rancher-ns
create-rancher-ns: helm-repo-add-rancher ## create rancher namespace; requires cert-manager
	@if ! kubectl get namespace ${RANCHER_NS} &> /dev/null ; then \
		kubectl create namespace ${RANCHER_NS} ; \
		make deploy-cert-manager ; \
	fi

.PHONY: dry-run-rancher
dry-run-rancher: create-rancher-ns ## create the rancher
	@if ! ${HELM} ls --namespace ${RANCHER_NS} | grep rancher | grep DEPLOYED; \
	then \
		${HELM} install \
			--dry-run \
			--namespace ${RANCHER_NS} \
			--version ${RANCHER_CHART_VERSION} \
			--set hostname=rancher.${GCP_DNS_DOMAIN} \
			rancher rancher-latest/rancher; \
	fi

.PHONY: deploy-rancher
deploy-rancher: create-rancher-ns ## create the rancher
	@if ! ${HELM} ls --namespace ${RANCHER_NS} | grep rancher | grep DEPLOYED; \
	then \
		${HELM} install \
			--namespace ${RANCHER_NS} \
			--version ${RANCHER_CHART_VERSION} \
			--set hostname=rancher.${GCP_DNS_DOMAIN} \
			rancher rancher-latest/rancher; \
			kubectl -n ${RANCHER_NS} rollout status deploy/rancher; \
	fi

.PHONY: upgrade-rancher
upgrade-rancher: create-rancher-ns ## upgrade rancher
	@if ${HELM} ls --namespace ${RANCHER_NS} | grep rancher | grep DEPLOYED; then \
		${HELM} upgrade rancher rancher-latest/rancher \
			--set hostname=rancher.${GCP_DNS_DOMAIN} \
			--version ${RANCHER_CHART_VERSION} \
			--namespace ${RANCHER_NS}; \
		kubectl -n ${RANCHER_NS} rollout status deploy/rancher; \
	else \
		make deploy-rancher; \
	fi

.PHONY: ls-rancher
ls-rancher: ## list the rancher
	@${HELM} ls --namespace ${RANCHER_NS} | grep rancher 

.PHONY: rancher-status
rancher-status: ## check rancher status
	@${HELM} status --namespace ${RANCHER_NS} rancher

.PHONY: destroy-rancher
destroy-rancher: config-kube ## destroy the  rancher
	@if ${HELM} ls --namespace ${RANCHER_NS} | grep rancher &>/dev/null; \
	then \
		${HELM} delete --namespace ${RANCHER_NS} rancher; \
	fi

.PHONY: rancher-pf
rancher-pf: config-kube ## setup port-forward to racher
	kubectl port-forward --namespace ${RANCHER_NS} svc/rancher 8080:80

## end of rancher.mk
