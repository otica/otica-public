ifndef COMMON
	COMMON := ${PWD}
endif

ifndef APP_NAMESPACE
	APP_NAMESPACE := default
endif

ifndef DEPLOY_TEMPLATE_DIR
	DEPLOY_TEMPLATE_DIR := ${COMMON}/templates
endif

ifndef DEPLOY_SEC_TEMPLATE_DIR
	DEPLOY_SEC_TEMPLATE_DIR := ${COMMON}/templates-sec
endif

ifndef DEPLOY_DIR
	DEPLOY_DIR := .deploy
endif

ifndef DEPLOY_SEC_DIR
	DEPLOY_SEC_DIR := .deploy_sec
endif

###
# Deployment utilitis

kc-check-gitignore:
	@if ! git check-ignore "${DEPLOY_SEC_DIR}" "${DEPLOY_SEC_DIR}/" &> /dev/null; \
	then \
		echo "!!!!! WARNING !!!!!"; \
		echo "!!!!! Please add \"${DEPLOY_SEC_DIR}\" and \"${DEPLOY_DIR}\" to .gitignore to prevent committing secrets into git"; \
		exit 1; \
	fi

.PHONY: kc-init
kc-init: kc-check-gitignore  config-kube ## initialize kube-deploy
	@if [ "$(MAKELEVEL)" -eq "0" ]; then \
		make kc-force-init ; \
	fi

# make sure gitignore the deploy dir and deploy_sec dir
.PHONY: kc-force-init
kc-force-init: config-kube
	@if ! kubectl get namespace ${APP_NAMESPACE} &> /dev/null ; then \
		kubectl create namespace ${APP_NAMESPACE} ; \
	fi
	@mkdir -p ${DEPLOY_DIR} ${DEPLOY_SEC_DIR}

.PHONY: kc-clean
kc-clean: ## cleanup kube-deploy
	@rm -rf ${DEPLOY_SEC_DIR} ${DEPLOY_DIR}

.PHONY: kc-render
kc-render: kc-init ## render deployment templates under ${DEPLOY_TEMPLATE_DIR} to ${DEPLOY_DIR} dir
	@if [ -d ${DEPLOY_TEMPLATE_DIR} ]; then \
		for f in `ls -p ${DEPLOY_TEMPLATE_DIR} | grep -v '/'`; do \
			cat ${DEPLOY_TEMPLATE_DIR}/$${f} | render.sh > ${DEPLOY_DIR}/$${f}; \
		done ; \
	fi
	# Also check if there are any .yml, .yaml, .json override in current directory
	@for f in `ls -p ${PWD} | grep -v '/' |  egrep "\.(json|yaml|yml)"`; do \
		cat ${PWD}/$${f} | render.sh > ${DEPLOY_DIR}/$${f}; \
	done
	echo "Deployment templates have been rendered to ${DEPLOY_DIR}"

.PHONY: kc-render-sec
kc-render-sec: kc-init ## render secret tempaltes under ${DEPLOY_SEC_TEMPLATE_DIR} to ${DEPLOY_SEC_DIR} dir
	@if [ -d "${DEPLOY_SEC_TEMPLATE_DIR}" ]; then \
		make vault-login ; \
		rm -rf ${DEPLOY_SEC_DIR}/* ; \
		for f in `ls ${DEPLOY_SEC_TEMPLATE_DIR}` ; do \
			cat ${DEPLOY_SEC_TEMPLATE_DIR}/$${f} | render.sh > ${DEPLOY_SEC_DIR}/$${f}; \
		done ; \
		echo "Secret templates have been rendered to ${DEPLOY_SEC_DIR}" ; \
	fi

.PHONY: kc-dry-run
kc-dry-run: kc-render ## render and dry-run apply all files under ./deploy
	kubectl apply --server-dry-run -f ${DEPLOY_DIR}

.PHONY: kc-diff
# https://github.com/kubernetes/kubernetes/pull/88739. --server-side will work in 1.18.
kc-diff: FILE=
kc-diff: kc-render ## render and diff a yaml file under ./deploy
	@if [ -f "${DEPLOY_DIR}/${FILE}" ]; then \
		kubectl diff -f ${DEPLOY_DIR}/${FILE} ; \
	else \
		kubectl diff -f ${DEPLOY_DIR} ; \
	fi

.PHONY: kc-restart
kc-restart: DEPLOY_FILE=deployment.yml
kc-restart: config-kube ## restart the ${DEPLOY_FILE}, default to deployment.yml
	@if [ -f "${DEPLOY_DIR}/${DEPLOY_FILE}" ]; then \
		kubectl rollout restart -f ${DEPLOY_DIR}/${DEPLOY_FILE} ; \
	else \
		echo "Not Found: ${DEPLOY_DIR}/${DEPLOY_FILE}, nothing to restart." ; \
	fi

.PHONY: kc-apply
kc-apply: kc-render ## render and apply deployment templates under ${DEPLOY_TEMPLATE_DIR}
	@if [[ "$(shell ls -A ${DEPLOY_DIR})" ]] && ls -1 ${DEPLOY_DIR} | egrep "\.(json|yaml|yml)" > /dev/null 2>&1; then \
		kubectl apply -f ${DEPLOY_DIR} ; \
	else \
		echo "No .(json|yaml|yml) files; skip kubectl apply" ; \
	fi

.PHONY: kc-delete
kc-delete: kc-render ## delete the deployment
	kubectl delete --ignore-not-found -f ${DEPLOY_DIR}

.PHONY: kc-apply-sec
kc-apply-sec: kc-render-sec ## render and apply secret templates under ${DEPLOY_SEC_TEMPLATE_DIR}
	@if [[ "$(shell ls -A ${DEPLOY_SEC_DIR})" ]] && ls -1 ${DEPLOY_SEC_DIR} | egrep "\.(json|yaml|yml)" > /dev/null 2>&1; then \
		kubectl apply -f ${DEPLOY_SEC_DIR} ; \
		rm -rf ${DEPLOY_SEC_DIR}/* ; \
	else \
		echo "No .(json|yaml|yml) files; skip kubectl apply" ; \
	fi

.PHONY: kc-delete-sec
kc-delete-sec: kc-render-sec ## delete the deployment
	kubectl delete --ignore-not-found -f ${DEPLOY_SEC_DIR}
	rm -rf ${DEPLOY_SEC_DIR}/*

.PHONY: kc-status
kc-status: config-kube ## show the status of pv,pvc,cm,secret,svc,pods
	kubectl get pv,pvc,cm,secret,svc,ing,pods -n ${APP_NAMESPACE}

###########
# Utilities
###########

.PHONY: get-images
get-images: POD=
get-images: config-kube ## show images running in pod: make POD=<podname>
	@if [ -z ${POD} ]; then \
		echo "make POD=<pod name>" ; \
	else \
		kubectl get pod ${POD} -o json | jq '.status.containerStatuses[] | { "image": .image, "imageID": .imageID }' ; \
	fi

# kube cert utilities
.PHONY: kc-ls-cert-issuer
kc-ls-cert-issuer: config-kube ## list all cert issuers
	kubectl get ClusterIssuer,Issuer -n ${APP_NAMESPACE}

.PHONY: kc-ls-cert
kc-ls-cert: config-kube ## list all certificates
	kubectl get certificates -n ${APP_NAMESPACE}

.PHONY: kc-get-cert
kc-get-cert: config-kube ## get all certificates
	kubectl get certificates -n ${APP_NAMESPACE} -o yaml

.PHONY: kc-show-cert
kc-show-cert: config-kube ## show cert in app namespace
	@show-kube-cert.sh ${APP_NAMESPACE}
