################################################################################
# gitlab-runner-helm.mk
# See https://docs.gitlab.com/runner/install/kubernetes.html#installing-gitlab-runner-using-the-helm-chart
################################################################################
ifndef HELM
	export HELM := helm
endif

ifndef GITLAB_RUNNER_RELEASE
	GITLAB_RUNNER_RELEASE=12.1.0
endif

ifndef GITLAB_RUNNER_CHART_VERSION
	export GITLAB_RUNNER_CHART_VERSION := 0.7.0
endif

ifndef GITLAB_RUNNER_NS
	export GITLAB_RUNNER_NS := ${APP_NAMESPACE}
endif

ifndef TEMPLATES
	missing_vars := ${missing_vars} TEMPLATES
endif

ifndef CHART_VALUES_TEMPLATES
	CHART_VALUES_TEMPLATES := ${FRAMEWORK_DIR}/helm-charts/gitlab-runner/latest/values.tmpl
endif

ifndef GCP_CREDENTIALS_KEY_PATH
	GCP_CREDENTIALS_PATH=secret/projects/${GCP_PROJECT_ID}/common/gcp-provision
endif

ifndef HELM_CHART_DIR
	HELM_CHART_DIR=${FRAMEWORK_DIR}/helm-charts
endif

.PHONY: helm-repo-add-gitlab-runner
helm-repo-add-gitlab-runner:
	@helm repo add gitlab https://charts.gitlab.io
	@helm repo update

.PHONY: create-gitlab-runner-ns
create-gitlab-runner-ns: config-kube helm-repo-add-gitlab-runner ## create gitlab-runner namespace
	@if ! kubectl get namespace ${GITLAB_RUNNER_NS} &> /dev/null ; then \
		kubectl create namespace ${GITLAB_RUNNER_NS} ; \
	fi

.PHONY: deploy-gitlab-runner
deploy-gitlab-runner: create-gitlab-runner-ns ## create the gitlab-runner
	@if ! ${HELM} ls gitlab-runner | grep DEPLOYED; \
	then \
		cat ${CHART_VALUES_TEMPLATES} | render.sh | \
		${HELM} install \
			-f - \
			--name gitlab-runner \
			--namespace ${GITLAB_RUNNER_NS} \
			--version ${GITLAB_RUNNER_CHART_VERSION} \
			gitlab/gitlab-runner; \
	fi

.PHONY: upgrade-gitlab-runner
upgrade-gitlab-runner: GITLAB_RUNNER_CHART_VERSION=0.7.0
upgrade-gitlab-runner: vault-login
upgrade-gitlab-runner: config-kube ## update the external-dns
	cat ${FRAMEWORK_DIR}/helm-charts/gitlab-runner/latest/values.tmpl | render.sh | \
		${HELM} upgrade --version ${GITLAB_RUNNER_CHART_VERSION} \
		-f - \
		gitlab-runner gitlab/gitlab-runner

.PHONY: ls-gitlab-runner
ls-gitlab-runner: ## list the gitlab-runner
	@${HELM} ls --all gitlab-runner

.PHONY: gitlab-runner-status
gitlab-runner-status: ## check gitlab-runner status
	@${HELM} status gitlab-runner

.PHONY: destroy-gitlab-runner
destroy-gitlab-runner: config-kube ## destroy the  gitlab-runner
	@if ${HELM} ls gitlab-runner | grep gitlab-runner &>/dev/null; \
	then \
		${HELM} delete --purge gitlab-runner; \
	fi

## end of gitlab-runner.mk
