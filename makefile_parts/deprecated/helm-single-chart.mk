

################################################################################
## helm-single-chart.mk
# Makefile targets for a project with a single helm chart
# Depends on
# - vault.mk
#
# If you want to target multiple helm charts:
#	- see documentation for helm-install.sh
#
################################################################################

ifndef HELM_CHART
	missing_vars := ${missing_vars} HELM_CHART
endif

ifndef HELM_RELEASE_NAME
	missing_vars := ${missing_vars} HELM_RELEASE_NAME
endif

.PHONY: helm-list
helm-list: config-kube ## show deployment if exists
	@helm list ${HELM_RELEASE_NAME}
	
.PHONY: helm-template
helm-template: ## show the templated helm chart to be deployed
	@debug_helm_template=true \
		${SCRIPTS_DIR}/helm-install.sh

.PHONY: helm-show-values
helm-show-values: ## show the filled-in gomplate template to be passed to the helm chart as values
	@debug_gomplate=true \
		${SCRIPTS_DIR}/helm-install.sh

.PHONY: helm-install
helm-install: vault-login config-kube ## deploy via helm install
	@${SCRIPTS_DIR}/helm-install.sh

.PHONY: helm-upgrade
helm-upgrade: vault-login config-kube ## upgrade deployment via helm upgrade
	@${SCRIPTS_DIR}/helm-upgrade.sh

.PHONY: helm-delete
helm-delete: vault-login config-kube ## destroy deployment via helm delete
	@${SCRIPTS_DIR}/helm-delete.sh
