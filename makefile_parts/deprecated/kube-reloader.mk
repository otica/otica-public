
################################################################################
# kube-reloader.mk
#    Reloader can watch changes in ConfigMap and Secret and do rolling upgrades 
#    on Pods with their associated Deployments, Daemonsets and Statefulsets.
# See https://github.com/stakater/Reloader
################################################################################

ifndef TEMPLATES
	missing_vars := ${missing_vars} TEMPLATES
endif

ifndef HELM
	export HELM := helm
endif

ifndef GKE_CLUSTER_NAME
	missing_vars := ${missing_vars} GKE_CLUSTER_NAME
endif

ifndef RELOADER_HELM_REOP
	RELOADER_HELM_REOP=https://stakater.github.io/stakater-charts
endif

# verson is not used because the repo is not supported it.
ifndef RELOADER_VERSION
	RELOADER_VERSION=v0.7.5
endif

ifndef RELOADER_NS
	RELOADER_NS=kube-reloader
endif

.PHONY: helm-add-reloader-repo
helm-add-reloader-repo: config-kube ## helm repo add reloader
	@if ! helm repo list | grep ${RELOADER_HELM_REOP} &> /dev/null ; then \
		helm repo add stakater ${RELOADER_HELM_REOP} ; \
		helm repo update ; \
	fi

.PHONY: create-reloader-ns
create-reloader-ns: helm-add-reloader-repo ## create reloader namespace
	@if ! kubectl get namespace ${RELOADER_NS} &> /dev/null ; then \
		kubectl create namespace ${RELOADER_NS} ; \
	fi

.PHONY: deploy-reloader
deploy-reloader: create-reloader-ns ## provisioning the reloader
	if ! ${HELM} ls reloader | grep reloader &>/dev/null ; \
	then \
		${HELM} install \
			--name reloader \
			--namespace ${RELOADER_NS} \
			stakater/reloader ; \
	fi
	
.PHONY: upgrade-reloader
upgrade-reloader: config-kube ## update the reloader
	@${HELM} update
	@${HELM} upgrade  \
		reloader stable/reloader

.PHONY: destroy-reloader
destroy-reloader: config-kube ## destroy the reloader
	@if ${HELM} ls reloader | grep reloader &>/dev/null; \
	then \
		${HELM} del --purge reloader; \
	fi

###########
## Test
###########

## end of reloader.mk
