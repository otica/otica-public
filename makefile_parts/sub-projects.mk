################################################################################
## sub-projects.mk
################################################################################

ifndef SUB_PROJECTS
	SUB_PROJECTS := sub-projects
endif

.PHONY: repos-mkdir
repos-mkdir:
	@if [[ ! -d ${SUB_PROJECTS} ]] ; then mkdir -p ${SUB_PROJECTS} ; fi

.PHONY: repos-help
repos-help: ## show help
	@repos-show-help.sh repos

.PHONY: repos-check
repos-check: repos.yaml ## check that repos.yaml is valid
	@git pull
	@${SCRIPTS_DIR}/update-repos.py check

.PHONY: repos-list
repos-list: repos.yaml ## list repos in repos.yaml
	@git pull
	@${SCRIPTS_DIR}/update-repos.py list

.PHONY: repos-details
repos-details: repos.yaml ## display repos details
	@git pull
	@${SCRIPTS_DIR}/update-repos.py details

.PHONY: repos-update-dryrun
repos-update-dryrun: repos-mkdir repos.yaml ## do a repos update in dry-run mode (filter=<regex> to filter)
	@git pull
	@${SCRIPTS_DIR}/update-repos.py --dry-run update

.PHONY: repos-update
repos-update: repos-mkdir repos.yaml ## update all repos (filter=<regex> to filter)
	@git pull
	@echo ""
	@${SCRIPTS_DIR}/update-repos.py update

.PHONY: repos-show-orphaned
repos-show-orphaned: repos-mkdir repos.yaml ## show orphaned sub-project/ directories
	@git pull
	@${SCRIPTS_DIR}/update-repos.py orphaned

.PHONY: repos-show-status
repos-show-status: repos-mkdir repos.yaml ## show repos with uncommitted changes
	@git pull
	@${SCRIPTS_DIR}/update-repos.py uncommitted
