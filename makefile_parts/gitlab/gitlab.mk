## gitlab.mk

# Dependency gitlab-cli
# Limits: Only process one-level repository format, e.g. gitlab-path/repository

ifndef GITLAB_SERVER
	missing_vars := ${missing_vars} GITLAB_SERVER
endif

ifndef GITLAB_SEC_FILE
	export GITLAB_SEC_FILE=.gitlab-ci.sec
endif

ifndef PUSH_TAG_PREFIX
	export PUSH_TAG_PREFIX := prod
endif

.PHONY: gl-init
gl-init: ## Check if GITLAB_REPO is defined
	@if [ -z "${GITLAB_REPO}" ]; then \
		echo "Error: GITLAB_REPO is missing"; \
		exit 1 ; \
	fi

.PHONY: gl-ls-runners
gl-ls-runners: gl-init  ##  List all runners
	@echo; ${SCRIPTS_DIR}/gitlab/gl-ls-runners.sh

.PHONY: gl-get-runner
gl-get-runner: gl-init  ##  Get runner by runner name (i.e. description)
	@if [ -z "${GITLAB_RUNNER_NAME}" ]; then \
		echo "GITLAB_RUNNER_NAME is not defined!"; \
	else \
		echo; ${SCRIPTS_DIR}/gitlab/gl-get-runner.sh ${GITLAB_RUNNER_NAME}; \
	fi

.PHONY: gl-get-runner-token
gl-get-runner-token: gl-init  ##  Get runner token by runner name (i.e. description)
	@if [ -z "${GITLAB_RUNNER_NAME}" ]; then \
		echo "GITLAB_RUNNER_NAME is not defined!"; \
	else \
		echo; ${SCRIPTS_DIR}/gitlab/gl-get-runner-token.sh ${GITLAB_RUNNER_NAME}; \
	fi

.PHONY: gl-add-sec
gl-add-sec: vault-login gl-init  ## add CI/CD secrets defined in GITLAB_SEC_FILE
	@${SCRIPTS_DIR}/gitlab/gl-add-all-sec.sh ${GITLAB_SEC_FILE}

.PHONY: gl-rm-sec
gl-rm-sec: gl-init  ## remove CI/CD secrets defined in GITLAB_SEC_FILE
	@${SCRIPTS_DIR}/gitlab/gl-rm-all-sec.sh ${GITLAB_SEC_FILE}

.PHONY: gl-set-slack
gl-set-slack: vault-login gl-init ## set slack notifactions on
	@${SCRIPTS_DIR}/gitlab/gl-set-slack.sh true


.PHONY: gl-merge-mr
gl-merge-mr: gl-init ## gitlab commit a merge request
	@if [ -z "${GITLAB_MERGE_REQUEST}" ]; then \
		${SCRIPTS_DIR}/gitlab/gl-merge-mr.sh ${GITLAB_MERGE_REQUEST}
	fi

# As 2019/08/27, slack notifactions can't turn off through API
# .PHONY: gl-set-slack-off
# gl-set-slack-off: gl-init ## set slack notifactions off
# 	${SCRIPTS_DIR}/gl-set-slack.sh false


.PHONY: gl-ls-triggers
gl-ls-triggers: gl-init  ## list CI/CD triggers
	@${SCRIPTS_DIR}/gitlab/gl-ls-triggers.sh

.PHONY: gl-add-trigger
gl-add-trigger: vault-login gl-init  ## add CI/CD trigger and save trigger token in vault
	@${SCRIPTS_DIR}/gitlab/gl-add-trigger.sh

.PHONY: gl-setup
gl-setup: gl-init  ## set common gitlab, secrets, and other integrations
	@if [ -a ${GITLAB_SEC_FILE} ]; then \
		make gl-add-sec ; \
	fi
	@make gl-set-slack
	@make gl-add-trigger

.PHONY: push-new-tag
push-new-tag: ## tag HEAD w/ the current date time and push the tag to deploy to GITLAB production pipeline (if set up)
	@if git tag --contains HEAD | grep -q "^${PUSH_TAG_PREFIX}"; then \
		prefixed_tags=$$(git tag --contains HEAD | grep "^${PUSH_TAG_PREFIX}"); \
		>&2 echo "already tag(s) for HEAD w/ prefix '${PUSH_TAG_PREFIX}': '$$prefixed_tags'"; \
		exit 1; \
	else \
		git tag ${PUSH_TAG_PREFIX}-$$(date +%Y-%m-%d_%H-%M-%S) && \
		git push --tags; \
	fi

.PHONY: gl-new-repo
gl-new-repo: gl-init  ## create a new gitlab repo
	@${SCRIPTS_DIR}/gitlab/gl-new-repo.sh
	@make gl-set-slack
	@echo Created git@${GITLAB_SERVER}:${GITLAB_REPO}.git
	@echo Please setup the local git repo by:
	@echo git init
	@echo git remote add origin git@${GITLAB_SERVER}:${GITLAB_REPO}.git
	@echo git add .
	@echo git commit -m "Initial commit"
	@echo git push -u origin master

## end of gitlab.mk
