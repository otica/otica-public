## gitlab.mk

ifndef GITLAB_SERVER
	missing_vars := ${missing_vars} GITLAB_SERVER
endif

.PHONY: gl-ls-runners
gl-ls-runners: gl-init  ##  List all runners
	@echo; ${SCRIPTS_DIR}/gitlab/gl-ls-runners.sh

.PHONY: gl-get-runner
gl-get-runner: ID=
gl-get-runner: ##  Get runner by runner name (i.e. description) or id. make ID=<number or desc>
	@if [ ! -z "${ID}" ]; then \
		echo; ${SCRIPTS_DIR}/gitlab/gl-get-runner.sh ${ID}; \
	else \
		echo "make gl-get-runner ID=<number or desc>" ; \
	fi

.PHONY: gl-get-runner-token
gl-get-runner-token: gl-init  ##  Get runner token by runner name (i.e. description)
	@if [ -z "${GITLAB_RUNNER_NAME}" ]; then \
		echo "GITLAB_RUNNER_NAME is not defined!"; \
	else \
		echo; ${SCRIPTS_DIR}/gitlab/gl-get-runner-token.sh ${GITLAB_RUNNER_NAME}; \
	fi

.PHONY: gl-clean-offline-runners
gl-clean-offline-runners: ID=
gl-clean-offline-runners:  ##  Remove offline runners
	@if [ ! -z "${ID}" ]; then \
		${SCRIPTS_DIR}/gitlab/gl-clean-offline-runners.sh ${ID} ; \
	else \
		${SCRIPTS_DIR}/gitlab/gl-ls-runners-status.sh offline ; \
		${SCRIPTS_DIR}/confirm.sh "Remove runners?" ; \
		${SCRIPTS_DIR}/gitlab/gl-clean-offline-runners.sh; \
	fi

.PHONY: gl-ls-runner-jobs
gl-ls-runner-jobs: ID=
gl-ls-runner-jobs:  ## list runner's jobs
	@if [ ! -z "${ID}" ]; then \
		${SCRIPTS_DIR}/gitlab/gl-ls-runner-jobs.sh ${ID} ; \
	fi

.PHONY: gl-ls-group-runners.sh
gl-ls-group-runners: GROUP=
gl-ls-group-runners: ## list group runners make GROUP=<group name>
	@${SCRIPTS_DIR}/gitlab/gl-ls-group-runners.sh ${GROUP}

.PHONY: gl-ls-runners-status
gl-ls-runners-status: STATUS=
gl-ls-runners-status: ## list runner status. make gl-ls-runner-status STATUS=<active,pause,online,offline>
	@${SCRIPTS_DIR}/gitlab/gl-ls-runners-status.sh ${STATUS}

.PHONY: gl-new-repo
gl-new-repo: gl-init  ## create a new gitlab repo
	@${SCRIPTS_DIR}/gitlab/gl-new-repo.sh
	@make gl-set-slack
	@echo Created git@${GITLAB_SERVER}:${GITLAB_REPO}.git
	@echo Please setup the local git repo by:
	@echo git init
	@echo git remote add origin git@${GITLAB_SERVER}:${GITLAB_REPO}.git
	@echo git add .
	@echo git commit -m "Initial commit"
	@echo git push -u origin master

## end of gitlab.mk
