##########
## KUBE ##
##########

ifndef KUBE_PLATFORM
	missing_vars := ${missing_vars} KUBE_PLATFORM
endif

ifeq ($(KUBE_PLATFORM), kube-vault)
	ifndef KUBE_CONTEXT
		missing_vars := ${missing_vars} KUBE_CONTEXT
	endif
else
	ifndef KUBE_CLUSTER_NAME
		missing_vars := ${missing_vars} KUBE_CLUSTER_NAME
	endif
endif

ifndef KUBE_NAMESPACE
	missing_vars := ${missing_vars} KUBE_NAMESPACE
endif

ifndef KUBE_TEMPLATES
	KUBE_TEMPLATES := ${COMMON}/templates
endif

ifndef KUBE_TEMPLATES_LOCAL
	KUBE_TEMPLATES_LOCAL := templates
endif

ifndef KUBE_BUILD_DIR
	KUBE_BUILD_DIR := ${BUILD_DIR}/templates
endif

ifneq ($(missing_vars),)
	_ := $(info )
	_ := $(info missing env var(s):)
	_ := $(info )
	_ := $(info ${missing_vars}))
	_ := $(info )
	_ := $(info you must populate the required env vars before continuing)
	_ := $(info )
	_ := $(error )
endif

export

####
# NAMESPACE
####

.PHONY: kube-namespace-create
kube-namespace-create: kube-config ## kube create namespace using KUBE_NAMESPACE
	@kubectl create namespace ${KUBE_NAMESPACE} || exit 0

.PHONY: kube-namespace-show
kube-namespace-show: kube-config ## show current Kubernetes namespace
	@${SCRIPTS_DIR}/kube-namespace-show.sh

####
# AUTHENTICATION AND CONFIGURATION
####

.PHONY: kube-creds
kube-creds:
	@${SCRIPTS_DIR}/auth/kube-get-credentials.sh

.PHONY: kube-config
kube-config: kube-creds ## authenticate and configure Kubernetes cluster
	@${SCRIPTS_DIR}/auth/kube-config-context.sh

####
# DEPLOY
####

.PHONY: kube-deploy-init
kube-deploy-init: kube-config # initialize the kube-deploy build dir
	@if ! git check-ignore "${KUBE_BUILD_DIR}/" &> /dev/null ; then \
		printf '\e[1;31m%s\n' "!!! ERROR !!!" ; \
		printf '%s\e[m\n' "Please add \"${KUBE_BUILD_DIR}\" to .gitignore" ; \
		exit 1 ; \
	fi
	@if [[ ! -f ${KUBE_BUILD_DIR} ]] ; then mkdir -p ${KUBE_BUILD_DIR} ; fi

.PHONY: kube-deploy-render
kube-deploy-render: kube-deploy-init ## render the kubernetes deployment templates
	@if [ -d ${KUBE_TEMPLATES} ]; then \
		for f in `ls -p ${KUBE_TEMPLATES} | grep -v '/ | egrep "\.(json|yaml|yml)"'`; do \
			cat ${KUBE_TEMPLATES}/$${f} | render.sh > ${KUBE_BUILD_DIR}/$${f}; \
		done ; \
	fi
	@if [ -d ${KUBE_TEMPLATES_LOCAL} ]; then \
		for f in `ls -p ${KUBE_TEMPLATES_LOCAL} | grep -v '/ | egrep "\.(json|yaml|yml)"'`; do \
			cat ${KUBE_TEMPLATES_LOCAL}/$${f} | render.sh > ${KUBE_BUILD_DIR}/$${f}; \
		done ; \
	fi
	@echo "Deployment templates have been rendered to ${KUBE_BUILD_DIR}"

.PHONY: kube-deploy-apply
kube-deploy-apply: kube-deploy-render kube-namespace-create ## apply the kubernetes deployment
	@if [[ "$(shell ls -A ${KUBE_BUILD_DIR})" ]] \
		&& ls -1 ${KUBE_BUILD_DIR} | egrep "\.(json|yaml|yml)" > /dev/null 2>&1 ; \
		then kubectl apply -f ${KUBE_BUILD_DIR} ; \
	else \
		echo "No .(json|yaml|yml) files; skip kubectl apply" ; \
	fi

.PHONY: kube-deploy-delete
kube-deploy-delete: kube-deploy-render ## delete the kubernetes deployment
	@if [[ "$(shell ls -A ${KUBE_BUILD_DIR})" ]] \
		&& ls -1 ${KUBE_BUILD_DIR} | egrep "\.(json|yaml|yml)" > /dev/null 2>&1 ; \
		then kubectl delete --ignore-not-found -f ${KUBE_BUILD_DIR} ; \
	else \
		echo "No .(json|yaml|yml) files; skip kubectl delete" ; \
	fi

####
# FAILED PODS
####

.PHONY: kube-failed-pods-ls
kube-failed-pods-ls: kube-config ## list failed pods in the current namespace
	@kube-failed-pods.sh -l ${APP_NAMESPACE}

.PHONY: kube-failed-pods-rm
kube-failed-pods-rm: kube-config ## delete failed pods in the current namespace
	@kube-failed-pods.sh -d ${APP_NAMESPACE}

####
# SHOW
####

.PHONY: kube-show-resources
kube-show-resources: kube-config ## show all Kubernetes resources in the current namespace
	@kubectl get all

.PHONY: kube-show-env
kube-show-env: ## show Kubernetes environment variables
	@kube-show-env.sh

.PHONY: kube-cert-expiration
kube-cert-expiration: ## show Kubernetes cluster certificate expiration
	@gcloud container clusters describe ${GKE_CLUSTER_NAME} --region ${GCP_REGION} \
	--format "value(masterAuth.clusterCaCertificate)" | base64 --decode | openssl x509 -noout -dates

####
# SSH
####

.PHONY: ssh
ssh: kube-config ## "login" to a Pod (optional: set NODE=N to connect to the Nth Pod)
	@${SCRIPTS_DIR}/kube/pod-login

# EOF
