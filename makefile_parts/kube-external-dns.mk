################################################################################
# external-dns.mk
################################################################################

HELM=helm

ifeq ($(EXTERNAL_DNS_VERSION),)
	missing_vars := ${missing_vars} EXTERNAL_DNS_VERSION
endif

ifeq ($(CLOUD_PLATFORM),)
	missing_vars := ${missing_vars} CLOUD_PLATFORM
endif

ifeq ($(KUBE_PLATFORM),)
	missing_vars := ${missing_vars} KUBE_PLATFORM
endif

ifneq ($(missing_vars),)
	_ := $(info )
	_ := $(info missing env var(s):)
	_ := $(info )
	_ := $(info ${missing_vars}))
	_ := $(info )
	_ := $(info you must populate the required env vars before continuing)
	_ := $(info )
	_ := $(error )
endif

# These are the values.yaml override files we want to include (order is important).
ifndef HELM_YAML_FILES
	export HELM_YAML_FILES=../common/values-local.yaml values-local.yaml ${FRAMEWORK_DIR}/helm-charts/external-dns/${EXTERNAL_DNS_VERSION}/values.tmpl
endif

ifndef EXTERNAL_DNS_KUBE_NAMESPACE
	export EXTERNAL_DNS_KUBE_NAMESPACE := kube-system
endif

# Set helm-wrapper.py environmnent variables.
export APP_NAMESPACE=${EXTERNAL_DNS_KUBE_NAMESPACE}
export HELM_CHART_VERSION=${EXTERNAL_DNS_VERSION}
export HELM_CHART_NAME=external-dns
export HELM_RELEASE=${HELM_CHART_NAME}
export HELM_REPO_URL=https://charts.bitnami.com/bitnami
export HELM_REPO_NAME=bitnami

#ifndef EXTERNAL_DNS_VERSION
#	export EXTERNAL_DNS_VERSION=v2.21.0
#endif

ifndef EXTERNAL_DNS_GOOGLE_PROJECT
	export EXTERNAL_DNS_GOOGLE_PROJECT=${GCP_PROJECT_ID}
endif

ifndef EXTERNAL_DNS_GCP_CREDENTIALS_PATH
	export EXTERNAL_DNS_GCP_CREDENTIALS_PATH=secret/projects/${GCP_PROJECT_NAME}/common/dns-admin-key
endif

ifndef TXT_OWNER_ID
	TXT_OWNER_ID := ${KUBE_CLUSTER_NAME}
endif

#ifndef ACME_DNS_PROVIDER
#	ACME_DNS_PROVIDER=${EXTERNAL_DNS_GOOGLE_PROJECT}-dns
#endif

# Turn external-dns manager to reduce the clouddns api polling
ifndef EXTERNAL_DNS_EVENTS
	EXTERNAL_DNS_EVENTS=true
endif
ifndef EXTERNAL_DNS_INTERVAL
	EXTERNAL_DNS_INTERVAL=30m
endif

.PHONY: exdns-check-env
exdns-check-env:
	@VERBOSE=0 kube-external-dns-variable-check.sh

.PHONY: exdns-show-env
exdns-show-env: exdns-check-env ## show ExternalDNS-relevant environment variables
	@VERBOSE=1 kube-external-dns-variable-check.sh

.PHONY: exdns-template
exdns-template: EXTERNAL_DNS_GCP_CREDENTIALS=""
exdns-template: vault-login
exdns-template: exdns-check-env ## render Helm templates
	@helm-wrapper.py template

.PHONY: exdns-show-diff
exdns-show-diff: EXTERNAL_DNS_GCP_CREDENTIALS=""
exdns-show-diff: vault-login
exdns-show-diff: exdns-check-env ## show difference between current and proposed Helm release templates
	@EXTERNAL_DNS_GCP_CREDENTIALS=`vault-read.sh ${EXTERNAL_DNS_GCP_CREDENTIALS_PATH}` ; \
	helm-wrapper.py diff

.PHONY: exdns-repo-update
exdns-repo-update: exdns-check-env ## update ExternalDNS Helm repository
	@${HELM} repo add ${HELM_REPO_NAME} ${HELM_REPO_URL} || true
	@${HELM} repo update ${HELM_REPO_NAME}

.PHONY: exdns-repo-list-versions
exdns-repo-list-versions: exdns-repo-update ## list all versions of the ExternalDNS chart
	@${HELM} search repo ${HELM_CHART_NAME} -l

.PHONY: exdns-upgrade
exdns-upgrade: EXTERNAL_DNS_GCP_CREDENTIALS=""
exdns-upgrade: vault-login
exdns-upgrade: kube-config exdns-repo-update ## upgrade the current ExternalDNS Helm release
	@EXTERNAL_DNS_GCP_CREDENTIALS=`vault-read.sh ${EXTERNAL_DNS_GCP_CREDENTIALS_PATH}` ; \
	helm-wrapper.py upgrade

.PHONY: exdns-create-ns
exdns-create-ns: exdns-check-env kube-config ## create ExternalDNS Kubernetes namespace
	@if ! kubectl get namespace ${EXTERNAL_DNS_NS} &> /dev/null ; then \
		kubectl create namespace ${EXTERNAL_DNS_NS} ; \
	fi

.PHONY: exdns-list
exdns-list: exdns-check-env kube-config ## show the current ExternalDNS Helm release
	@${HELM} list

.PHONY: exdns-show-details
exdns-show-details: exdns-check-env kube-config ## show details for the current ExternalDNS Helm release
	@${HELM} get all ${HELM_RELEASE}

.PHONY: exdns-install
exdns-install: EXTERNAL_DNS_GCP_CREDENTIALS=""
exdns-install: vault-login
exdns-install: exdns-check-env exdns-create-ns exdns-repo-update ## install ExternalDNS
	@if ! ${HELM} ls -n ${EXTERNAL_DNS_NS} | grep external-dns &>/dev/null ; \
	then \
		EXTERNAL_DNS_GCP_CREDENTIALS=`vault-read.sh ${EXTERNAL_DNS_GCP_CREDENTIALS_PATH}` ; \
		helm-wrapper.py install;
	fi

.PHONY: exdns-uninstall
exdns-uninstall: exdns-check-env kube-config ## destroy the current ExternalDNS Helm release
	@if ${HELM} ls -n ${EXTERNAL_DNS_NS} | grep external-dns &>/dev/null; \
	then \
		${HELM} del -n ${EXTERNAL_DNS_NS} ${HELM_RELEASE}; \
	fi

.PHONY: exdns-values-merge
exdns-values-merge: EXTERNAL_DNS_GCP_CREDENTIALS=""
exdns-values-merge: vault-login
exdns-values-merge: exdns-check-env ## show merged values.yaml files
	@EXTERNAL_DNS_GCP_CREDENTIALS=`vault-read.sh ${EXTERNAL_DNS_GCP_CREDENTIALS_PATH}` ; \
	helm-wrapper.py merge



###########
## Test
###########
#.PHONY: test-external-dns-ingress
#test-external-dns-ingress: vault-login config-kube ## deploy dns test (ingress) pod/service
#	@helm_chart=external-dns-test-ingress \
#		helm_release_name=external-dns-test-ingress \
#		${SCRIPTS_DIR}/helm-install.sh
#	@echo Test URL http://external-dns-ing.${GCP_DNS_DOMAIN}
#
#.PHONY: destroy-test-external-dns-ingress
#destroy-test-external-dns-ingress: vault-login config-kube ## destroy dns test (ingress) pod/service
#	@helm_release_name=external-dns-test-ingress \
#		${SCRIPTS_DIR}/helm-delete.sh
#
#.PHONY: test-external-dns-service
#test-external-dns-service: vault-login config-kube ## deploy dns test (service) pod/service
#	@helm_chart=external-dns-test-service \
#		helm_release_name=external-dns-test-service \
#		${SCRIPTS_DIR}/helm-install.sh
#	@echo Test URL http://external-dns-svc.${GCP_DNS_DOMAIN}
#
#.PHONY: destroy-test-external-dns-service
#destroy-test-external-dns-service: vault-login config-kube ## destroy dns test (service) pod/service
#	@helm_release_name=external-dns-test-service \
#		${SCRIPTS_DIR}/helm-delete.sh
#
#.PHONY: test-external-dns
#test-external-dns: test-external-dns-service ## deploy dns test (service) pod/service
#
#.PHONY: destroy-test-external-dns
#destroy-test-external-dns: destroy-test-external-dns-service ## destroy dns test (service) pod/service

## end of external-dns.mk
