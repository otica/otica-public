# Makefile to for developing and publising the Otica framework

ifndef FRAMEWORK_BUILD_DIR
  FRAMEWORK_BUILD_DIR=build
endif

ifndef FRAMEWORK_BUILD_DIR
  FRAMEWORK_RELEASE_REPO_BRANCH=master
endif

.PHONY: fw-check
fw-check: ## check that needed environment variables are set
	@framework-develop.py check-env

.PHONY: fw-show-env
fw-show-env: fw-make-build-dir ## show the framework development environment variables
	@framework-develop.py show-env

.PHONY: fw-make-build-dir
fw-make-build-dir:
	@mkdir -p ${FRAMEWORK_BUILD_DIR}

.PHONY: fw-tarball
fw-tarball: fw-make-build-dir ## create the distribution tarball and store in build/
	@framework-develop.py tarball

.PHONY: fw-show-versions
fw-show-versions: ## show the source and release Otica versions
	@framework-develop.py show-versions

.PHONY: fw-deploy-diff
fw-deploy-diff: ## show the git diff between the current repo and public repository
	@framework-develop.py deploy-diff

.PHONY: fw-deploy-status
fw-deploy-status: ## show files that are changing
	@framework-develop.py deploy-status

.PHONY: fw-deploy-commit
fw-deploy-commit: ## commit and push to release repository
	@framework-develop.py deploy-commit
