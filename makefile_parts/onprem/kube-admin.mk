ifndef K3S_SERVER
	missing_vars := ${missing_vars} K3S_SERVER
endif

ifndef K3S_API_HOST
	missing_vars := ${missing_vars} K3S_API_HOST
endif

ifndef KUBE_CLUSTER_NAME
	missing_vars := ${missing_vars} KUBE_CLUSTER_NAME
endif

ifndef KUBE_CLUSTER_DOMAIN_NAME
	missing_vars := ${missing_vars} KUBE_CLUSTER_DOMAIN_NAME
endif

ifndef KUBE_SEC_PATH
	missing_vars := ${missing_vars} KUBE_SEC_PATH
endif

ifndef KUBECONFIG_SEC_PATH
	KUBECONFIG_SEC_PATH := ${KUBE_SEC_PATH}/kubeconfig.admin
endif

ifndef OIDC_KUBECONFIG_SEC_PATH
	OIDC_KUBECONFIG_SEC_PATH := ${KUBE_SEC_PATH}/kubeconfig.oidc
endif

ifndef OIDC_RBAC_TEMPLATE
	OIDC_RBAC_TEMPLATE=${FRAMEWORK_DIR}/artifacts/kube-oidc/oidc-rbac.yaml
endif

.PHONY: ssh-node
ssh-node: ## ssh tunnel to a node: make ssh-node NODE=<node>
	@if [[ -z "${NODE}" ]]; then \
		make get-cluster-info ; \
		echo; echo Please select a node to ssh into, then run "make ssh-node NODE=<node>" ; \
	else \
		ssh kube-admin@${NODE} -q -i ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.key \
		-o "StrictHostKeyChecking=no" ; \
	fi

.PHONY: get-cluster-info
get-cluster-info:  ## get cluster info
	kubectl cluster-info
	kubectl get nodes -o wide

.PHONY: ssh-server
ssh-server: NODE=${K3S_SERVER}
ssh-server: ssh-node ## ssh to kube server

.PHONY: get-kubeconfig-from-server
get-kubeconfig-from-server: NODE=${K3S_SERVER}
get-kubeconfig-from-server: vault-login ## get cluster admin kubeconfig from server
	ssh kube-admin@${NODE} -q \
		-i ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.key \
		-o "StrictHostKeyChecking=no" \
		"sudo cat /etc/rancher/k3s/k3s.yaml" > .kubeconfig
	sed -i '' 's/127.0.0.1/${K3S_API_HOST}/g' .kubeconfig
	sed -i '' 's/default/${KUBE_CLUSTER_NAME}/g' .kubeconfig
	vault-write.sh ${KUBECONFIG_SEC_PATH} @.kubeconfig

.PHONY: get-kubeconfig-from-vault
get-kubeconfig-from-vault: vault-login ## get cluster admin kubeconfig from vault
	vault-read.sh ${KUBECONFIG_SEC_PATH} >.kubeconfig
	@echo "export KUBECONFIG=.kubeconfig # For kubectl auth"

.PHONY: merge-kubeconfig
merge-kubeconfig: get-kubeconfig-from-vault ## merge cluster admin kubeconfig to KUBECONFIG
	@if kubectl config get-contexts | grep ${KUBE_CLUSTER_NAME} &> /dev/null ; then \
		echo "Context ${KUBE_CLUSTER_NAME} is already in KUBECONFIG"; \
	else \
		make get-kubeconfig-from-vault; \
		merge-kubeconfig.sh .kubeconfig; \
	fi

.PHONY: use-kubeconfig
use-kubeconfig: merge-kubeconfig ## switch kubeconfig context to ${KUBE_CLUSTER_NAME}
	kubectl config use ${KUBE_CLUSTER_NAME}

.PHONY: apply-oidc-rbac
apply-oidc-rbac: ## apply OIDC RBAC
	cat ${OIDC_RBAC_TEMPLATE} | render.sh | kubectl apply -f -

# Generate kubeconfig for OIDC auth (through kubelogin)
# NOTE: do 'make get-kubeconfig' first!
.PHONY: create-oidc-kubeconfig
create-oidc-kubeconfig: KUBECONFIG_TEMPLATE=${FRAMEWORK_DIR}/artifacts/kube-oidc/kubeconfig-oidc.yaml
create-oidc-kubeconfig: ## generate kubeconfig for OIDC kubelogin
	@if [ -f "${OIDC_CLIENT_SECRET}" ]; then \
		echo "Error: Envvar OIDC_CLIENT_SECRET is not defined"; \
		exit 1; \
	fi
	@if [ -f "${OIDC_CLINENT_ID}" ]; then \
		echo "Error: Envvar OIDC_CLINENT_ID is not defined"; \
		exit 1; \
	fi
	@if [ -f "${OIDC_ISSUER_URL}" ]; then \
		echo "Error: Envvar OIDC_ISSUER_URL is not defined"; \
		exit 1; \
	fi
	@if [ -f ".kubeconfg" ]; then \
		make get-admin-kubeconfig-from-vault; \
	fi
	export KUBE_CA=$(shell kubectl --kubeconfig .kubeconfig config view --raw -o json | jq -r '.clusters[0].cluster."certificate-authority-data"'); \
	cat ${KUBECONFIG_TEMPLATE} | render.sh > .kubeconfig.oidc
	vault-write.sh ${OIDC_KUBECONFIG_SEC_PATH} @.kubeconfig.oidc
	@echo "export KUBECONFIG=.kubeconfig.oidc # For kubectl auth"

.PHONY: get-kube-ca
get-kube-ca: ## get kube api ca from .kubeconfig 
	if [ ! -f .kubeconfig-ca ]; then \
		if [ ! -f .kubeconfig ]; then \
			make get-admin-kubeconfig-from-vault; \
		fi; \
		kubectl --kubeconfig .kubeconfig config view --raw -o json | jq -r '.clusters[0].cluster."certificate-authority-data"' > .kubeconfig-ca

# https://github.com/rancher/k3os/issues/372
.PHONY: reset-node-passwd
reset-node-passwd: servers=${K3S_SERVER_IPS}
reset-node-passwd: config ## reset node passwords to enable node re-regiter
	@for n in ${servers}; do \
		echo removing ${servers}:/var/lib/rancher/k3s/server/cred/node-passwd ; \
		ssh kube-admin@$$n -q -i ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.key \
			-o "StrictHostKeyChecking=no" -- sudo rm /var/lib/rancher/k3s/server/cred/node-passwd ;  \
	done

.PHONY: purge-nodes
purge-nodes: ## delete NotReady nodes
	@if kubectl get nodes | grep ${KUBE_CLUSTER_NAME}- | grep NotReady; then \
		confirm.sh "Delete all NotReady nodes?" ; \
		kubectl get nodes | grep ${KUBE_CLUSTER_NAME}- | grep NotReady | awk '{print $$1}' | xargs kubectl delete node ; \
	fi
	@kubectl get nodes

.PHONY: get-cluster-sec
get-cluster-sec: vault-login ## get cluster credentials from vault
	@if vault-read.sh ${KUBE_SEC_PATH}/ssh_id_rsa &> /dev/null ; then \
		vault-read.sh ${KUBE_SEC_PATH}/ssh_id_rsa > ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.key ; \
		vault-read.sh ${KUBE_SEC_PATH}/ssh_id_rsa.pub > ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.pub ; \
		chmod 600  ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.key; \
	else \
		echo "No cluster sec found at ${KUBE_SEC_PATH}"; \
	fi

.PHONY: new-cluster-sec
new-cluster-sec: vault-login ## generate new set of cluster credentials and save to vault
	@if vault-read.sh ${KUBE_SEC_PATH}/ssh_id_rsa &> /dev/null ; then \
		vault-read.sh ${KUBE_SEC_PATH}/ssh_id_rsa > ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.key ; \
		vault-read.sh ${KUBE_SEC_PATH}/ssh_id_rsa.pub > ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.pub ; \
		chmod 600  ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.key; \
	else \
		make renew-cluster-sec; \
	fi

.PHONY: renew-cluster-sec
renew-cluster-sec: vault-login ## renew cluster credentials and save to vault
	vault-write.sh ${KUBE_SEC_PATH}/ssh_user kube-admin
	vault-pwgen.sh ${KUBE_SEC_PATH}/ssh_password
	vault-pwgen.sh ${KUBE_SEC_PATH}/cluster_token 99
	new-gcs-key.sh  ${KUBE_SEC_PATH}/aescbc.key
	ssh-keygen -b 4096 -t rsa -f id_rsa -q -N "" -C ${KUBE_CLUSTER_NAME}@${KUBE_CLUSTER_DOMAIN_NAME} -m PEM
	vault-write.sh ${KUBE_SEC_PATH}/ssh_id_rsa @id_rsa
	vault-write.sh ${KUBE_SEC_PATH}/ssh_id_rsa.pub @id_rsa.pub
	mv id_rsa ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.key
	mv id_rsa.pub ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.pub
	chmod 600 ${HOME}/.ssh/${KUBE_CLUSTER_NAME}.pub

.PHONY: reset-ssh-hosts
reset-ssh-hosts: ## reset ~/.ssh/known_hosts
	sed -i '' '/^${K3S_API_HOST}/d' ${HOME}/.ssh/known_hosts
