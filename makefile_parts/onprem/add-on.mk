ifndef CONTOUR_MANIFEST
	CONTOUR_MANIFEST := https://projectcontour.io/quickstart/contour.yaml
endif
ifndef LONGHORN_MANIFEST
	LONGHORN_MANIFEST := ${FRAMEWORK_DIR}/artifacts/longhorn/longhorn.yaml
endif
ifndef DASHBOARD_MANIFEST_DIR
	DASHBOARD_MANIFEST_DIR := ${FRAMEWORK_DIR}/artifacts/kube-dashboard
endif
ifndef DASHBOARD_MANIFEST
	DASHBOARD_MANIFEST := ${DASHBOARD_MANIFEST_DIR}/dashboard-oidc-v2.0.4.yaml
endif

# See https://projectcontour.io/getting-started/
# for use on K3s, treafik and serverlb must disabled
.PHONY: install-contour
install-contour: ## install contour ingress controller
	kubectl apply -f ${CONTOUR_MANIFEST}
	kubectl get -n projectcontour service envoy -o wide

.PHONY: destroy-contour
destroy-contour: ## destroy contour ingress controller
	kubectl delete -f ${CONTOUR_MANIFEST}

.PHONY: contour-info
contour-info: ## get contour ingress info
	kubectl get -n projectcontour service envoy -o wide

.PHONY: install-dashboard
install-dashboard: ## install kube dashboard
	kube_apply.sh ${DASHBOARD_MANIFEST}
	kubectl -n kubernetes-dashboard rollout status deploy/kubernetes-dashboard

.PHONY: gen-oidc-session-secret
gen-oidc-session-secret: vault-login ## Gernate OIDC session secret for dashboard oidc
	@if ! valut-read.sh  ${OIDC_CLIENT_SEC_PATH}/session_secret > /dev/null 2>&1; then \
		vault-write.sh ${OIDC_CLIENT_SEC_PATH}/session_secret $(shell pwgen.sh 32); \
	fi

update-dashboard: ## update kube dashboard image
	kubectl -n kubernetes-dashboard delete $(shell kubectl -n kubernetes-dashboard get pod -o name | grep dashboard)

.PHONY: dashboard-sa
dashboard-sa: ## create dashboard service accounts
	@kubectl apply -f ${DASHBOARD_MANIFEST_DIR}/dashboard-sa.yaml
	@get-svc-kubeconfig.sh kubernetes-dashboard kube-admin > .kubeconfig.admin_sa
	@get-svc-kubeconfig.sh kubernetes-dashboard kube-view > .kubeconfig.view_sa

.PHONY: destroy-dashboard
destroy-dashboard: ## destroy kube dashboard
	kube_delete.sh ${DASHBOARD_MANIFEST}
	
.PHONY: dashboard
dashboard: #dashboard-sa ## open kubernetes dashboard
	@echo "Dashboard auth kubeconfig is admin_kubeconfig/viw_kubeconfig"
	@if kubectl krew list | grep auth-proxy &> /dev/null ; then \
		kubectl auth-proxy -n kubernetes-dashboard http://kubernetes-dashboard.svc; \
	else \
		(sleep 3; open http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/) & \
		kubectl proxy; \
	fi

# Ref: https://github.com/longhorn/longhorn
.PHONY: install-longhorn
install-longhorn: ## install kube dashboard
	kubectl apply -f ${LONGHORN_MANIFEST}
	kubectl -n longhorn-system rollout status deploy/longhorn-driver-deployer 
	kubectl -n longhorn-system rollout status  ds/longhorn-manager
	kubectl -n longhorn-system rollout status deploy/longhorn-ui 

.PHONY: longhorn-ui
longhorn-ui: ## open kubernetes dashboar
	(sleep 3; open http://localhost:8080) &
	kubectl -n longhorn-system port-forward svc/longhorn-frontend 8080:80
