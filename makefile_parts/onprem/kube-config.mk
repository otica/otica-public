ifndef KUBE_CLUSTER_NAME
	missing_vars := ${missing_vars} KUBE_CLUSTER_NAME
endif

ifndef KUBE_SEC_PATH
	missing_vars := ${missing_vars} KUBE_SEC_PATH
endif

ifndef OIDC_REALM
	missing_vars := ${missing_vars} OIDC_REALM
endif

ifndef OIDC_KUBECONFIG_SEC_PATH
	OIDC_KUBECONFIG_SEC_PATH := ${KUBE_SEC_PATH}/kubeconfig.oidc
endif


.PHONY: use-kubelogin
use-kubelogin: install-kubelogin merge-oidc-kubeconfig ## setup client to enable OIDC kubelogin
	@if ! kubectl config get-contexts | grep ${KUBE_CLUSTER_NAME} &> /dev/null ; then \
		make merge-oidc-kubeconfig ; \
	fi
	kubectl config use ${KUBE_CLUSTER_NAME}

.PHONY: merge-oidc-kubeconfig
merge-oidc-kubeconfig: ## get .kubeconfig.oidc from vault and merge into KUBECONFIG
	@if kubectl config get-contexts | grep ${KUBE_CLUSTER_NAME} &> /dev/null ; then \
		echo "Context ${KUBE_CLUSTER_NAME} is already in KUBECONFIG"; \
	else \
		make vault-login; \
		vault-read.sh ${OIDC_KUBECONFIG_SEC_PATH} >.kubeconfig.oidc; \
		merge-kubeconfig.sh .kubeconfig.oidc; \
	fi

.PHONY: kubelogout
kubelogout: ## revoke kube OIDC auth token
	rm -rf ${HOME}/.kube/cache/oidc-login

# see https://github.com/int128/kubelogin
.PHONY: install-kubelogin
install-kubelogin: ## install kubelogin and kauthproxy
	@if ! kubectl krew list | grep oidc-login &> /dev/null ; then \
		kubectl krew install oidc-login ; \
	fi
	@if ! kubectl krew list | grep auth-proxy &> /dev/null ; then \
		kubectl krew install auth-proxy; \
	fi
	@echo oidc-login and auth-proxy installed

# user's ${OIDC_REALM} username should be ${USER}.local
# user's ${OIDC_REALM} password be populated by provision-readhat-sso at vault://user/${USER}/${APP_ENV}/${OIDC_REALM}/password
# pbcopy: command not found, see https://ostechnix.com/how-to-use-pbcopy-and-pbpaste-commands-on-linux/
.PHONY: mypass
mypass: APP_ENV := prod
mypass: user := ${USER}
mypass: vault-login ## get user's kubelogin password from vault to pbcopy
	vault-read.sh user/${user}/${OIDC_REALM}/${APP_ENV}/username
	@echo
	vault-read.sh user/${user}/${OIDC_REALM}/${APP_ENV}/password | pbcopy
	