ifndef VSPHERE_SERVER
	missing_vars := ${missing_vars} VSPHERE_SERVER
endif

ifndef VSPHERE_USER
	missing_vars := ${missing_vars} VSPHERE_USER
endif

ifndef VSPHERE_PASSWORD
	missing_vars := ${missing_vars} VSPHERE_PASSWORD
endif

ifndef VSPHERE_DATACENTER
	missing_vars := ${missing_vars} VSPHERE_DATACENTER
endif

ifndef HELM_CHART_DIR
	HELM_CHART_DIR=${FRAMEWORK_DIR}/helm-charts
endif

ifndef CPI_CSI_HELM_CHART
	CSI_HELM_CHART := ${HELM_CHART_DIR}/vsphere-cpi-csi/v2.0.1
endif

ifndef CSI_VALUE_TEMPLATE 
	CSI_VALUE_TEMPLATE:= ${HELM_CHART_DIR}/vsphere-cpi-csi/values.yaml
endif

# https://vsphere-csi-driver.sigs.k8s.io/
# https://github.com/kubernetes-sigs/vsphere-csi-driver
# https://github.com/kubernetes-sigs/vsphere-csi-driver/tree/master/manifests
# https://cloud-provider-vsphere.sigs.k8s.io/tutorials/kubernetes-on-vsphere-with-kubeadm.html
.PHONY: install-csi
install-csi: KUBECONFIG=.kubeconfig
install-csi: envvars ## install Kubernetes vSphere CPI & CSI
	cat ${CSI_VALUE_TEMPLATE} | render.sh | \
	helm install -f - vsphere-cpi-csi ${CSI_HELM_CHART} # --dry-run 

.PHONY: destroy-csi
destroy-csi: KUBECONFIG=.kubeconfig
destroy-csi: ## destroy Kubernetes vSphere CPI & CSI
	helm delete vsphere-cpi-csi -n default

.PHONY: upgrade-csi
upgrade-csi: envvars ## upgrade Kubernetes vSphere CPI & CSI
	cat ${CSI_VALUE_TEMPLATE} | render.sh | \
	helm upgrade -f - vsphere-cpi-csi ${CSI_HELM_CHART}
	make check-csi

.PHONY: check-csi
check-csi: ## check Kubernetes vSphere CPI & CSI status
	kubectl describe nodes | egrep "Name:|ProviderID|Taints:"
	@echo
	kubectl -n kube-system get ds/vsphere-cloud-controller-manager
	@echo 
	kubectl get daemonsets vsphere-csi-node --namespace=kube-system
	kubectl get csidrivers
	kubectl get CSINode

.PHONY: test-csi
test-csi: ## test Kubernetes vSphere CPI & CSI
	kubectl apply -f ${HELM_CHART_DIR}/vsphere-cpi-csi/test-csi.yaml

.PHONY: destroy-test-csi
destroy-test-csi: ## destroy test Kubernetes vSphere CPI & CSI
	kubectl delete -f ${HELM_CHART_DIR}/vsphere-cpi-csi/test-csi.yaml