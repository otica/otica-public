##################
## KUBE-CLUSTER ##
##################

ifndef KUBE_PLATFORM
	missing_vars := ${missing_vars} KUBE_PLATFORM
endif

ifndef KUBE_CLUSTER_NAME
	missing_vars := ${missing_vars} KUBE_CLUSTER_NAME
endif

ifndef KUBE_CLUSTER_LOCATION
	missing_vars := ${missing_vars} KUBE_CLUSTER_LOCATION
endif

ifndef KUBE_NAMESPACE
	missing_vars := ${missing_vars} KUBE_NAMESPACE
endif

ifneq ($(missing_vars),)
	_ := $(info )
	_ := $(info missing env var(s):)
	_ := $(info )
	_ := $(info ${missing_vars}))
	_ := $(info )
	_ := $(info you must populate the required env vars before continuing)
	_ := $(info )
	_ := $(error )
endif

export

.PHONY: kube-cluster-deploy-dryrun
kube-cluster-deploy-dryrun: auth ## dryrun the Kubernetes cluster deployment
	@$(MAKE) tf-plan

.PHONY: kube-cluster-deploy
kube-cluster-deploy: auth ## deploy the Kubernetes cluster and other related resources
	@$(MAKE) tf-apply

.PHONY: kube-cluster-destroy
kube-cluster-destroy: kube-config ## destroy the Kubernetes cluster and other related resources
	@printf '\e[1;31m%s\n' "WARNING!!!"
	@echo "This will destroy the $(shell echo -n "${KUBE_PLATFORM}" | tr '[:lower:]' '[:upper:]') Kubernetes cluster \"${KUBE_CLUSTER_NAME}\" hosted in \"${KUBE_CLUSTER_LOCATION}\"."
	@printf '%s\e[m' "Destroy the cluster?"
	@${SCRIPTS_DIR}/confirm.sh
	@${MAKE} tf-destroy

.PHONY: kube-cluster-failed-pods-ls
kube-cluster-failed-pods-ls: kube-config ## list failed pods in ALL namespaces
	@kube-failed-pods.sh -L

.PHONY: kube-cluster-failed-pods-rm
kube-cluster-failed-pods-rm: kube-config ## delete failed pods in ALL namespaces
	@kube-failed-pods.sh -D

.PHONY: kube-cluster-nodes-cordon
kube-cluster-nodes-cordon: kube-config ## disable scheduling on a node
	@kubectl get nodes
	@nodes=$$(kubectl get node -o json | jq -r '.items[].metadata.name') ; \
		for i in $${nodes}; do \
			kubectl get node $${i} --no-headers; \
			echo ; echo "Cordon $${i}?" ; \
			${SCRIPTS_DIR}/confirm.sh 2> /dev/null || continue ; \
			kubectl cordon $${i} ; \
		done

.PHONY: kube-cluster-nodes-drain
kube-cluster-nodes-drain: kube-config ## drain workload on a node
	@nodes=$$(kubectl get node -o json | jq -r '.items[] | select (.spec.unschedulable==true) .metadata.name') ; \
		for i in $${nodes}; do \
			kubectl get node $${i} ; \
			echo ; echo "Drain $${i}?" ; \
			${SCRIPTS_DIR}/confirm.sh 2> /dev/null || continue ; \
			kubectl drain $${i} --ignore-daemonsets --delete-local-data ; \
		done
	@echo "No more cordoned nodes to drain."

.PHONY: kube-cluster-nodes-uncordon
kube-cluster-nodes-uncordon: NODES=$(shell kubectl get node -o json | jq -r '.items[].metadata.name')
kube-cluster-nodes-uncordon: kube-config ## enable scheduling on a node
	@nodes=$$(kubectl get node -o json | jq -r '.items[] | select (.spec.unschedulable==true) .metadata.name') ; \
		for i in $${nodes}; do \
			kubectl get node $${i} ; \
			echo ; echo "Uncordon $${i}?" ; \
			${SCRIPTS_DIR}/confirm.sh 2> /dev/null || continue ; \
			kubectl uncordon $${i} ; \
		done
	@echo "No more cordoned nodes to uncordon."

.PHONY: kube-cluster-show-info
kube-cluster-show-info: kube-config ## show the Kubernetes cluster information
	@echo "---"
	@kubectl cluster-info

.PHONY: kube-cluster-show-nodes-info
kube-cluster-show-nodes-info: kube-config ## show the Kubernetes cluster nodes information
	@echo "---"
	@kubectl get nodes

.PHONY: kube-cluster-show-nodes-ips
kube-cluster-show-nodes-ips: kube-config ## show the Kubernetes cluster nodes IPs
	@echo "---"
	@kubectl get nodes -o custom-columns="NAME:.metadata.name,INTERNAL IP:.status.addresses[?(@.type=='InternalIP')].address,EXTERNAL IP:.status.addresses[?(@.type=='ExternalIP')].address"

.PHONY: kube-cluster-show-pool
kube-cluster-show-pool: kube-config ## in blue/green cluster, show which one is active
	@for item in $$(kubectl get nodes --output=name) ; do \
		printf "%s\n" "$${item}" | tr '\n' '\t' && kubectl get "$${item}" --output=json | \
		jq -r -S '.metadata.labels.generation ' 2>/dev/null ; \
	done

# EOF
