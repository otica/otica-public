###########################################
## NOTE: FOR BACKWARD COMPATIBILITY ONLY ##
## Please use framework.mk instead       ##
###########################################

THIS_DIR := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

_ := $(shell >&2 ${SCRIPTS_DIR}/deprecated.sh "Please use framework.mk instead of shared.mk in otica.yaml")

include ${THIS_DIR}/framework.mk
