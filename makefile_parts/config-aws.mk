##################
## CONFIG - AWS ##
##################

# NOTES:
# aws-config.mk
# Targets for setting up cloud vendor configuration
# Depends on ${SCRIPTS_DIR}

ifeq ($(VAULT_ENABLED),)
	export VAULT_ENABLED=true
endif
ifeq ($(AWS_REGION),)
	export AWS_REGION=us-west-2
endif

# REQUIRED VARS
ifeq ($(AWS_ACCOUNT_ID),)
	missing_vars := ${missing_vars} AWS_ACCOUNT_ID
endif
ifeq ($(AWS_CRED_SECRET),)
	missing_vars := ${missing_vars} AWS_CRED_SECRET
endif
ifeq ($(AWS_DEPLOYMENT_IAM_ROLE),)
	missing_vars := ${missing_vars} AWS_DEPLOYMENT_IAM_ROLE
endif

ifneq ($(missing_vars),)
	_ := $(info )
	_ := $(info missing env var(s):)
	_ := $(info )
	_ := $(info ${missing_vars}))
	_ := $(info )
	_ := $(info you must populate the required env vars before continuing)
	_ := $(info )
	_ := $(error )
endif

####
# TARGETS
####

.PHONY: aws-auth
aws-auth: vault-login ## AWS account authentication
	@if [[ "$(MAKELEVEL)" -eq "0" ]] && [[ ${VAULT_ENABLED} == "true" ]]; then \
		gen-aws-creds.sh "${AWS_CRED_SECRET}" "${AWS_DEPLOYMENT_IAM_ROLE}" ; \
	fi

# EOF
