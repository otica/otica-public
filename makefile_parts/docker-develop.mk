# Makefile to support developing Docker images

# Configuration:
#
# DOCKER_IMAGE: the name of the Docker image (e.g., "myapp")
# DOCKER_IMAGE_TAGS: the tags you want to use when building (white-space delimited).

ifndef DOCKER_BIN
  DOCKER_BIN=docker
endif

ifndef DOCKER_SOURCE_DIR
  DOCKER_SOURCE_DIR=${COMMON}/docker
endif

ifndef DOCKER_BUILD_DIR
  DOCKER_BUILD_DIR=build
endif

# We want to call the "vault-login" make dependency _only_ when
# DOCKER_REGISTRY_LOGIN_TYPE is set to "vault".
ifeq ($(DOCKER_REGISTRY_LOGIN_TYPE),vault)
  VAULT_LOGIN_DEPENDENCY=vault-login
else
  VAULT_LOGIN_DEPENDENCY=
endif

# Add any extra tools needed by this make module.
FRAMEWORK_TOOLS += docker

.PHONY: docker-check
docker-check: ## check that needed environment variables are set
	@docker-wrapper.py check-env

.PHONY: docker-show-env
docker-show-env: docker-prepare-build ## show docker-relevant environment variables
	@docker-wrapper.py show-env

.PHONY: docker-help
docker-help: ## show help for this module
	@show-module-help.sh docker-develop

.PHONY: docker-login
docker-login: docker-check $(VAULT_LOGIN_DEPENDENCY) ## login to private docker registry
	@docker-login.sh

# docker-prepare-build renders copies and renders all the files into the build directory.
# Be sure that your build-args.yaml file (if it exists) starts with "#!envsubst".
.PHONY: docker-prepare-build
docker-prepare-build: ## Populate build/ directory
	@rm -rf ${DOCKER_BUILD_DIR}/
	@mkdir -p ${DOCKER_BUILD_DIR}
	@render-file-dir.sh ${DOCKER_SOURCE_DIR} ${DOCKER_BUILD_DIR}

.PHONY: docker-build
docker-build: docker-check docker-prepare-build ## build Docker image (uses Docker cache)
	@docker-wrapper.py build

.PHONY: docker-kaniko-script
docker-kaniko-script: docker-check docker-prepare-build ## generate Kaniko build script
	@docker-wrapper.py gen-build-script

.PHONY: docker-build-nocache
docker-build-nocache: docker-check docker-prepare-build ## build Docker image (does NOT use Docker cache)
	@docker-wrapper.py --no-cache build

.PHONY: docker-push
docker-push: docker-login ## tag image the tags in DOCKER_IMAGE_TAGS and push
	@docker-wrapper.py tag-and-push

.PHONY: docker-push-dryrun
docker-push-dryrun: docker-check ## tag image the tags in DOCKER_IMAGE_TAGS and push (DRY RUN)
	@docker-wrapper.py --dryrun tag-and-push

