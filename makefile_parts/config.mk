###########################################
## NOTE: FOR BACKWARD COMPATIBILITY ONLY ##
## Please use config-gcp.mk instead      ##
###########################################

THIS_DIR := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))

_ := $(shell >&2 echo "--------------------")
_ := $(shell >&2 printf '\e[1;31m%-6s\e[m\n' "OTICA FRAMEWORK DEPRECATION WARNING")
_ := $(shell >&2 echo "Please use config-gcp.mk instead of config.mk in otica.yaml")
_ := $(shell >&2 echo "--------------------")
_ := $(shell sleep 1)

include ${THIS_DIR}/config-gcp.mk
