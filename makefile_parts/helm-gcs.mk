### Manage/use the GCS Helm plugin

ifndef HELM_BIN
  HELM_BIN=helm
endif

ifdef DEBUG
  HELM_GCS_DEBUG=true
endif

.PHONY: helm-gcs-check
helm-gcs-check: ## check necessary environment variables are set
	@VERBOSE=1 ${SCRIPTS_DIR}/helm-gcs-variable-check.sh

.PHONY: helm-gcs-install
helm-gcs-install: ## install the Google Cloud Storage Helm plugin
	@${SCRIPTS_DIR}/helm-install-plugin.sh gcs https://github.com/hayorov/helm-gcs.git

.PHONY: helm-gcs-init
helm-gcs-init: helm-gcs-install ## initialize the Google Cloud Storage Helm chart repository
	${HELM_BIN} gcs init ${HELM_REPO_URL}

.PHONY: helm-gcs-uninstall
helm-gcs-uninstall: ## remove the Google Cloud Storage Helm plugin
	${HELM_BIN} plugin uninstall gcs

.PHONY: helm-gcs-update
helm-gcs-update: ## update the Google Cloud Storage Helm plugin
	${HELM_BIN} plugin update gcs

