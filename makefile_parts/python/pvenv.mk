VENV_DIR=${BUILD_DIR}/venv

.PHONY: pv-create
pv-create: ## create the Python virtual environment in build/venv
	@mkdir -p ${VENV_DIR}
	@if [[ -z "${PVENV_SYSTEM_SITE_PACKAGES}" ]] ; then \
	  python3 -m venv ${VENV_DIR} ; \
	else \
	  python3 -m venv --system-site-packages ${VENV_DIR} ; \
	fi

.PHONE: pv-activate
pv-activate: ## activate Python virtual environment
	@echo "type this: 'source ${VENV_DIR}/bin/activate'"

.PHONE: pv-deactivate
pv-deactivate: ## de-activate Python virtual environment
	@echo "type this: 'deactivate'"

.PHONE: pv-install
pv-install: pv-activate ## install Python modules
	source ${VENV_DIR}/bin/activate && pip install --upgrade -r ../pv-requirements.txt

.PHONE: pv-list
pv-list: ## list installed Python modules
	source ${VENV_DIR}/bin/activate && pip list
