###############
## TERRAFORM ##
###############

# Required by terraform google providor
ifndef REQUIRE_APPLICATION_DEFAULT_CREDENTIALS
	REQUIRE_APPLICATION_DEFAULT_CREDENTIALS := true
endif

# Set Terraform defaults
ifndef TF_CMD
	TF_CMD := terraform
endif
ifndef TF_CATEGORY
	TF_CATEGORY := terraform
endif
ifndef TF_CODE_BASEDIR
	TF_CODE_BASEDIR := ${COMMON}
endif
ifndef TF_STATE_FILE
	TF_STATE_FILE=${TF_BACKEND_PREFIX}/default.tfstate
endif

export

.PHONY: tf-check-git-ignore
tf-check-git-ignore:
	@TF_BUILD_DIR_ROOT=$(shell echo ${TF_BUILD_DIR} | awk -F'/' '{print $$1}') ; \
	if ! git check-ignore "$${TF_BUILD_DIR_ROOT}" "$${TF_BUILD_DIR_ROOT}/" &> /dev/null ; then \
		echo "!!!!! WARNING !!!!!" ; \
		echo "!!!!! Please add \"$${TF_BUILD_DIR_ROOT}\" to .gitignore to prevent committing into git" ; \
		exit 1 ; \
	fi

#.PHONY: tf-new-key
#tf-new-key: ## generate a terraform backend encryption key and save to vault://${TF_KEY_VAULT_PATH}
#	@if [ ! -z ${TF_KEY_VAULT_PATH} ] ; then \
#		new-gcs-key.sh ${TF_KEY_VAULT_PATH} ; \
#	fi

.PHONY: tf-sync
tf-sync: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-sync: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-sync: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-sync: tf-check-git-ignore ## sync terraform files to build dir
	@if [[ ! -d ${TF_BUILD_DIR} ]] ; then mkdir -p ${TF_BUILD_DIR} ; fi
	@${SCRIPTS_DIR}/tf-sync.sh

.PHONY: tf-init
tf-init: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-init: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-init: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-init: tf-sync ## terraform initialize code
	@cd ${TF_BUILD_DIR} ; \
		${TF_CMD} init -get ; \
		${TF_CMD} validate ; \
		${TF_CMD} output -no-color -json | jq 'with_entries(.value |= .value)' > resources.json

.PHONY: tf-re-init
tf-re-init: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-re-init: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-re-init: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-re-init: tf-clean tf-init ## terraform re-init to re-install and upgrade all providers

.PHONY: tf-validate
tf-validate: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-validate: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-validate: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-validate: tf-init ## validate syntax of the terraform files
	@cd ${TF_BUILD_DIR} ; \
		${TF_CMD} validate

.PHONY: tf-plan
tf-plan: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-plan: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-plan: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-plan: tf-init ## terraform plan
	@cd ${TF_BUILD_DIR} ; \
		${TF_CMD} plan

.PHONY: tf-apply
tf-apply: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-apply: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-apply: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-apply: tf-init ## terraform apply
	@cd ${TF_BUILD_DIR} ; \
		${TF_CMD} apply ${tf_opt} ; \
		${TF_CMD} output -no-color -json | jq 'with_entries(.value |= .value)' > resources.json

.PHONY: tf-destroy
tf-destroy: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-destroy: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-destroy: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-destroy: tf-init ## terraform destroy
	@cd ${TF_BUILD_DIR} ; \
		${TF_CMD} output -no-color -json | jq 'with_entries(.value |= .value)' > resources.json ; \
		${TF_CMD} destroy ${tf_opt}

.PHONY: tf-show
tf-show: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-show: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-show: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-show: tf-init ## terraform show
	@cd ${TF_BUILD_DIR} ; \
		${TF_CMD} show

.PHONY: tf-refresh
tf-refresh: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-refresh: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-refresh: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-refresh: tf-init ## terraform refresh
	@cd ${TF_BUILD_DIR} ; \
		${TF_CMD} refresh

.PHONY: tf-output
tf-output: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-output: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-output: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-output: ## terraform output
	@if [[ ! -d ${TF_BUILD_DIR} ]] ; then $(MAKE) tf-init ; fi
	@cd ${TF_BUILD_DIR} ; \
		${TF_CMD} output

.PHONY: tf-output-json
tf-output-json: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-output-json: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-output-json: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-output-json: ## terraform output in json format
	@if [[ ! -d ${TF_BUILD_DIR} ]] ; then $(MAKE) tf-init ; fi
	@cd ${TF_BUILD_DIR} ; \
		${TF_CMD} output -json | jq 'with_entries(.value |= .value) | del(.var)'

.PHONY: tf-info
tf-info: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-info: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-info: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-info: tf-output-json ## terraform alias to tf-output-json

.PHONY: tf-clean
tf-clean: export TF_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}
tf-clean: export TF_MODULES_DIR=${TF_CODE_BASEDIR}/${TF_CATEGORY}-modules
tf-clean: export TF_BUILD_DIR=${BUILD_DIR}/${TF_CATEGORY}
tf-clean: ## remove the terraform build dir
	@rm -rf ${TF_BUILD_DIR}

# EOF
