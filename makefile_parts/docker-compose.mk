################################################################################
## docker-compose.mk
################################################################################


ifndef DOCKER_COMPOSE_SOURCE_DIR
	DOCKER_COMPOSE_SOURCE_DIR := ${COMMON}/docker
endif

ifndef DOCKER_COMPOSE_FILE
	DOCKER_COMPOSE_FILE := docker-compose.yml
endif

ifndef DOCKER_COMPOSE_BIN
	DOCKER_COMPOSE_BIN := docker-compose
endif

# We want to call the "vault-login" make dependency _only_ when
# DOCKER_REGISTRY_LOGIN_TYPE is set to "vault".
ifeq ($(DOCKER_REGISTRY_LOGIN_TYPE),vault)
  VAULT_LOGIN_DEPENDENCY=vault-login
else
  VAULT_LOGIN_DEPENDENCY=
endif

.PHONY: dc-help
dc-help: ## show help for this module
	@show-module-help.sh docker-compose

.PHONY: dc-build
dc-build: ## docker-compose build
	docker-compose-wrapper.sh build

.PHONY: dc-up
dc-up: ## docker-compose up (detached)
	docker-compose-wrapper.sh up

.PHONY: dc-down
dc-down: ## docker-compose down
	docker-compose-wrapper.sh down

.PHONY: dc-logs
dc-logs: ## follow logs
	docker-compose-wrapper.sh logs

.PHONY: dc-config
dc-config: ## validate and display docker-compose.yml file
	docker-compose-wrapper.sh config

.PHONY: dc-ps
dc-ps: ## list project containers
	docker-compose-wrapper.sh ps

