

################################################################################
# external-dns.mk
################################################################################

export HELM := helm

ifndef EXTERNAL_DNS_VERSION
	export EXTERNAL_DNS_VERSION=v2.21.0
endif

ifndef EXTERNAL_DNS_GOOGLE_PROJECT
	export EXTERNAL_DNS_GOOGLE_PROJECT=${GCP_PROJECT_ID}
endif

ifndef EXTERNAL_DNS_GCP_CREDENTIALS_PATH
	export EXTERNAL_DNS_GCP_CREDENTIALS_PATH=secret/projects/${GCP_PROJECT_NAME}/common/dns-admin-key
endif

ifndef EXTERNAL_DNS_NS
	export EXTERNAL_DNS_NS := kube-system
endif

ifndef TXT_OWNER_ID
	TXT_OWNER_ID := ${GKE_CLUSTER_NAME}
endif

ifndef ACME_DNS_PROVIDER
	ACME_DNS_PROVIDER=${EXTERNAL_DNS_GOOGLE_PROJECT}-dns
endif

# Turn external-dns manager to reduce the clouddns api polling
ifndef EXTERNAL_DNS_EVENTS
	EXTERNAL_DNS_EVENTS=true
endif
ifndef EXTERNAL_DNS_INTERVAL
	EXTERNAL_DNS_INTERVAL=30m
endif

.PHONY: create-external-dns-ns
create-external-dns-ns: config-kube ## create cert-manager namespace
	@if ! kubectl get namespace ${EXTERNAL_DNS_NS} &> /dev/null ; then \
		kubectl create namespace ${EXTERNAL_DNS_NS} ; \
	fi

.PHONY: update-dns-repo
update-dns-repo: ## Update dns helm repo
	@${HELM} repo add bitnami https://charts.bitnami.com/bitnami || true
	@${HELM} repo update

.PHONY: external-dns-show-release
external-dns-show-release: kube-config ## Show the current external-dns Helm release
	@${HELM} list

.PHONY: external-dns-show-details
external-dns-show-details: kube-config ## Show the details for the current external-dns Helm release
	@${HELM} get all external-dns

.PHONY: deploy-external-dns
deploy-external-dns: EXTERNAL_DNS_GCP_CREDENTIALS=""
deploy-external-dns: vault-login
deploy-external-dns: create-external-dns-ns  update-dns-repo ## provisioning the external-dns
	@if ! ${HELM} ls -n ${EXTERNAL_DNS_NS} | grep external-dns &>/dev/null ; \
	then \
		EXTERNAL_DNS_GCP_CREDENTIALS=`vault-read.sh ${EXTERNAL_DNS_GCP_CREDENTIALS_PATH}` ; \
		cat ${FRAMEWORK_DIR}/helm-charts/external-dns/latest/values.tmpl | render.sh \
		| ${HELM} install \
			-f - \
			--namespace ${EXTERNAL_DNS_NS} \
			--version ${EXTERNAL_DNS_VERSION} \
			external-dns bitnami/external-dns ; \
	fi

.PHONY: upgrade-external-dns
upgrade-external-dns: EXTERNAL_DNS_GCP_CREDENTIALS=""
upgrade-external-dns: vault-login
upgrade-external-dns: config-kube update-dns-repo ## update the external-dns
	@EXTERNAL_DNS_GCP_CREDENTIALS=`vault-read.sh ${EXTERNAL_DNS_GCP_CREDENTIALS_PATH}` ; \
	cat ${FRAMEWORK_DIR}/helm-charts/external-dns/latest/values.tmpl | render.sh \
		| ${HELM} upgrade -n ${EXTERNAL_DNS_NS} --version ${EXTERNAL_DNS_VERSION} \
		-f - \
		external-dns bitnami/external-dns

.PHONY: destroy-external-dns
destroy-external-dns: config-kube ## destroy the external-dns
	@if ${HELM} ls -n ${EXTERNAL_DNS_NS} | grep external-dns &>/dev/null; \
	then \
		${HELM} del -n ${EXTERNAL_DNS_NS} external-dns; \
	fi

###########
## Test
###########
.PHONY: test-external-dns-ingress
test-external-dns-ingress: vault-login config-kube ## deploy dns test (ingress) pod/service
	@helm_chart=external-dns-test-ingress \
		helm_release_name=external-dns-test-ingress \
		${SCRIPTS_DIR}/helm-install.sh
	@echo Test URL http://external-dns-ing.${GCP_DNS_DOMAIN}

.PHONY: destroy-test-external-dns-ingress
destroy-test-external-dns-ingress: vault-login config-kube ## destroy dns test (ingress) pod/service
	@helm_release_name=external-dns-test-ingress \
		${SCRIPTS_DIR}/helm-delete.sh

.PHONY: test-external-dns-service
test-external-dns-service: vault-login config-kube ## deploy dns test (service) pod/service
	@helm_chart=external-dns-test-service \
		helm_release_name=external-dns-test-service \
		${SCRIPTS_DIR}/helm-install.sh
	@echo Test URL http://external-dns-svc.${GCP_DNS_DOMAIN}

.PHONY: destroy-test-external-dns-service
destroy-test-external-dns-service: vault-login config-kube ## destroy dns test (service) pod/service
	@helm_release_name=external-dns-test-service \
		${SCRIPTS_DIR}/helm-delete.sh

.PHONY: test-external-dns
test-external-dns: test-external-dns-service ## deploy dns test (service) pod/service

.PHONY: destroy-test-external-dns
destroy-test-external-dns: destroy-test-external-dns-service ## destroy dns test (service) pod/service

## end of external-dns.mk
