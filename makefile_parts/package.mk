# Makefile to support creating packages (.deb and .rpm)

.PHONY: pkg-show-env
pkg-show-env: ## show package-relevant environment variables
	@pkg-wrapper.py show-env

.PHONY: pkg-download
pkg-download: pkg-clean ## download package source
	@pkg-wrapper.py download

.PHONY: pkg-build
pkg-build: pkg-download ## build package
	@pkg-wrapper.py build

.PHONY: pkg-clean
pkg-clean: ## clean package build directory
	@rm -rf ${BUILD_DIR}/* ${BUILD_DIR}/.*

.PHONY: pkg-build-no-download
pkg-build-no-download: ## build package but skip download
	@pkg-wrapper.py build

.PHONY: pkg-sign
pkg-sign: vault-login ## sign package
	@pkg-wrapper.py sign

.PHONY: pkg-push
pkg-push: vault-login ## upload to package repository
	@pkg-wrapper.py push

.PHONY: pkg-creds-helper
pkg-creds-helper: vault-login ## run the credentials helper
	@pkg-wrapper.py creds-helper

