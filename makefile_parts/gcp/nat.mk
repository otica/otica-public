################################################################################
## nat.mk
# Make targets for NAT gateway
# Depends on config-gcp.mk, {SCRIPTS_DIR}
################################################################################

ifndef NAT_DESTINATIONS_FILE
	NAT_DESTINATIONS_FILE := nat-destinations.conf
endif

ifndef GCP_REGION
	missing_vars := ${missing_vars} GCP_REGION
endif

ifndef GCP_NAT_TAGS
	missing_vars := ${missing_vars} GCP_NAT_TAGS
endif

.PHONY: nat-gateway
nat-gateway: config-gcloud ## create NAT-gateway in the GCP project default network
	@${SCRIPTS_DIR}/gcp/create-nat-gateway.sh
	make nat-routes
	make nat-firewall-rule

.PHONY: nat-routes
nat-routes: config ## setup NAT route defined in nat-destinations.conf
	@${SCRIPTS_DIR}/gcp/create-nat-routes.sh ${NAT_DESTINATIONS_FILE}

.PHONY: destroy-nat-routes
destroy-nat-routes: config ## delete all NAT routes defined in nat-destinations.conf
	@${SCRIPTS_DIR}/gcp/delete-nat-routes.sh ${NAT_DESTINATIONS_FILE}

.PHONY: recreate-nat-instance
recreate-nat-instance: config-gcloud  ## destroy and recreate the nat gateway instance
	${SCRIPTS_DIR}/gcp/delete-nat-gateway.sh
	${SCRIPTS_DIR}/gcp/create-nat-gateway.sh

.PHONY: destroy-nat
destroy-nat: config-gcloud  ## destroy the NAT-gateway
	@echo "This nat-gateway might be registered by external firewall/licencing control."
	@read -p "Are you sure to DESTROY the nat-gateway? [YES/NO] " ; \
	if [ "$$REPLY" = 'YES' ] ; \
	then \
		${SCRIPTS_DIR}/gcp/delete-nat-gateway.sh; \
		make destroy-nat-routes; \
		make destroy-nat-firewall-rule; \
	else \
		echo "NOT destroy" ; \
	fi

# Usually the nat tag should be setup by the instance creation, just for exceptions:
.PHONY: tag-instances-for-nat
tag-instances-for-nat: config-gcloud ## tag all instances in the project as the NAT source
	@${SCRIPTS_DIR}/gcp/tag-nat-instances.sh ${GCP_NAT_TAGS}

# If GKE is on a subnet, a explicit firewall rule is necessary to allow cluster to nat-gateway traffic
.PHONY: nat-firewall-rule
nat-firewall-rule: FWR_NAME=allow-gke-to-nat
nat-firewall-rule: ## create a firewall rule to allow gke to nat-gateway trafic
	@if ! gcloud compute firewall-rules describe ${FWR_NAME} --project ${GCP_PROJECT_ID} > /dev/null 2>&1; \
	then \
        gcloud compute --project ${GCP_PROJECT_ID} firewall-rules create ${FWR_NAME} \
			--description="allow trafic from gke cluster to NAT gateway" \
			--direction=INGRESS \
			--priority=888 \
			--network=default \
			--action=ALLOW \
			--rules=all \
			--source-tags=${GCP_NAT_TAGS} \
			--target-tags=nat-server;  \
    fi

.PHONY: destroy-nat-firewall-rule
destroy-nat-firewall-rule: FWR_NAME=allow-gke-to-nat
destroy-nat-firewall-rule: ## create firewall rule to all node access to nat-gateway
	@if gcloud compute firewall-rules describe ${FWR_NAME} --project ${GCP_PROJECT_ID} > /dev/null 2>&1; \
	then \
        gcloud compute --project ${GCP_PROJECT_ID} firewall-rules delete ${FWR_NAME}; \
    fi
