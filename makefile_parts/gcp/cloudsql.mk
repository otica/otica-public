
ifndef CLOUD_SQL_INSTANCE
	missing_vars := ${missing_vars} CLOUD_SQL_INSTANCE
endif

ifndef GCP_ARTIFACTS_BUCKET
	missing_vars := ${missing_vars} GCP_ARTIFACTS_BUCKET
endif

ifndef DB_NAME
	DB_NAME=default
endif
ifndef DB_USER
	DB_USER=admin
endif

.PHONY: sql-export
sql-export-tm: TM=$(shell date "+%Y%m%d-%H%M%S")
sql-export-tm: DST=gs://${GCP_ARTIFACTS_BUCKET}/sql_exports/${CLOUD_SQL_INSTANCE}/${DB_NAME}_${TM}.sql.gz
sql-export-tm: config-gcloud  ## export ${CLOUD_SQL_INSTANCE}/${DB_NAME} to gs://${GCP_ARTIFACTS_BUCKET}/${CLOUD_SQL_INSTANCE}/sql_exports/${DB_NAME}_<timestamp>.sql.gz
	gcloud sql export sql ${CLOUD_SQL_INSTANCE} ${DST} --database=${DB_NAME}

.PHONY: sql-export
sql-export: DST=gs://${GCP_ARTIFACTS_BUCKET}/sql_exports/${CLOUD_SQL_INSTANCE}/${DB_NAME}_latest.sql.gz
sql-export: sql-export-tm  ## export ${CLOUD_SQL_INSTANCE}/${DB_NAME} to gs://${GCP_ARTIFACTS_BUCKET}/sql_exports/${CLOUD_SQL_INSTANCE}/${DB_NAME}_latest.sql.gz

.PHONY: sql-ls-export
sql-ls-export: config-gcloud  ## list export files on gs://${GCP_ARTIFACTS_BUCKET}/${CLOUD_SQL_INSTANCE}/sql_exports/${DB_NAME}_*.sql.gz
	@gsutil ls "gs://${GCP_ARTIFACTS_BUCKET}/sql_exports/${CLOUD_SQL_INSTANCE}/${DB_NAME}_*"

.PHONY: sql-import
sql-import: SRC_URI=""
sql-import: SRC_DB_NAME=${DB_NAME}
sql-import: config-gcloud ## import SRC_URI to CLOUD_SQL_INSTANCE.DB_NAME
	gcloud sql import sql ${CLOUD_SQL_INSTANCE} ${SRC_URI} \
		--database=${DB_NAME} --user=${DB_USER}

.PHONY: sql-connect
sql-connect: UNAME=$(shell uname | tr '[:upper:]' '[:lower:]')
sql-connect: PGDATABASE=default
sql-connect: config-gcloud vault-login ## gcloud sql connect to ${CLOUD_SQL_INSTANCE}
	@if test "${UNAME}" = "darwin"; then \
		vault-read.sh ${SQL_SEC_PATH}/admin_pass | pbcopy ; \
		echo "The database login passsward is saved in copy/paste buffer" ; \
	fi
	user=$(shell vault-read.sh ${SQL_SEC_PATH}/admin_user); \
	gcloud sql connect ${CLOUD_SQL_INSTANCE} --user=$$user

.PHONY: sql-backup
sql-backup: config-gcloud  ## backup the ${CLOUD_SQL_INSTANCE}
	gcloud sql backup --instance=${CLOUD_SQL_INSTANCE}

.PHONY: sql-info
sql-info: config-gcloud  ## show info of ${CLOUD_SQL_INSTANCE}
	gcloud sql instances describe ${CLOUD_SQL_INSTANCE}

.PHONY: sql-proxy
sql-proxy: ${HOME}/bin/cloud_sql_proxy config-gcloud  ## proxy to ${CLOUD_SQL_INSTANCE}
	@CONNECTION_NAME=$(shell gcloud sql instances describe ${CLOUD_SQL_INSTANCE} --format json | jq -r ".connectionName"); \
	DB_VERSION=$(shell gcloud sql instances describe ${CLOUD_SQL_INSTANCE} --format json | jq -r ".databaseVersion"); \
	echo "Proxy to $${CONNECTION_NAME}; Crtl-C to terminate the proxy"; \
	if [[ $${DB_VERSION} == *"POSTGRES"* ]]; then\
		cloud_sql_proxy -instances=$${CONNECTION_NAME}=tcp:5432; \
	else \
		cloud_sql_proxy -instances=$${CONNECTION_NAME}=tcp:3306; \
	fi

.PHONY: test-sql-proxy
test-sql-proxy:  ## return true if sql proxy is ON to ${CONNECTION_NAME}
	@CONNECTION_NAME=$(shell gcloud sql instances describe ${CLOUD_SQL_INSTANCE} --format json | jq -r ".connectionName"); \
	if ! ps -f | grep 'cloud_sql_proxy -instances=${CONNECTION_NAME}' | grep -v grep &> /dev/null; then \
		echo "Please run 'make sql-proxy' first." ; \
		exit 1 ; \
	fi

${HOME}/bin/cloud_sql_proxy: UNAME=$(shell uname | tr '[:upper:]' '[:lower:]')
${HOME}/bin/cloud_sql_proxy:  ## install cloud_sql_proxy, ONLY tested for MacOS!!!
	curl -o ${HOME}/bin/cloud_sql_proxy https://dl.google.com/cloudsql/cloud_sql_proxy.${UNAME}.amd64
	chmod 755 ${HOME}/bin/cloud_sql_proxy
