# workload-identity.mk: for setting up workload indenity on GCP/GKE
# Ref: https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity
#
# workload-identity must have been enabled on the current kubernetes cluster, see
#   https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity#enable_on_existing_cluster


ifndef GCP_PROJECT_ID
	missing_vars := ${missing_vars} GCP_PROJECT_ID
endif

# the GSA_PROJECT_ID can be different from the current GCP_PROJECT_ID
# set the default GSA_PROJECT_ID to be ${GCP_PROJECT_ID}
ifndef GSA_PROJECT_ID
	GSA_PROJECT_ID := ${GCP_PROJECT_ID}
endif

ifndef KUBE_NAMESPACE
	missing_vars := ${missing_vars} KUBE_NAMESPACE
endif

ifndef GSA_NAME
	missing_vars := ${missing_vars} GSA_NAME
endif

ifndef KSA_NAME
	missing_vars := ${missing_vars} KSA_NAME
endif

.PHONY: wi-help
wi-help: ## help info on workload-identity
	open https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity

#.PHONY: wi-create-gsa
#wi-create-gsa: config-gcloud ## create a GCP service account ${GSA_NAME}
#	@if ! gcloud iam service-accounts list | grep ${GSA_NAME} &> /dev/null ; then \
#		gcloud iam service-accounts create ${GSA_NAME} \
#			--description="GSA for workload indenity" \
#			--display-name="${GSA_NAME}";  \
#			--project="${GSA_PROJECT_ID}"; \
#		echo "GCP service account ${GSA_NAME} has been creeated." ; \
#		echo "Please assign workload IAM roles to ${GSA_NAME} to enable its capabilities." ; \
#	fi

.PHONY: wi-create-ksa
wi-create-ksa: kube-config ## create a Kubernetes service account ${KSA_NAME}
	@if ! kubectl get serviceaccount ${KSA_NAME} &> /dev/null ; then \
		kubectl create serviceaccount --namespace ${KUBE_NAMESPACE} ${KSA_NAME} ; \
	fi

.PHONY: wi-add-binding
wi-add-binding: wi-create-ksa ## add IAM policy binding of ${KUBE_NAMESPACE}/${KSA_NAME} => ${GSA_NAME}
	gcloud iam service-accounts add-iam-policy-binding \
	  --role roles/iam.workloadIdentityUser \
	  --member "serviceAccount:${GSA_PROJECT_ID}.svc.id.goog[${KUBE_NAMESPACE}/${KSA_NAME}]" \
	  ${GSA_NAME}@${GSA_PROJECT_ID}.iam.gserviceaccount.com
	kubectl annotate serviceaccount --namespace ${KUBE_NAMESPACE} ${KSA_NAME} \
	    iam.gke.io/gcp-service-account=${GSA_NAME}@${GSA_PROJECT_ID}.iam.gserviceaccount.com

.PHONY: wi-rm-binding
wi-rm-binding: kube-config ## revoke IAM policy binding of ${KUBE_NAMESPACE}/${KSA_NAME} => ${GSA_NAME}
	gcloud iam service-accounts remove-iam-policy-binding \
	  --role roles/iam.workloadIdentityUser \
	  --member "serviceAccount:${GSA_PROJECT_ID}.svc.id.goog[${KUBE_NAMESPACE}/${KSA_NAME}]" \
	  ${GSA_NAME}@${GSA_PROJECT_ID}.iam.gserviceaccount.com

.PHONY: wi-get-binding
wi-get-binding: kube-config ## get IAM policy of ${GSA_NAME}
	@gcloud iam service-accounts get-iam-policy \
		${GSA_NAME}@${GSA_PROJECT_ID}.iam.gserviceaccount.com

.PHONY: wi-show-env
wi-show-env: ## show relevant GCP Workload Identity variables
	@VERBOSE=1 workload-identity-show.sh

# End of workload-identity.mk
