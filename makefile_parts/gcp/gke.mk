#########
## GKE ##
#########

ifndef GCP_PROJECT_ID
	missing_vars := ${missing_vars} GCP_PROJECT_ID
endif

ifndef GCP_REGION
	missing_vars := ${missing_vars} GCP_REGION
endif

ifndef GCP_ENVIRONMENT
	missing_vars := ${missing_vars} GCP_ENVIRONMENT
endif

ifneq ($(missing_vars),)
	_ := $(info )
	_ := $(info missing env var(s):)
	_ := $(info )
	_ := $(info ${missing_vars}))
	_ := $(info )
	_ := $(info you must populate the required env vars before continuing)
	_ := $(info )
	_ := $(error )
endif

export

.PHONY: gke-describe
gke-describe: kube-config ## show all the settings of the GKE cluster
	@gcloud container clusters describe ${KUBE_CLUSTER_NAME} --region=${GCP_REGION}

.PHONY: gke-get-ops
gke-get-ops: kube-config ## list GKE cluster operations
	@gcloud container operations list

.PHONY: gke-list-instances
gke-list-instances: kube-config ## list VM instances
	@echo "List of instances running in ${GCP_PROJECT_ID}" ; echo
	@printf "%-55s %-15s %-7s %-10s\n" NAME ZONE PREEMP. CREATETION
	@gcloud compute instances list \
		--filter="name ~ ${GCP_ENVIRONMENT}" \
		--format='get(name,zone,scheduling.preemptible,creationTimestamp)'|sed 's%https://.*/%%' | sort -k1

# eg. Allow from myip:
#   export GKE_AUTHZ_NET="`curl -s ipinfo.io/ip`/32"
# Allow from several subnets:
#   export GKE_AUTHZ_NET=123.64.0.0/16,10.65.0.0/16,192.168.0.0/16
.PHONY: gke-net-authz-enable
gke-net-authz-enable: kube-config ## enable master authorized networks
	@if [ -z ${GKE_AUTHZ_NET} ]; \
	then \
		echo "GKE_AUTHZ_NET is not defined" ; \
	else \
		gcloud container clusters update ${KUBE_CLUSTER_NAME} \
			--enable-master-authorized-networks \
			--master-authorized-networks ${GKE_AUTHZ_NET}; \
		echo "Cluster ${KUBE_CLUSTER_NAME} API can be accessed only from ${GKE_AUTHZ_NET} now" ; \
	fi

.PHONY: gke-net-authz-disable
gke-net-authz-disable: kube-config ## disable master authorized networks
	@gcloud container clusters update ${KUBE_CLUSTER_NAME} \
			--no-enable-master-authorized-networks
	@echo Cluster ${KUBE_CLUSTER_NAME} API can be accessed from anywhere now

.PHONY: gke-net-policy-enable
gke-net-policy-enable: kube-config ## enable clsuter network policy
	@gcloud container clusters update ${KUBE_CLUSTER_NAME} --update-addons=NetworkPolicy=ENABLED
	@gcloud container clusters update ${KUBE_CLUSTER_NAME} --enable-network-policy
	@echo "Network policy is enabled for cluster ${KUBE_CLUSTER_NAME}"

.PHONY: gke-net-policy-disable
gke-net-policy-disable: kube-config ## disable cluster network policy
	@gcloud container clusters update ${KUBE_CLUSTER_NAME} --no-enable-network-policy
	@echo "Network policy is disabled for cluster ${KUBE_CLUSTER_NAME}"

.PHONY: gke-node-uptime
gke-node-uptime: kube-config ## show the GKE cluster VM info sorted by creationTimestamp
	@echo "---"
	@gcloud compute instances list --filter="name: ( ${GCP_ENVIRONMENT} )"

.PHONY: gke-show-natgw
gke-show-natgw: NAT_GATEWAY=$$(gcloud compute routers list --format='value(name)')
gke-show-natgw: kube-config ## show NAT Gateway information
	@gcloud compute routers get-status ${NAT_GATEWAY} --region=${GCP_REGION}

.PHONY: gke-ssh-node
gke-ssh-node: kube-config ## ssh tunnel to a node: make ssh-node NODE=<node>
	@if [[ -z "${NODE}" ]] ; then \
		$(MAKE) gke-list-instances ; \
		echo ; echo "Please select a node to ssh into, then run \"make gke-ssh-node NODE=<node>\"" ; \
	else \
		ZONE=$$(gcloud compute instances list --filter="name=${NODE}" --format="table[no-heading](zone.basename())") ; \
		gcloud compute ssh --zone $${ZONE} --tunnel-through-iap ${NODE} ; \
	fi

.PHONY: gke-versions
gke-versions: kube-config ## show the available cluster master & node versions for the given zone
	@gcloud container get-server-config --region=${GCP_REGION}

# EOF
