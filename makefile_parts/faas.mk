##########
## FAAS ##
##########

# Set FAAS defaults
ifndef NPM_CMD
	NPM_CMD := npm
endif
ifndef FAAS_CATEGORY
	FAAS_CATEGORY := functions
endif
ifndef FAAS_CODE_BASEDIR
	FAAS_CODE_BASEDIR := ${COMMON}
endif

export

.PHONY: faas-clean
faas-clean: export FAAS_CODE_DIR=${FAAS_CODE_BASEDIR}/${FAAS_CATEGORY}
faas-clean: export FAAS_BUILD_DIR=${BUILD_DIR}/${FAAS_CATEGORY}
faas-clean: ## remove the build dir
	@rm -rf ${FAAS_BUILD_DIR}

.PHONY: faas-sync
faas-sync: export FAAS_CODE_DIR=${FAAS_CODE_BASEDIR}/${FAAS_CATEGORY}
faas-sync: export FAAS_BUILD_DIR=${BUILD_DIR}/${FAAS_CATEGORY}
faas-sync: ## sync faas files to build dir
	@${SCRIPTS_DIR}/faas-sync.sh

.PHONY: faas-init
faas-init: export FAAS_CODE_DIR=${FAAS_CODE_BASEDIR}/${FAAS_CATEGORY}
faas-init: export FAAS_BUILD_DIR=${BUILD_DIR}/${FAAS_CATEGORY}
faas-init: faas-sync ## faas initialize code
	@if [[ ! -d ${FAAS_BUILD_DIR}\node_modules ]] ; then \
		cd ${FAAS_BUILD_DIR} ; \
		echo ; echo "## Configuring ${FAAS_CATEGORY}" ; \
		${NPM_CMD} i --no-color --loglevel=error --legacy-bundling 2>/dev/null ; \
	fi

.PHONY: faas-apply
faas-apply: export FAAS_CODE_DIR=${FAAS_CODE_BASEDIR}/${FAAS_CATEGORY}
faas-apply: export FAAS_BUILD_DIR=${BUILD_DIR}/${FAAS_CATEGORY}
faas-apply: faas-init ## faas deployment
	@cd ${FAAS_BUILD_DIR} && ./node_modules/.bin/sls deploy 2>/dev/null

.PHONY: faas-destroy
faas-destroy: export FAAS_CODE_DIR=${FAAS_CODE_BASEDIR}/${FAAS_CATEGORY}
faas-destroy: export FAAS_BUILD_DIR=${BUILD_DIR}/${FAAS_CATEGORY}
faas-destroy: faas-init ## faas removal
	@cd ${FAAS_BUILD_DIR} && ./node_modules/.bin/sls remove 2>/dev/null || true

.PHONY: faas-info
faas-info: export FAAS_CODE_DIR=${FAAS_CODE_BASEDIR}/${FAAS_CATEGORY}
faas-info: export FAAS_BUILD_DIR=${BUILD_DIR}/${FAAS_CATEGORY}
faas-info: ## faas information
	@if [[ ! -d ${FAAS_BUILD_DIR} ]] ; then $(MAKE) faas-init ; fi
	@echo
	@cd ${FAAS_BUILD_DIR} && ./node_modules/.bin/sls info --verbose 2>/dev/null || true

# EOF
