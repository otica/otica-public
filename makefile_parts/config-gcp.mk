##################
## CONFIG - GCP ##
##################

# NOTES:
# config.mk
# Targets for setting up gcloud configuration
# Depends on vault.mk, {SCRIPTS_DIR}

ifndef GCP_CONFIGURATION
	missing_vars := ${missing_vars} GCP_CONFIGURATION
endif

ifndef GCP_PROJECT_ID
	missing_vars := ${missing_vars} GCP_PROJECT_ID
endif

ifndef GCP_INFRASTRUCTURE_BUCKET
	GCP_INFRASTRUCTURE_BUCKET=${GCP_PROJECT_ID}-infrastructure
endif

# Add tools used by this make module:
FRAMEWORK_TOOLS += vault gcloud

ifneq ($(missing_vars),)
	_ := $(info )
	_ := $(info missing env var(s):)
	_ := $(info )
	_ := $(info ${missing_vars}))
	_ := $(info )
	_ := $(info you must populate the required env vars before continuing)
	_ := $(info )
	_ := $(error )
endif

####
# TARGETS
####

.PHONY: config
config: config-kube ## alias for config-kube

.PHONY: config-gcloud
config-gcloud: ## config gcloud and setup auth
	@if [ "$(MAKELEVEL)" -eq "0" ]; then \
		${SCRIPTS_DIR}/config-gcp.sh ; \
	fi

.PHONY: revoke-gcloud
revoke-gcloud: ## revoke just gcloud credentials
	@gcloud auth revoke --all --quiet || true
	@rm -f ${HOME}/.config/gcloud/application_default_credentials.json

.PHONY: config-info
config-info: ## list gcloud config values
	@gcloud config list

.PHONY: config-kube
config-kube: ## config kubectl and setup kubectl auth
	@if [ "$(MAKELEVEL)" -eq "0" ] && [ ! -f "${KUBECONFIG}" ] ; then \
		${SCRIPTS_DIR}/config-gcp.sh ; \
		${SCRIPTS_DIR}/config-kube.sh ; \
	fi

.PHONY: config-app-default
config-app-default: ## Set application-default-credentials with personal credentials...
	@if [ "$(MAKELEVEL)" -eq "0" ] ; then \
		echo "Set application-default-credentials with personal credentials..." ; \
		gcloud auth application-default login ; \
	fi

.PHONY: revoke-kube
revoke-kube: ## revoke current kubernetes context
	@CTX=$$(kubectl config current-context) ; \
	if ! [ -z $${CTX} ] ; then \
		kubectl config delete-context $$CTX || true; \
	fi

.PHONY: revoke-kube-message
revoke-kube-message:
	@echo Revoking prior kubernetes credentials before obtaining new ones

.PHONY: config-gcloud-user
config-gcloud-user: GKE_USER_AUTH=true
config-gcloud-user: ## config gcloud and setup auth by using user oauth
	@${SCRIPTS_DIR}/config-gcp.sh

.PHONY: config-kube-user
config-kube-user: config-gcloud-user ## config kubectl and setup auth by using user oauth
	@${SCRIPTS_DIR}/config-kube.sh

.PHONY: revoke
revoke: ## revoke gcloud and gke credentials
	@$(MAKE) revoke-gcloud
	@$(MAKE) revoke-kube

.PHONY: gcp-tf-bucket-create
gcp-tf-bucket-create: config-gcloud ## create terraform remote state bucket in GCP
	@if ! gsutil ls -p ${GCP_PROJECT_ID} gs://${GCP_INFRASTRUCTURE_BUCKET} &> /dev/null ; then \
		echo creating gs://${GCP_INFRASTRUCTURE_BUCKET} ... ; \
		gsutil mb -p ${GCP_PROJECT_ID} gs://${GCP_INFRASTRUCTURE_BUCKET} ; \
		gsutil versioning set on gs://${GCP_INFRASTRUCTURE_BUCKET} ; \
		sleep 20 ; \
	else \
		echo "Found terraform remote state bucket:" ; \
		gsutil ls -p ${GCP_PROJECT_ID} gs://${GCP_INFRASTRUCTURE_BUCKET} ; \
	fi

# EOF
