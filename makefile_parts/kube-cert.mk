################################################################################
# cert.mk
################################################################################

ifndef TEMPLATES
	missing_vars := ${missing_vars} TEMPLATES
endif

ifndef CERT_TEMPLATES
	export CERT_TEMPLATES=${TEMPLATES}
endif

ifneq ($(missing_vars),)
	_ := $(info )
	_ := $(info missing env var(s):)
	_ := $(info )
	_ := $(info ${missing_vars}))
	_ := $(info )
	_ := $(info you must populate the required env vars before continuing)
	_ := $(info )
	_ := $(error )
endif

.PHONY: kube-cert
kube-cert: kube-config ## create certs defined in ${TEMPLATES}/cert.yml
	@if [ -a ${CERT_TEMPLATES}/cert.yml ]; then \
		kube_apply.sh ${CERT_TEMPLATES}/cert.yml ; \
	fi
	@if [ -a ${CERT_TEMPLATES}/certs.yml ]; then \
		kube_apply.sh ${CERT_TEMPLATES}/certs.yml ; \
	fi
	@for i in ${CERT_TEMPLATES}/*-cert.yml; do \
		kube_apply.sh $$i ; \
	done

.PHONY: destroy-kube-cert
destroy-kube-cert: kube-config ## destroy certs defined in ${TEMPLATES}/cert.yml
	@if [ -a ${CERT_TEMPLATES}/cert.yml ]; then \
		kube_delete.sh ${CERT_TEMPLATES}/cert.yml ; \
	fi
	@if [ -a ${CERT_TEMPLATES}/certs.yml ]; then \
		kube_delete.sh ${CERT_TEMPLATES}/certs.yml ; \
	fi
	@for i in ${CERT_TEMPLATES}/*-cert.yml; do \
		kube_delete.sh $$i ; \
	done

.PHONY: update-kube-cert
update-kube-cert: destroy-kube-cert kube-cert ## update cert
	@echo "NOTE: cert secrets and cert consumers may also need to be recycled to pickup the new cert"

.PHONY: ls-cert-issuer
ls-cert-issuer: kube-config ## list all cert issuers
	kubectl get ClusterIssuer,Issuer --all-namespaces

.PHONY: ls-kube-cert
ls-kube-cert: kube-config ## list all certificates
	kubectl get certificates --all-namespaces

.PHONY: get-kube-cert
get-kube-cert: kube-config ## get all certificates
	kubectl get certificates --all-namespaces -o yaml

.PHONY: show-kube-cert
show-kube-cert: kube-config ## show cert in app namespace
	@show-kube-cert.sh ${APP_NAMESPACE}

.PHONY: renew-all-kube-certs
renew-all-kube-certs: kube-config ## force renew all Let's Encrypt certitficates by deleting TLS secrets
	@confirm.sh "Force renew all Let's Encrypt certitficates by deleting TLS secrets"
	@renew-all-kube-certs.sh
## end of cert-manager.mk
