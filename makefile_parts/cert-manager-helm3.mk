################################################################################
# cert-manager.mk
# See https://cert-manager.io/docs/installation
# See https://github.com/jetstack/cert-manager
################################################################################
# ifndef HELM
# 	export HELM := helm3
# endif
export HELM := helm

ifndef CERT_MANAGER_NS
	export CERT_MANAGER_NS := cert-manager
endif

ifndef CERT_MANAGER_CHART_VERSION
	export CERT_MANAGER_CHART_VERSION=v1.0.4
endif

ifndef ISSUER_HELM_CHART_VERSION
	export ISSUER_HELM_CHART_VERSION := ${CERT_MANAGER_CHART_VERSION}
endif

ifndef ACME_URL
	export ACME_URL := https://acme-staging.api.letsencrypt.org/directory
endif

ifndef ACME_EMAIL
	missing_vars := ${missing_vars} ACME_EMAIL
endif

ifndef GCP_PROJECT_ID
	missing_vars := ${missing_vars} GCP_PROJECT_ID
endif

ifndef GCP_DNS_DOMAIN
	missing_vars := ${missing_vars} GCP_DNS_DOMAIN
endif

ifndef CA_COMMON_NAME
	missing_vars := ${missing_vars} CA_COMMON_NAME
endif

# NOTE cert-manager is not currently support set up renew-before-days, but the defualt is 30 days.
# See https://github.com/jetstack/cert-manager/blob/ce9e5ede2bc6d8ccbc8e4db086f84c17e3f4d3af/docs/user-guides/acme-http-validation.md
ifndef CERT_RENEW_BEFORE_DAYS
	export CERT_RENEW_BEFORE_DAYS := 30
endif

ifndef EXTERNAL_DNS_GOOGLE_PROJECT
	EXTERNAL_DNS_GOOGLE_PROJECT=${GCP_PROJECT_ID}
endif

ifndef EXTERNAL_DNS_GCP_CREDENTIALS_PATH
	EXTERNAL_DNS_GCP_CREDENTIALS_PATH=secret/projects/${GCP_PROJECT_ID}/common/dns-admin-key
endif

ifndef HELM_CHART_DIR
	HELM_CHART_DIR=${FRAMEWORK_DIR}/helm-charts
endif

### For CA Issuer
ifndef ISSUER_HELM_NAMESPACE
	ISSUER_HELM_NAMESPACE=${CERT_MANAGER_NS}
endif
ifndef ISSUER_HELM_RELEASE_NAME
	ISSUER_HELM_RELEASE_NAME := cert-issuer
endif

ifndef ISSUER_HELM_CHART
	ISSUER_HELM_CHART := ${HELM_CHART_DIR}/cert-issuer/${ISSUER_HELM_CHART_VERSION}
endif

ifndef ISSUER_VALUES_TMPL
	ISSUER_VALUES_TMPL := ${ISSUER_HELM_CHART}/values.tmpl
endif

ifndef CA_KEY_PATH
	CA_KEY_PATH=${SEC_PATH}/cert-issuer/${GCP_ENVIRONMENT}/ca.key
endif
ifndef CA_CERT_PATH
	CA_CERT_PATH=${SEC_PATH}/cert-issuer/${GCP_ENVIRONMENT}/ca.crt
endif

ifneq ($(missing_vars),)
	_ := $(info )
	_ := $(info missing env var(s):)
	_ := $(info )
	_ := $(info ${missing_vars}))
	_ := $(info )
	_ := $(info you must populate the required env vars before continuing)
	_ := $(info )
	_ := $(error )
endif

.PHONY: helm-repo-add-cert-manager
helm-repo-add-cert-manager:
	${HELM} repo add jetstack https://charts.jetstack.io
	${HELM} repo update

.PHONY: create-cert-manager-ns
create-cert-manager-ns: config-kube helm-repo-add-cert-manager ## create cert-manager namespace
	@if ! kubectl get namespace ${CERT_MANAGER_NS} &> /dev/null ; then \
		kubectl create namespace ${CERT_MANAGER_NS} ; \
		kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true ; \
	fi

.PHONY: dry-run-cert-manager
dry-run-cert-manager: create-cert-manager-ns ## create the cert-manager
	@if ! ${HELM} ls --namespace ${CERT_MANAGER_NS} | grep cert-manager | grep DEPLOYED; \
	then \
		${HELM} install \
			--dry-run \
			--namespace ${CERT_MANAGER_NS} \
			--version ${CERT_MANAGER_CHART_VERSION} \
			--set installCRDs=true \
			--set extraArgs={--cluster-resource-namespace=${CERT_MANAGER_NS}},webhook.enabled=false \
			cert-manager jetstack/cert-manager; \
	fi

.PHONY: deploy-cert-manager
deploy-cert-manager: create-cert-manager-ns ## create cert-manager
	if ! ${HELM} ls --namespace ${CERT_MANAGER_NS} | grep ^cert-manager | grep -i DEPLOYED; \
	then \
		${HELM} install \
			--namespace ${CERT_MANAGER_NS} \
			--version ${CERT_MANAGER_CHART_VERSION} \
			--set installCRDs=true \
			--set extraArgs={--cluster-resource-namespace=${CERT_MANAGER_NS}},webhook.enabled=false \
			cert-manager jetstack/cert-manager; \
	fi

.PHONY: upgrade-cert-manager
upgrade-cert-manager: create-cert-manager-ns ## upgrade cert-manager
	${HELM} upgrade \
		--install \
		--namespace ${CERT_MANAGER_NS} \
		--version ${CERT_MANAGER_CHART_VERSION} \
		--set extraArgs={--cluster-resource-namespace=${CERT_MANAGER_NS}},webhook.enabled=false \
		cert-manager jetstack/cert-manager

.PHONY: ls-cert-manager
ls-cert-manager: ## list the cert-manager
	@${HELM} ls --namespace ${CERT_MANAGER_NS} | grep cert-manager 

.PHONY: cert-manager-status
cert-manager-status: ## check cert-manager status
	@${HELM} status --namespace ${CERT_MANAGER_NS} cert-manager

.PHONY: destroy-cert-manager
destroy-cert-manager: config-kube ## destroy the  cert-manager
	@if ${HELM} ls --namespace ${CERT_MANAGER_NS} | grep cert-manager &>/dev/null; \
	then \
		${HELM} delete --namespace ${CERT_MANAGER_NS} cert-manager; \
	fi

#########################################################################
# Cert Issuers (local chart under ${SCRIPTS_DIR}/helm-charts/cert-issuer)
#########################################################################
.PHONY: dry-run-cert-issuer
dry-run-cert-issuer: vault-login config-kube create-issuer-ca ## deploy cert issuers
	@if ! ${HELM} ls -n ${ISSUER_HELM_NAMESPACE} | grep ${ISSUER_HELM_RELEASE_NAME} &>/dev/null ; \
	then \
		cat ${ISSUER_VALUES_TMPL} | render.sh | \
		${HELM} ${extra_global_args} \
			install --dry-run -f - \
			-n ${ISSUER_HELM_NAMESPACE} \
			${ISSUER_HELM_RELEASE_NAME} ${ISSUER_HELM_CHART} ; \
	fi

.PHONY: deploy-cert-issuer
deploy-cert-issuer: vault-login config-kube create-issuer-ca ## deploy cert issuers
	@if ! ${HELM} ls -n ${ISSUER_HELM_NAMESPACE} | grep ${ISSUER_HELM_RELEASE_NAME} &>/dev/null ; \
	then \
		cat  ${ISSUER_VALUES_TMPL} | render.sh | \
		${HELM} ${extra_global_args} \
			install -f - \
			-n ${ISSUER_HELM_NAMESPACE} \
			${ISSUER_HELM_RELEASE_NAME} ${ISSUER_HELM_CHART} ; \
	fi
	
.PHONY: upgrade-cert-issuer
upgrade-cert-issuer: vault-login config-kube ## upgrade cert issuers
	@if ${HELM} ls -n ${ISSUER_HELM_NAMESPACE} | grep ${ISSUER_HELM_RELEASE_NAME} &>/dev/null; \
	then \
		cat  ${ISSUER_VALUES_TMPL} | render.sh | \
		${HELM} ${extra_global_args} \
			upgrade -f - \
			--namespace "${ISSUER_HELM_NAMESPACE}" \
			${ISSUER_HELM_RELEASE_NAME} ${ISSUER_HELM_CHART} ; \
	fi

.PHONY: destroy-cert-issuer
destroy-cert-issuer: config-kube ## destroy cert issuers
	@if ${HELM} ls -n ${ISSUER_HELM_NAMESPACE} | grep ${ISSUER_HELM_RELEASE_NAME} &>/dev/null; \
	then \
		${HELM} delete -n ${ISSUER_HELM_NAMESPACE} ${ISSUER_HELM_RELEASE_NAME} ; \
	fi

.PHONY: show-cert-issuer
show-cert-issuer: config-kube ## show cert issuers
	@kubectl get clusterissuers.cert-manager.io,issuers.cert-manager.io --all-namespaces


.PHONY: create-issuer-ca
create-issuer-ca: vault-login ## Generate a Issuer CA private key and cert and save to vault
	@if ! vault-read.sh ${CA_KEY_PATH} &> /dev/null; then \
		make _create-issuer-ca ; \
	fi

.PHONY: _create-issuer-ca
_create-issuer-ca: vault-login
	openssl genrsa -out ca.key 2048
	# Try two possible openssl.cnf file for a valid v3_ca extention
	# See https://github.com/jetstack/cert-manager/issues/279.
	openssl req -x509 -new -nodes -key ca.key -subj "/CN=${CA_COMMON_NAME}" -days 3650 -reqexts v3_req -extensions v3_ca -out ca.crt -config /usr/local/etc/openssl/openssl.cnf || \
	openssl req -x509 -new -nodes -key ca.key -subj "/CN=${CA_COMMON_NAME}" -days 3650 -reqexts v3_req -extensions v3_ca -out ca.crt -config /etc/ssl/openssl.cnf
	vault-write.sh ${CA_KEY_PATH} @ca.key
	vault-write.sh ${CA_CERT_PATH} @ca.crt
	rm -f ca.key ca.crt

.PHONY: delete-cert-mananger-crd
delete-cert-mananger-crd: config-kube ## Delete all cert-mananger's CRDs
	confirm.sh "Delete all cert-manager crds, Are you sure?"
	kubectl delete crd \
		certificaterequests.cert-manager.io \
		certificates.cert-manager.io \
		challenges.acme.cert-manager.io \
		clusterissuers.cert-manager.io \
		issuers.cert-manager.io  \
		orders.acme.cert-manager.io
## end of cert-manager.mk
