# Makefile for generate and include envvars from envvars.sec

ifndef SEC_ENV_SRC_FILE
	SEC_ENV_SRC_FILE := envvars.sec
endif

ifndef SEC_ENV_DEST_FILE
	SEC_ENV_DEST_FILE := .envvars
endif

-include ${SEC_ENV_DEST_FILE}

PHONY: envvars-check-gitignore
envvars-check-gitignore:
	@if ! git check-ignore "${SEC_ENV_DEST_FILE}" &> /dev/null; \
	then \
		echo "!!!!! WARNING !!!!!"; \
		echo "!!!!! Please add \"${SEC_ENV_DEST_FILE}\" to .gitignore to prevent committing secrets into git"; \
		exit 1; \
	fi

PHONY: envvars
envvars: envvars-check-gitignore vault-login ## render $SEC_ENV_DEST_FILE from $SEC_ENV_SRC_FILE
	@if [ -f ${SEC_ENV_SRC_FILE} ]; then \
		mkdir -p ${shell dirname ${SEC_ENV_DEST_FILE}}; \
		cat ${SEC_ENV_SRC_FILE} | render.sh > ${SEC_ENV_DEST_FILE}; \
	fi

PHONY: env-clean
env-clean: ## remove $SEC_ENV_DEST_FILE
	rm -f ${SEC_ENV_DEST_FILE}
