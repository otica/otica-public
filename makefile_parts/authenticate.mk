######################################################
## Authenticate to a cloud platform
######################################################

ifeq ($(CLOUD_PLATFORM),)
	missing_vars := ${missing_vars} CLOUD_PLATFORM
endif

ifneq ($(missing_vars),)
	_ := $(info )
	_ := $(info missing env var(s):)
	_ := $(info )
	_ := $(info ${missing_vars}))
	_ := $(info )
	_ := $(info you must populate the required env vars before continuing)
	_ := $(info )
	_ := $(error )
endif

####
# TARGETS
####

.PHONY: auth
auth: ## authenticate to platform CLOUD_PLATFORM
	@${SCRIPTS_DIR}/auth/authenticate.sh

.PHONY: auth-show-env
auth-show-env: ## show authenticate-specific environment variables
	@${SCRIPTS_DIR}/auth/authenticate-show-env.sh
