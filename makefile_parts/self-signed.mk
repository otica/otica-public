# Create a self-signed key-pair.


.PHONY: ss-show-env
ss-show-env: ## show self-signed certificate environment
	@self-signed.py show-env

.PHONY: ss-gen-keypair
ss-gen-keypair: ## generate the self-signed keypair
	@mkdir -p ${BUILD_DIR}
	@self-signed.py gen-keypair

.PHONY: ss-clean
ss-clean: ## remove generated self-signed keypair
	@self-signed.py clean

.PHONY: ss-write-vault
ss-write-vault: ## write generated files into Vault
	@self-signed.py write-vault

.PHONY: ss-write-vault-force
ss-write-vault-force: ## write generated files into Vault (overwrite existing)
	@self-signed.py write-vault-force
