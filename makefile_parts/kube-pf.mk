################################################################################
# kube-pf.mk
# Access ports on running Pods from your local machine.
################################################################################

ifdef PORT
	export KUBE_PF_REMOTE_PORT=${PORT}
endif

ifndef KUBE_PF_LOCAL_PORT
	export KUBE_PF_LOCAL_PORT=${KUBE_PF_REMOTE_PORT}
endif

ifdef FILTER
	export KUBE_PF_FILTER=${FILTER}
endif

ifndef KUBE_PF_FILTER
	export KUBE_PF_FILTER="."
endif

.PHONY: kube-pf-set-running-pod
kube-pf-set-running-pod:
	$(eval RUNNING_POD := $(shell ${SCRIPTS_DIR}/kube/pods-running-and-ready | grep -P "${KUBE_PF_FILTER}" | head -n1))
	@if [ -z "${RUNNING_POD}" ]; then echo "error: no running Pods matched" && exit 1; fi

.PHONY: kube-pf-show-env
kube-pf-show-env: kube-config kube-pf-set-running-pod ## show kube-port-forward settings
	@if [ -z "${KUBE_PF_LOCAL_PORT}" ]; then echo "error: missing KUBE_PF_LOCAL_PORT" && exit 1; fi
	@if [ -z "${KUBE_PF_REMOTE_PORT}" ]; then echo "error: missing KUBE_PF_REMOTE_PORT" && exit 1; fi
	@echo "KUBE_PF_LOCAL_PORT:  ${KUBE_PF_LOCAL_PORT}"
	@echo "KUBE_PF_REMOTE_PORT: ${KUBE_PF_REMOTE_PORT}"
	@echo "KUBE_PF_FILTER:      '${KUBE_PF_FILTER}'"
	@echo "FILTER:              '${FILTER}'"
	@echo "Will log into Pod:   ${RUNNING_POD}"

.PHONY: kube-pf-connect
kube-pf-connect: kube-pf-set-running-pod ## connect to Pod using port-forwarding
	@kubectl port-forward ${RUNNING_POD} ${KUBE_PF_LOCAL_PORT}:${KUBE_PF_REMOTE_PORT}

