################################################################################
# container-insight.
################################################################################

export DEFAULT_NS := kube-system

ifndef TEMPLATES
	missing_vars := ${missing_vars} TEMPLATES
endif

CLOUDWATCH_NS := amazon-cloudwatch

.PHONY: create-namespace
create-namespace: config-kube ## Create namespace
	@if ! kubectl get namespace ${CLOUDWATCH_NS} &> /dev/null ; then \
		kubectl create namespace ${CLOUDWATCH_NS} ; \
	fi

.PHONY: deploy-container-insights
deploy-container-insights: config-kube create-namespace ## Deploy container insights
	${SCRIPTS_DIR}/kube_apply.sh ${TEMPLATES}/*.yml


## end of container-insight.mk
