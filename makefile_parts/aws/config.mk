
################################################################################
## config.mk
# Targets for setting up cloud vendor configuration
# Depends on ${SCRIPTS_DIR}
################################################################################

ifndef VAULT_ENABLED
	export VAULT_ENABLED=true
endif

ifndef AWS_ACCOUNT_ID
	export AWS_ACCOUNT_ID=$(shell vault-read.sh ${SEC_PATH}/common/aws_account_id)
endif

.PHONY: config
config: config-kube

# Always do config!
#-include config

.PHONY: config-kube
config-kube: config-sts ## config kubectl and setup kubectl auth
	@if [[ "$(MAKELEVEL)" -eq "0" ]] && [[ ${VAULT_ENABLED} == "true" ]]; then \
		${SCRIPTS_OTHERS}/config-kube.sh ; \
	fi

.PHONY: config-iam-user
config-iam-user: vault-login ## config cloud and setup auth
	@if [[ "$(MAKELEVEL)" -eq "0" ]] && [[ ${VAULT_ENABLED} == "true" ]]; then \
		vault2aws-iam-user.sh ; \
	fi

.PHONY: config-sts
config-sts: vault-login ## config cloud and setup auth
	@if [[ "$(MAKELEVEL)" -eq "0" ]] && [[ ${VAULT_ENABLED} == "true" ]]; then \
		config-sts.sh ; \
	fi

.PHONY: cluster-info
cluster-info: ## show cluster info
	@kubectl cluster-info
	@kubectl get all --all-namespaces

## end of config.mk
