
################################################################################
## ecr.mk
# Targets for working with AWS ECR.
################################################################################

include ../common/env.mk
include env.mk

ifndef ECR_REGION
	ECR_REGION := us-west-2
endif

export

.PHONY: ecr-login
ecr-login: config ## Authenticate to ECR
	@if [ "$(MAKELEVEL)" -eq "0" ]; then \
		${SCRIPTS_DIR}/aws/ecr-login.sh ; \
	fi

.PHONY: create-repo
create-repo: REPO=
create-repo: config-sts ## Create fico repository
	@aws ecr create-repository \
    --repository-name ${REPO} \
    --image-scanning-configuration scanOnPush=true \
    --region ${ECR_REGION}

.PHONY: delete-repo
delete-repo: REPO=
delete-repo: config-sts ## Delete repository
	@aws ecr delete-repository \
      --repository-name ${REPO} \
      --force

.PHONY: push-image
push-image: IMAGE=
push-image: IMAGE_REPO=
push-image: ## Tag (latest ) and pust image 
	docker tag ${IMAGE}:latest ${AWS_ACCOUNT_ID}.dkr.ecr.${ECR_REGION}.amazonaws.com/${IMAGE_REPO}:latest
	docker push ${AWS_ACCOUNT_ID}.dkr.ecr.${ECR_REGION}.amazonaws.com/${IMAGE_REPO}:latest

.PHONY: list-images
list-images: REPO=
list-images: # List images in REPO=
	@aws ecr list-images --repository-name=${REPO}

.PHONY: pull-images
pull-image: IMAGE=
pull-image: ## Pull image
	docker pull ${ECR_ENDPOINT}/${IMAGE}:latest

.PHONY: get-vendor-login
get-vendor-login: VENDOR_ECR=${VENDOR_AWS_ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com
get-vendor-login: config ## Get vendor docker login credential
	aws ecr get-login-password --region ${AWS_REGION} | \
		docker login --username AWS --password-stdin ${VENDOR_ECR}

## end of ecr.mk
