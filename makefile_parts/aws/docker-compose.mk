

################################################################################
## docker-compose.mk
# Targets for docker-compose ops
# Depends on vault.mk
################################################################################

ifndef AWS_ACCOUNT_ID
	export AWS_ACCOUNT_ID=$(shell vault-read.sh ${SEC_PATH}/common/aws_account_id)
endif

DOCKER_REGISTRY=${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_REGION}.amazonaws.com

export

ifndef COMPOSE_FILE
	COMPOSE_FILE := docker-compose.yml
endif

# needed for update-downstream-tag.sh
ifndef GIT_SERVER
	GIT_SERVER := ${GITLAB_SERVER}
endif

.PHONY: docker-composer-init
docker-composer-init:
	@if [ -z ${DOCKER_REPOSITORY} ]; then \
		echo DOCKER_REPOSITORY is not defined ; \
		false; \
	fi
	@if [ -z ${DOCKER_NAMESPACE} ]; then \
		echo DOCKER_NAMESPACE is not defined ; \
		false; \
	fi
	@if [ -z ${DOCKER_IMAGE} ]; then \
		echo DOCKER_IMAGE is not defined ; \
		false; \
	fi
	@if [[ "X${AWS_ENVIRONMENT}" != "Xprod" ]] && [[ "X${AWS_ENVIRONMENT}" != "Xdev" ]]; then \
		echo AWS_ENVIRONMENT is not defined or it has to be either "dev" or "prod" ; \
		false; \
	fi

.PHONY: create-repo
create-repo: config-sts ## Create repository.
	@-aws ecr create-repository \
    --repository-name ${DOCKER_REPOSITORY} \
    --image-scanning-configuration scanOnPush=true \
    --region ${AWS_REGION}

.PHONY: docker-login
docker-login: AWS_ACCOUNT_ID=$(shell VAULT_ADDR=${VAULT_ADDR} vault-read.sh ${SEC_PATH}/common/aws_account_id)
docker-login: ECR=${AWS_ACCOUNT_ID}.dkr.ecr.us-west-2.amazonaws.com
docker-login: config-sts ## Get vendor docker login credential
	@aws ecr get-login-password --region ${AWS_REGION} | \
		docker login --username AWS --password-stdin ${ECR}

.PHONY: build-docker
build-docker: docker-composer-init ## build docker image
	@if  [ -f Dockerfile ]; then \
		docker build --pull -t ${DOCKER_IMAGE}:latest . ; \
	elif [ -f ${COMPOSE_FILE} ]; then \
		docker-compose -f ${COMPOSE_FILE} build --pull; \
	fi

.PHONY: build-docker-stage
build-docker-stage: docker-composer-init ## build docker image
	@if  [ -f Dockerfile ]; then \
		docker build --pull -t ${DOCKER_IMAGE}:stage . ; \
	elif [ -f ${COMPOSE_FILE} ]; then \
		docker-compose -f ${COMPOSE_FILE} build --pull; \
	fi

.PHONY: app-up
app-up: ## docker-compose up, run in the backgroud
	@if [ -f  ${COMPOSE_FILE} ]; then \
		docker-compose -f ${COMPOSE_FILE} up -d ; \
	else \
		echo No ${COMPOSE_FILE} file! ; \
	fi

.PHONY: app-down
app-down: ## docker-compose down
	@if [ -f  ${COMPOSE_FILE} ]; then \
		docker-compose -f ${COMPOSE_FILE} down --remove-orphans ; \
	else \
		echo No ${COMPOSE_FILE} file! ; \
	fi
	
.PHONY: app-logs
app-logs: ## follow the all logs
	@if [ -f  ${COMPOSE_FILE} ]; then \
		docker-compose -f ${COMPOSE_FILE} logs -f ; \
	else \
		echo No ${COMPOSE_FILE} file! ; \
	fi
		
.PHONY: prune
prune: ## prune local docker containers and images
	docker container prune
	docker images prune

.PHONY: push-version
push-version: docker-login ## tag image with DOCKER_IMAGE_VERSION or git commit sha if DOCKER_IMAGE_VERSION is not given and push the image to project's docker registry
	@if [ -z ${DOCKER_IMAGE_VERSION} ]; then \
		IMAGE_TAG=${DOCKER_IMAGE}:`git rev-parse --verify HEAD` ; \
	else \
		IMAGE_TAG=${DOCKER_IMAGE}:${DOCKER_IMAGE_VERSION} ; \
	fi; \
	docker tag ${DOCKER_IMAGE}:latest ${DOCKER_REGISTRY}/$$IMAGE_TAG ; \
	echo Push ${DOCKER_REGISTRY}/$$IMAGE_TAG ... ; \
	docker push ${DOCKER_REGISTRY}/$$IMAGE_TAG

.PHONY: push-latest
push-latest: docker-login create-repo ## tag latest image and push to project's docker registry
	@IMAGE_TAG=${DOCKER_IMAGE}:latest; \
	docker tag ${DOCKER_IMAGE}:latest ${DOCKER_REGISTRY}/$$IMAGE_TAG ; \
	echo Push ${DOCKER_REGISTRY}/$$IMAGE_TAG ... ; \
	docker push ${DOCKER_REGISTRY}/$$IMAGE_TAG

.PHONY: push-stage
push-stage: docker-login ## tag stage image and push to project's docker registry
	@IMAGE_TAG=${DOCKER_IMAGE}:stage; \
	docker tag ${DOCKER_IMAGE}:stage ${DOCKER_REGISTRY}/$$IMAGE_TAG ; \
	echo Push ${DOCKER_REGISTRY}/$$IMAGE_TAG ... ; \
	docker push ${DOCKER_REGISTRY}/$$IMAGE_TAG

.PHONY: pull-latest
pull-latest: docker-login ## pull latest image from project's docker registry
	docker pull ${DOCKER_REGISTRY}/${DOCKER_IMAGE}:latest

.PHONY: pull-version
pull-version: docker-login ## pull image version from project's docker registry
	docker pull ${DOCKER_REGISTRY}/${DOCKER_IMAGE}:${DOCKER_IMAGE_VERSION}

.PHONY: update-downstream-tag
update-downstream-tag: TAG=$(shell git rev-parse --verify HEAD)
update-downstream-tag: TAG_FILE=common/tag.sh
update-downstream-tag: ## update ${DOWNSTREAM_REPO}/${TAG_FILE} with ${TAG}
	update-downstream-tag.sh ${DOWNSTREAM_REPO} ${TAG}

## end of docker-compose.mk
