####################################
# Terraform makefile targets
####################################

# set Terraform envs defaults
ifndef TF_CMD
	TF_CMD := terraform
endif

ifndef TF_DIR
	TF_DIR := terraform
endif

ifndef TF_MODULES_DIR
	TF_MODULES_DIR := terraform-modules
endif

ifndef TF_BUILD_DIR
	TF_BUILD_DIR := build
endif

export

#aws s3 mb s3://${INFRASTRUCTURE_BUCKET}
.PHONY: tf-setup-state
tf-setup-state: config-iam-user ## create terraform remote state bucket if not exists
	@if ! aws --profile ${AWS_PROFILE} s3 ls s3://${INFRASTRUCTURE_BUCKET} &> /dev/null; \
	then \
		confirm.sh "Running aws --profile ${AWS_PROFILE} s3 mb s3://${INFRASTRUCTURE_BUCKET}" ; \
		aws --profile ${AWS_PROFILE} s3 mb s3://${INFRASTRUCTURE_BUCKET} ; \
		sleep 20 ; \
	else \
		echo "Remote state is already setup at s3://${INFRASTRUCTURE_BUCKET}" ; \
	fi
