ifndef HELM
	HELM := helm
endif

.PHONY: helm-list
helm-list: config-kube ## show deployed helm charts
	@${HELM} list --all-namespaces
	
.PHONY: add-repos
helm-add-repos: ## add helm repository
	@${HELM} repo add stable https://kubernetes-charts.storage.googleapis.com/
	@${HELM} repo add bitnami  https://charts.bitnami.com/bitnami
	@${HELM} repo add strimzi  https://strimzi.io/charts/	
	@${HELM} repo add elastic  https://helm.elastic.co
	@${HELM} repo add incubator http://storage.googleapis.com/kubernetes-charts-incubator
	@${HELM} repo add codecentric https://codecentric.github.io/helm-charts
	@${HELM} repo add oteemocharts https://oteemo.github.io/charts

.PHONY: helm-update-repos
helm-update-repos: ## update helm repository
	@${HELM} repo update
