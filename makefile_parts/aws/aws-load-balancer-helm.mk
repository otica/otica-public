# https://docs.aws.amazon.com/eks/latest/userguide/aws-load-balancer-controller.html
# https://github.com/aws/eks-charts/tree/master/stable/aws-load-balancer-controller
# Install aws load balancer controller from AWS/EKS helm chart

ifndef HELM
	export HELM := helm
endif

export DEFAULT_NS := kube-system

.PHONY: upgrade-aws-load-balancer
upgrade-aws-load-balancer: CHART_VERSION=v1.1.5
upgrade-aws-load-balancer: config ## Install or upgrade aws-load-balancer
	@helm repo add eks https://aws.github.io/eks-charts
	@helm repo update
	@cat ${FRAMEWORK_DIR}/helm-charts/aws/aws-load-balancer-controller/latest/values.tmpl | render.sh > /tmp/aws-load-balancer.yml
	@${HELM} upgrade -n ${DEFAULT_NS} --version ${CHART_VERSION} \
		--install \
		-f /tmp/aws-load-balancer.yml \
		aws-load-balancer-controller eks/aws-load-balancer-controller

.PHONY: destroy-aws-load-balancer
destroy-aws-load-balancer: config ## destroy aws-load-balancer-controller
	@if ${HELM} ls -n ${DEFAULT_NS} | grepaws-load-balancer-controller &>/dev/null; \
	then \
		${HELM} delete -n ${DEFAULT_NS} aws-load-balancer-controller ; \
	fi

.PHONY: deploy-2048-game
deploy-2048-game: config ## Deploy 2048-game
	@kubectl apply -f ${TEMPLATES}/2048-game/2048-namespace.yaml
	@kubectl apply -f ${TEMPLATES}/2048-game
	echo "Have fun at https://2048-game.${DOMAIN_NAME}"
## end of aws-load-balancer.mk
