

################################################################################
## kube-cluster-tf.mk
# Makefile targets useful for kube cluster ops
################################################################################

ifndef CLUSTER_ENVIRONMENT
	missing_vars := ${missing_vars} CLUSTER_ENVIRONMENT
endif

ifndef KUBE_CLUSTER_NAME
	missing_vars := ${missing_vars} KUBE_CLUSTER_NAME
endif

.PHONY: recycle-node
recycle-node: NODES = $$(kubectl get node -o json  | jq -r '.items[].metadata.name')
recycle-node: ## Replace node with a new instance
	kubectl get nodes
	@for i in ${NODES}; do \
		echo "" ; \
		echo "Recycle $$i?" ; \
		confirm.sh 2> /dev/null || continue ; \
		instance_id=`kubectl get node $$i -o json | jq -r '.spec.providerID' | rev | cut -d'/' -f1 | rev ` ; \
		aws autoscaling terminate-instance-in-auto-scaling-group  \
		  --instance-id $$instance_id --no-should-decrement-desired-capacity ; \
	done

.PHONY: reboot-node
reboot-node: NODES = $$(kubectl get node -o json  | jq -r '.items[].metadata.name')
reboot-node: ## Reboot an instance
	kubectl get nodes
	@for i in ${NODES}; do \
		echo "" ; \
		echo "Reboot $$i?" ; \
		confirm.sh 2> /dev/null || continue ; \
		instance_id=`kubectl get node $$i -o json | jq -r '.spec.providerID' | rev | cut -d'/' -f1 | rev ` ; \
		echo $$instance_id ; \
		aws ec2 reboot-instances  \
		  --instance-id $$instance_id ; \
	done

.PHONY: get-ami
get-ami: ## Get latest EKS ami
	@echo "Latest image:"
	@aws ec2 describe-images --image-ids=$(shell aws ssm get-parameter --name /aws/service/eks/optimized-ami/1.16/amazon-linux-2/recommended/image_id --region ${AWS_REGION} --query "Parameter.Value" --output text) \
		| jq -r '.Images[] | .ImageId + " " + .Name'
	@echo "Current running image:"
	@kubectl get nodes -o json | jq -r '.items[0].metadata.labels."eks.amazonaws.com/nodegroup-image"'