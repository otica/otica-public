

################################################################################
# metrics-server.mk
################################################################################

ifndef TEMPLATES
	missing_vars := ${missing_vars} TEMPLATES
endif

ifndef HELM
	export HELM := helm
endif

export DEFAULT_NS := kube-system

# https://github.com/bitnami/charts/tree/master/bitnami/metrics-server/#installing-the-chart
.PHONY: upgrade-metrics-server
upgrade-metrics-server: CHART_VERSION=5.3.4
upgrade-metrics-server: config ## update metrics-server
	@cat ${FRAMEWORK_DIR}/helm-charts/${PROVIDER}/metrics-server/latest/values.tmpl | render.sh > /tmp/metrics-server.yml
	@${HELM} upgrade --version ${CHART_VERSION} -n ${DEFAULT_NS} \
		--install \
		-f /tmp/metrics-server.yml \
		metrics-server bitnami/metrics-server

.PHONY: destroy-metrics-server
destroy-metrics-server: config ## destroy the metrics-server
	@if ${HELM} ls -n ${DEFAULT_NS} | grep metrics-server &>/dev/null; \
	then \
		${HELM} delete -n ${DEFAULT_NS} metrics-server; \
	fi

.PHONY: test-metrics-server
test-metrics-server: config-kube ## Test metrics-server
	@kubectl get -n ${DEFAULT_NS} --raw "/apis/metrics.k8s.io/v1beta1/nodes" | jq -r '.'

## end of metrics-server.mk
