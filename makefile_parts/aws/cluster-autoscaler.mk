################################################################################
# container-insighe.
################################################################################

export DEFAULT_NS := kube-system

# https://github.com/kubernetes/autoscaler/releases
export  CA_APP_IMAGE := us.gcr.io/k8s-artifacts-prod/autoscaling/cluster-autoscaler:v1.15.7

ifndef TEMPLATES
	missing_vars := ${missing_vars} TEMPLATES
endif

.PHONY: deploy-cluster-autoscaler
deploy-cluster-autoscaler: config-kube ## Deploy cluster-autoscaler
	kube_apply.sh ${TEMPLATES}/cluster-autoscaler-autodiscover.yaml

.PHONY: destroy-cluster-autoscaler
destroy-cluster-autoscaler: config-kube ## Destroy cluster-autoscaler
	kube_delete.sh

## end of cluster-autoscaler.mk
