

################################################################################
# external-dns.mk
################################################################################

ifndef TEMPLATES
	missing_vars := ${missing_vars} TEMPLATES
endif

ifndef HELM
	export HELM := helm
endif

ifndef EXTERNAL_DNS_NS
	export EXTERNAL_DNS_NS := kube-system
endif

ifndef KUBE_CLUSTER_NAME
	missing_vars := ${missing_vars} KUBE_CLUSTER_NAME
endif

.PHONY: create-external-dns-ns
create-external-dns-ns: config-kube ## create cert-manager namespace
	@if ! kubectl get namespace ${EXTERNAL_DNS_NS} &> /dev/null ; then \
		kubectl create namespace ${EXTERNAL_DNS_NS} ; \
	fi

# https://github.com/bitnami/charts/tree/master/bitnami/external-dns
.PHONY: update-dns-repo
update-dns-repo: ## Update dns helm repo
	@HELM repo add bitnami https://charts.bitnami.com/bitnami || true
	@HELM repo update

.PHONY: upgrade-external-dns
upgrade-external-dns: EXTERNAL_DNS_VERSION=v3.4.0
upgrade-external-dns: ENV_ZONE_ID=$(shell (cd build; terraform output -json | jq -r '.main_zone_id.value'))
upgrade-external-dns: config-kube ## update the external-dns
	@${HELM} repo update
	@cat ${FRAMEWORK_DIR}/helm-charts/${PROVIDER}/external-dns/latest/values.tmpl | render.sh > /tmp/external-dns.yml
	@${HELM} upgrade -n ${EXTERNAL_DNS_NS} --version ${EXTERNAL_DNS_VERSION} \
		--install \
		-f /tmp/external-dns.yml \
		external-dns bitnami/external-dns

.PHONY: destroy-external-dns
destroy-external-dns: config-kube ## destroy the external-dns
	@if ${HELM} ls --all-namespaces | grep external-dns &>/dev/null; \
	then \
		${HELM} del -n ${EXTERNAL_DNS_NS} external-dns; \
	fi

## end of external-dns.mk
