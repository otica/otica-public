
.PHONY: release
release: NOW=$(shell date +"%Y-%m-%d.%H-%M-%S")
release: FAN_ENV := $(shell basename $(CURDIR))
release: ## promote stage image-tag to production
	@git pull
	@if [ ${FAN_ENV} = "prod" ]; \
	then \
		PROMOTE_FROM="uat"; \
	else \
		PROMOTE_FROM="dev"; \
	fi ; \
	image_tag=`cat ../$${PROMOTE_FROM}/image-tag.mk | grep -v ^# | sed 's/export //'` ; \
	confirm.sh "Promote $${PROMOTE_FROM} $$image_tag to ${KUBE_NAMESPACE} and restart the pods" ; \
	git pull ; \
	cp -f ../$${PROMOTE_FROM}/image-tag.mk $(basename ${PWD})/ ; \
	git add .; \
	git commit -m "${KUBE_NAMESPACE}-release-$$image_tag-${NOW}"; \
	git push
