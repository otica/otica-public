# Otica

Otica is a set of scripts, Makefile modules, and configuration conventions
that provide [infrastructure as a software][2] (IaaS) developers and
managers a simplified and consistent interface to manage their resources.
Otica has built-in support for several modern service deployment
technologies including Kubernetes, Google Cloud Platform, Vault,
Terraform, and Docker. The Otica project was forked from the School of
Medicine's [PS Cloud Framework][1] in May 2021.

By following Otica's configuration conventions and using its provided
make modules you can set up your configurations once and then
use this configuration for multiple projects.

Otica is extendable and flexible making it easy for you to add new
functionality when needed.

* [Documentation](docs/)

* [Support](docs/support)


[1]: https://gitlab.med.stanford.edu/gcp/ps-cloud-framework

[2]: https://en.wikipedia.org/wiki/Infrastructure_as_a_service
