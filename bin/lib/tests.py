#!/usr/bin/env python3

import unittest

import os
import pathlib

from otica.section import OticaSectionDict, OticaSection
from otica.oyaml   import OticaYaml

class TestOticaSection(unittest.TestCase):

    yaml_dict1 = {
        'environment':
        {
            'toplevel': ['framework.var'],
            'common':   [],
            'local':    ['local.var'],
        },
        'makefile_parts':
        {
            'otica':    [],
            'toplevel': ['custom.var'],
            'common':   ['common.var'],
        },
    }

    yaml_dict2 = {
        'environment':
        {
            'toplevel': [
                'framework.var',
                'busby.var',
                {'dev': ['aws.var', 'swa.var'], 'uat': ['gcp.var'], 'default': ['onprem.var']},
            ],
            'common':   ['common.var'],
            'local':    ['local.var'],
        },
        'makefile_parts':
        {
            'otica':    [],
            'toplevel': ['custom.var'],
            'common':   ['common.var'],
        },
    }


    def test_010_default_at_end(self):
        """Does the default section come at the end?"""
        section = OticaSectionDict()
        section['dev']     = ['a.var', 'b.var']
        section['default'] = ['c.var']
        section['uat']     = ['e.var', 'f.var']

        results = []
        for key, value in section.items():
           results.extend(value)

        # The result should be that the 'default' section ('c.var') comes at the end.
        self.assertEqual(results, ['a.var', 'b.var', 'e.var', 'f.var', 'c.var'])

    def test_020_otica_section(self):
        """Make an OticaSection and convert to YAML."""

        otica_section_object = [
            'framework.var',
            'base.var',
            {
                'dev':     ['onprem.var'],
                'uat':     ['aws.var'],
                'prod':    ['gcp.var', 'gke.var'],
                'default': ['onprem.var']
            }
        ]

        otica_section = OticaSection(otica_section_object)

        self.assertEqual(otica_section.all_environments, ['framework.var', 'base.var'])

        self.assertIn('dev',     otica_section.environments_dict)
        self.assertIn('uat',     otica_section.environments_dict)
        self.assertIn('prod',    otica_section.environments_dict)
        self.assertIn('default', otica_section.environments_dict)

        self.assertEqual(otica_section.environments_dict['dev'],     ['onprem.var'])
        self.assertEqual(otica_section.environments_dict['uat'],     ['aws.var'])
        self.assertEqual(otica_section.environments_dict['prod'],    ['gcp.var', 'gke.var'])
        self.assertEqual(otica_section.environments_dict['default'], ['onprem.var'])


        # Convert to a regular list.
        list1 = otica_section.to_list()
        self.assertEqual(len(list1), 3)
        self.assertEqual(list1[0], 'framework.var')
        self.assertEqual(list1[1], 'base.var')

        dict1 = list1[2]
        self.assertEqual(dict1['dev'],     ['onprem.var'])
        self.assertEqual(dict1['uat'],     ['aws.var'])
        self.assertEqual(dict1['prod'],    ['gcp.var', 'gke.var'])
        self.assertEqual(dict1['default'], ['onprem.var'])


        filenames = otica_section.get_filenames()
        self.assertEqual(len(filenames), 6)
        self.assertIn('onprem.var', filenames)
        self.assertIn('aws.var', filenames)
        self.assertIn('gcp.var', filenames)
        self.assertIn('gke.var', filenames)
        self.assertIn('base.var', filenames)
        self.assertIn('framework.var', filenames)

        # Append a name. Check for de-duping.
        otica_section.append('aws.var')
        self.assertEqual(len(otica_section.get_filenames()), 6)
        otica_section.append('aws1.var')
        self.assertEqual(len(otica_section.get_filenames()), 7)


        otica_section_object_empty = []
        otica_section_empty = OticaSection(otica_section_object_empty)
        self.assertEqual(otica_section_empty.to_list(), [])

        otica_section_object2 = [
            'framework.var',
            'base.var',
        ]
        otica_section2 = OticaSection(otica_section_object2)
        self.assertEqual(len(otica_section2.to_list()), 2)

    def test_030_otica_yaml(self):
        """Test OticaYaml object and methods."""

        otica_yaml = OticaYaml(TestOticaSection.yaml_dict2)

        self.assertEqual(otica_yaml.environment_toplevel.all_environments, ['framework.var', 'busby.var'])
        self.assertEqual(otica_yaml.environment_common.all_environments, ['common.var'])
        self.assertEqual(otica_yaml.environment_local.all_environments, ['local.var'])

        self.assertEqual(otica_yaml.makefile_parts_otica.all_environments, [])
        self.assertEqual(otica_yaml.makefile_parts_toplevel.all_environments, ['custom.var'])
        self.assertEqual(otica_yaml.makefile_parts_common.all_environments, ['common.var'])

        dict1 = otica_yaml.to_dict()
        self.assertIn('environment',    dict1)
        self.assertIn('makefile_parts', dict1)

        environment1    = dict1['environment']
        makefile_parts1 = dict1['makefile_parts']
        self.assertIn('toplevel', environment1)
        self.assertIn('common',   environment1)
        self.assertIn('local',    environment1)

        self.assertIn('otica',    makefile_parts1)
        self.assertIn('toplevel', makefile_parts1)
        self.assertIn('common',   makefile_parts1)

        self.assertTrue(len(otica_yaml.to_yaml_string()) > 20)
        print(f"JJJJ {otica_yaml.to_yaml_string()}")

        templates_dir = os.path.join(pathlib.Path().resolve().parent.parent, "templates")
        makefile = otica_yaml.make_from_template(templates_dir)
        print(makefile)

################################################################################

def main():
    unittest.main(verbosity=1)
    #unittest.main()

if __name__ == "__main__":
    main()
