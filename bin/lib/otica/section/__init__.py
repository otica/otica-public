"""
The OticaSection class represents a special structure used in the otica.yaml file.
"""

# pylint: disable=superfluous-parens
# pylint: disable=wrong-import-position

import yaml

from collections.abc import MutableMapping

from typing import Union, Optional, Dict, List, Any
SectionDict = Dict[str, List[str]]

from otica.ologging import progress
from otica.utility  import dedup

class OticaSectionError(Exception):
    """Use for errors in parsing an OticaSection."""
    def __init__(self, message: str):
        self.message = message

class OticaSectionDict(MutableMapping):
    """A special dict-like structure that ensures 'default' stays at the end.

    OticaSectionDict emulates a dict but when iterating over it using the
    items() method ensure that the 'default' key (if it exists) always
    comes at the end. As a by-product, when iterating over OticaSectionDict
    the order of the other keys is preserved.


    Example:

        section = OticaSectionDict()
        section['dev']     = ['a.var', 'b.var']
        section['default'] = ['c.var']
        section['uat']     = ['e.var', 'f.var']
        for key, value in section.items():
           print(f"{key}, {value}"

    will result in:

        dev, ['a.var', 'b.var']
        uat, ['e.var', 'f.var']
        default, ['c.var']

    Note how 'default' comes last.

    IMPORTANT! Be sure to iterate using the items() method. The following will
    NOT work:

        section = OticaSectionDict()
        section['dev']     = ['a.var', 'b.var']
        section['default'] = ['c.var']
        for key, value in section:
           print(f"{key}, {value}"

    Instead, you should do

        section = OticaSectionDict()
        section['dev']     = ['a.var', 'b.var']
        section['default'] = ['c.var']
        for key, value in section.items():
           print(f"{key}, {value}"


    """
    def __init__(self, *args, **kwargs):
        self.store = dict()
        self.keys  = []
        self._index = 0

        self.update(dict(*args, **kwargs))

    def __getitem__(self, key):
        return self.store[key]

    def __setitem__(self, key, value):
        self.store[key] = value
        self.keys.append(key)

        if ('default' in self.keys):
            self.keys.remove('default')
            self.keys.append('default')

    def __delitem__(self, key):
        del self.store[key]

    def __next__(self):
        if self._index < len(self.keys):
            item = self.keys[self._index]
            self._index += 1
            return item
        else:
            raise StopIteration

    def __iter__(self):
        self._index = 0
        return self

    def list(self):
        list1 = list(self)
        if ('default' in list1):
            list1.remove('default')
            list1.append('default')

        return list1

    def __len__(self):
        return len(self.store)

    def __str__(self) -> str:
        pieces = []
        for key, value in self.items():
            pieces.append(f"'{key}': {str(value)}")

        return '{' + ', '.join(pieces) + '}'

    def to_dict(self) -> Dict[str, Any]:
        """Convert to a regular dict."""
        d = {}
        for key, value in self.items():
            d[key] = value

        return d


class OticaSection():
    """A special structure that represents a section of the otica.yaml file.

    An OticaSection, such as environment:toplevel is a list whose elements
    are either strings or a dict. There is only allowed to be one dict.

    Example:

        environment:
          toplevel:
            - framework.var
            - base.var
            - dev:
                - onprem.var
              uat:
                - aws.var
              prod:
                - gcp.var
                - gke.var
              default:
                - onprem.var

    The environment:toplevel section is, represented as a Python object:

      [
        'framework.var',
        'base.var',
        {
          'dev': ['onprem.var'],
          'uat': ['aws.var'],
          'prod': ['gcp.var', 'gke.var'],
          'default': ['onprem.var']
        }
      ]

    We store the strings that are not in the dict
    """
    def __init__(self, yaml_section: List[Union[str, SectionDict]],
                 label: Optional[str] = None ):
        super().__init__()

        self.all_environments  = []
        self.environments_dict = OticaSectionDict()

        # This is optional and is only used for verbose mode
        self.label = label

        number_of_dicts = 0

        progress(f"yaml_section has type {type(yaml_section)}")

        for element in yaml_section:
            if (isinstance(element, str)):
                # Elements not in the dict.
                self.all_environments.append(element)

            elif (isinstance(element, dict)):
                # Elements in the dict.
                if (number_of_dicts >= 1):
                    msg = f"you can only have one dict in an OticaSection"
                    raise OticaSectionError(msg)

                # Populate self.elements_dict.
                for key, value in element.items():
                    self.environments_dict[key] = value

                number_of_dicts += 1

                # Verify that each element of self.environments is a string
                # mapping to a list.
                for key, value in self.environments_dict.items():
                    if (not isinstance(key, str)):
                        msg = f"environment dict must map strings to lists: <{key}> is not a string"
                        raise OticaSectionError(msg)
                    if (not isinstance(value, list)):
                        msg = f"environment dict must map strings to lists <{value}> is not a list"
                        raise OticaSectionError(msg)
            else:
                msg = f"element {element} in section is not a list or dict"
                raise OticaSectionError(msg)

    def __str__(self) -> str:
        label = self.format_label()
        return_value = (
            f"[section '{label}'] all environments: {self.all_environments}, "
            f"specific environments: {self.environments_dict}"
        )
        return return_value

    def to_list(self):
        """Convert back to a regular Python object."""
        d = []

        if (len(self.all_environments) > 0):
            d.extend(self.all_environments)

        if (len(self.environments_dict) > 0):
            d.append(self.environments_dict.to_dict().copy())

        return d

    def format_label(self) -> str:
        """Format the label property suitable for displaying"""
        if (self.label is not None):
            label = self.label
        else:
            label = "unlabeled"

        return label

    ### START OF PROPERTIES
    @property
    def all_environments(self) -> List[str]:
        """The getter for the all_environments property."""
        return self._all_environments

    @all_environments.setter
    def all_environments(self, value: List[str]) -> None:
        """The setter for the all_environments property

        The sett does auto-deduping.
        """
        value_deduped = dedup(value)
        self._all_environments = value_deduped

    @property
    def environments_dict(self) -> SectionDict:
        """The getter for the environments_dict property."""
        return self._environments_dict

    @environments_dict.setter
    def environments_dict(self, value: SectionDict) -> None:
        """The setter for the environments_dict property
        """
        self._environments_dict = value

    ### END OF PROPERTIES

    def get_filenames(self) -> List[str]:
        """Get all the filenames.

        This list is all_environments plus the concatenation
        of the values of the dict (suitably deduped).
        """
        filenames = self.all_environments.copy()

        for environment in self.environments_dict:
            filenames += self.environments_dict[environment]

        filenames_deduped = dedup(filenames)

        label = self.format_label()
        progress(f"filenames from section '{label}' are {filenames_deduped}")

        return filenames_deduped


    def append(self, filename: str) -> None:
        """Add a filename to the all_environments section. Be sure to de-dupe.
        """
        all_environments = self.all_environments.copy()
        all_environments.append(filename)
        self.all_environments = all_environments

def merge(section1: OticaSection, section2: OticaSection) -> OticaSection:
    """Given two OticaSections merge them, removing any duplicates.

    At this time we don't attempt to de-dupe the dictionary section.

    Be sure to preserve order.
    """

    # Step 1. Combine the all_environment's list of strings.
    all_environments1 = section1.all_environments
    all_environments2 = section2.all_environments
    all_environments_merged = dedup(all_environments1 + all_environments2)

    # Step 2. Combine the dicts
    environments_dict1 = section1.environments_dict
    environments_dict2 = section2.environments_dict

    environments_dict_merged = {}
    for key, value in environments_dict1.items():
           environments_dict_merged[key] = value

    for key, value in environments_dict2.items():
           environments_dict_merged[key] = value

    yaml_section = all_environments_merged.copy() + [environments_dict_merged]

    section_merged = OticaSection(yaml_section)

    return section_merged
