"""
Module containing all Otica-specific Exception classes.
"""

class OticaNoTopLevelDirectory(Exception):
    """An exception to be used when looking for a toplevel
    directory and finding none."""

class OticaMissingFileFromDirectory(Exception):
    """Use this exception when a required file is missing from
    a particular directory"""
    def __init__(self, directory: str, file_name: str):
        self.file_name = file_name
        self.directory = directory

class OticaUnknownPlatform(Exception):
    """Use when platform name is not recognized"""
    def __init__(self, message: str):
        self.message = message

class OticaGenericError(Exception):
    """Use for other unspecified Otica errors"""
    def __init__(self, message: str):
        self.message = message
