import logging
import sys

# We simplify the logging message format and send all
# logs to standard output. We default to INFO.
logging.basicConfig(level=logging.INFO,
                    format='%(message)s',
                    stream = sys.stdout)

def progress(msg: str) -> None:
    """Show progress messages (if verbose is True)"""
    logging.debug(f"progress: {msg}")

def info(msg: str) -> None:
    """Show more important messages."""
    logging.info(msg)

def verbose_enable() -> None:
    level = logging.DEBUG
    logging.getLogger().setLevel(level)

def verbose_disable() -> None:
    level = logging.INFO
    logging.getLogger().setLevel(level)
