"""Represents the content of an otica.yaml string.

       An otica.yaml file looks like this:

         environment:
           toplevel:
             - framework.var
             - dev: onprem.var
               uat: gcp.var
               prod:
                 - prod1.var
                 - prod2.var
               default: onprem.var
           common:
             - common.var
           local:
             - local.var

         makefile_parts:
           otica:
             - framework.mk
             - vault.mk
             - authenticate.mk
           toplevel:
             - project.mk
           common:
             - app.mk

This class takes the Python dict version of the above as its main data and
does validation on it as well as verifying there are no extraneous sections or
overlaps. It also provides methods to generate the otica.yaml file.
"""

# pylint: disable=superfluous-parens

import os
import pprint
import yaml

import jinja2

from typing import Any, Dict, List, Union, cast # pylint: disable=wrong-import-order

from otica.ologging import progress
from otica.section  import OticaSection, merge

class OticaYamlFileParse(Exception):
    """Use when there are invalid or overlapping sections in an
    otica-yaml dict."""
    def __init__(self, message: str):
        self.message = message


class OticaYaml():
    """Main class."""

    ENVIRONMENT_SECTIONS    = ['toplevel', 'common',   'local']
    MAKEFILE_PARTS_SECTIONS = ['otica',    'toplevel', 'common']

    makefile_tmpl_basename = 'Makefile_v4.tmpl'

    def __init__(self, otica_yaml_dict: Dict[str, Any]):
        super().__init__()

        self.otica_yaml_dict = otica_yaml_dict

        # Fill-in any missing sections.
        self.normalize()

        # Raise an error if there are any invalid elements.
        self.check_for_invalid_elements()

        # Fill in the properties.
        self.populate()

        # Make sure there are no overlaps in the sections.
        self.validate()

    def to_dict(self) -> Dict[str, Any]:
        """Convert self to a dict."""
        d = {}

        d['environment'] = {
            'toplevel': self.environment_toplevel.to_list(),
            'common':   self.environment_common.to_list(),
            'local':    self.environment_local.to_list()
        }

        d['makefile_parts'] = {
            'otica':    self.makefile_parts_otica.to_list(),
            'toplevel': self.makefile_parts_toplevel.to_list(),
            'common':   self.makefile_parts_common.to_list(),
        }

        return d

    def to_yaml_string(self) -> str:
        return str(yaml.safe_dump(self.to_dict(), sort_keys=False))

    def normalize(self) -> None:
        """Fill in any missing elements with empty arrays. For example,
           if otica_yaml_dict (represented in YAML) looks like this:

          {
            'environment':
              {
                'toplevel': ['framework.var'],
                'local':    ['local.var'],
              },
            'makefile_parts':
              {
                'toplevel': ['custom.var'],
                'common':   ['common.var'],
              },
           }

          Then add the missing environment:common and makefile_parts:toplevel to get

          {
            'environment':
              {
                'toplevel': ['framework.var'],
                'common':   [],
                'local':    ['local.var'],
              },
            'makefile_parts':
              {
                'otica':    [],
                'toplevel': ['custom.var'],
                'common':   ['common.var'],
              },
           }
        """
        if (self.otica_yaml_dict is None):
            self.otica_yaml_dict = {}

        if ('environment' not in self.otica_yaml_dict):
            self.otica_yaml_dict['environment'] = {}

        if ('makefile_parts' not in self.otica_yaml_dict):
            self.otica_yaml_dict['makefile_parts'] = {}

        # Add empty lists for any missing ENVIRNOMENT_SECTIONS
        for section in OticaYaml.ENVIRONMENT_SECTIONS:
            if ((section not in self.otica_yaml_dict['environment'])
                or (self.otica_yaml_dict['environment'][section] is None)):
                self.otica_yaml_dict['environment'][section] = []

        # Add empty lists for any missing MAKEFILE_PARTS_SECTIONS
        for section in OticaYaml.MAKEFILE_PARTS_SECTIONS:
            if ((section not in self.otica_yaml_dict['makefile_parts'])
                or (self.otica_yaml_dict['makefile_parts'][section] is None)):
                self.otica_yaml_dict['makefile_parts'][section] = []


    ### START OF PROPERTIES
    @property
    def environment_toplevel(self) -> OticaSection:
        """The getter for the environment_toplevel property."""
        return self._environment_toplevel

    @environment_toplevel.setter
    def environment_toplevel(self, value: OticaSection) -> None:
        """The setter for the environment_toplevel property"""
        self._environment_toplevel = value

    @property
    def environment_common(self) -> OticaSection:
        """The getter for the environment_common property."""
        return self._environment_common

    @environment_common.setter
    def environment_common(self, value: OticaSection) -> None:
        """The setter for the environment_common property"""
        self._environment_common = value

    @property
    def environment_local(self) -> OticaSection:
        """The getter for the environment_local property."""
        return self._environment_local

    @environment_local.setter
    def environment_local(self, value: OticaSection) -> None:
        """The setter for the environment_local property"""
        self._environment_local = value

    @property
    def makefile_parts_otica(self) -> OticaSection:
        """The getter for the makefile_parts_otica property."""
        return self._makefile_parts_otica

    @makefile_parts_otica.setter
    def makefile_parts_otica(self, value: OticaSection) -> None:
        """The setter for the makefile_parts_otica property"""
        self._makefile_parts_otica = value

    @property
    def makefile_parts_toplevel(self) -> OticaSection:
        """The getter for the makefile_parts_toplevel property."""
        return self._makefile_parts_toplevel

    @makefile_parts_toplevel.setter
    def makefile_parts_toplevel(self, value: OticaSection) -> None:
        """The setter for the makefile_parts_toplevel property"""
        self._makefile_parts_toplevel = value

    @property
    def makefile_parts_common(self) -> OticaSection:
        """The getter for the makefile_parts_common property."""
        return self._makefile_parts_common

    @makefile_parts_common.setter
    def makefile_parts_common(self, value: OticaSection) -> None:
        """The setter for the makefile_parts_common property"""
        self._makefile_parts_common = value
    ### END OF PROPERTIES

    @staticmethod
    def check_no_overlap(section1: OticaSection,
                         section2: OticaSection,
                         section1_description: str,
                         section2_description: str) -> None:
        """Given two sections (section1 and section2) check to see if there are any
        elements in both sections. If there are, raise an exception. The
        parameters section1_description and section2_description are text
        strings that describe the two sections and are only used when the
        exception is raised.
        """

        list1 = section1.all_environments
        list2 = section2.all_environments

        if (list1 and list2):
            intersection = list(set(list1) & set(list2))

            if (len(intersection) > 0):
                msg = (f"you cannot have the same file in both the '{section1_description}' "
                       f"and '{section2_description}' sections "
                       f"(overlap: {intersection})")
                raise OticaYamlFileParse(msg)

    def check_for_invalid_elements(self) -> None:
        """Check for invalid sections in environment. If any are found raise
        an OticaYamlFileParse exception."""
        environment = self.otica_yaml_dict['environment']
        for section in environment:
            if (section not in OticaYaml.ENVIRONMENT_SECTIONS):
                msg = f"environment:{section} is not a recognized section"
                raise OticaYamlFileParse(msg)

        # Check for invalid sections in makefile_parts:
        makefile_parts = self.otica_yaml_dict['makefile_parts']
        for section in makefile_parts:
            if (section not in OticaYaml.MAKEFILE_PARTS_SECTIONS):
                msg = f"makefile_parts:{section} is not a recognized section"
                raise OticaYamlFileParse(msg)

    def populate(self) -> None:
        """Take the self.otica_yaml_dict dict and populate the various OticaYaml
        properties. Assumes that the object has already been normalized
        (see normalize method above).
        """
        otica_yaml_dict = self.otica_yaml_dict
        progress('populating OticaYaml object')

        environment          = otica_yaml_dict['environment']
        environment_toplevel = environment['toplevel']
        environment_common   = environment['common']
        environment_local    = environment['local']

        # The next three assignments convert a YAML section into an
        # OticaSection.
        if (not isinstance(environment_toplevel, OticaSection)):
            progress("converting environment_toplevel into an OticaSection")
            self.environment_toplevel = OticaSection(environment_toplevel,
                                                     label='environment_toplevel')
        else:
            progress("environment_toplevel is already an OticaSection")
            self.environment_toplevel = environment_toplevel

        if (not isinstance(environment_common, OticaSection)):
            progress("converting environment_common into an OticaSection")
            self.environment_common = OticaSection(environment_common,
                                                     label='environment_common')
        else:
            progress("environment_common is already an OticaSection")
            self.environment_common = environment_common

        if (not isinstance(environment_local, OticaSection)):
            progress("converting environment_local into an OticaSection")
            self.environment_local = OticaSection(environment_local,
                                                     label='environment_local')
        else:
            progress("environment_local is already an OticaSection")
            self.environment_local = environment_local

        progress(f"environment_toplevel: {self.environment_toplevel}")
        progress(f"environment_common:   {self.environment_common}")
        progress(f"environment_local:    {self.environment_local}")

        # ###        # ###        # ###        # ###        # ###        # ###        # ###

        makefile_parts = otica_yaml_dict['makefile_parts']
        makefile_parts_otica    = makefile_parts['otica']
        makefile_parts_toplevel = makefile_parts['toplevel']
        makefile_parts_common   = makefile_parts['common']

        if (not isinstance(makefile_parts_otica, OticaSection)):
            progress("converting makefile_part_otica into an OticaSection")
            self.makefile_parts_otica = OticaSection(makefile_parts_otica,
                                                     label='makefile_parts_otica')
        else:
            progress("makefile_parts_otica is already an OticaSection")
            self.makefile_parts_otica = makefile_parts_otica

        if (not isinstance(makefile_parts_toplevel, OticaSection)):
            progress("converting makefile_part_toplevel into an OticaSection")
            self.makefile_parts_toplevel = OticaSection(makefile_parts_toplevel,
                                                     label='makefile_parts_toplevel')
        else:
            progress("makefile_parts_toplevel is already an OticaSection")
            self.makefile_parts_toplevel = makefile_parts_toplevel

        if (not isinstance(makefile_parts_common, OticaSection)):
            progress("converting makefile_part_common into an OticaSection")
            self.makefile_parts_common = OticaSection(makefile_parts_common,
                                                     label='makefile_parts_common')
        else:
            progress("makefile_parts_common is already an OticaSection")
            self.makefile_parts_common = makefile_parts_common

        progress(f"makefile_parts_otica:    {self.makefile_parts_otica}")
        progress(f"makefile_parts_toplevel: {self.makefile_parts_toplevel}")
        progress(f"makefile_parts_common:   {self.makefile_parts_common}")

    def validate(self) -> None:
        """Do extra validation of the OticaYaml object. Currently these
        extra validations only check that there are no inappropriate overlaps
        between sections."""
        # It is an error to have any overlaps between the 'common' and 'toplevel'
        # sections in 'environment'.
        OticaYaml.check_no_overlap(self.environment_common,
                                   self.environment_toplevel,
                                   'environment:common',
                                   'environment:toplevel'
        )

        # It is an error to have any overlaps between the
        # 'makefile_parts:otica', 'makefile_parts:toplevel', and
        # 'makefile_parts:common', sections.
        OticaYaml.check_no_overlap(self.makefile_parts_otica,
                                   self.makefile_parts_toplevel,
                                   'makefile_parts:otica',
                                   'makefile_parts:toplevel'
        )
        OticaYaml.check_no_overlap(self.makefile_parts_toplevel,
                                   self.makefile_parts_common,
                                   'makefile_parts:toplevel',
                                   'makefile_parts:common'
        )
        OticaYaml.check_no_overlap(self.makefile_parts_otica,
                                   self.makefile_parts_common,
                                   'makefile_parts:otica',
                                   'makefile_parts:common'
        )

    def  __str__(self) -> str:
        """Convert the OticaYaml object to a string (used for debugging purposes)."""
        pprint_obj = pprint.PrettyPrinter(indent=4)
        return pprint_obj.pformat(self.otica_yaml_dict)

    def make_from_template(self, templates_dir: str) -> str:
        """Make the makefile.mk file from the Makefile template.
        """

        makefile_template_dir = os.path.join(templates_dir, 'makefiles')
        template_loader       = jinja2.FileSystemLoader(searchpath=makefile_template_dir)
        template_env          = jinja2.Environment(loader=template_loader)

        template = template_env.get_template(OticaYaml.makefile_tmpl_basename)

        # Populate the make_config object that is used by the Jinja template.
        make_config: dict[str, Any] = {}

        make_config['environment'] = {}
        make_config['environment']['toplevel'] = self.environment_toplevel
        make_config['environment']['common']   = self.environment_common
        make_config['environment']['local']    = self.environment_local

        make_config['makefile_parts'] = {}
        make_config['makefile_parts']['otica']    = self.makefile_parts_otica
        make_config['makefile_parts']['toplevel'] = self.makefile_parts_toplevel
        make_config['makefile_parts']['common']   = self.makefile_parts_common

        return str(template.render(make_config=make_config))

    def add(self, self2: 'OticaYaml') -> 'OticaYaml':
        """Given a second OticaYaml object self2 append self2's elements to self's. Be sure
           to remove any duplicates."""
        progress("adding an OticaYaml to another OticaYaml")

        env_dict  = {}
        make_dict = {}

        env_dict['toplevel'] = merge(self.environment_toplevel, self2.environment_toplevel)
        env_dict['common']   = merge(self.environment_common, self2.environment_common)
        env_dict['local']    = merge(self.environment_local, self2.environment_local)

        make_dict['otica']    = merge(self.makefile_parts_otica, self2.makefile_parts_otica)
        make_dict['toplevel'] = merge(self.makefile_parts_toplevel, self2.makefile_parts_toplevel)
        make_dict['common']   = merge(self.makefile_parts_common, self2.makefile_parts_common)

        otica_dict3 = {}
        otica_dict3['environment']    = env_dict
        otica_dict3['makefile_parts'] = make_dict

        progress("creating the merged OticaYaml object otica_yaml3")
        otica_yaml3 = OticaYaml(otica_dict3)

        return otica_yaml3

    @staticmethod
    def yaml_explanation_string() -> str:
        """A string that is put at the start of otica.yaml to explain
        how the otica.yaml file is configured and used."""
        explain_string="""
# Explanation of the different sections.
#
# environment:
#   toplevel:
#     # variable files in the environment:toplevel list are synced from the
#     # toplevel project's common/env_variables/ directory into the
#     # subproject's common/env_variables directory.
#     - framework.var
#   common:
#     # variable files listed here are included from the
#     # common/env_variables directory in the current subproject.
#     - common.var
#   local:
#     # variable files listed here are included from the
#     # directory running the make command.
#     - local.var
#
# makefile_parts:
#   otica:
#     # makefile modules listed here are included from the Otica
#     # source repository.
#     - framework.mk
#     - vault.mk
#     - authenticate.mk
#   toplevel:
#     # makefile modules listed here are included from the toplevel's
#     # common/makefile_parts directory.
#     # - project.mk
#   common:
#     # makefile modules listed here are included from the current
#     # subproject's common/makefile_parts directory.
#     # - app.mk
"""
        return explain_string
