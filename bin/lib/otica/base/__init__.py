"""A base class for other Otica classes. Provides some useful methods
that these other classes can use such as progress and exit_with_error."""

# pylint: disable=superfluous-parens
# pylint: disable=logging-fstring-interpolation

import hashlib
import logging
import os
import pathlib
import re
import subprocess
import sys
import yaml

from typing import Any, Optional, List, Dict  # pylint: disable=wrong-import-order

from otica.ologging import progress, info, verbose_enable, verbose_disable
from otica.oyaml    import OticaYaml
from otica.section  import OticaSection

from otica.utility  import exit_with_error, deprecation
from otica.utility  import validate_files_exist, copy_no_overwrite, make_dir
from otica.utility  import get_file_hash, copy_if_changed, create_empty_no_overwrite

from otica.errors   import OticaGenericError
from otica.errors   import OticaNoTopLevelDirectory


# We simplify the logging message format and send all
# logs to standard output. We default to INFO.
logging.basicConfig(level=logging.INFO,
                    format='%(message)s',
                    stream = sys.stdout)



class OticaBase():
    """A base class for other Otica classes. Provides some useful methods
    that these other classes can use such as progress and exit_with_error."""
    def __init__(self, fake_parameter: bool = False):
        pass

class OticaDir(OticaBase):
    """A class that represents an Otica directory. This directory should be a
    toplevel, subproject, or framework (i.e., Otica source) directory.
    This is mainly useful to give a short-hand way to return the paths to the
    various subdirectories of common/.
    """

    def __init__(self, path: str, common: str = 'common'):
        self.path = path

        self.env_variables_dir  = os.path.join(self.path, common, 'env_variables')
        self.makefile_parts_dir = os.path.join(self.path, common, 'makefile_parts')
        self.scripts_dir        = os.path.join(self.path, common, 'scripts')
        self.templates_dir      = os.path.join(self.path, common, 'templates')


class SubprojectPlatform(OticaBase):
    """An object representing a "platform", that is, one of the entries
    in SUBPROJECT_PLATFORMS_FILENAME file."""
    def __init__(self, name: str, config: Dict[str, Any]):
        self.name       = name
        self.otica_yaml = OticaYaml(config)

class SubprojectPlatforms(OticaBase):
    """The class that represents a subproject otica.yaml settings.
    """

    # The name of the platforms YAML file.
    SUBPROJECT_PLATFORMS_FILENAME = 'subproject-platforms.yaml'

    def __init__(self, otica_obj: 'Otica'):
        self.otica_obj  = otica_obj
        self.platforms: Dict[str, SubprojectPlatform] = {}

        msg = 'setting up SubprojectPlatform object'
        progress(msg)

        # Step 1. Load the subproject platforms file. Will use the default
        # if not top-level platforms file can be found.
        self.platforms_file = self.get_toplevel_platforms_file()
        self.load(self.platforms_file)

    def get(self, key: str) -> SubprojectPlatform:
        """Given a platform name return the SubprojectPlatform corresponding
        to that name. Maps the name "generic" to the "ALL" platform."""
        # Remember that 'generic' maps to the default 'ALL'.
        if (key == 'generic'):
            val = self.platforms.get('ALL')
        else:
            val = self.platforms.get(key)

        if (val is None):
            msg = "there is no platform '{key}'"
            exit_with_error(msg)
            raise Exception(msg) # To make mypy happy!

        return val

    def add(self, platform1: str, platform2: str) -> OticaYaml:
        """Merge platform1 with platform2 with
           the settings in platform2 ADDING to those of platform1.
           Returns an OticaYaml object."""
        progress(f"adding '{platform2}' to '{platform1}'")

        # They are both not None
        otica_yaml1 = self.get(platform1).otica_yaml
        otica_yaml2 = self.get(platform2).otica_yaml
        return otica_yaml1.add(otica_yaml2)

    def get_toplevel_platforms_file(self) -> str:
        """Return the full path to the toplevel platforms file. If the toplevel
        platforms file does not exist return the full path to the Otica project
        default platforms file."""
        toplevel_path      = self.otica_obj.get_toplevel_path()
        platform_file_path = os.path.join(toplevel_path,
                                          'common',
                                          SubprojectPlatforms.SUBPROJECT_PLATFORMS_FILENAME)
        if os.path.exists(platform_file_path):
            msg = f"found toplevel platforms file {platform_file_path}"
            progress(msg)
            return platform_file_path

        msg = 'no toplevel platforms file found; will use Otica default'
        progress(msg)
        templates_dir      = self.otica_obj.path_to_otica_yaml_template_dir()
        platform_file_path = os.path.join(templates_dir,
                                          SubprojectPlatforms.SUBPROJECT_PLATFORMS_FILENAME)
        if (not os.path.exists(platform_file_path)):
            msg = f"could not find default platforms file '{platform_file_path}'"
            raise OticaGenericError(msg)

        return platform_file_path

    def names(self) -> List[str]:
        """Return an array of all the platform names. These are the names
        as listed in the subproject-platforms.yaml file."""
        names = []
        for platform_name in self.platforms:
            names.append(platform_name)

        return names

    def friendly_names(self) -> List[str]:
        """Same as names above but replaces 'ALL' with 'generic'."""
        names = self.names()
        names.remove('ALL')
        names.append('generic')
        return names

    def load(self, platforms_file: str) -> None:
        """Given a platforms file read the file and populate the platforms property
        with the file's contents."""
        with open(platforms_file, encoding="ascii") as file:
            platforms = yaml.load(file, Loader=yaml.FullLoader)
            msg = f"loaded platforms file {platforms_file}"
            progress(msg)

        # We want one platform object for each platform.
        for platform in platforms.keys():
            msg = f"processing platform '{platform}' from platforms file"
            progress(msg)
            platform_dict = SubprojectPlatform(platform, platforms[platform])
            self.platforms[platform] = platform_dict

class Otica(OticaBase):
    """
    This class does most of the work of parsing the otica.yaml file as well
    as setting up the common/ directory.
    """

    PROJECT_TYPE_TOPLEVEL    = 'toplevel'
    PROJECT_TYPE_SUBPROJECT  = 'subproject'
    PROJECT_TYPE_ENVIRONMENT = 'environment'
    project_types = [
        PROJECT_TYPE_TOPLEVEL,
        PROJECT_TYPE_SUBPROJECT,
        PROJECT_TYPE_ENVIRONMENT,
    ]
    PROJECT_TEXT_LIST = (f"'{PROJECT_TYPE_TOPLEVEL}', "
                         f"'{PROJECT_TYPE_SUBPROJECT}', "
                         f"or '{PROJECT_TYPE_ENVIRONMENT}'"
    )

    # The basename of the Makefile template
    makefile_tmpl_basename = 'Makefile_v4.tmpl'

    def __init__(self,
                 verbose:          bool          = False,
                 action:           Optional[str] = None,
                 project_type:     Optional[str] = None,
                 platform:         Optional[str] = None,
                 environment_name: str ='new-env',
                 subtype:          str = "generic"):

        self.verbose          = verbose
        self.config_file      = 'otica.yaml'
        self.action           = action
        self.project_type     = project_type
        self.platform         = platform
        self.environment_name = environment_name
        self.subtype          = subtype

        if (self.verbose):
            verbose_enable()
        else:
            verbose_disable()

        self.makefile_parts_otica:    OticaSection
        self.makefile_parts_toplevel: OticaSection
        self.makefile_parts_common:   OticaSection

        # This will get populated by an OticaYaml object once
        # the otica.yaml file is read.
        self.otica_yaml: OticaYaml

        # _toplevel_indicator_filename is the name of the file
        # that exists in a directory indicating that the directory is
        # a toplevel Otica project.
        self._toplevel_indicator_filename = '.otica-top-level'

        # Set the path to the Otica source code base directory (this
        # corresponds to the environment variable FRAMEWORK_DIR).
        self.framework_path = self.get_framework_path()

        # Set the Otica project toplevel directory:
        self.toplevel_path  = self.get_toplevel_path()
        progress(f"project toplevel directory is {self.toplevel_path}")

        progress(f"action is           '{action}'")
        progress(f"project_type is     '{project_type}'")
        progress(f"subtype is          '{self.subtype}'")
        progress(f"environment_name is '{environment_name}'")

        if (action == 'version'):
            print(Otica.get_otica_version())
            sys.exit(0)

        if (action == 'create'):
            if (project_type is None):
                msg = f"when creating indicate the project type: {Otica.PROJECT_TEXT_LIST}"
                exit_with_error(msg)
            elif (project_type not in Otica.project_types):
                msg = f"project type must be one of {Otica.PROJECT_TEXT_LIST}"
                exit_with_error(msg)

        self.framework_var_file = 'framework.var'

        # Create some OticaDir objects.
        self.otica_dir    = OticaDir(path=self.framework_path, common="")
        self.toplevel_dir = OticaDir(path=self.toplevel_path)
        self.project_dir  = OticaDir(path=os.path.join('.'))

        # Create the SubprojectPlatforms object
        self.subproject_platforms = SubprojectPlatforms(self)

        platform_names = self.subproject_platforms.friendly_names()
        if ((self.platform is not None) and (self.platform not in platform_names)):
            msg = f"'{self.platform}' is not a recognized platform ({platform_names})"
            exit_with_error(msg)


    ### START OF PROPERTIES
    @property
    def action(self) -> Optional[str]:
        """The getter for the action property"""
        return self._action

    @action.setter
    def action(self, value: str) -> None:
        """The setter for the action property"""
        self._action = value

    @property
    def project_type(self) -> Optional[str]:
        """The getter for the project_type property"""
        return self._project_type

    @project_type.setter
    def project_type(self, value: str) -> None:
        """The setter for the project_type property"""
        self._project_type = value

    @property
    def subtype(self) -> str:
        """The getter for the subtype property"""
        return self._subtype

    @subtype.setter
    def subtype(self, value: str) -> None:
        """The setter for the subtype property"""
        self._subtype = value

    @property
    def platform(self) -> Optional[str]:
        """The getter for the platform property"""
        return self._platform

    @platform.setter
    def platform(self, value: str) -> None:
        """The setter for the platform property"""
        self._platform = value
    ### END OF PROPERTIES

    ### Boolean functions (is_something)
    def is_toplevel_project(self) -> bool:
        """
        Return True if this is (or will be) an Otica toplevel directory,
        False otherwise.
        """
        if (self.project_type == 'toplevel'):
            return True

        if (self.is_toplevel_dir(os.getcwd())):
            return True

        return False


    def is_create(self) -> bool:
        """
        Return True if the action is a create, False otherwise.
        """
        return (self.action == 'create')

    def is_update(self) -> bool:
        """
        Return True if the action is an update, False otherwise.
        """
        return (self.action == 'update')

    def is_validate(self) -> bool:
        """
        Return True if the action is validate, False otherwise.
        """
        return (self.action == 'validate')

    def is_env(self) -> bool:
        """
        Return True if the project_type is an environment, False otherwise.
        """
        return (self.project_type == Otica.PROJECT_TYPE_ENVIRONMENT)

    def is_toplevel_dir(self, path: str) -> bool:
        """Is path an Otica toplevel directory?

        A directory is an Otica toplevel directory if, and only if, it contains
        a file with the name self._toplevel_indicator_filename.
        """
        toplevel_indicator_path = os.path.join(path, self._toplevel_indicator_filename)
        return os.path.exists(toplevel_indicator_path)

    ### END OF Boolean functions (is_something)


    ### END OF File copying methods

    ### Path methods
    def path_to_otica_yaml(self) -> str:
        """Return the path to this directory's otica.yaml file"""
        return os.path.join('.', self.config_file)

    def templates_dir(self) -> str:
        """Convenience method to return path to the Otica source directory
        containing the templates file used when creating Otica projects.
        """
        return self.otica_dir.templates_dir

    def path_to_otica_yaml_template_dir(self) -> str:
        """Return the path to the directory containing otica.yaml template files."""
        return os.path.join(self.templates_dir(), 'otica-yaml')


    def get_framework_path(self) -> str:
        """This is the path to the Otica source code directory.

        We get the path by finding the directory containing _this_ file
        ("lib/otica/base/__init__.py") and going up several levels.

        """
        cur_dir  = os.path.dirname(os.path.abspath(__file__))

        cur_path       = pathlib.Path(cur_dir)
        framework_path = cur_path.parent.parent.parent.parent
        progress(f"framework path is {framework_path}")

        return str(framework_path)

    def get_toplevel_path(self) -> str:
        """Get the path to the Otica toplevel project directory.

        Here is the algorithm:

        1. If the TL_BASEDIR_OVERRIDE environment variable is set to a
        non-empty value, return that.

        2. If we are doing a "create toplevel" then return the current
        directory.

        3. If the current directory already is an Otica toplevel project
        directory return the current directory.

        4. Walk up the parent directory tree looking for a top-level
        directory until we find one.

        5. If there is no Otica toplevel project raise an
        OticaNoTopLevelDirectory exception.

        """

        # 1. If the TL_BASEDIR_OVERRIDE environment variable is set to a
        # non-empty value, return that.
        tl_basedir_override = os.environ.get('TL_BASEDIR_OVERRIDE')
        if ((tl_basedir_override is not None) and (tl_basedir_override != "")):
            return tl_basedir_override

        cur_dir = os.getcwd()

        # 2. Are we in the midst of a toplevel directory create? If so, return
        # the current directory.
        if (self.is_create() and (self.project_type == Otica.PROJECT_TYPE_TOPLEVEL)):
            return cur_dir

        # 3. Are we currently in a toplevel directory? If so, return the
        # current directory.
        if (self.is_toplevel_dir(cur_dir)):
            # The current directory already is an Otica project directory.
            return cur_dir

        # 4. Go up the directory tree from the current directory looking for
        # for a toplevel directory.
        root_dir = pathlib.Path(cur_dir).root

        while (cur_dir != root_dir):
            # Get the parent of cur_dir.
            cur_parent = os.path.dirname(cur_dir)

            # Does this directory look like an Otica toplevel directory?
            if (self.is_toplevel_dir(cur_parent)): # pylint: disable=no-else-return
                msg = f"{cur_parent} appears to be an Otica directory"
                progress(msg)
                return cur_parent
            else:
                msg = f"{cur_parent} appears NOT to be an Otica directory"
                progress(msg)
                cur_dir = cur_parent

        # 5. If we get here then we could find no parent directory
        # corresponding to an Otica directory. This is an error.
        raise OticaNoTopLevelDirectory

    ### END OF Path methods

    def otica_yaml_exists(self) -> bool:
        """Return True if the otica.yaml file exists, False otherwise"""
        return os.path.exists(self.path_to_otica_yaml())

    def parse_yaml(self) -> None:
        """Parse the config_file (otica.yaml) to create an OticaYaml object.
        """
        progress(f"parsing {self.config_file}")

        if (not os.path.exists(self.config_file)):
            msg = f"missing configuration file {self.config_file}"
            exit_with_error(msg)
        else:
            msg = f"found configuration file {self.config_file}"
            progress(msg)

        # Read in YAML file
        with open(self.config_file, encoding="ascii") as file:
            otica_yaml_dict = yaml.full_load(file)

        self.otica_yaml = OticaYaml(otica_yaml_dict)

    def validate_files(self) -> None:
        """Validate that the files in the environment and makefile_parts sections
        of the otica.yaml file actually exist. Will throw an exception if any files
        are missing. Call this method only AFTER parsing otica.yaml."""

        progress('validating env_variables files')
        validate_files_exist(self.project_dir.env_variables_dir,
                             self.otica_yaml.environment_common.get_filenames())
        validate_files_exist(self.toplevel_dir.env_variables_dir,
                             self.otica_yaml.environment_toplevel.get_filenames())

        progress('validating makefile_parts files')
        validate_files_exist(self.otica_dir.makefile_parts_dir,
                             self.otica_yaml.makefile_parts_otica.get_filenames())
        validate_files_exist(self.toplevel_dir.makefile_parts_dir,
                             self.otica_yaml.makefile_parts_toplevel.get_filenames())
        validate_files_exist(self.project_dir.makefile_parts_dir,
                             self.otica_yaml.makefile_parts_common.get_filenames())

    def copy_toplevel_env_files(self) -> None:
        """Copy any specified variable files from the Otica toplevel project
           common/env_variables directory (if they exist) into the local
           common/env_variables directory.
        """

        dry_run = False
        if (self.action == 'check'):
            msg = "[copy_toplevel_env_files] running with the 'check' option"
            progress(msg)
            dry_run = True
        else:
            msg = ('copying specified env files from the '
                   'Otica toplevel project common/env_variables '
                   'directory into local common/env_variables')
            progress(msg)

        files_changed = []
        if (self.otica_yaml.environment_toplevel is not None):
            for var_file in self.otica_yaml.environment_toplevel.get_filenames():
                # Copy the file from the toplevel Otica project.
                # Note that these files _will_ overwrite existing copies!
                source = os.path.join(self.toplevel_dir.env_variables_dir, var_file)
                target = os.path.join(self.project_dir.env_variables_dir,  var_file)

                # If the source file does not exist in the toplevel Otica project,
                # and the target also does not exist, create an empty file.
                if (copy_if_changed(source, target, f"common/env_variables/{var_file}", dry_run=dry_run)):
                    files_changed.append(var_file)
                    if (not dry_run):
                        msg = (f"copied file {var_file} from your "
                               f"toplevel Otica project into common/env_variables/{var_file}")
                        info(msg)

        if (dry_run and (len(files_changed) > 0)):
            msg = (
                f"WARNING: One or more of your environment files are not in sync\n"
                f"with their corresponding Otica project toplevel files.\n"
                f"Run 'make otica-update' to bring the files into sync.\n"
                f"Here are the out-of-sync file(s)\n"
                f"--------------------------------"
            )
            info(msg)
            for file1 in files_changed:
                print(file1)


    def initialize_common_var_files(self) -> None:
        """Initialize as empty files any files listed in environment:common.
           Do not overwrite any existing files. For common/env-variables/common.var
           we copy the one from the templates directory.
        """
        progress('initializing common/env_variables var files')
        for var_file in self.otica_yaml.environment_common.get_filenames():
            progress(f"processing var file '{var_file}'")
            match_obj = re.match(r'^.*\.(sh|mk)$', var_file)
            if (match_obj):
                ext = match_obj.group(1)
                msg = (f"{var_file}: variable files with the .{ext} "
                        "extension are deprecated; please use the .var extension")
                deprecation(msg)

            # If var_file has a matching filename in the templates directory copy that
            # one instead of making an empty file.
            source = os.path.join(self.templates_dir(), 'var-files', var_file)
            target = os.path.join(self.project_dir.env_variables_dir, var_file)
            if (os.path.exists(source)):
                progress("there is a template version of '{var_file}' so will use that one")
                copy_no_overwrite(source, target, target)
            else:
                create_empty_no_overwrite(target, f"common/env_variables/{var_file}")

    def create_otica_project_makefile(self) -> None:
        """If this is an Otica project (i.e., toplevel) directory we want to
           create a Makefile at the toplevel that sets COMMON correctly.
        """
        if (os.path.exists('Makefile')):
            progress("skipping create of 'Makefile' file (already exists)")
        else:
            with open("Makefile", "w", encoding="ascii") as text_file:
                text_file.write("export COMMON := common\n")
                text_file.write("include ${COMMON}/makefile.mk\n")
            info("created Makefile")

    def create_otica_repos_yaml(self) -> None:
        """Create a starter repos.yaml file for a toplevel Otica project."""
        otica_yaml_templates_path = self.path_to_otica_yaml_template_dir()

        source = os.path.join(otica_yaml_templates_path, 'repos.yaml.template')
        target = os.path.join(os.getcwd(), 'repos.yaml')
        copy_no_overwrite(source, target, target)

    def copy_otica_env_examples(self) -> None:
        """Copy example Otica project files into the env/ directory.
        """
        templates_dir = self.templates_dir()
        example_files = ['gcp.var', 'aws.var']
        for example_file in example_files:
            source = os.path.join(templates_dir, 'var-files', f"{example_file}.example")
            target = os.path.join('common', 'env_variables', f"{example_file}.example")
            copy_no_overwrite(source, target, f"{example_file}.example")

    def copy_otica_framework_var(self) -> None:
        """For a toplevel Otica project we copy the framework.var file from the
           templates directory.
        """
        templates_dir = self.templates_dir()
        source = os.path.join(templates_dir, self.framework_var_file)
        target = os.path.join('common', 'env_variables', self.framework_var_file)
        copy_if_changed(source, target, 'common/env_variables/framework.var')

    def copy_gitignore(self) -> None:
        """We copy a starter .gitignore from the templates directory. We do NOT
           keep this file in sync, rather, we copy it once and then do not
           copy again (unless it gets deleted). The .gitignore contents
           will depend on whther this is a toplevel Otica project or a
           subproject.

        """
        templates_dir  = self.templates_dir()
        git_ignore_dir = os.path.join(templates_dir, 'dot.gitignore')

        if (self.is_toplevel_project()):
            source = os.path.join(git_ignore_dir, 'toplevel')
        else:
            source = os.path.join(git_ignore_dir, 'subproject')

        target = os.path.join('.', '.gitignore')
        copy_no_overwrite(source, target, '.gitignore')

    def otica_project_link_framework_var(self) -> None:
        """For a toplevel Otica project create the file env/framework.var that
           links to the file common/framework.var. This makes it possible for
           subprojects to sync their own framework.var file to the one at the
           Otica project level.
        """
        # Only make symlink if file does not already exist
        target = os.path.join('env', self.framework_var_file)
        if (os.path.isfile(target) or os.path.islink(target)):
            progress("framework.var already exists so no need to link")
        else:
            os.symlink('../common/framework.var', target)

    def makemake(self) -> None:
        """Make the common/makefile.mk file"""

        # NOTE: Once we get to Python 3.10 change this to a match/case.
        if (self.action == 'check'):
            progress("running makemake with the 'check' option")
            self.parse_yaml()
            self.copy_toplevel_env_files()
        elif (self.is_update()):
            ## UPDATE
            # There is nothing to do for an 'env' update.
            if (self.is_env()):
                info('there is no update action for an env')
            else:
                self.makemake_update()
        elif (self.is_validate()):
            ## VALIDATE
            self.parse_yaml()
            self.validate_files()
            print("otica.yaml is valid")
        else:
            ## CREATE
            if (self.is_env()):
                self.makemake_env_create()
            else:
                self.makemake_create()
                self.makemake_update()
                self.validate_files()

    def makemake_update(self) -> None:
        """This UPDATES an existing project/subproject"""
        progress("running makemake update")

        self.parse_yaml()
        otica_yaml = self.otica_yaml

        progress("setting up directories, makefiles, and environment files")

        progress("creating common directory")
        make_dir('common')

        progress("creating common/env_variables directory")
        make_dir(os.path.join('.', 'common', 'env_variables'))

        progress("creating common/makefile_parts directory")
        make_dir(os.path.join('.', 'common', 'makefile_parts'))

        progress("creating common/scripts directory")
        make_dir(os.path.join('.', 'common', 'scripts'))

        progress("dealing with common/makefile.mk")
        makefile_mk_path    = os.path.join('common', 'makefile.mk')
        new_makefile_string = otica_yaml.make_from_template(self.templates_dir())

        if (os.path.exists(makefile_mk_path)):
            old_md5sum = get_file_hash(makefile_mk_path)
            new_md5sum = hashlib.md5(new_makefile_string.encode('utf-8')).hexdigest()
            progress(f"old_md5sum: {old_md5sum}")
            progress(f"new_md5sum: {new_md5sum}")

            if (old_md5sum == new_md5sum):
                progress('old and new makefile string are the same')
                info('skipping update of common/makefile.mk (no changes)')
                write_new_makefile = False
            else:
                progress('old and new makefile string hashes differ')
                info('updating common/makefile.mk')
                write_new_makefile = True
        else:
            info("creating common/makefile.mk")
            write_new_makefile = True

        if (write_new_makefile):
            with open('common/makefile.mk', 'w', encoding="ascii") as file3:
                progress('writing new makefile string to common/makefile.mk')
                file3.write(new_makefile_string)
                file3.close()

        progress("copying toplevel directory files")
        self.copy_toplevel_env_files()
        self.copy_gitignore()

        ## In this last section we do things differently depending on whether
        ## this is an Otica toplevel directory or not.
        if (self.is_toplevel_project()):
            progress("performing toplevel project specific tasks")

            # Create the directory .otica and put in it a README file.
            # This directory indicates that this directory is a toplevel Otica project.
            self.create_otica_project_indicator()

            # Create the Makefile at the base directory. Will only
            # contain a line including the makefile.mk in common
            self.create_otica_project_makefile()

            # Put into common/env_variables/ directory some example
            # .var files.
            self.copy_otica_env_examples()
            self.copy_otica_framework_var()

            # Make the sub-projects/ directory and create a starter
            # repos.yaml file.
            make_dir('sub-projects')
            self.create_otica_repos_yaml()

            # Copy the default subproject-platforms.yaml file into common/
            self.create_subproject_platforms_yaml()
        else:
            progress("performing subproject specific tasks")

            progress("creating common/env_variables variable files")
            self.initialize_common_var_files()

    def makemake_create(self) -> None:
        """This CREATES a new project, subproject, or environment"""
        progress("running makemake CREATE")

        # Error Check #1. If the otica.yaml file already exists, exit with an error.
        if (self.otica_yaml_exists()):
            exit_with_error("cannot create a new project if otica.yaml already exists")
        else:
            progress("otica.yaml does not yet exist, so will create one from a template")

        # Error Check #2. If not creating a toplevel project, but this
        # appears to already _be_ a toplevel directory, abort.
        if ((self.project_type != 'toplevel') and (self.is_toplevel_dir(os.getcwd()))):
            msg = f"cannot create a {self.project_type} in a toplevel directory " \
                  f"(file '{self._toplevel_indicator_filename}' exists)"
            exit_with_error(msg)

        # Create common/ (needed by self.extra_create_setup())
        docker_dir = os.path.join('common')
        make_dir(docker_dir)

        if (self.project_type == 'toplevel'):
            otica_yaml_string = self.makemake_create_toplevel()
        else:
            # From the subprojects file subproject-platforms.yaml (at the
            # toplevel, if it exists, or from the Otica project itself)
            # create a starter otica.yaml file.
            otica_yaml_string = self.makemake_create_subproject_otica_yaml()
            self.extra_create_setup()

        progress("writing otica.yaml file from string")
        with open(self.path_to_otica_yaml(), 'w', encoding="ascii") as file1:
            file1.write(otica_yaml_string)
            file1.write("\n")
            file1.close()
        info("wrote otica.yaml")

    def makemake_create_subproject_otica_yaml(self) -> str:
        """Return a string to be used as the Otica subproject otica.yaml file for
        a new subproject. Use the framework directory template file unless there is
        a toplevel template."""

        subproject_platforms = self.subproject_platforms
        progress(f"self.platform is {self.platform}")

        # Get the platform configuration from subproject_platforms corresponding
        # to platform. Add it to the generic platform.
        if (self.platform is not None):
            otica_yaml = subproject_platforms.add('generic', self.platform)
        else:
            otica_yaml = subproject_platforms.platforms['generic'].otica_yaml

        # We add in some extra makefile modules if subtype is set.
        if (self.subtype is not None):
            progress(f"adding subtype {self.subtype}'s Make module")
            if (self.subtype == 'helm'):
                otica_yaml.makefile_parts_otica.append('helm-develop.mk')
            elif (self.subtype == 'docker'):
                otica_yaml.makefile_parts_otica.append('docker-develop.mk')

        explanation_string = OticaYaml.yaml_explanation_string()
        yaml_string        = otica_yaml.to_yaml_string()

        return "---\n" + explanation_string + "\n" + yaml_string

    def makemake_create_toplevel(self) -> str:
        """Create an otica.yaml file for a new Otica toplevel project."""
        progress("installing Otica project otica.yaml template file")
        template_name = 'main.yaml.template'

        otica_yaml_templates_path = self.path_to_otica_yaml_template_dir()
        source = os.path.join(otica_yaml_templates_path, template_name)

        with open(source, 'r', encoding="ascii") as file1:
            data = file1.read()

        return data

    def extra_create_setup(self) -> None:
        """Add extra files during subproject create. These extra files will depend on
           the subproject's subtype."""
        progress("calling extra create setup")
        if (self.subtype == 'docker'):
            # Create common/docker
            docker_dir = os.path.join('common', 'docker')
            make_dir(docker_dir)

            # Copy a skeleton Dockerfile
            template_name = 'Dockerfile.template'
            source = os.path.join(self.templates_dir(), template_name)

            with open(source, 'r', encoding="ascii") as file1:
                data = file1.read()

            dockerfile_path = os.path.join(docker_dir, 'Dockerfile')
            info(f"writing a default Dockerfile for you at {dockerfile_path}")
            with open(dockerfile_path, 'w', encoding="ascii") as file1:
                file1.write(data)
                file1.close()
        elif (self.subtype == 'helm'):
            # Run "helm create" to create an initial helm chart in common/helm.
            cmd = ['helm', 'create', 'common/helm']
            subprocess.check_call(cmd)
            info('initialized Helm chart in common/helm')


    def create_subproject_platforms_yaml(self) -> None:
        """Create (if missing) the toplevel file common/subproject-platforms.yaml."""
        # Copy the default subproject platforms file to common/
        platforms_file = SubprojectPlatforms.SUBPROJECT_PLATFORMS_FILENAME
        otica_yaml_templates_path = self.path_to_otica_yaml_template_dir()
        source = os.path.join(otica_yaml_templates_path, platforms_file)

        target = os.path.join('.', 'common', platforms_file)
        copy_no_overwrite(source, target, 'subprojects-platforms.yaml file')


    def makemake_env_create(self) -> None:
        """Create a new environment directory.
        The directory will have the name self.environment_name."""
        name = self.environment_name
        progress(f"environment name is {name}")

        if (name and os.path.isdir(name)):
            exit_with_error(f"the directory '{name}' already exists")

        os.mkdir(name)

        makefile_template_path = os.path.join(self.templates_dir(), 'new-env-makefile.tmpl')

        progress(f"making {name}/Makefile")
        copy_no_overwrite(makefile_template_path, f"{name}/Makefile", f"{name}/Makefile")

        progress(f"making file {name}/local.var")
        target = os.path.join(name, 'local.var')
        source = os.path.join(self.templates_dir(), 'var-files', 'local.var')
        copy_no_overwrite(source, target, f"{name}/local.var")

        info(f"setup new environment directory '{name}'")

    def create_otica_project_indicator(self) -> None:
        """Create the toplevel indicator file, that is, the file that
        is used to signal that the directory is a toplevel Otica project."""

        progress('making toplevel project indicator file')
        source = os.path.join(self.templates_dir(), 'dot.otica-top-level')
        target = os.path.join('.', self._toplevel_indicator_filename)
        copy_no_overwrite(source, target, f"adding {self._toplevel_indicator_filename}")


    @staticmethod
    def get_otica_version() -> str:
        """Return the Otica version from the OTICA_VERSION file."""
        my_parent = pathlib.Path(__file__).parent.parent.parent.parent.parent.resolve()
        version_file = os.path.join(my_parent, 'OTICA_VERSION')
        with open(version_file, encoding="ascii") as vfile:
            version_string = vfile.read().strip()

        return version_string
