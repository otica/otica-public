import hashlib
import os
import shutil
import sys

from typing import List

# pylint: disable=superfluous-parens

from otica.errors   import OticaMissingFileFromDirectory
from otica.ologging import progress, info

### Generic file methods

def validate_files_exist(path: str, file_names: List[str]) -> None:
    """Given a directory (path) and a list of file names (file_names) verify
    that for each file in the list of file names a file of that name
    exists in the directory. Raise an OticaMissingFileFromDirectory
    exception if any are missing.
    """
    msg = f"validating files in directory {path}"
    progress(msg)

    for file_name in file_names:
        msg = f"verifying that there is a file '{file_name}' in directory {path}"
        progress(msg)
        path_to_file = os.path.join(path, file_name)
        if (not os.path.exists(path_to_file)):
            raise OticaMissingFileFromDirectory(directory=path, file_name=file_name)

        msg = f"verified file '{file_name}' is in directory {path}!"
        progress(msg)

def get_file_hash(path: str) -> str:
    """Return the MD5 hash (hex version) of the file given by path"""
    with open(path, 'r', encoding='ascii') as file:
        contents = file.read()
        md5sum   = hashlib.md5(contents.encode('utf-8')).hexdigest()
    return md5sum

def make_dir(path: str) -> None:
    """Make a local directory unless the directory already exists.
    """
    if (os.path.isdir(path)):
        msg = f"skipping create of {path} directory (already exists)"
        progress(msg)
    else:
        msg = f"creating {path}/ directory"
        info(msg)
        os.makedirs(path)

### File copying methods
def copy_if_changed(source: str, target: str, shortname: str, dry_run: bool=False) -> bool:
    """"Copy source onto target, but only if the source file is different than target.
    Return True if copy happened, False otherwise.

    If the dry_run parameter is True print out the files to be copied
    rather than actually copying them.
    """
    if (os.path.isdir(target)):
        msg = f"copy_no_overwrite target ({target}) cannot be a directory"
        exit_with_error(msg)

    should_copy = False
    if (not os.path.exists(target)):
        should_copy = True
        msg = f"{shortname} does not exist, so copying"
        info(msg)
    else:
        source_md5sum   = get_file_hash(source)
        target_md5sum   = get_file_hash(target)

        if (source_md5sum == target_md5sum):
            msg = f"{shortname} source and target are the same so not copying"
            progress(msg)
            should_copy = False
        else:
            should_copy = True
            if (not dry_run):
                msg = f"{shortname} source and target are different so copying"
                info(msg)

    if (should_copy and (not dry_run)):
        shutil.copy(source, target)

    return should_copy

def copy_no_overwrite(source: str, target: str, shortname: str) -> None:
    """Copy source onto target, but skip if target already exists. The target
       parameter must not be a directory. The shortname serves in the
       progress section and is meant to give a shorter version of the
       file being copied.
    """
    if (os.path.isdir(target)):
        msg = f"copy_no_overwrite target ({target}) cannot be a directory"
        exit_with_error(msg)

    if (os.path.exists(target)):
        msg = f"file {shortname} exists so not overwriting"
        progress(msg)
    else:
        progress(f"copying '{source}' onto '{target}'")
        shutil.copy(source, target)
        msg = f"copied {shortname}"
        info(msg)

def create_empty_no_overwrite(target: str, shortname: str) -> None:
    """Create the file specified by target as an empty file.
       Do not overwite if target already exists.
    """
    if (os.path.exists(target)):
        msg = f"file {shortname} exists so not overwriting"
        progress(msg)
    else:
        progress(f"creating empty file '{shortname}'")
        open(target, 'a', encoding='ascii').close() # pylint: disable=consider-using-with

def dedup(array: List[str]) -> List[str]:
    """Given an array return an the array with duplicates removed (preserves
       order).
    """
    tracker = {}
    dedupped = []
    for element in array:
        if (element in tracker):
            # skip
            pass
        else:
            dedupped.append(element)
            tracker[element] = True

    return dedupped

def exit_with_error(msg: str) -> None:
    """Exit with an error message."""
    sys.stderr.write(f"error: {msg}\n")
    sys.exit(1)


def deprecation(msg: str) -> None:
    """Print a deprecation warning message"""
    print(f"DEPRECATION WARNING: {msg}")
