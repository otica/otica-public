import logging
import os
import pathlib
import subprocess
import sys
import tempfile

from typing import Tuple

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

def otica_bin_dir() -> str:
    parent_dir = os.path.join(pathlib.Path(__file__).parent.parent.resolve())
    bin_dir = os.path.join(parent_dir, 'bin')
    return bin_dir

def path_to_cli() -> str:
    return os.path.join(otica_bin_dir(), 'otica')

def run_otica_cmd_in_directory(directory_path: str, cmd: list[str]) -> Tuple[str, str, int]:
    os.chdir(directory_path)
    otica_cli = path_to_cli()
    otica_cmd = [otica_cli] + cmd
    return run(otica_cmd)

def run(cmd: list[str]) -> Tuple[str, str, int]:
    proc = subprocess.Popen(cmd,
                            stdout = subprocess.PIPE,
                            stderr = subprocess.PIPE,
                            )
    stdout, stderr = proc.communicate()
    return (stdout.decode("utf-8") , stderr.decode("utf-8") , proc.returncode)

def make_tempdir() -> str:
    tdir = tempfile.TemporaryDirectory()
    return str(tdir.name)

def logme(msg: str) -> None:
    log = logging.getLogger("TestLog")
    log.debug(msg)

def make_toplevel(directory_path: str) -> Tuple[str, str, int]:
    os.chdir(directory_path)
    otica_cli = path_to_cli()

    cmd = [otica_cli, 'create', 'toplevel']
    return run(cmd)

def make_subproject(directory_path: str) -> Tuple[str, str, int]:
    # First create the directory.
    os.makedirs(directory_path)
    os.chdir(directory_path)

    otica_cli = path_to_cli()

    cmd = [otica_cli, 'create', 'subproject', 'generic']
    return run(cmd)


