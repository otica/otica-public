#!gomplate
# file maps environment variables and vault secrets to helm values
# See https://github.com/kubernetes/charts/tree/master/stable/external-dns

## The DNS provider where the DNS records will be created (options: aws, google, inmemory, azure )
provider: google
google:
  project: '{{ .Env.EXTERNAL_DNS_GOOGLE_PROJECT }}'
  serviceAccountKey: '{{ .Env.EXTERNAL_DNS_GCP_CREDENTIALS }}'

# When using the TXT registry, a name that identifies this instance of ExternalDNS
txtOwnerId: '{{ .Env.GKE_CLUSTER_NAME }}'

## Limit possible target zones by domain suffixes (optional)
##
{{ if getenv "EXTERNAL_DNS_DOMAIN_FILTERS" }}
domainFilters: [ {{ .Env.EXTERNAL_DNS_DOMAIN_FILTERS }} ]
{{ end }}

## Modify how DNS records are sychronized between sources and providers (options: sync, upsert-only )
policy: sync

# Verbosity of the logs (options: panic, debug, info, warn, error, fatal)
logLevel: info

## When enabled, prints DNS record changes rather than actually performing them
##
{{ if getenv "EXTERNAL_DNS_DRY_RUN" }}
dryRun: '{{ .Env.EXTERNAL_DNS_DRY_RUN }}'
{{ end }}
## Adjust the interval for DNS updates
##
{{ if getenv "EXTERNAL_DNS_INTERVAL" }}
interval: '{{ .Env.EXTERNAL_DNS_INTERVAL }}'
{{ end }}

rbac:
  ## If true, create & use RBAC resources
  ##
  create: true
  serviceAccountName: external-dns