# This directory is used to deploy official [external-dns helm](https://github.com/kubernetes/charts/tree/master/stable/external-dns)

- values.yaml: overrides the official external-dns Helm chart.