# Changelog
All notable changes to this project will be documented in this file.

This file's format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The date string is generated with this command: `date --iso-8601=seconds`
(or, if that does not work, try `date +"%Y-%m-%dT%H:%M:%S%:z"`).

## [1.14.1] - 2025-02-13T18:08:08:z

### Fixed

* [render2.sh] The documentation for render2.sh says that it supports the
render-jinja2 filter. However, that support was never added to render2.sh.
This release fixes that: render2.sh now recognizes the `#!render-jinja2`
she-bang.

* [variable-files-warning.sh] Added "SKIP_OTICA_CHECK" option for situations
that warrant it (e.g. in the toplevel repo). The check could be modified to
handle the toplevel repo; however, there could potentially be other reasons
that we may want to skip the check in the future.

* [scripts/lib/helm_wrapper.py] Fixed a bug where a caller to run_helm() was
expecting two return values but run_helm() actually returns three.

### Changed

* [scripts/tf-sync.sh] Change the Terraform sync script tf-sync.sh so that it uses
render2.sh rather than render.sh.

* [cicd/gitlab/otica-kaniko-gar.yml] Update CI/CD file to support some extra
environment variables.

* [bin/otica] Add a "check" action that checks to see if any enviromnent
variable files need to be updated from the toplevel directory.

* [makefile_parts/framework.mk] Do the above "otica check" on every make.

* [bin/check-tools]

  - Fail on versions of Python 3.8 or earlier
    and to display a deprecation warning for Python 3.9 and Python 3.10. Thus,
    Python 3.11 is the minimum non-deprecated version supported.

  - If yq is installed but not at least version 4.x then fail; if yq is not installed give a warning
    instead.

## [1.13.4] - 2024-09-27T10:17:25:z

### Fixed

* [helm.mk] Better OCI-based repository integration. Add helm-login target.

* [helm-develop.mk] Add dhelm-login target. Define `KUBECONFIG` in such a
way as to avoid warning messages about unsafe file permissions.

* [python-build-push.sh] Remove unnecessary section where we install
Python packages from requirements.txt provided in Python package source.

### Added

* [framework.mk] Added a new target: "otica-update". This is a convenience
function that frees you from having to change into the subproject root
directory before running "otica update".

* [scripts/pyproject-toml-version.py] Add Python script to extract the
version number from a pyproject.toml file. For this script to work the
"toml" Python package must be installed.

### Changed

* [python/pvenv.mk] Change "requirements.txt" to "pv-requirements.txt".

## [1.13.3] - 2024-08-05T15:31:38-07:00

### Added

* [python/pvenv.mk] Added a new Makefile module `python/pvenv.mk` to assist
in managing a Python virtual environment in an Otica subproject.

* [self-signed.mk] Added a new Makefile module `self-signed.mk` that
manages self-signed certificates. See
[documentation](docs/make-modules/self-signed/README.md) for more
information.

### Changed

* [docs] Updated Docker build CI/CD Gitlab documents to reflect that
Google's Container Registry is going away in 2025.

* [helm.mk] Added support for OCI-based Helm chart repositories.

## [1.13.2] - 2024-06-18T16:15:05-07:00

### Fixed

* [framework.mk] Remove the requirement that `TL_BASEDIR` be set for make
commands to work. There are situations, for example in CI/CD, where there
is no toplevel-directory. Furthermore, the only time when `TL_BASEDIR`
really needs to be defined is when running an `otica` command such as
`otica create` or `otica update`.

### Changed

* [docker-develop.mk] Add `docker-prepare-build` as a dependency to the
the `docker-show-env` make target. If `DOCKER_IMAGE_TAGS_HOOK` is defined
then the `docker-show-env` make target calls the script pointed to by
`DOCKER_IMAGE_TAGS_HOOK`. This script might need to access files rendered
by `docker-prepare-build`. Hence, we need to run `docker-prepare-build`
before `docker-show-env`.

## [1.13.1] - 2024-05-22T13:20:40-07:00

### Added

* [package.mk] A new makefile-part that provides package building,
signing, and pushing capabilities to Otica.  package.mk references
pkg-wrapper.py which executes various functions exposed by
package-wrapper.py, which is an Otica library that contains the logic
for setting up the Containers that are used to perform the package
building, signing, and pushing.  Currently supported package types
are: Debian, RedHat (Enterprise Linux), and Python.  Additional
information on how package-wrapper.py works can be found
[here](docs/make-modules/package/README.md)

* Added script `scripts/gcp/download-python.sh` to download Python package
files from GCP's Artifact Registry using existing GCP credentials. Useful
when building a Docker image and the image requires non-public Python
packages stored in Google's Artifact Registry.

* Added support for a new environment variable `TL_BASEDIR_OVERRIDE` to
override calculated value of `TL_BASEDIR` in cases where the top-level
directory is not where it normally is (or does not exist at all, e.g., in
a CI/CD context). See section "Overriding the `TL_BASEDIR variable`" of
the ["Framework-level configuration" documentation
page](docs/configuration/framework)

* Added the `kube-cert-expiration` target to the `kube.mk` Makefile
module. This target displays the not-before and (more importantly) the
not-after dates for the current configured Kubernetes cluster.

* Added two scripts `scripts/base64-encode-file` and
`scripts/base64-decode-file`. The `base64-encode-file` script _replaces_ a
file with its Base64-encoded version with its name changed by appending
`.b64`. The `base64-decode-file` script reverses the process.

### Changed

* Update `bin/check-tools` to fail on versions of Python 3.8 or earlier
and to display a deprecation warning for Python 3.9.

* [docker-develop.mk] Add the flag `--compressed-caching=false` to Kaniko
build scripts generated by `make docker-kaniko-script`. This should help
reduce memory use when generating a Docker image with Kaniko and avoid
those unpleasant `ERROR: Job failed: command terminated with exit code
137` messages.


### Fixed

* Added some surrounding double quotes in part of
`scripts/auth/authenticate-gcp.sh`.

* Added missing dependency to one of the make targets for
the `framework-develop.mk` make-module.

##  [1.12.1] - 2023-11-07T10:30:16-08:00

### Added

* Added new script render-file-dir.sh that conditionally sends all files
in a directory through render2.sh. The behavior of this new script depends
on the new environment variable `RENDER_MODE`. See ["Rendering
configuration (`RENDER_MODE`)"](docs/configuration/render) for more
information.

* Added new script extract-dpkg-version.sh to extract the version number of
a Debian package changelog file. It does this by cloning the Debian
package repository and then running a Docker container to extract the
version number. This is particularly useful when used in combination with
`DOCKER_IMAGE_TAGS_HOOK` in the docker-develop.mk Make-module.

### Fixed

* One more type hint fixed that was causing problems with Python versions
prior to 3.9.

* Fixed issue with environment-specific inclusion code causing subproject
creates to fail.

* Prior to this release the `docker-develop.mk` make-module ran _all_ files in the
directory common/docker through render2.sh. This would take a very long
time to run in cases where there where many files in common/docker or when
there certain binary files in common/docker. The `docker-develop.mk`
make-module now uses the new render-file-dir.sh script when preparing to
do a Docker build so that in those cases where there are an unusually
large number of files, or files that render2.sh has trouble parsing, you
can set `RENDER_MODE` to "NONE" or "TMPL" to work-around this issue.
See ["Rendering configuration (`RENDER_MODE`)"](docs/configuration/render)
for more information.


##  [1.11.2] - 2023-11-06T09:55:27-08:00

### Fixed

* Introduced a problem with release 1.11.1 by adding type hints to some
Python code that required Python 3.9 or later. This broke Otica when used
with Python 3.7 and 3.8. However, we are currently supporting Python 3.7
and above. So, this release fixes the problem by changing the type hints
to be compatible with Python 3.7 and later.


##  [1.11.1] - 2023-10-25T08:29:00-07:00

### Fixed

* Fixed an issue with `scripts/auth/authenticate-gcp.sh` where the script
would fail when the GCP configuration did not already exist in the local
environment. The script now first checks to see if the configuration
already exists and, if not, creates it.

### Changed

* Added feature Jira IEDO-2175 to support environment-specific inclusion
of variable files; see section "Environment-specific toplevel variable
files" of `docs/configuration/variables` for details on this feature.

* As part of the IEDO-2175 work refactored the bin/otica script Python code
into separate modules. Also added type hints to much of the code.

* Dropping support for Python 3.6 (which reached support end-of-life in December 2021).
Running check-tools with a version of Python 3.6 or earlier will result in an error.
Deprecating support for Python 3.7 (see "Deprecated" section below).

### Deprecated

* Marking the use of Python 3.7 as deprecated. [Support for Python 3.7
ended](https://devguide.python.org/versions/) (including security fixes)
in June 2023. We will drop Otica support for Python 3.7 in Q1 or Q2 of 2024.
The `check-tools` script outputs a deprecation warning if it detects
Python 3.7.


##  [1.10.3] - 2023-09-07T13:13:32-07:00

### Fixed

* Fixed an issue with the `dhelm-render-templates` make target of the
`helm-develop` make-module.

### Changed

* Remove some unnecessary code in `bin/check-tools`.

### Added

- Added new environment variable `DOCKER_IMAGE_TAGS_HOOK` that allows the
`docker-devel` make-module to dynamically generate image tags. See the
`docker-devel` documentation for more information.

##  [1.10.2] - 2023-06-04T18:06:30-07:00

### Added

- Added `cicd/gitlab/otica-kaniko-gar.yml` for pushing to Google's
Artifact Registry. This is just a copy of
`cicd/gitlab/otica-kaniko-gcr.yml` but having its own filename makes its
purpose clear. Also added a corresponding documentation page.

### Fixed

- In the script `scripts/auth/kube-config-context.sh` re-order the code
to set the Kubernetes context _before_ attempting to connect to the cluster.

- `bin/check-tools`: the `file` tool check was showing the wrong error
message. Also, removed `check-tools`'s dependency on
`scripts/deprecated.sh` so as to make `check-tools` easier to run if the
rest of Otica is not installed.

### Changed

- Improve performance of the `kube-config` target in the `kube.mk` Make
module: in the script `kube-get-credentials-gke.sh` if the
`GKE_CLUSTER_AVAILABILITY_TYPE` environment variable is set use it to
determine if authenticating against a regional or zonal GKE cluster. If
not, use the slower "gcloud container clusters list ..." command.

- Added new target for the `kube.mk` Make module: the `ssh` target will
"login" to a container on a running Pod using the `pod-login` script.
Needs to have some environment variables set. For more information see the
[`pod-login` man page](/docs/manpages/pod-login/).

- Removed the dependency of `bin/check-tools` on `realpath`: the Apple Mac
has a version of `realpath` with command-line options that differ from
Debian's `realpath`. This was causing `bin/check-tools` to fail on some
people's Macs. To resolve this rewrote `bin/check-tools` to
get paths in a way that does not require `realpath`.

- In the `kube.mk` Make module when using credentials from Vault to
authenticate to an on-premise Kubernetes cluster if the credentials in
Vault were updated, running `make kube-config` would _not_ update the
local kubeconfig file because the credentials merge script `konfig` was
preferring the local kubeconfig credentials over the Vault credentials. To
address this changed merge order in `scripts/konfig/konfig` so that now
the Vault credentials take precedence over existing kubeconfig
credentials.


##  [1.10.1] - 2023-03-30T12:46:22-07:00

### Fixed

- Fixed a bug in `docker-login.sh`: when running "gcloud auth configure-docker" forgot
to provide the Docker registry path.

### Deprecated

- Marking the use of any version of Python prior to version 3.7 as
deprecated. [Support for Python 3.6
ended](https://devguide.python.org/versions/) (including security fixes)
in December 2021. In at least one Otica script we have to work around
differences between Python 3.6 and Python 3.7. So for these reasons we are
deprecating the use of Python 3.6 and earlier. We will remove Otica
support for these older Python versions at some point in the near future.

- The `check-tools` script will output a deprecation warning if it detects
a version of Python prior to Python 3.7.

### Changed

- Updated the `update-repos` Python script so that it works with Python 3.6 (but see above
Deprecated section).

- BREAKING CHANGE! The `helm.mk` make module has been updated to use the
new `render2.sh` filter script. In the past it was using the older
`render.sh` script which did an implicit environment variable
substitution. Now, however, if you want any sort of filtering applied to a
`values-local.yaml` file add a `#!`-string supported by `render2.sh`.

- Added new `helm-upsert` target to `helm.mk` make-module. This target does the
same thing as `helm-upgrade` except if the Helm chart is not installed it will
install it.

- Changed the `make docker-show-env` target: if the authentication type is
`gcloud` do not show the `DOCKER_REGISTRY_USERNAME` and
`DOCKER_REGISTRY_PASSWORD_PATH` values as those two variables are not
relevant when using `glcoud` authentication and showing them in the
`gcloud` authentication context makes it seem like they _are_ relevant.

##  [1.9.1] - 2023-01-04T10:37:15-08:00

### Added

- A new script `render2.sh` that is intended to replace `render.sh`. The
main difference between the two scripts is that `render.sh` runs files
without a `#!`-string at the top of the file through `envsubst` while
`render2.sh` does not. You can run make `render2.sh` run in legacy mode,
i.e., acts like `render.sh` by setting environment variable
`RENDER_LEGACY_MODE` equal to "YES".

- Man pages for `vault-read.sh` and `vault-write.sh` in the
`docs/manpages` directory.

- A new documentation page for dealing with secrets in Otica at
`docs/secrets`.

- New documentation for the `vault2kube.sh` script.

- A new make-module `datasealer.mk` and associated script
`datasealer.sh` help generate and store in Vault Shibboleth client
datasealer secrets. See
[Shibboleth Client VersionedDataSealer](https://shibboleth.atlassian.net/l/cp/mToJ9rh2)
for more information on Shibboleth datasealer files.

- A new make target for the `repos` make-module: `show-repos-status`. This
target lists all repos in `repos.yaml` and for each indicated whether the
repo is "clean", that is, whether the repo has any uncommited or unpushed
changes.

### Changed

- BREAKING CHANGE! The `docker-develop.mk` make module has been updated to
use the new `render2.sh` filter script. The module copies all files in
`common/docker` into the build directory and then runs each one through
`render2.sh`. If you want to do environment substitution on any file in
`common/docker` you now must put `#!envsubst` at the top of that
file. Before this change the `common/docker/build-args.yaml` file was
automatically having its environment variables expanded, but going
forward you will need to add the string `#!envsubst` explicitly at the top
of `common/docker/build-args.yaml` for environment variable substitution
to happen.

- Added some new checks to `check-tools`:

  * Verify that the version of bash installed supports "globstar". The
    globstar feature is used in the script `render2-dir.sh`. Bash 4 added
    globstar over a decade ago, so this is not likely to be a problem.

  * Verify that the openssl command line utility is installed. The
    `datasealer.mk` make-module uses `openssl` to generate random data for
    the datasealer secret file.

  * Verify that the `file` utility exists and supports the `--mime` option.
    We use `file --mime` to distinguish between ASCII and binary file when
    running files through render scripts.

- Re-factored the `helm.mk` and `helm-develop.mk` make modules so that
they no longer assume that the Helm chart repository is the public Helm
chart managed by IEDO. Also added a new make module `helm-gcs.mk` to
manage the [Helm Google Cloud Storage (GCS)
plugin](https://github.com/hayorov/helm-gcs) and initialize Helm chart
repositories in GCS buckets.


##  [1.8.3] - 2022-11-16T08:31:33-08:00

### Fixed

- When using the Kubernetes platform `kube-vault` the Kubernetes
credentials are extracted from Vault, saved in a local file `.kubeconfig`,
and then merged with the current Kubernetes configurations. To avoid
`.kubeconfig` getting committed by Git we add `.kubeconfig` to the default
`.gitignore` file.

- The script `auth/kube-config-context.sh` that is supposed to set the
namespace and current context was not setting the current Kubernetes context
correctly.


##  [1.8.2] - 2022-09-02T10:48:26-07:00

### Changed

- In the `docker-develop.mk` and `docker-compose.mk` make modules call the
`vault_login` dependency only when the environment variable
`DOCKER_REGISTRY_LOGIN_TYPE` is set to "vault".

- In the `docker-develop.mk` make module add support for a new environment
variable DOCKER_KANIKO_NO_PUSH that disables pushing kaniko docker builds
to a destination repository and instead runs kaniko with the "--no-push"
option.

### Fixed

- Fixed a bug in the script that displays makefile target information. The
old (bad) behavior was that if the makefile module does not have any
targets with only letters in their names it would ignore _all_ the
targets. For example, if the only target was "fail2ban" then that
make target would not show up in the make help output. Fixed this bug.

- In `kube-cert.mk` changed some `config-kube`'s to `kube-config`.


##  [1.8.1] - 2022-06-14T09:13:56-07:00

### Fixed

- `otica` script was not handling unknown platforms correctly.

- The `docker-login.sh` script was not logging into Docker Hub correctly.
Fixed this by omitting the server name entirely when logging into Docker
Hub (docker.io).

### Changed

- Removed the `kube-config` dependency on the `kube-show-env` make target in the
`kube.mk` make module.

### Added

- Added support for Kubernetes authentication and configuration using a
kubeconfig YAML string stored in Vault (kube-vault). As part of this
support we use the `konfig` Kubernetes configuration merging tool (see
[https://github.com/corneliusweig/konfig](https://github.com/corneliusweig/konfig));
the LICENSE file was updated to incorporate `konfig`'s licensing.

- Added manpage for `check-tools`.

- Added new script `scripts/kube/pods-running-and-ready` which returns a list
of "ready and running" Pods in the current Kubernetes namespace. For more information
on how the script works see its man page in `docs/manpages/`.

- Added `--minimal` option to `check-tools` to restrict verifications to
only those software components needed by the `otica` script.

- Added a check in `check-tools` that git supports the `-C` option.


##  [1.7.1] - 2022-05-23T15:17:44-07:00

### Changed

- Moved basic functions of kube-deploy.mk (apply, delete, render) into kube.mk.
  Deleted kube-images-get function. Deprecated kube-deploy.mk.

- Add a test to bin/check-tools to verify that the Python 3 PyYAML module supports
  the `yaml.full_load` method.

##  [1.7.0] - 2022-05-18T09:04:41-07:00

### Changed

- Updated required vars `HELM_CHART_NAME`, `HELM_REPO_NAME`, and `HELM_REPO_URL`
  for helm.mk

- Added functions that lists or delete failed pods across one namespace or
  all namespaces. This replaces the previous function `kube-delete-failed-pods`.
  The single namespace functions are added to kube.mk and the all namespaces
  functions are added at the cluster level kube-cluster.mk

- Several changes to the `otica` script:

  * Rewrite user interface for the `otica` script to use "subcommands" a
  la git, e.g., `otica update`, `otica version`, `otica create subproject
  generic`, etc.

  * When creating a new subproject you must now supply an additional "platform"
  argument. Otica comes with one platform: `generic`. You defined additional
  platforms in the toplevel directory.

  * Removed the `platform:` element from `otica.yaml` as it was not being
  used and it could be confused with the new platform argument required
  when creating a new subproject.

  * Added the option `--name` to the otica script to specify a directory
  name when creating a new environment (defaults to `new-env`).

  * Instead of creating an empty local.var in new environment directories
  create one containing comments with suggested settings.

  * Instead of creating an empty common/env-variables/common.var in new
  subprojects create one containing comments with suggested settings.

- Removed some old template files that are no longer being used.

### Added

- Added the beginning of a Python-based set of unit tests. Currently only
testing the bin/otica executable.

##  [1.6.3] - 2022-05-02T06:12:03-07:00

### Changed

- Simplified the argument parsing part of `bin/check-tools` to remove
dependency on the getopt utility which might not be available to Mac users
(although Mac users _can_ install getopt using Homebrew). Since
`bin/check-tools` is the first script new installers run to find out what
software is missing we want to make it as easy to run as possible.

- Updated `scripts/kube-delete-shutdown-pods.sh` to use value of 'Shutdown'
or 'Terminated'. Newer versions of Kubernetes now use 'Terminated' pods.

- Added `ifneq missing_vars` conditionals to makefiles that use it. These
need to be defined outside of framework.mk if targets in other files are
being called directly. This has been previously added to some makefiles
but not all of them that require it.

- Renamed `makefile_parts/archive/kube-cluster.mk` as
`makefile_parts/archive/kube-cluster-gcloud.mk` and kept it as archived.
The reason for this is that the gcloud method should be deprecated in
favor of the terraform method. The terraform method is now named
`makefile_parts/kube-cluster.mk`.

- Changed `bin/check-tools` so that it now runs all tests rather than
stopping at the first failed test. This is useful for those situations
where one does not need all the suggested software but wants to know all
of what is missing.

### Fixed

- If two different docker-compose projects are run on the same machine the
`docker-compose.mk` make module could not distinguish between them causing
many problems. The solution is to require that the environment variable
`COMPOSE_PROJECT_NAME` be set to a name unique to that docker-compose
project.

### Added

- New filter `render-jinja2` that renders a Jinja2 file sending the results to
standard output.


##  [1.6.2] - 2022-04-13T09:50:45-07:00

### Fixed

- framework.mk: made the default list of tools to
check defined in `framework.mk` the same as the default list in
`bin/check-tools`. (Jira issue #1691)

- Use bash specific "-n" and "-z" for checking variables in some shell
scripts. This fixes a bug making the GCP authentication script fail.

### Changed

- gcp/workload-identity.mk: refactored the GCP Workload Identity make file
removing some of the targets and make compatible with kube.mk; also
created documentation.

- bin/check-tools: if check-tools displays an error that a tool is
missing, explain why that tool is necessary to Otica.


##  [1.6.1] - 2022-03-30T09:03:03-07:00

### Changed

- scripts/auth/authenticate-gcp.sh: rewritten to make it run faster.

- Changed `#!/bin/bash` to `#!/usr/bin/env bash` in many Bash shell
scripts. (Merge #3 from otica/otica.)

### Fixed

- scripts/lib/docker_wrapper.py: Removed a `docker logout` that would
logout from Docker in the middle of pushing tags. Symptom: could push one
tag but not two or more.


##  [1.6.0] - 2022-03-21T07:57:39-07:00

### Changed

- bin/otica: add the `version` action to display the Otica version from
the `OTICA_VERSION` file. (Issue #3 from otica/otica issues.)

- docker-wrapper.py: Re-factored the docker-wrapper script into a Python 3 library
in `scripts/lib/docker_wrapper.py`.

- otica-abstract.py: added another dict containing unsupported
environment variables. If an environment variable appears in this dict
then the application will raise an exception. Useful for when you want an
environment variable _not_ to defined.

- bin/checktools: Check that `pwgen` is installed.

- helm-develop.mk make module: added new target `dhelm-push-force` to
allow packaging and uploading a Helm package with the same version number
as an already-uploaded package.

### Fixed

- bin/otica: add better messaging when "otica create" fails due to not
being able to determine the toplevel directory. (Issue #2 from otica/otica
issues.)

- helm_wrapper.py: in the `helm-values-merge` target forgot to read the
chart's `values.yaml` file before reading the `values-local.yaml` files in
the common and environment directories. Fixed that.

- helm_wrapper.py: do an environment variable check at the very start.
Also don't try to delete any render files if the render_files have not yet
been defined.


## [1.5.0] - 2022-02-22T08:42:34-08:00

### Added

- Rework the older `external-dns-helm3.mk` into `kube-external-dns.mk`.
Add documentation for `kube-external-dns.mk`.

- Added `auth-show-env` make target for `authenticate.mk` make module.
This shows the environment variables relevant to the `authenticate.mk`
make module. This can help debug authentication issues.

- Added documentation for the `kube-sec.mk` make module and the
`kube2vault` filter script.

### Changed

- Removed the "kube-namespace-delete" make target in the `kube.mk` module
as it is too easy to mistakenly run this with very bad results. If you
need to delete a Kubernetes namespace, do it manually.

- In the otica.yaml template file that is provided on a new subproject create,
replace "config-gcp.mk" with "authenticate.mk".

- In `kube-sec.mk` make the default secrets YAML file be `secrets.yaml`.
This can be overridden by setting `KUBE_SEC_DEF_FILE`.

### Fixed

- In the script `helm-diff.sh` simplify the use of `mktemp` so it works on
both Linux and Mac OS.

## [1.4.0] - 2022-01-28T06:06:01-08:00

### Added

- New script `scripts/kube-delete-shutdown-pods.sh` that deletes all
Pods in the "Shutdown" state in the current Kubernetes namespace.

- Added `kube-namespace-show` to `kube.mk` to show current configured namespace.

- Documentation: added a tutorial for an Otica Helm subproject.

- Documentation: added some documentation for the `vault.mk` make module.


### Changed

- Re-factored cloud platform and Kubernetes cluster authentication and
configuration to make them separate make modules: `authenticate.mk` and
`kube.mk`. Introduced new environment variable `CLOUD_PLATFORM` to
indicate which cloud platform is being used; takes values such as `gcp`,
`aws`. Moved authentication scripts into `scripts/auth/`. New make module
`authenticate.mk` and updated module `kube.mk` are to replace
`config-gcp.mk` while the new scripts in `scripts/auth` replace the
scripts `scripts/config-gcp.sh` and `scripts/config-kube.sh`.
Documentation covering these changes is at
`docs/make-modules/authenticate/` and `docs/make-modules/kube`.

### Fixed

- `docker-develop.mk`: fix a bug where files in the `build/` directory would
persist even if removed from the source `common/docker/` directory.

- `framework-develop.py`: copy a .gitignore file into the base directory
of the released repository containing `*.pyc`. This way when Python cache
files get created inside of a checked-out `bin/otica` (as they sometimes
do) they will not confuse Git into thinking there are some
non version-controlled files.

## [1.3.0] - 2022-01-13T07:39:52-08:00

### Added

- Documentation: added documentation for the `kube.mk` makefile module.

- Re-factored the `docker-compose.mk` module to be simpler and only wrap
`docker compose` commands. Renamed the old `docker-compose.mk` make module
to `docker-compose-legacy.mk`.

### Changed

- `bin/otica`: when creating a new Helm subproject using `otica
--subtype=helm create subproject` do a `helm create` inside `common/helm`
so that there are some starter files in `common/helm`.

- `docker-develop.mk`: Changed the deprecation warning for
`DOCKER_IMAGE_TAG` to an error message. The `DOCKER_IMAGE_TAGS` variable
should be used in place of `DOCKER_IMAGE_TAG`.

### Fixed

- `makefile_parts/helm-develop.mk`: the Makefile target `dhelm-gcs-init`
was dependent on the target `dhelm-repo-add`, but `dhelm-repo-add` depends
on `dhelm-gcs-init`, not vice versa.

- Add a new validation check: `framework.mk` must appear as the FIRST entry in the
`makefile_parts:otica` section.


## [1.2.0] - 2021-12-16T09:50:39-08:00

### Added

- Documentation: added a page describing how to use the GitLab CI/CD
file `cicd/gitlab/otica-kaniko-dockerhub.yml`.

- cicd/gitlab/otica-kaniko-gcr.yml: a GitLab-compatible CI/CD file that
can be used to generate Docker images and upload the images to Google's
Container Registry. For more details see the gitlab/cicd documentation
pages.

### Changed

- docker-develop.mk: changed make target `docker-build-script` to
`docker-kaniko-script` to better communicate what the make target does.

- docker-wrapper.py: add the `--cleanup` option to the end of the kaniko build
command to reduce space resource usage when building multiple images in one
docker run.

## [1.1.0] - 2021-12-15T11:57:08-08:00

### Changed

- docker-develop.mk: replaced in-line code for the "docker-login" target
with a shell script scripts/docker-login.sh. Also added a new
configuration variable DOCKER_REGISTRY_LOGIN_TYPE which can take one of
the values "vault", "gcloud", "file". Note that this gives an additional
way to authenticate to a remote Docker registry, namely, by putting the
password into a file. For more details on this change see the
docker-develop.mk documentation.

### Added

- sub-projects.mk: add a new make target `repos-show-orphaned` to show
the directories in `sub-projects/` that are missing from `repos.yaml`.

- make-kaniko-cmds: a new script in bin/ that generates a file containing
the Kaniko commands needed to build a Docker container for each docker project
environment. See docs for more details.

- cicd/gitlab/otica-kaniko-dockerhub.yml: a GitLab-compatible CI/CD file
that can be used to generate Docker images and upload the images to
Dockerhub. For more details see the docker-develop.mk documentation.

### Fixed

- Documentation: fix several typos.

## [1.0.0] - 2021-12-09T08:23:08-08:00

### Added

- This is the first public release.

### Changed

- framework-develop.py: change the tags used for new releases from
"otica-version-number" to "release/otica-version-number".

## [0.3.3] - 2021-12-08T17:56:00-08:00

### Changed

- LICENSE: updated the license file in anticipation of a public release.

## [0.3.2] - 2021-11-19T13:29:02-08:00

### Changed

- terraform.mk: Removed `TF_GLOBAL_CODE_BASEDIR` since a global `TF_CODE_BASEDIR` can
  be overridden after make. Example: `make tf-init TF_CODE_BASEDIR=test`. Also added
  `TF_STATE_FILE` definition.

- faas.mk: Removed `FAAS_GLOBAL_CODE_BASEDIR` since a global `FAAS_CODE_BASEDIR` can
  be overridden after make. Example: `make faas-init TF_CODE_BASEDIR=test`

### Added

- Added a documentation page for the terraform.mk make module.

## [0.3.1] - 2021-11-19T07:29:58-08:00

### Changed

- (Breaking change) Variable files in the toplevel directory and in
subprojects are now expected to be put in their respective
`common/env_variables` directories:

        OLD                             NEW
        <toplevel dir>/common/*.var     <toplevel dir>/common/env_variables/*.var
        <subproject dir>/common/*.var   <subproject dir>/common/env_variables/*.var

- (Breaking change) Change how otica.yaml is structured. New section `toplevel` in
`environment` and `makefile_parts`. The changes:

        OLD                          NEW
        environment:otica            environment:toplevel
        makefile_parts:framework     makefile_parts:otica
        NONE                         makefile_parts:toplevel
                                     (points to toplevel makefile_parts dir)

- To support referring to directories in the top-level `common/` directory
there are new system-wide environment variables:

        TL_BASEDIR:        the toplevel directory
        TL_COMMON:         ${TL_BASEDIR}/common
        TL_ENV_VARIABLES:  ${TL_BASEDIR}/env_variables
        TL_MAKEFILE_PARTS: ${TL_BASEDIR}/makefile_parts
        TL_SCRIPTS:        ${TL_BASEDIR}/scripts
        TL_TEMPLATES:      ${TL_BASEDIR}/templates

- (Breaking change) bin/otica: The three possible targets for `otica
create` have changed:

        OLD
        main  --> toplevel
        sub   --> subproject
        env   --> environment

  For example, what used to be `otica create main` is now `otica create toplevel`.

## [0.2.1] - 2021-11-17T05:25:22-08:00

### Changed
- (Breaking change) An Otica top-level project directory must now contain
a file with the name .otica-top-level to indicate that the directory is,
in fact, a top-level project. If this file is missing the otica script
will fail. The file contents can be anything. This file will be
automatically created when doing an "otica create main".

- terraform.mk: Allow `TF_CODE_BASEDIR` to be overridden per makefile
target, and implement for global changes `TF_GLOBAL_CODE_BASEDIR`.

- faas.mk: Allow `FAAS_CODE_BASEDIR` to be overridden per makefile target,
and implement for global changes `FAAS_GLOBAL_CODE_BASEDIR`

### Added
- bin/otica: Add the "validate" action to parse the otica.yaml file and
verify that the various referenced variable files and makefile modules exist.

### Removed

- Removed framework-env.sh as it is no longer used.

## [0.1.11] - 2021-11-11T16:53:40-08:00

### Added
- bin/otica: Add .gitignore file during top-level project create.

### Removed
- Removed framework-env.sh as it is no longer used.

## [0.1.10] - 2021-11-10T15:29:52-08:00

### Changed
- framework-develop.sh: Ignore source and release repo version numbers when doing
deploy-status and deploy-diff.
- docker-wrapper.py: Changed things so that it is OK for some environment
variables to be undefined when running the docker-show-env action.

## [0.1.9] - 2021-11-08T10:01:58-08:00

### Changed
- Add "git" to the default list of tools to check in framework.mk.
- Re-fix issue where functions.sh raises an error if `DEBUG` is not defined.

## [0.1.8] - 2021-11-05T20:05:45-07:00

### Changed
- Remove extraneous target and rename another in framework-develop.mk.

### Added
- When creating a new subproject via "otica create sub" add a starter
.gitignore file.

## [0.1.7] - 2021-11-05T09:03:10-07:00

### Changed
- In docker-develop and config-gcp make modules append to `FRAMEWORK_TOOLS` the programs
needed.
- In framework.mk call bin/check-tools with the --verbose flag so that the user
can see which programs and libraries are being checked.

### Fixed
- scripts/functions.sh had an issue with the progress function when DEBUG was
not set.

## [0.1.6]

### Fixed
- Fix Python error in docker-wrapper.py with iterating over a dict.

## [0.1.5]

### Added
- Added new variable FRAMEWORK_DIR_SYNC that can be used to control how
often the code is synced; see [docs](docs/configuration/framework/) for details.
- Added OTICA_VERSION file. Contains the release version of the Otica code (follows
[semantic versioning](https://semver.org/spec/v2.0.0.html)).
- New make module: framework-develop.mk to aid in the release of new versions of Otica.
For details see its [documentation](docs/make-modules/framework-develop/).

### Changed
- The variable "framework.var" is now required and MUST be included in the
environment:otica section of otica.yaml.
- Re-factored the Otica code updating process.
