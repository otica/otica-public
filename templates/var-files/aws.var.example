# Sample var file for AWS

# REQUIRED
# This is a name that is used to describe the services that are grouped together.
# It will be used in the naming of resources that are created.
# Example(s): `my-company-gitlab` or `my-project-name` or `my-team-name-servives`
#
# IMPORTANT: Please only use lower case letters and hyphens for word separators
# as many of AWS services dont support caps, underscores or spaces in their naming.
export AWS_PROJECT_NAME=my-project-name

# REQUIRED
# The account-id of your AWS Account.
export AWS_ACCOUNT_ID=999999999999

# REQUIRED (usually)
# See https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html
# for more information. AWS_DEFAULT_REGION is set for aws-cli.
export AWS_REGION=us-west-2
export AWS_DEFAULT_REGION=${AWS_REGION}

# SOMETIMES NEEDED
# Some services may be installed in a single Availability Zones.
# Specify this if needed.
export AWS_AZ=${AWS_REGION}a

# REQUIRED (if NOT using vault)
# Set to false if you are not using Vault authentication
# NOTE: If not using Vault auth, you will need to manually set your own variables
# AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY as needed -- DO NOT STORE THESE SECRETS IN THE VAR FILE.
export VAULT_ENABLED=true

# REQUIRED (if using Vault auth)
# This is the cred secret setup by your administrator.
export AWS_CRED_SECRET=aws-example-account-dev

# REQUIRED (if using Vault auth)
# This is the IAM access role setup by your administrator.
export AWS_DEPLOYMENT_IAM_ROLE=AdministratorAccess

# REQUIRED
# This is the project name under secrets that you have access to setup by your administrator.
export VAULT_PROJECT_NAME=my-team

# REQUIRED
# HTTPS endpoint for your Vault server.
export VAULT_ADDR=https://vault.example.com

# REQUIRED
# The secrets path in Vault where your AWS_PROJECT_NAME secrets are stored.
export SEC_PATH=secret/projects/${VAULT_PROJECT_NAME}/${AWS_PROJECT_NAME}

# REQUIRED (if you use Terraform)
# The AWS Bucket where you store your Terraform state.
export TF_INFRASTRUCTURE_BUCKET=${AWS_ACCOUNT_ID}-${AWS_PROJECT_NAME}-infrastructure
